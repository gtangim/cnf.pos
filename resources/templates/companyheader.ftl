${NORMAL}
${CENTER}${DOUBLE_HEIGHT}${BOLD}Community Natural Foods
${REGULAR}${FONTB}${CENTER}${BOLD}202 - 61st Avenue SW, Calgary, AB
<#if receipt?has_content>
${CENTER}${BOLD}Community is hiring!
${CENTER}Please see Customer Service for Details

<#if isTemp?has_content>
${FONTA}
${CENTER}${BOLD}
${CENTER}${BOLD}
${CENTER}${BOLD}----------------
${CENTER}${BOLD}*TEMPORARY SLIP*
${CENTER}${BOLD}----------------
${CENTER}${BOLD}
${CENTER}${BOLD}
${CENTER}${BOLD}WARNING: NOT A VALID RECEIPT
${CENTER}${BOLD}Transaction was not completed
${CENTER}${BOLD}
${CENTER}${BOLD}
</#if>
<#if trainingMode>
${FONTA}
${CENTER}${BOLD}------
${CENTER}${BOLD}*DEMO*
${CENTER}${BOLD}------
</#if>
<#if isOld?has_content>
${FONTA}
${CENTER}${BOLD}********
${CENTER}${BOLD}* COPY *
${CENTER}${BOLD}********

</#if>
<#if parentOrderId?has_content>
${NORMAL}
${CENTER}${DOUBLE_WIDTH}${BOLD}*REFUND*
${NORMAL}${FONTB}${CENTER}Original Tx# ${parentOrderId}
<#else>
${NORMAL}
${CENTER}${DOUBLE_WIDTH}${BOLD}*PURCHASE*
</#if>
${NORMAL}${FONTB}${CENTER}Tx# ${sequenceNumber}<#if orderDate?has_content>, Date:${orderDate}</#if>
${FONTB}${CENTER}Clerk: ${salesRepFirstName}<#if customerFirstName?has_content>, Customer: ${customerFirstName}</#if>
<#if billingAccount?has_content>
${CENTER}Billing Account# ${billingAccount}
</#if>
</#if>
${NORMAL}
