${NORMAL}${FONTB}
<#if !isTemp?has_content>
${CENTER}${BOLD}GST # 101072080RT0001
${CENTER}Returns and Exchange Only
${CENTER}Accepted Within 14 Days With Receipt
${CENTER}(Some Exceptions Apply)
${CENTER}Questions or comments?
${CENTER}Email us: newsletters@mycnf.com
${CENTER}Visit Our Website:
${CENTER}www.communitynaturalfoods.com
${CENTER}View great recipes and more!
${CENTER}(403) 229-2383 10th Ave Loction
${CENTER}(403) 541-0606 Chinook Location
${CENTER}(403) 542-6705 Kingsland Farmers' Market
###BARCODE:${orderIdBarcode}###
${CENTER}${BOLD}${sequenceNumber}

${CENTER}${BOLD}THANK-YOU
</#if>


${PAPER_CUT}