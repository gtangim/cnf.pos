<#assign ESC="\x1b">
<#assign GS="\x1d">
<#assign LF="\x0a">
<#assign FR_E="[82]">
<#assign FR_EE="[90]">
<#assign FR_C="[87]">


<#assign NORMAL=ESC+"\x61\x00" + ESC+"!\x00">

<#assign UNDERLINE=ESC+"_1">
<#assign DOUBLEUNDERLINE=ESC+"_2">
<#assign FONTA=ESC+"M0">
<#assign FONTB=ESC+"M1">

<#assign BOLD=ESC+"E\x01">



<#assign LANG=ESC+"R\x01">

<#assign LEFT=ESC+"a0">
<#assign CENTER=ESC+"a1">
<#assign RIGHT=ESC+"a2">



<#assign REGULAR=ESC+"!\x00">
<#assign DOUBLE_WIDTH=ESC+"!\x20">
<#assign DOUBLE_HEIGHT=ESC+"!\x10">
<#assign DOUBLE=ESC+"!\x30">
<#assign ITALIC=ESC+"|iC">




<#assign PAPER_CUT=LF+LF+LF+GS+"V1">

<#assign TIMESTAMP="EEE, MMM d yyyy HH:mm:ss z">

<#setting locale="en_US">

<#function formatDateTime dt>
	<#return dt?datetime(TIMESTAMP)>
</#function>
