<#include "common.ftl">
<#include "companyheader.ftl">

------------------------------------------
${CENTER}${BOLD}RECEIPT OF PAYMENT TO YOUR
${CENTER}${BOLD}BILLING ACCOUNT
------------------------------------------

<#list payments as payment>
${BOLD}***** ${payment.status} *****
${BOLD}${payment.text?left_pad(30)}  ${payment.amount?string.currency?left_pad(10)}
<#list payment.descriptions as desc>
${(desc.title+desc.text!"??")?left_pad(25)}
</#list>

</#list>



<#include "companyfooter.ftl">


<#--
        //orderId
        //transactionId
        //drawerNo
        //userId
        //orderDate
        //date
        //salesRep
        //customer
        //saleDiscountValue
		//subtotal
        //grandTotal
        //change


        // Items...
        //  productId
        //  isTaxable
        //  quantity
        //  itemTotal
        //  description
        //  uomShortName
        //  basePrice
        //  scalable
        //    featureText
        //    featureValue
        //    discountText
        //    discountValue
        //    promoText
        //    promoValue
        //    overrideText
        //    overrideValue

        // Promos...
        //   promoText
        //   promoValue

        // Taxes...
        //   taxText
        //   taxValue


        // payments
        //   paymentText
        //   paymentValue
        //   payInfos...
        //     payInfoTitle
        //     payInfoText

-->
