<#include "common.ftl">
<#if printMerchant==true>
<#include "companyheader.ftl">
${BOLD}${CENTER}***** ${paymentType} *****${NORMAL}
<#if accountType?has_content>
${NORMAL}${REGULAR}
${NORMAL}${REGULAR}ACCT: ${accountType}
${NORMAL}${REGULAR}
</#if>
<#if cashBack?has_content>
${NORMAL}${REGULAR}${"Amount: "?left_pad(30)}${subAmount?string.currency?left_pad(12)}${NORMAL}
${NORMAL}${REGULAR}${"Cash Back: "?left_pad(30)}${cashBack?string.currency?left_pad(12)}${NORMAL}
</#if>
${NORMAL}${REGULAR}${"Total Amount: "?left_pad(30)}${amount?string.currency?left_pad(12)}${NORMAL}
${NORMAL}${REGULAR}${FONTB}
${"Card Number:"?left_pad(20)}  ${cardNumber?left_pad(22)}
${"Date/Time:"?left_pad(20)}  ${paymentDate?left_pad(22)}
<#if csi?has_content && csi=='Q'>
${"Reference#:"?left_pad(20)}  ${transactionNumber?left_pad(20)} C
<#elseif  csi?has_content>
${"Reference#:"?left_pad(20)}  ${transactionNumber?left_pad(20)} ${csi}
<#else>
${"Reference#:"?left_pad(20)}  ${transactionNumber?left_pad(22)}
</#if>
${"Auth#:"?left_pad(20)}  ${authCode?left_pad(22)}
<#if emvApplicationId?has_content>
${emvApplicationName}
${emvApplicationId}
<#if emvTvr?has_content && emvTvr!='0000000000'>${emvTvr}</#if><#if emvTsi?has_content>${emvTsi}</#if>
</#if>
${NORMAL}${REGULAR}
<#if condCode?has_content && (condCode=='608' || condCode=='609')>
${NORMAL}${REGULAR}DECLINED BY CARD - 990
<#elseif condCode?has_content && condCode=='607'>
${NORMAL}${REGULAR}CARD REMOVED - 991
</#if>
<#if cvm?has_content && cvm?contains("Pin")>
${NORMAL}${REGULAR}VERIFIED BY PIN
</#if>
<#if csi?has_content && csi=='F'>
${NORMAL}${REGULAR}${FONTB}CHIP CARD SWIPED
<#elseif csi?has_content && csi=='G'>
${NORMAL}${REGULAR}${FONTB}CHIP CARD KEYED
<#elseif approved?has_content && csi?has_content && csi=='Q'>
${NORMAL}${REGULAR}${FONTB}CHIP CARD MALFUNCTION
</#if>
${NORMAL}${REGULAR}
<#if approved?has_content>
${BOLD}${CENTER}** ${respCode} Approved - Thank You ${respCode24} **
<#elseif cancelled?has_content>
${BOLD}${CENTER}** Transaction Cancelled **
<#elseif timeout?has_content>
${BOLD}${CENTER}** Transaction Not Completed **
<#elseif condCode?has_content && (condCode=='606' || condCode=='607' || condCode=='608' || condCode=='609' || condCode=='616' || condCode=='640')>
${BOLD}${CENTER}** Transaction Not Completed **
<#elseif !respCode?has_content >
${BOLD}${CENTER}** Transaction Not Completed **
<#elseif cardType=='DEBIT_CARD' && respCode?has_content && (respCode=='05' || respCode=='51' || respCode=='54' || respCode=='55' || respCode=='57' || respCode=='58' || respCode=='61' || respCode=='62' || respCode=='65' || respCode=='75' || respCode=='82' || respCode=='92' )>
${BOLD}${CENTER}** ${respCode} Transaction Not Approved ${respCode24} **
<#elseif cardType=='DEBIT_CARD'>
${BOLD}${CENTER}** ${respCode} Transaction Not Completed ${respCode24} **
<#elseif condCode?has_content && condCode=='99'>
${BOLD}${CENTER}** ${respCode} Transaction Not Approved ${respCode24} **
<#elseif condCode?has_content && condCode=='307'>
${BOLD}${CENTER}** ${respCode} Transaction Not Completed ${respCode24} **
<#else>
${BOLD}${CENTER}** ${respCode} Transaction Not Completed ${respCode24} **
</#if>
<#if signCustomer==true>


SIGNATURE (Customer)





________________________________________
<#if paymentType=='PURCHASE' || paymentType=='REFUND CORRECTION'>
CARDHOLDER WILL PAY CARD ISSUER ABOVE
AMOUNT PURSUANT TO CARDHOLDER AGREEMENT
</#if>
<#elseif cvm?has_content && cvm=='None' && accountType?has_content && accountType!='FLASH DEFAULT'>

NO SIGNATURE TRANSACTION

</#if>

${BOLD}${CENTER}*** MERCHANT COPY ***

${PAPER_CUT}

</#if>
<#if printCustomer==true>
<#include "companyheader.ftl">
<#if language=="E">
${BOLD}${CENTER}***** ${paymentType} *****${NORMAL}
<#if accountType?has_content>
${NORMAL}${REGULAR}
${NORMAL}${REGULAR}ACCT: ${accountType}
${NORMAL}${REGULAR}
</#if>
<#if cashBack?has_content>
${NORMAL}${REGULAR}${"Amount: "?left_pad(30)}${subAmount?string.currency?left_pad(12)}${NORMAL}
${NORMAL}${REGULAR}${"Cash Back: "?left_pad(30)}${cashBack?string.currency?left_pad(12)}${NORMAL}
</#if>
${NORMAL}${REGULAR}${"Total Amount: "?left_pad(30)}${amount?string.currency?left_pad(12)}${NORMAL}
${NORMAL}${REGULAR}${FONTB}
${"Card Number:"?left_pad(20)}  ${cardNumber?left_pad(22)}
${"Date/Time:"?left_pad(20)}  ${paymentDate?left_pad(22)}
<#if csi?has_content && csi=='Q'>
${"Reference#:"?left_pad(20)}  ${transactionNumber?left_pad(20)} C
<#elseif  csi?has_content>
${"Reference#:"?left_pad(20)}  ${transactionNumber?left_pad(20)} ${csi}
<#else>
${"Reference#:"?left_pad(20)}  ${transactionNumber?left_pad(22)}
</#if>
${"Auth#:"?left_pad(20)}  ${authCode?left_pad(22)}
<#if emvApplicationId?has_content>
${emvApplicationName}
${emvApplicationId}
<#if emvTvr?has_content && emvTvr!='0000000000'>${emvTvr}</#if><#if emvTsi?has_content>${emvTsi}</#if>
</#if>
${NORMAL}${REGULAR}
<#if condCode?has_content && (condCode=='608' || condCode=='609')>
${NORMAL}${REGULAR}DECLINED BY CARD - 990
<#elseif condCode?has_content && condCode=='607'>
${NORMAL}${REGULAR}CARD REMOVED - 991
</#if>
<#if csi?has_content && csi=='F'>
${NORMAL}${REGULAR}${FONTB}CHIP CARD SWIPED
<#elseif csi?has_content && csi=='G'>
${NORMAL}${REGULAR}${FONTB}CHIP CARD KEYED
</#if>
${NORMAL}${REGULAR}
<#if approved?has_content>
${BOLD}${CENTER}** ${respCode} Approved - Thank You ${respCode24} **
<#elseif cancelled?has_content>
${BOLD}${CENTER}** Transaction Cancelled **
<#elseif timeout?has_content>
${BOLD}${CENTER}** Transaction Not Completed **
<#elseif condCode?has_content && (condCode=='606' || condCode=='607' || condCode=='608' || condCode=='609' || condCode=='616' || condCode=='640')>
${BOLD}${CENTER}** Transaction Not Completed **
<#elseif !respCode?has_content >
${BOLD}${CENTER}** Transaction Not Completed **
<#elseif cardType=='DEBIT_CARD' && respCode?has_content && (respCode=='05' || respCode=='51' || respCode=='54' || respCode=='55' || respCode=='57' || respCode=='58' || respCode=='61' || respCode=='62' || respCode=='65' || respCode=='75' || respCode=='82' || respCode=='92' )>
${BOLD}${CENTER}** ${respCode} Transaction Not Approved ${respCode24} **
<#elseif cardType=='DEBIT_CARD'>
${BOLD}${CENTER}** ${respCode} Transaction Not Completed ${respCode24} **
<#elseif condCode?has_content && condCode=='99'>
${BOLD}${CENTER}** ${respCode} Transaction Not Approved ${respCode24} **
<#elseif condCode?has_content && condCode=='307'>
${BOLD}${CENTER}** ${respCode} Transaction Not Completed ${respCode24} **
<#else>
${BOLD}${CENTER}** ${respCode} Transaction Not Completed ${respCode24} **
</#if>
<#if signMerchant==true>


SIGNATURE (Merchant)




________________________________________

<#elseif cvm?has_content && cvm=='None' && accountType?has_content && accountType!='FLASH DEFAULT'>

NO SIGNATURE TRANSACTION

</#if>


${NORMAL}${CENTER}--Important-- 
${NORMAL}${CENTER}Retain this copy for your records

${BOLD}${CENTER}*** CUSTOMER COPY ***
<#else>
${BOLD}${CENTER}***** ${paymentTypeFr} *****${NORMAL}
<#if accountType?has_content>
${NORMAL}${REGULAR}
${NORMAL}${REGULAR}COMPTE: ${accountTypeFr}
${NORMAL}${REGULAR}
</#if>
<#if cashBack?has_content>
${NORMAL}${REGULAR}${"Montant: "?left_pad(30)}${subAmount?string.currency?left_pad(12)}${NORMAL}
${NORMAL}${REGULAR}${"REMISE D'ARGENT: "?left_pad(30)}${cashBack?string.currency?left_pad(12)}${NORMAL}
</#if>
${NORMAL}${REGULAR}${"Montant total: "?left_pad(30)}${amount?string.currency?left_pad(12)}${NORMAL}
${NORMAL}${REGULAR}${FONTB}
${"Numero Carte:"?left_pad(20)}  ${cardNumber?left_pad(22)}
${"Date/Heure:"?left_pad(20)}  ${paymentDate?left_pad(22)}
<#if csi?has_content && csi=='Q'>
${"Numero Reference:"?left_pad(20)}  ${transactionNumber?left_pad(20)} C
<#elseif  csi?has_content>
${"Numero Reference:"?left_pad(20)}  ${transactionNumber?left_pad(20)} ${csi}
<#else>
${"Numero Reference:"?left_pad(20)}  ${transactionNumber?left_pad(22)}
</#if>
${"Autorisation:"?left_pad(20)}  ${authCode?left_pad(22)}
<#if emvApplicationId?has_content>
${emvApplicationName}
${emvApplicationId}
<#if emvTvr?has_content && emvTvr!='0000000000'>${emvTvr}</#if><#if emvTsi?has_content>${emvTsi}</#if>
</#if>
${NORMAL}${REGULAR}
<#if condCode?has_content && (condCode=='608' || condCode=='609')>
${NORMAL}${REGULAR}REFUS${FR_EE}E PAR LA CARTE - 990
<#elseif condCode?has_content && condCode=='607'>
${NORMAL}${REGULAR}CARTE RETIR${FR_EE}E - 991
</#if>
<#if csi?has_content && csi=='F'>
${NORMAL}${REGULAR}${FONTB}CARTE A PUCE GLISS${FR_EE}E
<#elseif csi?has_content && csi=='G'>
${NORMAL}${REGULAR}${FONTB}CARTE A PUCE TAP${FR_EE}E
</#if>
${NORMAL}${REGULAR}
<#if approved?has_content>
${BOLD}${CENTER}** ${respCode} Approuv${FR_E}e - Merci ${respCode24} **
<#elseif cancelled?has_content>
${BOLD}${CENTER}** Operation Annul${FR_E}e **
<#elseif timeout?has_content>
${BOLD}${CENTER}** Operation Non Complet${FR_E}e **
<#elseif condCode?has_content && (condCode=='606' || condCode=='607' || condCode=='608' || condCode=='609' || condCode=='616' || condCode=='640')>
${BOLD}${CENTER}** Operation Non Complet${FR_E}e **
<#elseif !respCode?has_content >
${BOLD}${CENTER}** Operation Non Complet${FR_E}e **
<#elseif cardType=='DEBIT_CARD' && respCode?has_content && (respCode=='05' || respCode=='51' || respCode=='54' || respCode=='55' || respCode=='57' || respCode=='58' || respCode=='61' || respCode=='62' || respCode=='65' || respCode=='75' || respCode=='82' || respCode=='92' )>
${BOLD}${CENTER}** ${respCode} Operation Refus${FR_E}e ${respCode24} **
<#elseif cardType=='DEBIT_CARD'>
${BOLD}${CENTER}** ${respCode} Operation Non Complet${FR_E}e ${respCode24} **
<#elseif condCode?has_content && condCode=='99'>
${BOLD}${CENTER}** ${respCode} Operation Refus${FR_E}e ${respCode24} **
<#elseif condCode?has_content && condCode=='307'>
${BOLD}${CENTER}** ${respCode} Operation Non Complet${FR_E}e ${respCode24} **
<#else>
${BOLD}${CENTER}** ${respCode} Operation Non Complet${FR_E}e ${respCode24} **
</#if>
<#if signMerchant==true>


SIGNATURE (Commer${FR_C}ant)




________________________________________

<#elseif cvm?has_content && cvm=='None' && accountType?has_content && accountType!='FLASH DEFAULT'>

Aucune op${FR_E}ration de signature

</#if>


${NORMAL}${CENTER}--Important--
${NORMAL}${CENTER}Conserver cette copie pour vos dossiers

${BOLD}${CENTER}*** COPIE DU CLIENT ***
</#if>

${PAPER_CUT}
</#if>