<#include "common.ftl">
<#include "companyheader.ftl">



${CENTER}${DOUBLE_HEIGHT}${BOLD}*** ${title} ***${NORMAL}${REGULAR}



------------------------------------------
<#list summaryList as summary>
<#if summary.hasData>
${BOLD}${summary.title?left_pad(42)}${NORMAL}${REGULAR}
${"Sale Value"?left_pad(26)}: ${summary.saleValue?string.currency?left_pad(14)}
<#if isBalance>
${"Declared Value"?left_pad(26)}: ${summary.declaredValue?string.currency?left_pad(14)}
${""?left_pad(26)}  ${""?left_pad(10,"-")}
${"Balance"?left_pad(26)}: ${summary.diffValue?string.currency?left_pad(14)}
</#if>


</#if>
</#list>
<#if billingAccounts?has_content>
------------------------------------------
${BOLD}    ACCOUNT CHARGES SUMMARY${NORMAL}${REGULAR}
------------------------------------------
<#list billingAccounts as ba>
${BOLD}${ba.billingAccountId?left_pad(26)}  ${ba.amount?string.currency?left_pad(14)}
  - ${ba.description}

</#list>
------------------------------------------
</#if>

------------------------------------------
${BOLD}${FLOAT.title?left_pad(42)}${NORMAL}${REGULAR}
${"Starting Value"?left_pad(26)}: ${FLOAT.startValue?string.currency?left_pad(14)}
${"Sale Value"?left_pad(26)}: ${FLOAT.saleValue?string.currency?left_pad(14)}
${"Cash In Value"?left_pad(26)}: ${FLOAT.inValue?string.currency?left_pad(14)}
${"Cash Out Value"?left_pad(26)}: ${FLOAT.outValue?string.currency?left_pad(14)}
${"Cash ROA Value"?left_pad(26)}: ${FLOAT.roaValue?string.currency?left_pad(14)}
------------------------------------------
${"Total Float Value"?left_pad(26)}: ${FLOAT.calculatedValue?string.currency?left_pad(14)}
<#if isBalance>
${"Declared Value"?left_pad(26)}: ${FLOAT.declaredValue?string.currency?left_pad(14)}
${""?left_pad(26)}  ${""?left_pad(10,"-")}
${"Balance"?left_pad(26)}: ${FLOAT.diffValue?string.currency?left_pad(14)}
</#if>
------------------------------------------


------------------------------------------
${BOLD}${TOTAL.title?left_pad(42)}${NORMAL}${REGULAR}
${"Sale Value"?left_pad(26)}: ${TOTAL.saleValue?string.currency?left_pad(14)}
<#if isBalance>
${"Declared Value"?left_pad(26)}: ${TOTAL.declaredValue?string.currency?left_pad(14)}
${""?left_pad(26)}  ${""?left_pad(10,"-")}
${"Balance"?left_pad(26)}: ${TOTAL.diffValue?string.currency?left_pad(14)}
</#if>
------------------------------------------




${PAPER_CUT}

