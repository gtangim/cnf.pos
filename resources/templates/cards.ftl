<#include "common.ftl">
<#include "companyheader.ftl">



${CENTER}${DOUBLE_HEIGHT}${BOLD}*** MONERIS POSPAD SETTLEMENT REPORT ***${NORMAL}${REGULAR}



------------------------------------------
${BOLD}SETTLEMENT BREAKDOWN${NORMAL}${REGULAR}
<#list payList as pay>
${NORMAL}${REGULAR}
${pay.payType?left_pad(30)}: ${pay.value?string.currency?left_pad(10)}
${NORMAL}${REGULAR}
</#list>

------------------------------------------
${BOLD}SETTLEMENT SUMMARY${NORMAL}${REGULAR}
${NORMAL}${REGULAR}
${message}
${NORMAL}${REGULAR}
${"Local Error Count"?left_pad(30)}: ${logCountDifference?string.number?left_pad(10)}
${"Local Error Value"?left_pad(30)}: ${logValueDifference?string.currency?left_pad(10)}
${NORMAL}${REGULAR}
${"Server Error Count"?left_pad(30)}: ${serverCountDifference?string.number?left_pad(10)}
${"Server Error Value"?left_pad(30)}: ${serverValueDifference?string.currency?left_pad(10)}
------------------------------------------


${PAPER_CUT}

