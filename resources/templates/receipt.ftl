<#include "common.ftl">
<#include "companyheader.ftl">
${NORMAL}${REGULAR}

PRODUCT                      QTY     PRICE
==========================================
<#list items as item>
${NORMAL}${REGULAR}${item.description}<#list item.features as feature><#if !feature?starts_with("Discard")>,${feature}<#else>*</#if></#list>
${item.productDisplayId?right_pad(23)}${item.isTaxable}${item.quantity?string?left_pad(9)}${item.itemTotalBeforeOverride?string("#0.00")?left_pad(9)}${FONTB}
<#if item.isScalable>
 - ${item.quantity}(${item.uomShortName}) @ $${item.basePrice}/${item.uomShortName}
</#if>
<#if item.override?has_content>
${item.override.text?right_pad(45)} ${item.override.valueReduction?string("#0.00")?left_pad(10)}
</#if>
<#if item.inf_sale?has_content>
    ${item.inf_sale.text?right_pad(45)}
</#if>
<#list item.chargedFeatures as feature>
${feature.text?right_pad(45)} ${feature.value?string("#0.00")?left_pad(10)}
</#list>
<#if item.discount?has_content>
${item.discount.text?right_pad(45)} ${item.discount.value?string("#0.00")?left_pad(10)}
</#if>
<#list item.promos as promo>
<#if (promo.code?has_content)>
  - Promo Code: ${promo.code}
</#if>
${promo.text?right_pad(45)} ${promo.value?string("#0.00")?left_pad(10)}
</#list>
</#list>
<#if (promos?size>0)>
${NORMAL}${REGULAR}------------------------------------------
</#if>
<#list promos as promo>
<#if (promo.code?has_content)>
Promo Code: ${promo.code}
</#if>
  ${promo.text?right_pad(29)} ${promo.value?string("#0.00")?left_pad(10)}
</#list>
${NORMAL}${REGULAR}------------------------------------------
<#if discount?has_content>
${discount.text?left_pad(30)}: ${discount.value?string.currency?left_pad(10)}
</#if>
${"SUB TOTAL:"?left_pad(31)} ${subtotal?string.currency?left_pad(10)}
<#list taxes as tax>
${tax.text?left_pad(30)}: ${tax.value?string.currency?left_pad(10)}
</#list>
${"GRAND TOTAL:"?left_pad(31)} ${grandTotal?string.currency?left_pad(10)}

<#if priceSavings?has_content>
${BOLD}${"Sp. Price Savings:"?left_pad(30)}  ${priceSavings?string.currency?left_pad(10)}${NORMAL}
</#if>
<#if discountSavings?has_content>
${BOLD}${"Manual Discount Savings:"?left_pad(30)}  ${discountSavings?string.currency?left_pad(10)}${NORMAL}
</#if>
<#if promoSavings?has_content>
${BOLD}${"Promotional Savings:"?left_pad(30)}  ${promoSavings?string.currency?left_pad(10)}${NORMAL}
</#if>
<#if totalSavings?has_content>
------------------------------------------
${BOLD}${"Total Savings:"?left_pad(30)}  ${totalSavings?string.currency?left_pad(10)}${NORMAL}
</#if>

<#list payments as payment>
<#if payment.status=='Refunded' || payment.status=='Paid'>
${BOLD}${CENTER}***** ${payment.status} *****${NORMAL}
${BOLD}${payment.text?left_pad(30)}  ${payment.amount?string.currency?left_pad(10)}${NORMAL}
<#list payment.descriptions as desc>
${(desc.title+desc.text!"??")?left_pad(25)}
</#list>
</#if>
</#list>


${BOLD}${"CHANGE:"?left_pad(30)}  ${change?string.currency?left_pad(10)}${NORMAL}


<#include "companyfooter.ftl">


<#--
        //orderId
        //transactionId
        //drawerNo
        //userId
        //orderDate
        //date
        //salesRep
        //customer
        //saleDiscountValue
		//subtotal
        //grandTotal
        //change


        // Items...
        //  productId
        //  isTaxable
        //  quantity
        //  itemTotal
        //  description
        //  uomShortName
        //  basePrice
        //  scalable
        //    featureText
        //    featureValue
        //    discountText
        //    discountValue
        //    promoText
        //    promoValue
        //    overrideText
        //    overrideValue

        // Promos...
        //   promoText
        //   promoValue

        // Taxes...
        //   taxText
        //   taxValue


        // payments
        //   paymentText
        //   paymentValue
        //   payInfos...
        //     payInfoTitle
        //     payInfoText

-->
