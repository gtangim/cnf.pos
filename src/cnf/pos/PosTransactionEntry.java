package cnf.pos;
import cnf.pos.cart.resources.ResourceManager;
import org.joda.time.DateTime;

import java.text.SimpleDateFormat;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 3/15/11
 * Time: 3:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class PosTransactionEntry {
    public static SimpleDateFormat sdf = new SimpleDateFormat("'the' yyyy MMMMM dd 'at' HH:mm:ss");

    protected String txId;
    protected String name;
    protected PosTransaction tx;
    protected DateTime date;

    private static int seq = 0;
    public PosTransactionEntry(PosTransaction tx)
    {
        this.tx = tx;
        this.txId = Integer.toString(++seq);
        this.date = new DateTime();
        this.name = txId+" - "+sdf.format(date.toDate());
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PosTransaction getTx() {
        return tx;
    }

    public void setTx(PosTransaction tx) {
        this.tx = tx;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }
}
