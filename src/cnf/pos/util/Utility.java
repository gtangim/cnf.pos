package cnf.pos.util;

import javolution.util.FastList;
import javolution.util.FastMap;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by russela on 4/10/2015.
 */
public class Utility {
    public static final String module = Utility.class.getName();


    static DecimalFormat priceDecimalFormat = new DecimalFormat("#,##0.00");


    public static String fancyTitle(String s)
    {
        StringBuilder sb= new StringBuilder();
        for(Character c:s.toCharArray())
            if (c==' ') sb.append("    ");
            else {sb.append(c); sb.append(' ');}
        return sb.toString();
    }
    public static String formatPrice(BigDecimal price) {
        if (price == null) return "";
        return priceDecimalFormat.format(price);
    }
    public static String padString(String str, int setLen, boolean padEnd, char padChar) {
        if (str == null) {
            return null;
        }
        if (setLen == 0) {
            return str;
        }
        int stringLen = str.length();
        int diff = setLen - stringLen;
        if (diff < 0) {
            return str.substring(0, setLen);
        } else {
            StringBuilder newString = new StringBuilder();
            if (padEnd) {
                newString.append(str);
            }
            for (int i = 0; i < diff; i++) {
                newString.append(padChar);
            }
            if (!padEnd) {
                newString.append(str);
            }
            return newString.toString();
        }
    }

    private static StringBuilder numBuilder = new StringBuilder(20);

    public static String decompressNumber(String compressedVal) throws Exception
    {
        numBuilder.setLength(0);
        int n = compressedVal.length();
        char last='*';
        for (int i=0;i<n;i++)
        {
            char c = compressedVal.charAt(i);
            if (c>='0' && c<='9') numBuilder.append(c);
            else if (c>='A' && c<='J')
            {
                int v = (int)(c-'A');
                char d = (char)('0'+v);
                numBuilder.append(d);
                numBuilder.append(d);
            }
            else if (c>='K' && c<='Z')
            {
                if (last<'0' || last>'9') throw new NumberFormatException("Invalid compressed number format!");
                int rep = (int)(c - 'H') - 1;
                for (int j=0;j<rep;j++) numBuilder.append(last);
            }
            else throw new NumberFormatException("Invalid compressed number format!");
            last = c;
            if (numBuilder.length()>20) throw new NumberFormatException("Invalid compressed number format!");
        }
        return numBuilder.toString();
    }
    public static String compressNumber(long val)
    {
        numBuilder.setLength(0);
        String str = Long.toString(val);
        int n = str.length();
        char last = '*';
        int rep = 0;
        for (int i=0;i<n;i++)
        {
            char c = str.charAt(i);
            if (last=='*' || c!=last)
            {
                if (rep>0)
                {
                    if (rep==1) numBuilder.append(last);
                    else if (rep==2) {
                        int v = (int) (last - '0');
                        char doubleChar = (char)('A'+v);
                        numBuilder.append(doubleChar);
                    }
                    else
                    {
                        numBuilder.append(last);
                        char mult = (char)('H' + rep);
                        numBuilder.append(mult);
                    }
                }
                rep=1;
                last=c;
            }
            else rep++;
        }
        if (rep>0)
        {
            if (rep==1) numBuilder.append(last);
            else if (rep==2) {
                int v = (int) (last - '0');
                char doubleChar = (char)('A'+v);
                numBuilder.append(doubleChar);
            }
            else
            {
                numBuilder.append(last);
                char mult = (char)('H' + rep);
                numBuilder.append(mult);
            }
        }
        return numBuilder.toString();
    }

    public static boolean equals(String str1, String str2)
    {
        return (str1 == null ? str2 == null : str1.equals(str2));
    }

    public static String formatPrice(double price) {
        return priceDecimalFormat.format(price);
    }
    public static <V, V1 extends V> Map<String, V> toMap(String name1, V1 value1) {
         Map fields = FastMap.newInstance();
         fields.put(name1, value1);
         return fields;
    }
    public static <V, V1 extends V, V2 extends V> Map<String, V> toMap(String name1, V1 value1, String name2, V2 value2) {
         Map fields = FastMap.newInstance();
         fields.put(name1, value1);
         fields.put(name2, value2);
         return fields;
    }
    public static <V, V1 extends V, V2 extends V, V3 extends V> Map<String, V> toMap(String name1, V1 value1, String name2, V2 value2, String name3, V3 value3) {
         Map fields = FastMap.newInstance();
         fields.put(name1, value1);
         fields.put(name2, value2);
         fields.put(name3, value3);
         return fields;
    }
    public static <V, V1 extends V, V2 extends V, V3 extends V, V4 extends V> Map<String, V> toMap(String name1, V1 value1, String name2, V2 value2, String name3, V3 value3, String name4, V4 value4) {
         Map fields = FastMap.newInstance();
         fields.put(name1, value1);
         fields.put(name2, value2);
         fields.put(name3, value3);
         fields.put(name4, value4);
         return fields;
    }
    public static <V, V1 extends V, V2 extends V, V3 extends V, V4 extends V, V5 extends V> Map<String, V> toMap(String name1, V1 value1, String name2, V2 value2, String name3, V3 value3, String name4, V4 value4, String name5, V5 value5) {
        Map<String, V> fields = FastMap.newInstance();
        fields.put(name1, value1);
        fields.put(name2, value2);
        fields.put(name3, value3);
        fields.put(name4, value4);
        fields.put(name5, value5);
        return fields;
    }
    public static <V, V1 extends V, V2 extends V, V3 extends V, V4 extends V, V5 extends V, V6 extends V> Map<String, V> toMap(String name1, V1 value1, String name2, V2 value2, String name3, V3 value3, String name4, V4 value4, String name5, V5 value5, String name6, V6 value6) {
        Map<String, V> fields = FastMap.newInstance();
        fields.put(name1, value1);
        fields.put(name2, value2);
        fields.put(name3, value3);
        fields.put(name4, value4);
        fields.put(name5, value5);
        fields.put(name6, value6);
        return fields;
    }
    @SuppressWarnings("unchecked")
    public static <K, V> Map<String, V> toMap(Object... data) {
        if (data == null) {
            return null;
        }
        if (data.length == 1 && data[0] instanceof Map) {
            // Fix for javac's broken type inferring
            return UtilGenerics.<String, V>checkMap(data[0]);
        }
        if (data.length % 2 == 1) {
            throw new IllegalArgumentException("You must pass an even sized array to the toMap method");
        }
        Map<String, V> map = FastMap.newInstance();
        for (int i = 0; i < data.length;) {
            map.put((String) data[i++], (V) data[i++]);
        }
        return map;
    }
    public static <T> List<T> toList(T obj1) {
        List<T> list = FastList.newInstance();

        list.add(obj1);
        return list;
    }
    public static <T> List<T> toList(T obj1, T obj2) {
        List<T> list = FastList.newInstance();

        list.add(obj1);
        list.add(obj2);
        return list;
    }
    public static <T> List<T> toList(T obj1, T obj2, T obj3) {
        List<T> list = FastList.newInstance();

        list.add(obj1);
        list.add(obj2);
        list.add(obj3);
        return list;
    }
    public static <T> List<T> toList(T obj1, T obj2, T obj3, T obj4) {
        List<T> list = FastList.newInstance();

        list.add(obj1);
        list.add(obj2);
        list.add(obj3);
        list.add(obj4);
        return list;
    }
    public static <T> List<T> toList(T obj1, T obj2, T obj3, T obj4, T obj5) {
        List<T> list = FastList.newInstance();

        list.add(obj1);
        list.add(obj2);
        list.add(obj3);
        list.add(obj4);
        list.add(obj5);
        return list;
    }
    public static <T> List<T> toList(T obj1, T obj2, T obj3, T obj4, T obj5, T obj6) {
        List<T> list = FastList.newInstance();

        list.add(obj1);
        list.add(obj2);
        list.add(obj3);
        list.add(obj4);
        list.add(obj5);
        list.add(obj6);
        return list;
    }
    public static <T> List<T> toList(Collection<T> collection) {
        if (collection == null) return null;
        if (collection instanceof List) {
            return (List<T>) collection;
        } else {
            List<T> list = FastList.newInstance();
            list.addAll(collection);
            return list;
        }
    }

    public static DateTime startOfDay()
    {
        DateTime now = new DateTime();
        DateTime ret = new DateTime(now.year().get(), now.monthOfYear().get(), now.dayOfMonth().get(), 0,0,0,0);
        return ret;
    }
    private static Map<String,Properties> propertyResources = FastMap.newInstance();
    private static Map<String,Properties> urlCache = FastMap.newInstance();
    public static Properties getProperties(String resource) {
        if (resource == null || resource.length() <= 0) {
            return null;
        }
        String cacheKey = resource.replace(".properties", "");
        Properties properties = propertyResources.get(cacheKey);
        if (properties == null) {
            try {
                URL url = UtilURL.fromResource(resource);
                if (url == null)
                    return null;
                properties = getProperties(url);
                propertyResources.put(cacheKey, properties);
            } catch (MissingResourceException e) {
                Debug.log(e, module);
            }
        }
        if (properties == null) {
            Debug.log("[UtilProperties.getProperties] could not find resource: " + resource, module);
            return null;
        }
        return properties;
    }
    public static Properties getProperties(URL url) {
        if (url == null) {
            return null;
        }
        Properties properties = urlCache.get(url.toString());
        if (properties == null) {
            try {
                properties = new Properties();
                properties.load(url.openStream());
                urlCache.put(url.toString(), properties);
            } catch (Exception e) {
                Debug.log(e, module);
            }
        }
        if (properties == null) {
            Debug.log("[UtilProperties.getProperties] could not find resource: " + url, module);
            return null;
        }
        return properties;
    }

    public static Element getXmlRootElement(String xmlFilename) throws GeneralException {
        Document document = getXmlDocument(xmlFilename);

        if (document != null) {
            return document.getDocumentElement();
        } else {
            return null;
        }
    }

    private static Map<String,Document> loaderCache = FastMap.newInstance();
    public static Document getXmlDocument(String xmlFilename) throws GeneralException {
        Document document = (Document) loaderCache.get(xmlFilename);

        if (document == null) {
            synchronized (ResourceLoader.class) {
                document = (Document) loaderCache.get(xmlFilename);
                if (document == null) {
                    URL confUrl = UtilURL.fromResource(xmlFilename);

                    if (confUrl == null) {
                        throw new GeneralException("ERROR: could not find the [" + xmlFilename + "] XML file on the classpath");
                    }

                    try {
                        document = readXmlDocument(confUrl);
                    } catch (Exception e) {
                        throw new GeneralException("Error reading " + xmlFilename + "", e);
                    }
                    if (document != null) {
                        loaderCache.put(xmlFilename, document);
                    }
                }
            }
        }
        return document;
    }

    public static boolean isEmpty(String s) {
        return ((s == null) || (s.length() == 0));
    }
    public static boolean isNotEmpty(String s) {
        return ((s != null) && (s.length() > 0));
    }

    public static boolean isInteger(String s) {
        if (isEmpty(s)) return false;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }
    public static boolean isIntegerInRange(String s, int a, int b) {
        if (isEmpty(s)) return false;
        if (!isSignedInteger(s)) return false;
        int num = Integer.parseInt(s);
        return ((num >= a) && (num <= b));
    }
    public static boolean isSignedInteger(String s) {
        if (isEmpty(s)) return false;
        try {
            Integer.parseInt(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static int elapsedMs(DateTime t)
    {
        Duration d = new Duration(t,new DateTime());
        return (int)d.getMillis();
    }


    public static Document readXmlDocument(URL url) throws Exception {
        return readXmlDocument(url, true);
    }

    public static Document readXmlDocument(URL url, boolean validate) throws Exception {
        if (url == null) {
            Debug.logWarning("[UtilXml.readXmlDocument] URL was null, doing nothing", module);
            return null;
        }
        InputStream is = url.openStream();
        Document document = readXmlDocument(is, validate, url.toString());
        is.close();
        return document;
    }

    public static Document readXmlDocument(InputStream is, boolean validate, String docDescription) throws Exception {
        if (is == null) {
            Debug.logWarning("[UtilXml.readXmlDocument] InputStream was null, doing nothing", module);
            return null;
        }
        long startTime = System.currentTimeMillis();
        Document document = null;
        DocumentBuilderFactory factory = new org.apache.xerces.jaxp.DocumentBuilderFactoryImpl();
        factory.setValidating(validate);
        factory.setNamespaceAware(true);
        factory.setAttribute("http://xml.org/sax/features/validation", validate);
        factory.setAttribute("http://apache.org/xml/features/validation/schema", validate);
        DocumentBuilder builder = factory.newDocumentBuilder();
        if (validate) {
            LocalResolver lr = new LocalResolver(new DefaultHandler());
            ErrorHandler eh = new LocalErrorHandler(docDescription, lr);

            builder.setEntityResolver(lr);
            builder.setErrorHandler(eh);
        }
        document = builder.parse(is);

        double totalSeconds = (System.currentTimeMillis() - startTime)/1000.0;
        if (Debug.verboseOn()) Debug.logVerbose("XML Read " + totalSeconds + "s: " + docDescription, module);
        return document;
    }
    public static List<? extends Element> childElementList(Element element, String childElementName) {
        if (element == null) return null;
        List<Element> elements = FastList.newInstance();
        Node node = element.getFirstChild();
        if (node != null) {
            do {
                if (node.getNodeType() == Node.ELEMENT_NODE && (childElementName == null ||
                        childElementName.equals(node.getNodeName()))) {
                    Element childElement = (Element) node;

                    elements.add(childElement);
                }
            } while ((node = node.getNextSibling()) != null);
        }
        return elements;
    }
    public static Element firstChildElement(Element element, Set<String> childElementNames) {
        if (element == null) return null;
        Node node = element.getFirstChild();
        if (node != null) {
            do {
                if (node.getNodeType() == Node.ELEMENT_NODE && childElementNames.contains(node.getLocalName())) {
                    Element childElement = (Element) node;

                    return childElement;
                }
            } while ((node = node.getNextSibling()) != null);
        }
        return null;
    }
    public static Element firstChildElement(Element element, String childElementName, String attrName, String attrValue) {
        if (element == null) return null;
        Node node = element.getFirstChild();
        if (node != null) {
            do {
                if (node.getNodeType() == Node.ELEMENT_NODE && (childElementName == null ||
                        childElementName.equals(node.getNodeName()))) {
                    Element childElement = (Element) node;

                    String value = childElement.getAttribute(attrName);

                    if (value != null && value.equals(attrValue)) {
                        return childElement;
                    }
                }
            } while ((node = node.getNextSibling()) != null);
        }
        return null;
    }



    /**
     * Local entity resolver to handle J2EE DTDs. With this a http connection
     * to sun is not needed during deployment.
     * Function boolean hadDTD() is here to avoid validation errors in
     * descriptors that do not have a DOCTYPE declaration.
     */
    public static class LocalResolver implements EntityResolver {

        private boolean hasDTD = false;
        private EntityResolver defaultResolver;

        public LocalResolver(EntityResolver defaultResolver) {
            this.defaultResolver = defaultResolver;
        }

        /**
         * Returns DTD inputSource. If DTD was found in the dtds Map and inputSource was created
         * flag hasDTD is set to true.
         * @param publicId - Public ID of DTD
         * @param systemId - System ID of DTD
         * @return InputSource of DTD
         */
        public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
            //Debug.logInfo("resolving XML entity with publicId [" + publicId + "], systemId [" + systemId + "]", module);
            hasDTD = false;
            String dtd = "";
            if (isNotEmpty(dtd)) {
                if (Debug.verboseOn()) Debug.logVerbose("[UtilXml.LocalResolver.resolveEntity] resolving DTD with publicId [" + publicId +
                        "], systemId [" + systemId + "] and the dtd file is [" + dtd + "]", module);
                try {
                    URL dtdURL = UtilURL.fromResource(dtd);
                    if (dtdURL == null) {
                        throw new GeneralException("Local DTD not found - " + dtd);
                    }
                    InputStream dtdStream = dtdURL.openStream();
                    InputSource inputSource = new InputSource(dtdStream);

                    inputSource.setPublicId(publicId);
                    hasDTD = true;
                    if (Debug.verboseOn()) Debug.logVerbose("[UtilXml.LocalResolver.resolveEntity] got LOCAL DTD input source with publicId [" +
                            publicId + "] and the dtd file is [" + dtd + "]", module);
                    return inputSource;
                } catch (Exception e) {
                    Debug.logWarning(e, module);
                }
            } else {
                // nothing found by the public ID, try looking at the systemId, or at least the filename part of it and look for that on the classpath
                int lastSlash = systemId.lastIndexOf("/");
                String filename = null;
                if (lastSlash == -1) {
                    filename = systemId;
                } else {
                    filename = systemId.substring(lastSlash + 1);
                }

                URL resourceUrl = UtilURL.fromResource(filename);

                if (resourceUrl != null) {
                    InputStream resStream = resourceUrl.openStream();
                    InputSource inputSource = new InputSource(resStream);

                    if (isNotEmpty(publicId)) {
                        inputSource.setPublicId(publicId);
                    }
                    hasDTD = true;
                    if (Debug.verboseOn()) Debug.logVerbose("[UtilXml.LocalResolver.resolveEntity] got LOCAL DTD/Schema input source with publicId [" +
                            publicId + "] and the file/resource is [" + filename + "]", module);
                    return inputSource;
                } else {
                    Debug.logWarning("[UtilXml.LocalResolver.resolveEntity] could not find LOCAL DTD/Schema with publicId [" +
                            publicId + "] and the file/resource is [" + filename + "]", module);
                    return null;
                }
            }
            //Debug.logInfo("[UtilXml.LocalResolver.resolveEntity] local resolve failed for DTD with publicId [" +
            //        publicId + "] and the dtd file is [" + dtd + "], trying defaultResolver", module);
            return defaultResolver.resolveEntity(publicId, systemId);
        }

        /**
         * Returns the boolean value to inform Id DTD was found in the XML file or not
         * @return boolean - true if DTD was found in XML
         */
        public boolean hasDTD() {
            return hasDTD;
        }
    }

    /** Local error handler for entity resolver to DocumentBuilder parser.
     * Error is printed to output just if DTD was detected in the XML file.
     */
    public static class LocalErrorHandler implements ErrorHandler {

        private String docDescription;
        private LocalResolver localResolver;

        public LocalErrorHandler(String docDescription, LocalResolver localResolver) {
            this.docDescription = docDescription;
            this.localResolver = localResolver;
        }

        public void error(SAXParseException exception) {
            String exceptionMessage = exception.getMessage();
            Pattern valueFlexExpr = Pattern.compile("value '\\$\\{.*\\}'");
            Matcher matcher = valueFlexExpr.matcher(exceptionMessage.toLowerCase());
            if (localResolver.hasDTD() && !matcher.find()) {
                Debug.logError("XmlFileLoader: File "
                    + docDescription
                    + " process error. Line: "
                    + String.valueOf(exception.getLineNumber())
                    + ". Error message: "
                    + exceptionMessage, module
               );
            }
        }

        public void fatalError(SAXParseException exception) {
            if (localResolver.hasDTD()) {
                Debug.logError("XmlFileLoader: File "
                    + docDescription
                    + " process fatal error. Line: "
                    + String.valueOf(exception.getLineNumber())
                    + ". Error message: "
                    + exception.getMessage(), module
               );
            }
        }

        public void warning(SAXParseException exception) {
            if (localResolver.hasDTD()) {
                Debug.logError("XmlFileLoader: File "
                    + docDescription
                    + " process warning. Line: "
                    + String.valueOf(exception.getLineNumber())
                    + ". Error message: "
                    + exception.getMessage(), module
               );
            }
        }
    }
}
