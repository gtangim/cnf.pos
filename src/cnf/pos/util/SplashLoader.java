/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.util;

import java.awt.EventQueue;


public class SplashLoader implements Runnable {

    public static final String module = SplashLoader.class.getName();
    private static SplashScreen screen = null;
    private String logoFileName;
    private String version;

    public void load(String logoFile, String versionLabel) {
        this.logoFileName = logoFile;
        this.version = versionLabel;
        Thread t = new Thread(this);
        t.setName(this.toString());
        t.setDaemon(false);
        t.run();
    }

    public void unload() {
        SplashLoader.close();
    }

    public static SplashScreen getSplashScreen() {
        return screen;
    }

    public static void close() {
        if (screen != null) {
            EventQueue.invokeLater(new SplashScreenCloser());
        }
    }

    public void run() {
        if (logoFileName != null) {
            screen = new SplashScreen(logoFileName, version);
            screen.splash();
        }
    }

    private static final class SplashScreenCloser implements Runnable {
        public void run() {
            screen.close();
            screen = null;
        }
    }
}
