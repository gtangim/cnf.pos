package cnf.pos.cart;

import apu.jpos.util.StringUtil;
import javolution.util.FastList;
import javolution.util.FastMap;
import javolution.util.FastSet;
import org.joda.time.DateTime;
import cnf.pos.util.*;
import cnf.pos.cart.resources.*;
import cnf.pos.cart.resources.datastructures.*;
import cnf.pos.cart.resources.elements.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 1/19/11
 * Time: 3:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class PosShoppingCart {
    public static final String module = PosShoppingCartPayment.class.getName();


    public static final String TAXABLE_NET_VALUE = "tn";
    public static final String ITEM_NET_VALUES = "in";
    public static final String ITEM_DISCOUNT = "idsc";
    public static final String SALES_DISCOUNT = "sdsc";
    public BigDecimal ZERO = null;

    String prefix = "";
    ResourceManager resource;
    FastList<PosShoppingCartItem> items;
    FastList<PosShoppingCartPayment> payments;
    //String transactionId;
    String salesChannelId;
    AtomicDiscount saleDiscount;
    BigDecimal saleDiscountValue;

    protected String salesRepPartyId=null;
    //protected String managerPartyId;
    protected String customerPartyId=null;
    protected String customerExternalId=null;
    protected String billingAccountId=null;

    protected String parentOrderId=null;
    //protected String orderId=null;
    protected DateTime orderDate=null;
    protected Map storeParams;
    protected Map promoParams;

    protected boolean useGrossPrice;
    protected boolean roaMode=false;
    protected boolean donation=false;
    protected boolean qtyNegative=false;

    protected UserElement salesRep=null;
    protected CustomerElement customer=null;
    protected String receiptOption = null;

    protected FastList featureTypeNames;

    protected List<TaxElement> taxRates;
    protected FastMap<String,BigDecimal> taxValues;
    //protected HashMap<String,HashMap<String,PosShoppingCartItem>> taxableItems;
    protected FastMap<String,ArrayList<String>> promoCodes = null;



    protected ValueTreeNode values=null;

    protected BigDecimal grandTotal;
    protected BigDecimal subTotal;
    protected BigDecimal priceSavings;
    protected BigDecimal promoSavings;
    protected BigDecimal discountSavings;
    protected BigDecimal cashRounding;
    protected BigDecimal gcRounding;
    protected FastList<String> notes;



    // resources used for internal processing...
    //private HashMap<String,String> fMap = new HashMap<String, String>();
    private String finalLayerInd = null;
    private boolean taxApplied = false;
    private FastList<Integer> fullScope = null;
    private PromoEngine promoEngine;
    private boolean isReadOnly=false; // Do not allow altering the cart if this is set
    private boolean isPaymentProcessing=false;
    private String[] itemId;
    private String[] paymentId;
    private String userLoginId;
    private String companyParty;
    private FastMap<String,Boolean> isPreTaxFeature;
    private int itemVoidCount=0;

    // *********************** Initialization *****************************
    private static int nextTrainID=0;
    private static String getNextTrainID(ResourceManager r)
    {
        return "*"+r.getSettings().getLegacyOrderIdPrefix()
                +Utility.padString(Integer.toString(nextTrainID++),4,false,'0');
    }

    public PosShoppingCart(String salesChannelId)
        throws GeneralException
    {
        this.resource = ResourceManager.Resource;
        ZERO = BigDecimal.ZERO.setScale(resource.getSettings().getScale(),
                resource.getSettings().getRoundingMode());
        items = FastList.newInstance();
        payments = FastList.newInstance();
        this.prefix = resource.getSettings().getLegacyOrderIdPrefix();
        //if (resource.getSecurity().isTrainingMode()) this.orderId=getNextTrainID(resource);
        //else
        //{
        //    long legacyId = resource.getSecurity().getNextSeqId();
        //    if (legacyId<0) throw new GeneralException("Unable to generate an Order Id for this transaction!");
        //    this.orderId=prefix+Utility.padString(Long.toString(legacyId),10,false,'0');
        //}
        //this.transactionId = txId;
        this.salesChannelId = salesChannelId;
        this.notes = FastList.newInstance();
        SettingsResource s = resource.getSettings();
        this.promoParams=null;
        this.storeParams = setStoreParams(null
            ,s.getStore().StoreGroupId,null
                ,null,s.getCurrency());
        useGrossPrice = s.getStore().UseGrossPrice;
        this.saleDiscount = null;
        this.saleDiscountValue=null;
        String sRep = resource.getSecurity().getUser().UserId;
        salesRep = resource.getSecurity().getUser();
        reloadPartyParams();
        // This Shopping Cart doesn't allow sales without a sales rep. If no sales rep
        // can be assigned (i.e. online sale) then a default salesRep Party must be assigned.
        if (salesRep==null) throw new GeneralException("Error: No sales Rep session found!");
        // Now load the tax items
        taxRates = resource.getTaxes().getStoreTaxes();
        taxValues = FastMap.newInstance();
        //taxableItems = new HashMap<String, HashMap<String, PosShoppingCartItem>>();
        //for(TaxAuthorityRateProduct tarp:taxRates)
        //    taxableItems.put(tarp.getTaxAuthorityRateSeqId()
        //            ,new HashMap<String, PosShoppingCartItem>());
        // Initialize Shopping Cart Layer Specific vars
        //fMap = resource.getSettings().getMap();
        subTotal=ZERO;
        grandTotal=ZERO;
        priceSavings=ZERO;
        promoSavings=ZERO;
        discountSavings=ZERO;
        cashRounding=ZERO;
        gcRounding=ZERO;
        promoCodes = FastMap.newInstance();
        promoEngine= new PromoEngine(items,resource);

    }


    // *********************** Cart Items *****************************
    public PosShoppingCartItem addItem(AtomicProductInput p)
        throws GeneralException
    {
        checkReadOnly();
        checkROA("Add Product");
        if (p==null) throw new GeneralException("Cannot add empty product!");
        if (Utility.isEmpty(p.getProdId())
            || Utility.isEmpty(p.getSku()) || p.getQty()==null)
            throw new GeneralException("Cannot add invalid product!");
        BigDecimal qty = p.getQty();
        if (qty.compareTo(ZERO)<0) qty=qty.negate();

        if (parentOrderId!=null && qtyNegative) qty=p.getQty().negate();
        PosShoppingCartItem itm = new PosShoppingCartItem(resource, constructPriceRuleParams()
                ,p.getSku(),p.getInputValue(), p.getProdId(),p.getParentProdId(),
                qty,p.getPrice(),p.isSelected(), p.getItemType(), useGrossPrice);
        itm.setPromoAllowed(resource.getProducts().getProduct(p.getProdId()).IsPromoEnabled);
        itm.setDiscountAllowed(resource.getProducts().getProduct(p.getProdId()).IsDiscountAllowed);
        //if (itm == null) throw new GeneralException("Unable to add shopping cart item!");
        items.add(itm);
        return itm;
    }

    public void removeItem(PosShoppingCartItem item)
        throws GeneralException
    {
        checkReadOnly();
        checkROA("Remove Product");
        int ind=0;
        for(PosShoppingCartItem x:items)
        {
            if (x.getIndex()==item.getIndex())
            {
                items.remove(ind);
                itemVoidCount++;
                break;
            }
            ind++;
        }
    }

    public Map constructPriceRuleParams()
    {
        return storeParams;
        // also combine the party params...
    }

    public void reloadPartyParams()
    {
        SettingsResource s = resource.getSettings();
        if (storeParams==null)
            this.storeParams = setStoreParams(null
                ,s.getStore().StoreGroupId,null
                    ,null,s.getCurrency());
        else clearPartyParams(storeParams);
        promoParams = PromoResource.clearPartyParams(promoParams);
        if (salesRep!=null)
        {
            FastSet<String> roles = FastSet.newInstance();
            roles.addAll(salesRep.Roles);
            storeParams = setPartyParams(storeParams
                    , salesRep.UserId, null
                    , null, roles);
            promoParams = PromoResource.setPartyParams(promoParams
                    , salesRep.UserId, null
                    , null, roles);
        }
        if (customer!=null)
        {
            FastSet cgroups = FastSet.newInstance();
            cgroups.addAll(customer.Groups);
            promoParams = PromoResource.setPartyParams(promoParams
                    , customer.PartyId, customer.ClassificationId
                    , cgroups, null);
        }
    }


    // *********************** Cart Payments *****************************
    public PosShoppingCartPayment addPayment(PosShoppingCartPayment payment)
        throws GeneralException
    {
        checkReadOnly();
        if (payment!=null)
        {
            BigDecimal amt = payment.getAmount();
            String t = payment.getPayType();

            if ("EXT_BILLACT".equals(t))
            {
                if (payments.size()>0)
                    throw new GeneralException("Splitting payment using billable account not allowed!");
                else if (amt.compareTo(getDue())!=0)
                    throw new GeneralException("Splitting payment using billable account not allowed!");
                billingAccountId = payment.getReferenceNumber();
            }
            else if ("CASH".equals(t))
            {
                String currency = payment.getInputAmountUomId();
                PosShoppingCartPayment cashPayment = getCashPayment(currency);
                if (cashPayment!=null)
                {
                    removePayments("CASH",currency);
                    payment = new PosShoppingCartPayment(
                            resource,"CASH",cashPayment.getInputAmount().add(payment.getInputAmount())
                            ,currency,null,null,null,null,null,null,false
                    );
                }

                amt = payment.getAmount();  // Need to refresh from possible payment update above
                BigDecimal due = getDue();
                BigDecimal dueRounded = getDueWithRounding();

                if (isRefundPaymentNeeded())
                {
                    if (amt.compareTo(dueRounded)!=0)
                        throw new GeneralException("Splitting refunds into multiple payment methods is not allowed!");
                    cashRounding = due.subtract(amt);
                }
                else
                {
                    /*if (amt.compareTo(due)>=0)
                    {
                        cashRounding = due.subtract(amt);
                    } */
                    if (amt.compareTo(dueRounded)>=0)
                    {
                        int rnd = resource.getSettings().getPennyRounding();
                        int change = amt.subtract(dueRounded).movePointRight(2).intValue();
                        change = (change/rnd) * rnd;
                        BigDecimal c = new BigDecimal(change).movePointLeft(2);
                        cashRounding = due.subtract(amt).add(c);
                    }
                }
            }
            else if ("GIFT_CERTIFICATE".equals(payment.getPayType()))
            {
                if (isRefundPaymentNeeded())
                    throw new GeneralException("This form of refund is not supported!");
                BigDecimal due = getDue();
                BigDecimal dueRounded = getDueWithRounding();
                if (amt.compareTo(dueRounded)>=0)
                {
                    if (amt.compareTo(due)>=0)
                        gcRounding = due.subtract(amt);
                    else
                        cashRounding = due.subtract(dueRounded);
                }
            }
            else if ("VENDOR_COUPON".equals(payment.getPayType())){
                if (isRefundPaymentNeeded() && amt.compareTo(getDue())<0)
                    throw new GeneralException("You cannot apply a vendor coupon amount that is greater than the total refunded amount!");
            }
            else if (isRefundPaymentNeeded())
            {
                // General rule for any refund that is not a cash or a gift certificate: Must refund exact amount
                if (amt.compareTo(getDue())!=0)
                    throw new GeneralException("Splitting refunds into multiple payment methods is not allowed!");
            }
            payments.add(payment);
        }
        return payment;
    }

    public PosShoppingCartPayment getPayment(int index)
    {
        for (PosShoppingCartPayment payment : payments)
            if(payment.getIndex()==index) return payment;
        return null;
    }

    public List<PosShoppingCartPayment> getPayment(String payType)
    {
        List<PosShoppingCartPayment> ret = FastList.newInstance();
        for (PosShoppingCartPayment pay : payments)
            if (pay.getPayType().equals(payType)) ret.add(pay);
        return ret;
    }

    public PosShoppingCartPayment getCashPayment(String currency)
    {
        for(PosShoppingCartPayment p: payments)
            if (p.isActive() && p.getPayType().equals("CASH") && Utility.equals(currency,p.getInputAmountUomId()))
                return p;
        return null;
    }

    public void clearPayment(int index)
        throws GeneralException
    {
        checkReadOnly();
        checkROA("Clear Payment");
        FastList<PosShoppingCartPayment> newPayments = FastList.newInstance();
        for (PosShoppingCartPayment payment : payments)
            if(payment.getIndex()!=index) newPayments.add(payment);
        payments=newPayments;
    }

    public void removePayments(String payType, String currencyUom)
        throws GeneralException
    {
        checkReadOnly();
        FastList<PosShoppingCartPayment> newPayments = FastList.newInstance();
        for (PosShoppingCartPayment payment : payments)
            if(!payment.getPayType().equals(payType)
                    || !Utility.equals(currencyUom,payment.getInputAmountUomId()))
                newPayments.add(payment);
        payments=newPayments;
    }

    public void voidPayment(int index)
        throws GeneralException
    {
        checkReadOnly();
        checkROA("Void Payment");
        for (PosShoppingCartPayment payment : payments)
            if (payment.getIndex()==index && payment.isActive())
                payment.setTransactionStatus(PosShoppingCartPayment.STAT_VOIDED);
    }


    // *********************** Totals *****************************
    public boolean isRefundPaymentNeeded()
    {
        int c = getGrandTotal().compareTo(ZERO);
        if (c<0) return true;
        else return false;
    }
    public BigDecimal getGrandTotal()
    {
        if (roaMode) return ZERO;
        return grandTotal;
    }
    public BigDecimal getSubTotal()
    {
        if (roaMode) return ZERO;
        return subTotal;
    }
    public BigDecimal getTax(String taxApplId)
    {
        if (roaMode) return ZERO;
        if (taxValues.containsKey(taxApplId)) return taxValues.get(taxApplId);
        else return ZERO;
    }




    public int getNumberOfPayment()
    {
        return payments.size();
    }
    public BigDecimal getPaymentTotal()
    {
        BigDecimal tot = ZERO;
        for(PosShoppingCartPayment payment:payments)
            if(payment.isActive())
                tot=tot.add(payment.getAmount());
        return tot;
    }
    public BigDecimal getDue()
    {
        if (roaMode) return ZERO;
        return getGrandTotal().subtract(getPaymentTotal()).subtract(cashRounding).subtract(gcRounding);
    }

    public BigDecimal getCashRounding() {
        return cashRounding;
    }

    public BigDecimal getGcRounding() {
        return gcRounding;
    }

    public BigDecimal getDueWithRounding()
    {
        BigDecimal dollar = getDue();
        int round = resource.getSettings().getPennyRounding();
        if (round<=1) return dollar;
        boolean neg = false;
        int penny = dollar.movePointRight(2).intValue();
        if (penny<0) {neg=true;penny=-penny;}
        int remainder = penny % round;
        if (remainder == 0) return dollar;
        if (remainder>(round/2)) penny = penny - remainder + round;
        else penny = penny - remainder;
        if (neg) penny=-penny;
        return new BigDecimal(penny).movePointLeft(2);
    }



    public BigDecimal getFinishAmount(String currency, String paymentMethodId) {
        if (roaMode) return ZERO;
        //if (currency==null || !"CASH".equals(paymentMethodId))
        //{
        if (paymentMethodId.equals("CASH") || paymentMethodId.equals("GIFT_CERTIFICATE"))
            return getDueWithRounding();
        else return getDue();
        //}
        /*BigDecimal tot = ZERO;
        for(PosShoppingCartPayment payment:payments)
            if(payment.isActive())
            {
                if (!(payment.getPayType().equals("CASH") && payment.getInputAmountUomId().equals(currency)))
                    tot=tot.add(payment.getAmount());
            }
        return getGrandTotal().subtract(tot);*/
    }
    public boolean canFinishWithAmount(BigDecimal amount, String currency, String paymentMethodId)
    {
        if (isReadOnly) return false;
        if (roaMode) return true;
        boolean requireRefund = isRefundPaymentNeeded();
        BigDecimal due = getFinishAmount(currency, paymentMethodId);
        if (requireRefund==false && amount.compareTo(due)>=0) return true;
        else if (requireRefund && amount.compareTo(due)==0) return true;
        else return false;
    }




    // *********************** Sale Discount *****************************
    public void setDiscount(String disc, String reasonId, String reportingCode) throws GeneralException
    {
        checkReadOnly();
        checkROA("Set Discount");
        AtomicDiscount discount = new AtomicDiscount(disc, reasonId, reportingCode, resource);
        if (!discount.isValid()) throw new GeneralException("Invalid discount amount: "+disc);
        this.saleDiscount = discount;
    }
    public void clearDiscount(boolean clearItemDiscounts) throws GeneralException
    {
        checkReadOnly();
        checkROA("Clear Discount");
        this.saleDiscount=null;
        if (clearItemDiscounts) for(PosShoppingCartItem item:items)
            item.clearDiscount();
    }
    public String getDiscountText()
    {
        if (saleDiscount==null) return null;
        else return "Sale Discount: " + saleDiscount.getDiscountText();
    }
    public BigDecimal getDiscountAmount()
    {
        if (saleDiscount==null) return null;
        return saleDiscountValue;
    }





    // *********************** Properties *****************************
    public ResourceManager getResource() {
        return resource;
    }

    public void setResource(ResourceManager resource) {
        this.resource = resource;
    }

    public String getSalesChannelId() {
        return salesChannelId;
    }

    public void setSalesChannelId(String salesChannelId) throws GeneralException{
        checkReadOnly();
        this.salesChannelId = salesChannelId;
    }

    public AtomicDiscount getSaleDiscount() {
        return saleDiscount;
    }

    public String getSalesRepPartyId() {
        return salesRepPartyId;
    }

    public void setSalesRepPartyId(String salesRepPartyId) throws GeneralException {
        checkReadOnly();
        this.salesRepPartyId = salesRepPartyId;
    }

    public void addNote(String note) throws GeneralException
    {
        checkReadOnly();
        checkROA("Add Note");
        notes.add(note);
    }

    public List<String> getNotes()
    {
        return notes;
    }

    public boolean isReadOnly() {
        return isReadOnly;
    }

    public boolean isPaymentProcessing() {
        return isPaymentProcessing;
    }

    public void setPaymentProcessing(boolean isPaymentProcessing) {
        this.isPaymentProcessing = isPaymentProcessing;
    }
//public String getManagerPartyId() {
    //    return managerPartyId;
    //}

    //public void setManagerPartyId(String managerPartyId) throws GeneralException{
    //    checkReadOnly();
    //    this.managerPartyId = managerPartyId;
    //}

    public String getCustomerPartyId() {
        return customerPartyId;
    }

    public void setCustomerPartyId(String customerPartyId) throws GeneralException{
        checkReadOnly();
        this.customerPartyId = customerPartyId;
    }

    public boolean isEmpty()
    {
        return items.size()==0 && payments.size()==0;
    }

    public boolean  hasNoItem()
    {
        return items.size()==0;
    }

    public boolean isPayable()
    {
        if (roaMode) return true;
        else if (items.size()>0) return true;
        else return false;
    }

    /*public String getOrderId() {
        if (orderId==null) return "";
        else return orderId;
    }*/

    public DateTime getOrderDate()
    {
        if (orderDate==null) return new DateTime();
        else return orderDate;
    }

    /*public void setOrderId(String orderId) throws GeneralException {
        checkReadOnly();
        this.orderId = orderId;
    }*/

    public String getParentOrderId() {
        return parentOrderId;
    }

    public void setParentOrderId(String parentOrderId) throws GeneralException {
        checkReadOnly();
        checkROA("Enter Refund Mode");
        if (Utility.isEmpty(parentOrderId))
            this.parentOrderId=null;
        else
        {
            this.parentOrderId = parentOrderId;
            qtyNegative=true;
        }
    }

    public int size()
    {
        return items.size();
    }

    public int getItemIndex(PosShoppingCartItem item)
    {
        for (int i=0;i<items.size();i++)
            if (items.get(i).getIndex()==item.getIndex()) return i;
        return -1;
    }
    public List<PosShoppingCartItem> getItems()
    {
        return items;
    }

    public int getItemIndex(PosShoppingCartPayment payment)
    {
        for (int i=0;i<payments.size();i++)
            if (payments.get(i).getIndex()==payment.getIndex()) return i;
        return -1;
    }
    public List<PosShoppingCartPayment> getPayments()
    {
        return payments;
    }

    public List<PosShoppingCartPayment> getActivePayments()
    {
        List<PosShoppingCartPayment> ret = new ArrayList<PosShoppingCartPayment>();
        for (PosShoppingCartPayment p : payments)
            if (p.isActive()) ret.add(p);
        return ret;
    }


    public boolean addPromoCode(String promoCodeId) throws GeneralException
    {
        checkReadOnly();
        checkROA("Add Promo Code");
        PromoElement promo = resource.getPromos().getPromoCodePromo(promoCodeId);
        if (promo==null) return false;
        ArrayList<String> pcList = promoCodes.get(promo.Id);
        if (pcList==null) promoCodes.put(promo.Id,(pcList=new ArrayList<String>()));
        pcList.add(promoCodeId);
        return true;
    }

    public void clearPromoCodes() throws GeneralException
    {
        checkReadOnly();
        checkROA("Clear Promo Code");
        promoCodes = FastMap.newInstance();
    }


    public UserElement getSalesRep() {
        return salesRep;
    }

    public void setSalesRep(UserElement salesClerk) throws GeneralException {
        checkReadOnly();
        this.salesRep = salesClerk;
        reloadPartyParams();
    }

    public CustomerElement getCustomer() {
        return customer;
    }

    public String getCustomerDisplayName()
    {
        if (customer==null) return "N/A";
        else return customer.FirstName;
    }
    public String getSalesRepDisplayName()
    {
        if (salesRep==null) return "N/A";
        else return salesRep.FirstName;
    }

    public void setCustomer(CustomerElement customer) throws GeneralException {
        checkReadOnly();
        if (customer==null && isRefund()) throw new GeneralException("You cannot clear customer in refund mode."
        +"\r\nIf you need to switch customer, void and restart refund!");
        this.customer = customer;
        reloadPartyParams();
    }

    public String getPromoName(String promoId)
    {
        PromoElement pe = resource.getPromos().getPromo(promoId);
        if (pe!=null) return "  ** " + pe.Name+" **";
        return "  ** UNKNOWN_PROMO **";
    }

    public String getCustomerExternalId() {
        return customerExternalId;
    }

    public BigDecimal getPriceSavings() {
        return priceSavings;
    }

    public BigDecimal getPromoSavings() {
        return promoSavings;
    }

    public BigDecimal getDiscountSavings() {
        return discountSavings;
    }

    public boolean isDonation() {
        return donation;
    }

    public void setDonation(boolean donation) {
        this.donation = donation;
    }

    public boolean isQtyNegative() {
        return qtyNegative;
    }

    public void toggleQuantitySign()
    {
        qtyNegative=!qtyNegative;
    }

    public BigDecimal getTotalSavings()
    {
        return priceSavings.add(promoSavings.add(discountSavings));
    }

    public void setCustomerExternalId(String customerExternalId) throws GeneralException {
        checkReadOnly();
        if (Utility.isEmpty(customerExternalId))
            this.customerExternalId=null;
        else this.customerExternalId = customerExternalId;
    }

    public FastList<PromoAdjustment> getPromoAdjustments(int index)
    {
        if (promoEngine==null) return FastList.newInstance();
        PromoApplication gpa = promoEngine.getGlobalPromoApplications();
        if (gpa==null) return FastList.newInstance();
        FastList<PromoAdjustment> padList = gpa.getAdjustments(index);
        if (padList==null) return FastList.newInstance();
        else return padList;
    }

    public FastList<PromoAdjustment> getItemDistributedPromos(int index){
        if (promoEngine==null) return FastList.newInstance();
        FastList<PromoApplication> paList = promoEngine.getPromoApplications();
        FastList<PromoAdjustment> padList = FastList.newInstance();
        for(PromoApplication pa: paList){
            PromoAdjustment pad = pa.getDistributedAdjustment(index);
            if (pad!=null) padList.add(pad);
        }
        return padList;
    }

    public FastList<String> getFeatureTypeNames()
    {
        if (featureTypeNames==null)
        {
            List<ShoppingCartLayerElement> layers = resource.getSettings().getCartLayers();
            featureTypeNames = FastList.newInstance();
            for(ShoppingCartLayerElement scl : layers)
                if (scl.PrimaryTypeId== SCLPrimaryType.CL_FEATURE) {
                    if (scl.SecondaryTypeId== SCLSecondaryType.CSL_FT_CHG) featureTypeNames.add("CHARGE_FEATURE");
                    else if (scl.SecondaryTypeId== SCLSecondaryType.CSL_FT_OPT) featureTypeNames.add("OPTIONAL_FEATURE");
                    else if (scl.SecondaryTypeId== SCLSecondaryType.CSL_FT_REQ) featureTypeNames.add("REQUIRED_FEATURE");
                    else if (scl.SecondaryTypeId== SCLSecondaryType.CSL_FT_STD) featureTypeNames.add("STANDARD_FEATURE");
                    else if (scl.SecondaryTypeId== SCLSecondaryType.CSL_FT_STD) featureTypeNames.add("DISTINGUISHING_FEAT");
                }
        }
        return featureTypeNames;
    }

    public boolean isFeaturePreTax(String featureId)
    {
        if (isPreTaxFeature.get(featureId)==null || isPreTaxFeature.get(featureId)==true) return true;
        else return false;
    }

    public String getBillingAccountId() {
        return billingAccountId;
    }

    public boolean isRoaMode() {
        return roaMode;
    }

    public void setRoaMode(boolean roaMode, String billingAccountId) {
        this.roaMode = roaMode;
        if (roaMode) this.billingAccountId = billingAccountId;
    }

    public int getItemVoidCount() {
        return itemVoidCount;
    }

    // *********************** Primary Cart Processing *****************************
    public void refreshCartTotals() throws Exception
    {
        isPreTaxFeature = FastMap.newInstance();
        fullScope = FastList.newInstance();
        int n = items.size();
        priceSavings=ZERO;
        promoSavings=ZERO;
        discountSavings=ZERO;
        for (int i=0;i<n;i++)
        {
            fullScope.add(i);
            priceSavings = priceSavings.add(items.get(i)
                    .getSelectedPrice().getPriceSaving(useGrossPrice)
                    .negate().multiply(items.get(i).getQuantity()));
        }

        taxApplied = false;
        values = new ValueTreeNode(0);



        // First compute layer 0 (Price Layer)...
        String curLayerInd="0";
        processPriceLayer(curLayerInd);

        // Now go through the configuration to calculate other layers...
        List<ShoppingCartLayerElement> layers = resource.getSettings().getCartLayers();
        String lastLayerInd = null;
        for (ShoppingCartLayerElement layer:layers)
        {
            // First start a new layer...
            curLayerInd = Integer.toString(layer.LayerId);
            ValueTreeNode curLayer = new ValueTreeNode(0);
            values.addChild(curLayerInd,curLayer);

            // Now get the layer params...
            String condInd = curLayerInd;
            String actInd = curLayerInd;
            if (layer.ConditionLayerId>=0) condInd = Integer.toString(layer.ConditionLayerId);
            if (layer.ActionLayerId>=0) actInd = Integer.toString(layer.ActionLayerId);
            SCLPrimaryType pType=layer.PrimaryTypeId;
            SCLSecondaryType sType=layer.SecondaryTypeId;
            if (SCLPrimaryType.CL_FEATURE==pType)
                // Process features of a specific appl type....
                processFeatureLayer(curLayerInd,lastLayerInd,actInd,sType);
            else if (SCLPrimaryType.CL_DISCOUNT==pType && SCLSecondaryType.CSL_DS_ITM==sType)
                // Process Item Discount....
                processItemDiscountLayer(curLayerInd,lastLayerInd,actInd);
            else if (SCLPrimaryType.CL_DISCOUNT==pType && SCLSecondaryType.CSL_DS_SAL==sType)
                // Process Sale Discount....
                processSaleDiscountLayer(curLayerInd,lastLayerInd,actInd);
            else if (SCLPrimaryType.CL_TAX==pType)
            {
                // Process Sales Tax....
                processTaxLayer(curLayerInd,lastLayerInd,actInd);
                taxApplied = true;
            }
            else if (SCLPrimaryType.CL_PROMO==pType)
                // Process Sales Promotions....
                processPromoLayer(curLayerInd,lastLayerInd,condInd,actInd);
            lastLayerInd = curLayerInd;
        }
        finalLayerInd=curLayerInd;

        // Totalize amounts...
        double tot = values.getValue(curLayerInd);
        if (Double.isNaN(tot))
            throw new Exception("Grand Total has not been calculated (Layer config error)!");
        grandTotal = new BigDecimal(tot).setScale(resource.getSettings().getScale(),
                resource.getSettings().getRoundingMode());
        subTotal=grandTotal;
        if (!useGrossPrice)
        {
            for(BigDecimal taxValue:taxValues.values())
                subTotal=subTotal.subtract(taxValue);
        }
    }

    public double distributeTaxableNet(PosShoppingCartItem item, double value)
    {
        if (!taxApplied && item.isTaxable())
        {
            double netValue = item.calculateTaxableNet(value);
            for(String taxAppl : item.getApplicableTaxRates())
            {
                double netTotal = values.getValue(taxAppl);
                if (Double.isNaN(netTotal)) netTotal=0;
                netTotal+=netValue;
                values.setValue(taxAppl,netTotal);
            }
            return netValue;
        }
        return value;
    }
    public double distributeTaxableNet(List<Integer> scope, String actInd, double value) throws Exception
    {
        double totalNetValue = 0;
        double totalScopeValue = 0;
        if (scope==null || scope.size()==0) return value;
        if (taxApplied) return value;
        // Find the total value...
        for(int i:scope)
        {
            String Index = Integer.toString(i);
            double val = values.getValue(actInd,Index);
            if (Double.isNaN(val)) throw  new Exception("Description not found from Layer: " + actInd+"!");
            totalScopeValue += val;
        }

        // Now calculate
        for(int i:scope)
        {
            String Index = Integer.toString(i);
            PosShoppingCartItem item = items.get(i);
            double val = values.getValue(actInd,Index);
            double iNet = item.calculateTaxableNet(val*value/totalScopeValue);
            if (item.isTaxable())
            {
                for(String taxAppl : item.getApplicableTaxRates())
                {
                    double netTotal = values.getValue(taxAppl);
                    if (Double.isNaN(netTotal)) netTotal=0;
                    netTotal+=iNet;
                    values.setValue(taxAppl,netTotal);
                }
                totalNetValue+=iNet;
            }
            else totalNetValue+=(val*value/totalScopeValue);
        }
        return totalNetValue;
    }

    public void processPriceLayer(String curLayerInd) throws Exception
    {
        double tot = 0;
        for(int i=0;i<items.size();i++)
        {
            String Index = Integer.toString(i);
            PosShoppingCartItem item = items.get(i);
            double iValue = item.getSubtotal().doubleValue();
            values.setValue(curLayerInd,Integer.toString(i),items.get(i).getSubtotal().doubleValue());
            double itemNet = distributeTaxableNet(item,iValue);
            values.setValue(ITEM_NET_VALUES, Index, itemNet);
            tot+=iValue;
        }
        values.setValue(curLayerInd,tot);
    }

    private double getLastLayerTotal(String lastLayerInd, String actInd) throws Exception
    {
        double tot = values.getValue(actInd);
        if (Double.isNaN(tot)) throw new Exception("Could not get Action total value from cart layer " + actInd+"!");
        double lastTot = tot;
        if (lastLayerInd!=null) lastTot = values.getValue(lastLayerInd);
        if (Double.isNaN(lastTot)) lastTot=tot;
        return lastTot;
    }

    private double getLastLayerItem(String lastLayerInd,String actInd, String Index, double actLayerItem) throws Exception
    {
        if (Double.isNaN(actLayerItem)) throw new Exception("Could not get Action item value from cart layer " + actInd+"!");
        double iLastValue = actLayerItem;
        if (lastLayerInd!=null) iLastValue = values.getValue(lastLayerInd,Index);
        if (Double.isNaN(iLastValue)) iLastValue=actLayerItem;
        return iLastValue;
    }

    private void copyItemValues(String lastLayerInd,String curLayerInd)
    {
        for(int i=0;i<items.size();i++)
        {
            String Index = Integer.toString(i);
            values.setValue(curLayerInd,Index
                    ,values.getValue(lastLayerInd,Index));
        }
    }

    public void processFeatureLayer(String curLayerInd, String lastLayerInd
            , String actInd, SCLSecondaryType sType) throws Exception
    {
        //String faType = fMap.get(sType);
        if (sType==null)
            throw new Exception("Invalid Shopping Cart Layer entry! Feature layer must have a secondary type!");
        String faType = null;
        if (sType==SCLSecondaryType.CSL_FT_CHG) faType = "CHARGE_FEATURE";
        else if (sType==SCLSecondaryType.CSL_FT_DIS) faType="DISTINGUISHED_FEAT";
        else if (sType==SCLSecondaryType.CSL_FT_OPT) faType="OPTIONAL_FEATURE";
        else if (sType==SCLSecondaryType.CSL_FT_REQ) faType="REQUIRED_FEATURE";
        else if (sType==SCLSecondaryType.CSL_FT_STD) faType="STANDARD_FEATURE";
        double tot = getLastLayerTotal(lastLayerInd,actInd);
        for(int i=0;i<items.size();i++)
        {
            String Index = Integer.toString(i);
            PosShoppingCartItem item = items.get(i);
            List<FeatureElement> faeList = item.getFeatures(faType);
            double iActValue = values.getValue(actInd,Index);
            double iLastValue = getLastLayerItem(lastLayerInd,actInd,Index,iActValue);

            for(FeatureElement fae:faeList)
            {
                double fVal = item.getFeatureDisplayValue(fae).doubleValue();
                double featureNet = distributeTaxableNet(item,fVal);
                values.setValue(ITEM_NET_VALUES, Index, fae.FeatureId,featureNet);
                isPreTaxFeature.put(fae.FeatureId,!taxApplied);
                iLastValue+= fVal;
                tot+=fVal;
            }
            values.setValue(curLayerInd, Index, iLastValue);
        }
        values.setValue(curLayerInd,tot);
    }

    public void processItemDiscountLayer(String curLayerInd, String lastLayerInd, String actInd) throws Exception
    {
        double tot=getLastLayerTotal(lastLayerInd,actInd);
        for(int i=0;i<items.size();i++)
        {
            String Index = Integer.toString(i);
            PosShoppingCartItem item = items.get(i);
            double iActValue = values.getValue(actInd,Index);
            double iLastValue = getLastLayerItem(lastLayerInd,actInd,Index,iActValue);
            BigDecimal iDiscValue = item.getItemDiscountAmount(iActValue);
            if (iDiscValue!=null)
            {
                discountSavings=discountSavings.add(iDiscValue);
                double dVal = iDiscValue.doubleValue();
                double discNet = distributeTaxableNet(item,dVal);
                values.setValue(ITEM_DISCOUNT,Index,discNet);
                iLastValue+=dVal;
                tot+=dVal;
            }
            values.setValue(curLayerInd,Index,iLastValue);
        }
        values.setValue(curLayerInd,tot);
    }

    public void processSaleDiscountLayer(String curLayerInd, String lastLayerInd,String actInd) throws Exception
    {
        double actTot = values.getValue(actInd);
        double lastTot=getLastLayerTotal(lastLayerInd,actInd);
        if (saleDiscount==null) {
            saleDiscountValue=null;
        }
        else
        {
            saleDiscountValue = saleDiscount.calculateRoundedDiscount(actTot);
            double sdVal = saleDiscountValue.doubleValue();
            double sdNet = distributeTaxableNet(fullScope,actInd,sdVal);
            values.setValue(SALES_DISCOUNT,sdNet);
            lastTot+=sdVal;
            discountSavings=discountSavings.add(saleDiscountValue);
        }
        values.setValue(curLayerInd,lastTot);
        copyItemValues(lastLayerInd,curLayerInd);
    }

    public void processTaxLayer(String curLayerInd, String lastLayerInd, String actInd) throws Exception
    {
        taxValues.clear();
        double lastTot=getLastLayerTotal(lastLayerInd,actInd);
        for (TaxElement t:taxRates)
        {
            double rate = t.TaxPercent.movePointLeft(2).doubleValue();
            double taxNet = values.getValue(t.TaxId);
            if (Double.isNaN(taxNet)) taxNet=0;
            BigDecimal appliedTax = new BigDecimal(taxNet*rate).setScale(resource.getSettings().getScale(),
                    resource.getSettings().getRoundingMode());
            taxValues.put(t.TaxId,appliedTax);
            lastTot+=appliedTax.doubleValue();
        }
        values.setValue(curLayerInd, lastTot);
        copyItemValues(lastLayerInd,curLayerInd);
    }

    public void processPromoLayer(String curLayerInd, String lastLayerInd,String condInd, String actInd) throws Exception
    {
        double totalSavings = 0;
        promoParams.put("VALUES",values);
        promoParams.put("CODES",promoCodes);
        promoEngine.applyPromo(promoParams,condInd,actInd,useGrossPrice);
        FastList<PromoApplication> promoApplications = promoEngine.getPromoApplications();
        PromoApplication gpa = promoEngine.getGlobalPromoApplications();


        double lastTot=getLastLayerTotal(lastLayerInd,actInd);
        for(int i=0;i<items.size();i++)
        {
            String Index = Integer.toString(i);
            PosShoppingCartItem item = items.get(i);
            double iActValue = values.getValue(actInd,Index);
            double iLastValue = getLastLayerItem(lastLayerInd,actInd,Index,iActValue);
            // distribute taxable net of item promo...

            FastList<PromoAdjustment> itemAdjustments = gpa.getAdjustments(i);
            if (itemAdjustments!=null)
                for (PromoAdjustment adj:itemAdjustments)
                {
                    double pVal = adj.getAmount().doubleValue();
                    double pNet = distributeTaxableNet(item,pVal);
                    adj.setTaxableNet(pNet);
                    iLastValue+=pVal;
                    promoSavings=promoSavings.add(adj.getAmount());
                }
            values.setValue(curLayerInd,Index,iLastValue);
        }

        // Distribute taxable net of order promo

        FastList<PromoAdjustment> orderAdjustments = gpa.getAdjustments(-1);
        if (orderAdjustments!=null)
            for(PromoAdjustment adj:orderAdjustments)
            {
                double opVal = adj.getAmount().doubleValue();
                double opNet = distributeTaxableNet(fullScope,actInd,opVal);
                adj.setTaxableNet(opNet);
                promoSavings=promoSavings.add(adj.getAmount());
            }

        lastTot+=gpa.getTotal();
        values.setValue(curLayerInd,lastTot);
    }


    // *********************** Order Save *****************************
    public boolean canFinalize()
    {
        if (isReadOnly) return false;
        if (roaMode)
        {
            BigDecimal pt = getPaymentTotal();
            if (pt.compareTo(ZERO)>0)
                return true;
            else return false;
        }
        if (items.size()>0 && payments.size()>0)
        {
            if (isRefundPaymentNeeded() && getDue().compareTo(ZERO)==0) return true;
            else if (!isRefundPaymentNeeded() && getDue().compareTo(ZERO)<=0) return true;
        }
        return false;
    }

    public boolean isRefund()
    {
        return parentOrderId!=null;
    }
    public boolean isOrderRefundValid()
    {
        if (parentOrderId!=null)
        {
            if (resource.getSettings().filterReceipt(parentOrderId)!=null
                    || customer!=null
                    || Utility.isNotEmpty(customerExternalId))
                return true;
            else
                return false;
        }
        else
            return true;
    }
    public void finalizeOrder() throws GeneralException
    {
        checkReadOnly();
        SettingsResource s = resource.getSettings();
        orderDate = new DateTime();
        companyParty = s.getStore().CompanyName;
        userLoginId = resource.getSecurity().getUser().UserNumber;
        salesRepPartyId = "_NA_";
        customerPartyId = "_NA_";
        if (salesRep!=null) salesRepPartyId=salesRep.UserId;
        if (customer!=null) customerPartyId=customer.PartyId;

        if (roaMode)
        {
            if (!canFinalize()) throw new GeneralException("Cannot Complete Receive of Account Process!"
                    +"\r\nMake sure you have added a valid payment!");
            if (Utility.isEmpty(billingAccountId))
                throw new GeneralException("No Billing Account assigned to this customer!");
            this.isReadOnly=true;
            return;
        }

        if (!canFinalize()) throw new GeneralException("Cannot Save sale! Sale is not complete!");
        isReadOnly=true;
    }

    /*public void finalizeOrder() throws GeneralException
    {
        checkReadOnly();
        SettingsResource s = resource.getSettings();
        BigDecimal grandTotal = getGrandTotal();
        BigDecimal subTotal = getSubTotal();
        orderDate = UtilDateTime.nowTimestamp();
        companyParty = s.getStore().CompanyName;
        userLoginId = resource.getSecurity().getUser().UserNumber;
        salesRepPartyId = "_NA_";
        customerPartyId = "_NA_";
        if (salesRep!=null) salesRepPartyId=salesRep.UserId;
        if (customer!=null) customerPartyId=customer.PartyId;

        if (roaMode)
        {
            if (!canFinalize()) throw new GeneralException("Cannot Complete Receive of Account Process!"
            +"\r\nMake sure you have added a valid payment!");
            if (UtilValidate.isEmpty(billingAccountId))
                throw new GeneralException("No Billing Account assigned to this customer!");
            FastList<Payment> pList = FastList.newInstance();
            FastList<PaymentApplication> paList = FastList.newInstance();
            h.addAllROAPayments(pList,paList);
            if (!resource.getSettings().isTrainingMode())
            {
                r.createOrUpdate(pList);
                r.createOrUpdate(paList);
                resource.getSettings().postReceiveOfAccountToDb(getPaymentTotal());
            }
            this.isReadOnly=true;
            return;
        }


        if (!canFinalize()) throw new GeneralException("Cannot Save sale! Sale is not complete!");
        h.setupId();
        OrderHeader oh = new OrderHeader();
        FastList<OrderItem> oiList= FastList.newInstance();
        FastList<OrderAdjustment> oaList = FastList.newInstance();
        FastList<OrderStatus> osList = FastList.newInstance();
        FastList<OrderRole> orList = FastList.newInstance();
        FastList<OrderItemShipGroup> oisgList = FastList.newInstance();
        FastList<OrderItemShipGroupAssoc> oisgcList = FastList.newInstance();
        FastList<OrderHeaderNote> ohnList = FastList.newInstance();
        FastList<NoteData> ndList = FastList.newInstance();
        FastList<OrderPaymentPreference> oppList = FastList.newInstance();
        FastList<Payment> pList = FastList.newInstance();


        // Basic Settings of order header
        oh.setOrderId(orderId);
        oh.setOrderTypeId("SALES_ORDER");
        oh.setSalesChannelEnumId(salesChannelId);
        oh.setOrderDate(orderDate);
        oh.setPriority("2");
        oh.setEntryDate(orderDate);
        oh.setStatusId("ORDER_COMPLETED");
        oh.setCreatedBy(s.getUser().getUserId());
        oh.setCurrencyUom(s.getCurrency());
        oh.setOriginFacilityId(s.getFacility().getFacilityId());
        oh.setProductStoreId(s.getStore().getProductStoreId());
        oh.setTerminalId(s.getTerminal().getPosTerminalId());
        oh.setNeedsInventoryIssuance("Y");
        oh.setInternalCode(parentOrderId);
        oh.setRemainingSubTotal(grandTotal);
        oh.setGrandTotal(grandTotal);
        oh.setSubTotal(subTotal);
        oh.setBillFromPartyId(companyParty);
        oh.setIsGrossPriceMode(useGrossPrice?"Y":"N");
        if (UtilValidate.isNotEmpty(parentOrderId)) oh.setParentOrderId(parentOrderId);
        if (customer!=null)
        {
            oh.setCustomerId(customer.getLoyaltyId());
            oh.setCustomerPartyId(customer.getPartyId());
            oh.setCustomerExternalId(customerExternalId);
        }
        oh.setPaidAmount(getPaymentTotal());
        oh.setChangeAmount(getDue().negate());
        if (donation) oh.setIsDonation("Y");
        else oh.setIsDonation("N");

        // next do order notes
        if (parentOrderId!=null) h.addNote(ohnList,ndList,"ORDER_REFUND",parentOrderId);
        for(String note:notes) h.addNote(ohnList,ndList,"ORDER_NOTE",note);

        // next do order roles
        orList.add(h.Role(salesRepPartyId,"SALES_REP"));
        orList.add(h.Role(companyParty,"BILL_FROM_VENDOR"));
        orList.add(h.Role(customerPartyId,"END_USER_CUSTOMER"));
        orList.add(h.Role(customerPartyId,"PLACING_CUSTOMER"));
        orList.add(h.Role("_NA_","SHIP_TO_CUSTOMER"));
        orList.add(h.Role(customerPartyId,"BILL_TO_CUSTOMER"));

        // next add payments
        h.addAllPayments(oppList,pList);
        if (billingAccountId!=null)
        {
            if (customer==null)
                throw new GeneralException("Invalid billing account transaction!");
            if (oppList==null || oppList.size()!=1)
                throw new GeneralException("Invalid billing account transaction!");
            if (!"EXT_BILLACT".equals(oppList.get(0).getPaymentMethodTypeId()))
                throw new GeneralException("Invalid billing account transaction!");
            if (getDue().compareTo(BigDecimal.ZERO)!=0)
                throw new GeneralException("Billing account payment must be equal to \r\nthe full cart amount!");
            oppList.get(0).setStatusId("PAYMENT_NOT_RECEIVED");
            pList.get(0).setStatusId("PMNT_NOT_PAID");
            oh.setBillingAccountId(billingAccountId);
            oh.setBillToPartyId(customer.getPartyId());
            //orList.add(h.Role(customerPartyId,"BILL_TO_CUSTOMER"));
        }
        BigDecimal change = getDue();
        if (ZERO.compareTo(change)!=0)
        {
            try
            {
                h.addPayment(new PosShoppingCartPayment(
                    resource,"CASH",change,"CAD",null,"CHANGE",null,null,null,null
                        ,false
                ),oppList,pList);
            }
            catch(Exception ex)
            {
                Debug.logError(ex,"Error adding change!",module);
                throw new GeneralException(ex.getMessage());
            }
        }


        // Now do the items...
        double totalListedValue=0;
        double totalSelectedValue=0;
        double totalFinalValue=0;
        double totalFeatureStd = 0;
        double totalFeaturePreTax=0;
        double totalFeaturePostTax=0;
        double totalItemDiscount=0;
        double totalSaleDiscount=0;
        double totalItemPromo=0;
        double totalOrderPromo=0;
        BigDecimal totalCost = ZERO;
        BigDecimal totalTax=ZERO;

        int i=0;
        for(PosShoppingCartItem item:items)
        {
            // First Create the item entry
            String Index = Integer.toString(i);
            OrderItem oi = h.Item(i);
            oi.setStatusId("ITEM_COMPLETED");
            oi.setCreatedBy(s.getUser().getUserId());
            oiList.add(oi);
            totalListedValue+=oi.getNetValueListed().doubleValue();
            totalSelectedValue+=oi.getNetValueSelected().doubleValue();
            totalFinalValue+=oi.getNetValueFinal().doubleValue();

            totalCost = totalCost.add(item.getCost().multiply(item.getQuantity()));

            // First add feature adjustments...
            double featureStd = 0;
            double featurePreTax = 0;
            double featurePostTax = 0;
            for(String featureType:getFeatureTypeNames())
            {
                List<FeatureApplicationElement> fList = item.getFeaturesApplications(featureType);
                for(FeatureApplicationElement fae:fList)
                {
                    double Val = values.getValue(ITEM_NET_VALUES, Index, fae.getFeatureId());
                    if (Double.isNaN(Val)) Val=0;
                    if (fae.getFeatureApplTypeId().equals("STANDARD_FEATURE")) featureStd+=Val;
                    else if(isPreTaxFeature.get(fae.getFeatureId())==null
                            || isPreTaxFeature.get(fae.getFeatureId())==true)
                        featurePreTax+=Val;
                    else featurePostTax+=Val;
                    oaList.add(h.Feature(i,item.getFeatureDisplayValue(fae),Val,fae));
                }
            }
            oi.setNetValueFeatureStd(new BigDecimal(featureStd));
            oi.setNetValueFeaturePreTax(new BigDecimal(featurePreTax));
            oi.setNetValueFeaturePostTax(new BigDecimal(featurePostTax));
            totalFeatureStd+=featureStd;
            totalFeaturePreTax+=featurePreTax;
            totalFeaturePostTax+=featurePostTax;

            // Next do Item Discounts
            double discNet=0;
            if (item.getItemDiscount()!=null)
            {
                BigDecimal discAmount = item.getItemDiscountAmount();
                discNet = values.getValue(ITEM_DISCOUNT, Index);
                oaList.add(h.Discount(i,item.getItemDiscount().getPercent(),discAmount,discNet));
                totalItemDiscount+=discNet;
            }
            oi.setNetValueDiscount(new BigDecimal(discNet));

            // Next do Promo
            double promoNet = 0;
            for(PromoAdjustment pad : getPromoAdjustments(i))
            {
              oaList.add(h.Promo(i,pad));
              promoNet+=pad.getTaxableNet();
              totalItemPromo+=pad.getTaxableNet();
            }
            oi.setNetValuePromo(new BigDecimal(promoNet));

            i++;
        }

        totalCost = totalCost.setScale(resource.getSettings().getScale(),
                resource.getSettings().getRoundingMode());

        // Now do order level stuff...

        // Do Order Level Promo adjustments
        for(PromoAdjustment pad : getPromoAdjustments(-1))
        {
          oaList.add(h.Promo(-1,pad));
          totalOrderPromo+=pad.getTaxableNet();
        }

        // Do Sale Discount
        if (saleDiscount!=null)
        {
            double sdNet = values.getValue(SALES_DISCOUNT);
            oaList.add(h.Discount(-1,saleDiscount.getPercent(),saleDiscountValue,sdNet));
            totalSaleDiscount+=sdNet;
        }

        // Do Tax Entry
        List<TaxAuthorityRateProduct> tList = resource.getTaxes().getTaxRates();
        for(TaxAuthorityRateProduct tarp:tList)
        {
            BigDecimal taxValue = getTax(tarp.getTaxAuthorityRateSeqId());
            if (!taxValue.equals(ZERO)) oaList.add(h.Tax(tarp,taxValue));
            totalTax=totalTax.add(taxValue);
        }

        // Enter all the sums in orderHeader

        oh.setNetValueListed(new BigDecimal(totalListedValue));
        oh.setNetValueSelected(new BigDecimal(totalSelectedValue));
        oh.setNetValueFinal(new BigDecimal(totalFinalValue));
        oh.setNetValueFeatureStd(new BigDecimal(totalFeatureStd));
        oh.setNetValueFeaturePreTax(new BigDecimal(totalFeaturePreTax));
        oh.setNetValueFeaturePostTax(new BigDecimal(totalFeaturePostTax));
        oh.setNetValueDiscountItem(new BigDecimal(totalItemDiscount));
        oh.setNetValueDiscountSale(new BigDecimal(totalSaleDiscount));
        oh.setNetValuePromoItem(new BigDecimal(totalItemPromo));
        oh.setNetValuePromoSales(new BigDecimal(totalOrderPromo));
        oh.setTaxTotal(totalTax);
        oh.setCostTotal(totalCost);

        // next enter shipping info (NO_SHIPPING)
        h.addShipping(oisgList,oisgcList);

        // next do order status
        osList.add(h.Status("ORDER_COMPLETED",-1,-1));
        i=0;
        for(PosShoppingCartItem item:items)
        {
            osList.add(h.Status("ITEM_COMPLETED",i,-1));
            i++;
        }
        i=0;
        for(PosShoppingCartPayment payment:payments){
            if (payment.isActive())
                osList.add(h.Status("PAYMENT_RECEIVED",-1,i));
            i++;
        }


        // Configure counts...
        oh.setOrderAdjustmentCount(new Long(oaList.size()));
        oh.setPaymentCount(new Long(pList.size()));
        oh.setPaymentPrefCount(new Long(oppList.size()));
        oh.setRoleCount(new Long(orList.size()));
        oh.setStatusCount(new Long(osList.size()));
        oh.setItemCount(new Long(oiList.size()));
        oh.setNoteCount(new Long(ndList.size()));
        oh.setOrderItemShipGroupCount(new Long(oisgList.size()));
        oh.setOrderItemShipGroupAssocCount(new Long(oisgcList.size()));

        if (!resource.getSettings().isTrainingMode())
        {
            r.createOrUpdate(oh);
            r.createOrUpdate(ndList);
            r.createOrUpdate(ohnList);
            r.createOrUpdate(orList);
            r.createOrUpdate(oiList);
            r.createOrUpdate(oaList);
            r.createOrUpdate(osList);
            r.createOrUpdate(oisgList);
            r.createOrUpdate(oisgcList);
            r.createOrUpdate(oppList);
            r.createOrUpdate(pList);
            SettingsResource settings = resource.getSettings();
            settings.postOrderToDb(orderId, grandTotal);
            if (itemVoidCount>0) settings.postItemVoid(itemVoidCount);
            int poCount = 0;
            for(PosShoppingCartItem item:getItems())
                if (item.getOverrideReason()!=null) poCount++;
            if (poCount>0) settings.postPriceOverride(poCount);
        }
        // Order is finished...
        isReadOnly=true;
    } */







    // *********************** Helper methods *****************************
    private boolean getBoolean(String b,boolean defaultValue)
    {
        if (Utility.isEmpty(b)) return defaultValue;
        char c = b.charAt(0);
        if (c=='Y' || c=='y' || c=='1' || c=='T' || c=='t') return true;
        else return false;
    }

    public void checkReadOnly() throws GeneralException
    {
        if (isReadOnly) throw new GeneralException("Cart is in read-only mode!");
    }

    public void checkROA(String operation) throws GeneralException
    {
        if (roaMode) throw new GeneralException("Cannot perform requested operation\r\n[" + operation +"]"
                +" while in Receive of Account Mode!");
    }

    public boolean isUseGrossPrice() {
        return useGrossPrice;
    }

    public static Map setProductParams(Map params, String productId, BigDecimal qty, PriceElement prices
            , Set<String> categories, Set<String> features, boolean useGrossPrice)
    {
        if (params==null) params = FastMap.newInstance();
        if (categories==null) categories = FastSet.newInstance();
        if (features==null) features = FastSet.newInstance();
        params.put("PRIP_PRODUCT_ID",productId);
        params.put("PRIP_PROD_CAT_ID",categories);
        params.put("PRIP_QUANTITY",qty);
        params.put("PRIP_FEAT_ID",features);
        params.put("PRIP_LIST_PRICE",prices.ListPrice);
        params.put("EXTRA_PRICE_ELEMENT",prices);
        params.put("EXTRA_USE_GROSS_PRICE",useGrossPrice);
        return params;
    }

    public static Map clearPartyParams(Map params)
    {
        if (params==null) params = FastMap.newInstance();
        params.put("PRIP_PARTY_ID",FastSet.newInstance());
        params.put("PRIP_PARTY_CLASS",FastSet.newInstance());
        params.put("PRIP_GRP_MEM",FastSet.newInstance());
        params.put("PRIP_ROLE_TYPE",FastSet.newInstance());
        return params;
    }

    public static Map setPartyParams(Map params, String partyId, String classification
            , FastSet<String> groups, Set<String> roles)
    {
        if (params==null) params = FastMap.newInstance();
        if (partyId!=null) ((Set<String>)params.get("PRIP_PARTY_ID")).add(partyId);
        if (classification!=null) ((Set<String>)params.get("PRIP_PARTY_CLASS"))
                .add(classification);
        if (groups!=null) ((Set<String>)params.get("PRIP_GRP_MEM")).addAll(groups);
        if (roles!=null) ((Set<String>)params.get("PRIP_ROLE_TYPE")).addAll(roles);
        return params;
    }


    public static Map setStoreParams(Map params, String productStoreGroupId, String productCatalogId
            , String websiteId, String currencyUomId)
    {
        if (params==null) params = FastMap.newInstance();
        params.put("PRIP_PROD_SGRP_ID",productStoreGroupId);
        params.put("PRIP_PROD_CLG_ID",productCatalogId);
        params.put("PRIP_WEBSITE_ID",websiteId);
        params.put("PRIP_CURRENCY_UOMID",currencyUomId);
        return params;
    }


    public String getReceiptOption() {
        return receiptOption;
    }

    public void setReceiptOption(String receiptOption) {
        this.receiptOption = receiptOption;
    }
}
