package cnf.pos.cart.resources;

import bongo.node.NodeApi;
import bongo.node.entities.DateTimeXmlConverter;
import bongo.util.SimpleLogger;
import bongo.util.Utility;
import cnf.loader.BinaryBuffer;
import cnf.loader.PosDataReader;
import cnf.node.NodeClient;
import cnf.node.entities.*;
import com.thoughtworks.xstream.XStream;
import org.joda.time.DateTime;
import cnf.pos.util.Debug;
import cnf.pos.cart.resources.elements.*;
import cnf.pos.device.DeviceLoader;

import java.io.File;
import java.math.BigDecimal;
import java.nio.file.*;
/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/21/11
 * Time: 2:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class ResourceManager implements Runnable {
    //public static final String Version = "1.20";  // Next version is 2.00 which is the CNF POS without bongo node
    public static final String Version = "2.04";

    public static final String module = ResourceManager.class.getName();
    public static String PosConfigFileName = "resources/data/PosConfig.xml";
    public static String PosStateFileName = "resources/data/PosStates.xml";
    public static String PosStateHistoryFileName = "resources/data/PosStateHistory.dat";
    public static String OldPosPropertyFileName = "resources/pos.properties";
    public static int MaxStateHistorySize = 100 * 1024 * 1024;  // 100MB
    public static ResourceManager Resource;

    public static String CustomerFilePrefix = "PosCustomer";
    public static String SecurityFilePrefix = "PosSecurity";
    public static String DataFilePrefix = "PosData";
    public static String ResourceFolder = "./resources/data/";




    protected SettingsResource settings;
    protected ConversionResource conversions;
    protected ProductResource products;
    protected PromoResource promos;
    protected TaxResource taxes;
    protected SecurityResource security;
    protected CustomerResource customers;
    protected NodeClient node;



    //protected XuiSession session;

    private boolean loadingData = false;
    private XStream xmlConverter;

    private PosConfig config = null;
    private PosSecurityData securityData = null;
    private PosCustomerData customerData = null;
    private PosCoreData coreData = null;
    private PosDataReader deserializer = new PosDataReader();
    private Thread backgroundDataLoader;

    public ResourceManager()
    {
        Debug.logInfo("********************************************",module);
        Debug.logInfo("       Initializing POS Resources", module);
        Debug.logInfo("********************************************",module);
        //this.session=session;
        // Instantiate the entity model
        xmlConverter = new XStream();
        xmlConverter.alias("PosConfig", PosConfig.class);
        xmlConverter.alias("TcpLinkConfig", TcpLinkConfig.class);
        xmlConverter.alias("PosSettlement", PosSettlement.class);
        xmlConverter.alias("PosSettlementDeclare", PosSettlementDeclare.class);
        xmlConverter.alias("decimal",BigDecimal.class);
        xmlConverter.registerConverter(new DateTimeXmlConverter());
        Resource = this;
    }

    private void loadNode()
    {
        try
        {
            //NodeApi api;
            NodeApi customerScreen=null;
            String syncNodeId = config.StoreId+"T"+Integer.toString(config.TerminalNumber);

            if (config.CustomerScreenConfig!=null) {
                customerScreen = new NodeApi(config.CustomerScreenConfig.Address, config.CustomerScreenConfig.Port
                        , syncNodeId, Version);
            }
            if (customerScreen!=null) {
                customerScreen.addXmlAlias("CashierIdentity", CashierIdentity.class);
                customerScreen.addXmlAlias("CustomerIdentity", CustomerIdentity.class);
                customerScreen.addXmlAlias("A", PosAct.class);
                customerScreen.addXmlAlias("PosCashierSessionEvent", PosCashierSessionEvent.class);
                customerScreen.addXmlAlias("PosCashTransfer", PosCashTransfer.class);
                customerScreen.addXmlAlias("PosDiscountDetails", PosDiscountDetails.class);
                customerScreen.addXmlAlias("PosFeatureDetails", PosFeatureDetails.class);
                customerScreen.addXmlAlias("PosIdentity", PosIdentity.class);
                customerScreen.addXmlAlias("PosPaymentDetails", PosPaymentDetails.class);
                customerScreen.addXmlAlias("PosPromoDetails", PosPromoDetails.class);
                customerScreen.addXmlAlias("PosReceiveOfAccount", PosReceiveOfAccount.class);
                customerScreen.addXmlAlias("PosSales", PosSales.class);
                customerScreen.addXmlAlias("PosSalesItem", PosSalesItem.class);
                customerScreen.addXmlAlias("PosSettlement", PosSettlement.class);
                customerScreen.addXmlAlias("PosSettlementDeclare", PosSettlementDeclare.class);
                customerScreen.addXmlAlias("PosVoid", PosVoid.class);
                customerScreen.setDebug(true);
            }

            node = new NodeClient(this,customerScreen);
            Debug.logInfo("Pos is now connected to the sync node!", module);
        }
        catch (Exception ex)
        {
            Debug.logError(ex,"ERROR: Failed to start the second Screen api, application must exit!"
                    ,module);
            System.exit(1);
        }
    }

    public void loadAll() throws Exception
    {
        //createInitialStates();
        if (!Utility.fileExists(PosConfigFileName)) throw new Exception("Unable to find POS Config File!");
        String xmlString = Utility.Read_File(PosConfigFileName);
        PosConfig cfg = (PosConfig)xmlConverter.fromXML(xmlString);
        cfg.validate();
        config = cfg;
        loadNode();
        loadDataModels();



        DeviceLoader.load(config);
        settings = new SettingsResource(config, coreData, securityData);
        customers = new CustomerResource(customerData);
        conversions = new ConversionResource(coreData, securityData.getStore().RoundingMode);
        promos = new PromoResource(coreData);
        taxes = new TaxResource(coreData,config.StoreId);
        products = new ProductResource(coreData, conversions);
        StoreElement store = settings.getStore();
        security = new SecurityResource(config,securityData
                , store.UserLockoutThreshold,store.UserLockoutDurationMs);

        backgroundDataLoader = new Thread(this);
        backgroundDataLoader.setName("LOADER");
        backgroundDataLoader.setDaemon(true);
        backgroundDataLoader.start();
    }

    public void manifestDataModels() throws Exception{
        if (loadingData) return;
        synchronized (this) {
            settings.updateDataModel(coreData, securityData);
            customers.updateDataModel(customerData);
            conversions.updateDataModel(coreData,securityData.getStore().RoundingMode);
            promos.updateDataModel(coreData);
            taxes.updateDataModel(coreData,config.StoreId);
            products.updateDataModel(coreData, conversions);
            security.updateDataModel(securityData);
        }
    }


    public void loadDataModels() throws Exception {
        synchronized (this)
        {
            if (loadingData) return;
            try {
                Debug.log("===============================");
                Debug.log(" Loading Pos Core Data Models");
                Debug.log("===============================");
                loadingData = true;
                Debug.log(">>>>>>>>>> Loading Security Data Model...");
                PosSecurityData sEntity = (PosSecurityData) load(SecurityFilePrefix,
                        PosSecurityData.class, true);
                if (sEntity==null) throw  new Exception("Unable to load the security data model!");
                securityData = sEntity;

                Debug.log(">>>>>>>>>> Loading Customer Data Model...");
                PosCustomerData cEntity = (PosCustomerData) load(CustomerFilePrefix,
                        PosCustomerData.class, true);
                if (cEntity==null) throw  new Exception("Unable to load the customer data model!");
                customerData = cEntity;

                Debug.log(">>>>>>>>>> Loading Pos Core Data Model...");
                PosCoreData pEntity = (PosCoreData) load(DataFilePrefix, PosCoreData.class, true);
                if (pEntity==null) throw  new Exception("Unable to load the core data model!");
                coreData = pEntity;
            }
            catch (Exception ex)
            {
                Debug.log(ex);
                Debug.logError("Unable to load one or more Data Models!", module);
                throw ex;
            }
            finally {
                loadingData=false;
            }
        }
    }


    private void createInitialStates() throws Exception
    {
        if (!Utility.fileExists(PosConfigFileName))
        {
            PosConfig pos = new PosConfig();
            pos.StoreId = "S2";
            pos.TerminalNumber = 1;
            pos.BongoNodeConfig = new TcpLinkConfig("localhost",1978,"./runtime/logs/BongoSyncLog.txt");
            pos.validate();
            pos.CustomerPatterns.add("(4[09]0000\\\\d{5})");
            pos.PromoCodePatterns.add("00(40000\\\\d{4})");
            pos.PromoCodePatterns.add("^(40000\\\\d{4})");
            pos.PromoCodePatterns.add("^998001");
            pos.InputFilters.add("(\\\\d{3,13})");
            pos.InputFilters.add("(\\\\d{11,12})\\\\d");
            pos.InputFilters.add("[0]{6}(\\\\d{5,6})");
            pos.InputFilters.add("[0]{6}(\\\\d{5,6})\\\\d");
            pos.InputFilters.add("[0]{2}(\\\\d{2})");
            pos.PriceLabelPatterns.add("^(27[256789]\\\\d{3})\\\\d(\\\\d{4})");
            pos.KeyboardDeviceName = "GenericKeyboard";
            pos.ScannerDeviceName = "GenericScanner";
            pos.ScaleDeviceName = "GenericScale";
            pos.InstallDateTime =  new DateTime();
            pos.LastModifiedDateTime =  new DateTime();
            pos.LastActionBeforeSave = "New";
            pos.ModifiedByApplication = ResourceManager.Version;

            String xmlString = xmlConverter.toXML(pos);
            Utility.stringToFile(PosConfigFileName,xmlString);
        }
    }


    private Object load(String filePrefix, Class dataClass, boolean fallback) throws Exception {

        // First Try to load from the hotfolder....
        File targetFilePath = new File(Utility.appendDir(ResourceFolder, filePrefix + ".dat"));
        try {
            File hotpath = new File(config.SnapshotHotFolder);
            File[] candidates = hotpath.listFiles();
            File selectedFile = null;
            long id = -1;
            for (File c : candidates){
                String cname = c.getName().toLowerCase().trim();
                if (!cname.endsWith(".dat")) continue;
                if (cname.startsWith(filePrefix.toLowerCase())) {
                    String[] fileParts = cname.replace(".dat", "").split("_");
                    if (fileParts.length == 2) {
                        long seq = Long.parseLong(fileParts[1]);
                        if (seq > id) {
                            selectedFile = c;
                            id = seq;
                        }
                    }
                }

            }
            if (selectedFile == null)
                throw new Exception("No datafile starting with '" + filePrefix + "' found in the hotfolder: "
                        + config.SnapshotHotFolder);
            BinaryBuffer dataStream = new BinaryBuffer(selectedFile.getAbsolutePath());
            Object entity = deserializer.Deserialize(dataStream, dataClass);
            if (dataClass == PosSecurityData.class)
                ((PosSecurityData) entity).validate(config.StoreId, config.getTerminalId());
            else if (dataClass == PosCustomerData.class)
                ((PosCustomerData) entity).validate();
            else if (dataClass == PosCoreData.class)
                ((PosCoreData) entity).validate(config.StoreId);
            Files.copy(selectedFile.toPath(), targetFilePath.toPath(), (CopyOption)StandardCopyOption.REPLACE_EXISTING);
            return entity;
        } catch (Exception e) {
            Debug.logError(e, "Unable to load data file from the hot folder.");
            if (!fallback) return null;
        }

        // Now try to load from the data folder
        Debug.logWarning("Trying to load the data file from the POS offline folder.", module);
        try {
            if (!targetFilePath.exists())
                throw new Exception("Data File '" + targetFilePath.getName() + " not found!");
            BinaryBuffer dataStream = new BinaryBuffer(targetFilePath.getAbsolutePath());
            Object entity = deserializer.Deserialize(dataStream, dataClass);
            if (dataClass == PosSecurityData.class)
                ((PosSecurityData) entity).validate(config.StoreId, config.getTerminalId());
            else if (dataClass == PosCustomerData.class)
                ((PosCustomerData) entity).validate();
            else if (dataClass == PosCoreData.class)
                ((PosCoreData) entity).validate(config.StoreId);
            return entity;
        } catch (Exception e) {
            Debug.logError(e, "Unable to load data file from the POS Data Folder.");
        }
        return null;
    }

    private boolean hasNewerSnapshot(String filePrefix, long currentSeq){
        File hotpath = new File(config.SnapshotHotFolder);
        File[] candidates = hotpath.listFiles();
        for (File c : candidates){
            String cname = c.getName().toLowerCase().trim();
            if (!cname.endsWith(".dat")) continue;
            if (cname.startsWith(filePrefix.toLowerCase())) {
                String[] fileParts = cname.replace(".dat", "").split("_");
                if (fileParts.length == 2) {
                    long seq = Long.parseLong(fileParts[1]);
                    if (seq>currentSeq) return true;
                }
            }
        }
        return false;
    }


    public NodeClient getNode() {
        return node;
    }

    public SettingsResource getSettings() {
        return settings;
    }

    public ConversionResource getConversions() {
        return conversions;
    }

    public ProductResource getProducts() {
        return products;
    }

    public PromoResource getPromos() {
        return promos;
    }

    public TaxResource getTaxes() {
        return taxes;
    }

    public SecurityResource getSecurity() {
        return security;
    }

    public CustomerResource getCustomers() {
        return customers;
    }

    public boolean isLoadingData() {
        return loadingData;
    }

    public PosConfig getConfig() {
        return config;
    }

    @Override
    public void run() {
        Debug.log("Starting background data loader!");
        while(true) {
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
            }
            try {
                loadingData = true;
                synchronized (this) {
                    if (hasNewerSnapshot(SecurityFilePrefix, Long.parseLong(securityData.SnapshotId)))
                    {
                        Debug.log(">>>>>>>>>> Loading New Security Data Model...");
                        PosSecurityData sEntity = (PosSecurityData) load(SecurityFilePrefix,
                                PosSecurityData.class, false);
                        if (sEntity!=null) securityData = sEntity;
                    }
                    if (hasNewerSnapshot(CustomerFilePrefix, Long.parseLong(customerData.SnapshotId)))
                    {
                        Debug.log(">>>>>>>>>> Loading New Customer Data Model...");
                        PosCustomerData cEntity = (PosCustomerData) load(CustomerFilePrefix,
                                PosCustomerData.class, false);
                        if (cEntity!=null) customerData = cEntity;
                    }
                    if (hasNewerSnapshot(DataFilePrefix, Long.parseLong(coreData.SnapshotId)))
                    {
                        Debug.log(">>>>>>>>>> Loading New Pos Core Data Model...");
                        PosCoreData pEntity = (PosCoreData) load(DataFilePrefix, PosCoreData.class, true);
                        if (pEntity!=null) coreData = pEntity;
                    }
                }
            } catch (Exception ex) {
                Debug.logError(ex, "Failed Data Query and reload!");
            } finally {
                loadingData = false;
            }

        }
    }
}
