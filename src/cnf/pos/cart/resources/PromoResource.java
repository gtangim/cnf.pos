package cnf.pos.cart.resources;

import javolution.util.FastList;
import javolution.util.FastMap;
import javolution.util.FastSet;
import cnf.pos.util.Debug;
import cnf.pos.cart.resources.elements.*;

import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/18/11
 * Time: 11:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class PromoResource {

    public static final String module = PromoResource.class.getName();

    protected RoundingMode rounding;
    protected HashMap storeParams;
    protected FastMap<String,PromoElement> promoLookup;
    protected FastMap<String,PromoElement> promoCodeLookup;
    protected PosCoreData coreData = null;
    protected FastList<String> productsOnStoreSale;

    public PromoResource(PosCoreData core) throws Exception
    {
        updateDataModel(core);
    }

    public void updateDataModel(PosCoreData core) throws Exception
    {
        productsOnStoreSale = FastList.newInstance();
        for(ProductElement p: core.Products)
            if (p.IsActive && p.HasStorePromoPrice()) productsOnStoreSale.add(p.ProductId);
        boolean updated = (coreData!=null);
        if (core==null) throw new Exception("Pos Core Data cannot be null!");
        if (coreData==null || !coreData.SnapshotId.equals(core.SnapshotId)) {
            coreData = core;
            promoLookup = FastMap.newInstance();
            promoCodeLookup = FastMap.newInstance();
            for (PromoElement p : coreData.Promos) {
                promoLookup.put(p.Id, p);
                for (String pc : p.PromoCodes) promoCodeLookup.put(pc, p);
                for (PromoRuleElement rule : p.Rules){
                    for(PromoConditionElement cond: rule.Conditions)
                        if (cond.IsOnSaleCategory()){
                            for(String pid: productsOnStoreSale)
                                cond.addMember(pid);
                        }
                    for (PromoActionElement act:rule.Actions)
                        if (act.IsOnSaleCategory()) {
                            for(String pid: productsOnStoreSale)
                                act.addMember(pid);
                        }
                }
            }
            if (updated) Debug.log("Promo Resource has been updated!");
        }
    }


    public List<PromoElement> getPromos()
    {
        return coreData.Promos;
    }
    public PromoElement getPromo(String promoId){
        if (promoLookup.containsKey(promoId)) return promoLookup.get(promoId);
        else return null;
    }
    public PromoElement getPromoCodePromo(String promoCode)
    {
        if (promoCodeLookup.containsKey(promoCode)) return promoCodeLookup.get(promoCode);
        else return null;
    }


    public static Map clearPartyParams(Map params)
    {
        if (params==null) params = FastMap.newInstance();
        params.put("PPIP_PARTY_ID",FastSet.newInstance());
        params.put("PPIP_PARTY_CLASS",FastSet.newInstance());
        params.put("PPIP_GRP_MEM",FastSet.newInstance());
        params.put("PPIP_ROLE_TYPE",FastSet.newInstance());
        return params;
    }

    public static Map setPartyParams(Map params, String partyId, String classification
            , Set<String> groups, Set<String> roles)
    {
        if (params==null) params = FastMap.newInstance();
        if (partyId!=null) ((FastSet<String>)params.get("PPIP_PARTY_ID")).add(partyId);
        if (classification!=null) ((FastSet<String>)params.get("PPIP_PARTY_CLASS"))
                .add(classification);
        if (groups!=null) ((FastSet<String>)params.get("PPIP_GRP_MEM")).addAll(groups);
        if (roles!=null) ((FastSet<String>)params.get("PPIP_ROLE_TYPE")).addAll(roles);
        return params;
    }






}
