package cnf.pos.cart.resources;

import apu.jpos.util.StringUtil;
import cnf.pos.cart.resources.elements.FeatureElement;
import javolution.util.FastList;
import javolution.util.FastMap;
import cnf.pos.util.Debug;
import cnf.pos.cart.resources.elements.PosCoreData;
import cnf.pos.cart.resources.elements.ProductElement;
import cnf.pos.cart.resources.elements.ProductIdentificationElement;
import cnf.pos.util.Utility;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 1/26/11
 * Time: 4:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProductResource {

    Map<String, String> flipProducts = null;
    protected Map<String,List<ProductElement>> prodLookup = null;
    protected Map<String,List<ProductIdentificationElement>> prodIdLookup = null;
    protected Map<String,ProductElement> prods = null;
    protected Map<String, FeatureElement> features = null;
    //protected List<String> prodsOnStoreSale = null;
    protected PosCoreData coreData = null;

    public ProductResource(PosCoreData core, ConversionResource conv) throws Exception
    {
        updateDataModel(core, conv);
    }

    public void updateDataModel(PosCoreData core, ConversionResource conv) throws Exception
    {
        boolean updated = (coreData!=null);
        if (core==null) throw new Exception("Pos Core Data cannot be null!");
        if (coreData==null || !coreData.SnapshotId.equals(core.SnapshotId)) {
            coreData = core;
            prodLookup = FastMap.newInstance();
            prodIdLookup = FastMap.newInstance();
            prods = FastMap.newInstance();
            flipProducts = FastMap.newInstance();
            features = FastMap.newInstance();

            for (ProductElement p : coreData.Products)
                if (p.IsActive) {
                    prods.put(p.ProductId, p);
                    registerProductId(new ProductIdentificationElement(null, p.ProductId, p.ProductId, null), p, conv);
                    for (ProductIdentificationElement pi : p.ProductCodes)
                        registerProductId(pi, p, conv);
                }
            for (FeatureElement f: coreData.Features) {
                f.FeatureApplTypeId="OPTIONAL_FEATURE";
                features.put(f.FeatureId, f);

            }
            if (updated) Debug.log("Product Resource has been updated!");
        }
    }

    private void registerProductId(ProductIdentificationElement pi, ProductElement p, ConversionResource conv)
    {
        List<ProductElement> pList = null;
        List<ProductIdentificationElement> piList = null;
        String id = pi.ProductCode;
        if (prodLookup.containsKey(id)) pList = prodLookup.get(id);
        else
        {
            pList = FastList.newInstance();
            prodLookup.put(id,pList);
        }
        if (prodIdLookup.containsKey(id)) piList = prodIdLookup.get(id);
        else
        {
            piList = FastList.newInstance();
            prodIdLookup.put(id,piList);
        }
        pList.add(p);
        piList.add(pi);
        if (id.length()==4){
            String p1 = Utility.padString(p.Price.getActivePrice(BigDecimal.ONE).getPrice().getNetPrice().toString()
                    + "/"+conv.getUom(p.UomId).Abbrev,12,false,' ');
            String p2 = "";
            if (p.IsScalable && p.UomId.startsWith("WT_"))
            {
                double f = conv.convert(BigDecimal.ONE,p.UomId, "WT_lb").doubleValue();
                double pr = p.Price.getActivePrice(BigDecimal.ONE).getPrice().getNetPriceDouble()/f;
                BigDecimal d = new BigDecimal(pr).setScale(2, RoundingMode.HALF_EVEN);
                p2 = Utility.padString(d.toString()+"/lb", 12, false,' ');
            }
            String desc = id + "  " + Utility.padString(p.Name,32,true,' ') + p1 + p2;
            flipProducts.put(id+"_"+pi.ProductId, desc);
        }
    }


    public FeatureElement getOptionalFeature(String featureId)
    {
        if (Utility.isEmpty(featureId)) return null;
        if (features.containsKey(featureId)) return features.get(featureId);
        else return null;
    }
    public ProductElement getProduct(String productId)
    {
        if (Utility.isEmpty(productId)) return null;
        if (prods.containsKey(productId)) return prods.get(productId);
        else return null;
    }
    public ProductElement getProduct(ProductIdentificationElement pi)
    {
        if (pi==null) return null;
        if (prods.containsKey(pi.ProductId)) return prods.get(pi.ProductId);
        else return null;
    }
    public List<ProductElement> getProductListByCode(String productCode)
    {
        if (Utility.isEmpty(productCode)) return null;
        if (prodLookup.containsKey(productCode)) return prodLookup.get(productCode);
        else return null;
    }

    public Map<String, String> getFlipProducts()
    {
        return flipProducts;
    }

    public List<ProductIdentificationElement> getProductIdListByCode(String productCode)
    {
        if (Utility.isEmpty(productCode)) return null;
        if (prodIdLookup.containsKey(productCode)) return prodIdLookup.get(productCode);
        else return null;
    }

    public String getVersion(){
        return coreData.SnapshotId + " [Model: " + coreData.DataModelVersion+"]";
    }
}
