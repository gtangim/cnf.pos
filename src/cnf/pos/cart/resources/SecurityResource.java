package cnf.pos.cart.resources;

import apu.jpos.util.Utility;
import bongo.node.entities.DateTimeXmlConverter;
import cnf.node.NodeClient;
import cnf.node.entities.*;
import cnf.pos.PosCardSettlementInfo;
import com.thoughtworks.xstream.XStream;
import javolution.util.FastList;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import cnf.pos.util.Debug;
import cnf.pos.cart.resources.elements.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by russela on 3/25/2015.
 */
public class SecurityResource {
    public static final String module = ConversionResource.class.getName();

    protected PosSecurityData securityData = null;
    protected UserElement loggedInUser = null;
    protected PosConfig config = null;
    protected XStream xmlConverter = null;
    protected String rollBackState = null;
    protected int lockoutThreshold;
    protected int lockoutDurationMs;
    protected boolean trainingMode = false;
    protected Map<String, UserLoginAttempt> loginAttempts = new HashMap<String, UserLoginAttempt>();
    private BigDecimal ZERO = new BigDecimal("0.00");
    DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss");

    public SecurityResource(PosConfig cfg, PosSecurityData securityData
            , int lockoutThreshold, int lockoutDurationMs) throws Exception
    {
        config = cfg;
        this.lockoutThreshold = lockoutThreshold;
        this.lockoutDurationMs = lockoutDurationMs;
        updateDataModel(securityData);
        xmlConverter = new XStream();
        //xmlConverter.alias("PosState", PosState.class);
        xmlConverter.alias("PosSettlement", PosSettlement.class);
        xmlConverter.alias("PosSettlementDeclare", PosSettlementDeclare.class);
        xmlConverter.alias("UserLoginAttempt", UserLoginAttempt.class);
        xmlConverter.alias("PosCashTransfer", PosCashTransfer.class);
        xmlConverter.alias("PosIdentity", PosIdentity.class);
        xmlConverter.alias("PosReceiveOfAccount", PosReceiveOfAccount.class);
        xmlConverter.alias("CustomerIdentity", CustomerIdentity.class);
        xmlConverter.alias("PosPaymentDetails", PosPaymentDetails.class);
        xmlConverter.alias("decimal", BigDecimal.class);
        xmlConverter.registerConverter(new DateTimeXmlConverter());
    }

    public void updateDataModel(PosSecurityData sec) throws Exception
    {
        boolean updated = (securityData !=null);
        if (sec==null) throw new Exception("Security Data cannot be null!");
        if (securityData ==null || !securityData.SnapshotId.equals(sec.SnapshotId)) {
            securityData = sec;
            if (updated) Debug.log("Security Resource has been updated!");
        }
    }


    public UserElement authenticate(String userId, String password) throws Exception
    {
        if (cnf.pos.util.Utility.isEmpty(userId) || cnf.pos.util.Utility.isEmpty(password))
            throw new Exception("Invalid username/password provided!");
        UserElement u = getUser(userId);
        if (u==null)
        {
            getLogin(userId).markFailed();
        }
        else if (u.matchesCredential(userId, password))
        {
            if (!u.IsActive) throw new Exception("User is not active!");
            if (!u.hasRole("SALES_REP")) throw new Exception("User is not a Pos Sales Clerk!");
            if (getLogin(u.UserId).isLockedOut(lockoutThreshold,lockoutDurationMs)) {
                getLogin(u.UserId).markFailed();
                throw new Exception("User is locked out!");
            }
            boolean newLogin = (loggedInUser==null);
            if (loggedInUser==null) loggedInUser = u;
            getLogin(u.UserId).unlock();
            return u;
        }
        else {
            getLogin(u.UserId).markFailed();
        }
        throw new Exception("Incorrect UserId/Password!");
    }


    public UserElement getUser() {
        return loggedInUser;
    }

    public UserElement getUser(String userId) throws Exception
    {
        List<UserElement> matches = FastList.newInstance();
        for(UserElement u : securityData.Users) if (u.matchesIdentity(userId)) matches.add(u);
        if (matches.size()==1) return matches.get(0);
        else if (matches.size()==0) return null;
        else throw new Exception("User Id is not unique!");
    }

    public UserElement getSystemUser()
    {
        try {
            UserElement sys = getUser("system");
            return sys;
        } catch (Exception e) {
            return null;
        }
    }



    public void logout(){
        loggedInUser = null;
    }

    public void unlockUser(String userId)
    {
        // TODO: Implement this...
        //UserLoginAttempt ul = state.getLogin(userId);
        //ul.unlock();
    }


    public boolean isTrainingMode() {
        return trainingMode;
    }

    public void toggleTrainingMode() throws Exception {
        trainingMode = !trainingMode;
    }

    public String getTrainingMode() {
        if (trainingMode) return "ON";
        else return "OFF";
    }

    public String getVersion(){
        return securityData.SnapshotId + " [Model: " + securityData.DataModelVersion+"]";
    }

    public UserLoginAttempt getLogin(String userId)
    {
        userId = userId.toLowerCase().trim();
        if (loginAttempts.containsKey(userId)) return loginAttempts.get(userId);
        UserLoginAttempt ul = new UserLoginAttempt(userId);
        loginAttempts.put(userId, ul);
        return ul;
    }

    private long nextTxId = Long.parseLong(new DateTime().toString("yyMMdd")) * 10000;
    public long getNextTxId() {
        return nextTxId++;
    }

}
