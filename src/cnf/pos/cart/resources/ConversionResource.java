package cnf.pos.cart.resources;

import javolution.util.FastMap;
import cnf.pos.util.Debug;
import cnf.pos.cart.resources.elements.PosCoreData;
import cnf.pos.cart.resources.elements.UomElement;
import cnf.pos.util.Utility;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 1/25/11
 * Time: 11:10 AM
 * To change this template use File | Settings | File Templates.
 */
public class ConversionResource {

    protected PosCoreData coreData = null;
    protected Map<String,UomElement> uomLookup = null;
    protected RoundingMode roundingMode = null;

    public ConversionResource(PosCoreData core, RoundingMode roundingMode) throws Exception
    {
        updateDataModel(core, roundingMode);
    }

    public void updateDataModel(PosCoreData core, RoundingMode roundingMode) throws Exception
    {
        boolean updated = (coreData!=null);
        if (core==null) throw new Exception("Pos Core Data cannot be null!");
        if (coreData==null || !coreData.SnapshotId.equals(core.SnapshotId)) {
            coreData = core;
            uomLookup = FastMap.newInstance();
            for (UomElement u : coreData.UomConversions.UomList)
                uomLookup.put(u.Id, u);
            this.roundingMode = roundingMode;
            if (updated) Debug.log("Conversion Resource has been updated!");
        }
    }

    public BigDecimal convert(BigDecimal value, String sourceUomId, String targetUomId)
    {
        if (value==null) return null;
        if (Utility.isEmpty(sourceUomId) || Utility.isEmpty(targetUomId)) return null;
        if (sourceUomId.equals(targetUomId)) return value;
        BigDecimal factor = coreData.UomConversions.getConversionFactor(sourceUomId, targetUomId);
        if (factor==null) return null;
        double factorD = factor.doubleValue();
        double res = value.doubleValue() * factorD;
        int scale = value.scale()>factor.scale()?value.scale():factor.scale();
        return new BigDecimal(res).setScale(scale, roundingMode);
    }

    public double convert(double value, String sourceUomId, String targetUomId)
    {
        if (Utility.isEmpty(sourceUomId) || Utility.isEmpty(targetUomId)) return Double.NaN;
        BigDecimal factor = coreData.UomConversions.getConversionFactor(sourceUomId, targetUomId);
        if (factor==null) return Double.NaN;
        double factorD = factor.doubleValue();
        double res = value * factorD;
        return res;
    }

    public BigDecimal getConversionFactor(String from, String to)
    {
        return coreData.UomConversions.getConversionFactor(from,to);
    }


    public UomElement getUom(String uomId)
    {
        if (Utility.isEmpty(uomId)) return null;
        if (uomLookup.containsKey(uomId)) return uomLookup.get(uomId);
        else return null;
    }
}
