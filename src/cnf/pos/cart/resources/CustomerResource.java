package cnf.pos.cart.resources;

import apu.jpos.util.StringUtil;
import javolution.util.FastMap;
import cnf.pos.util.Debug;
import cnf.pos.cart.resources.elements.BillingAccountElement;
import cnf.pos.cart.resources.elements.CustomerElement;
import cnf.pos.cart.resources.elements.PosCustomerData;

import java.util.Map;

/**
 * Created by russela on 3/31/2015.
 */
public class CustomerResource {
    protected PosCustomerData customerData = null;
    protected Map<String,CustomerElement> customerLookup = null;
    protected Map<String,CustomerElement> customerPhoneLookup = null;
    protected Map<String,BillingAccountElement> billingAccounts = null;

    public CustomerResource(PosCustomerData custData) throws Exception
    {
        updateDataModel(custData);
    }

    public void updateDataModel(PosCustomerData cust) throws Exception
    {
        if (cust==null) throw new Exception("Customer Data cannot be null!");
        boolean updated = (customerData !=null);
        if (customerData ==null || !customerData.SnapshotId.equals(cust.SnapshotId)) {
            customerData = cust;
            customerLookup = FastMap.newInstance();
            customerPhoneLookup = FastMap.newInstance();
            billingAccounts = FastMap.newInstance();
            for (CustomerElement c : customerData.Customers)
                if (c.IsActive) {
                    customerLookup.put(c.CustomerId, c);
                    if (!StringUtil.empty(c.CustomerPhoneNumber)) customerPhoneLookup.put(c.CustomerPhoneNumber, c);
                    for (BillingAccountElement ba : c.BillingAccounts) billingAccounts.put(ba.BillingAccountId, ba);
                }
            if (updated) Debug.log("Customer Resource has been updated!");
        }
    }

    public CustomerElement getCustomer(String custId)
    {
        if (customerLookup.containsKey(custId)) return customerLookup.get(custId);
        else return null;
    }
    public CustomerElement getCustomerByPhone(String custPhone)
    {
        if (customerPhoneLookup.containsKey(custPhone)) return customerPhoneLookup.get(custPhone);
        else return null;
    }

    public BillingAccountElement getBillingAccount(String billingAccountId){
        if (billingAccounts.containsKey(billingAccountId)) return billingAccounts.get(billingAccountId);
        else return null;
    }

    public String getVersion(){
        return customerData.SnapshotId + " [Model: " + customerData.DataModelVersion+"]";
    }

}
