package cnf.pos.cart.resources.elements;

import cnf.pos.util.Utility;

import java.math.BigDecimal;

/**
 * Created by RusselA on 3/26/2015.
 */
public class PosTerminalElement {
    public String PosTerminalId;
    public String TerminalName;
    public String FacilityId;

    public String TareUomId;
    public Boolean EnableAlwaysPopDrawer;
    public Boolean EnableOnScreenKeyboard;
    public Boolean AllowPosCashBack;
    //public Boolean EnablePrintPaymentSlips;
    public Boolean AlwaysPrintCustomerPaymentSlips;
    public Boolean AlwaysPrintMerchantPaymentSlips;
    public int AutoLockTerminalMs;
    public BigDecimal QuantityLimitCount;
    public BigDecimal QuantityLimitWeight;
    public BigDecimal PaymentLimit;
    public Boolean EnablePrintReceiptBeforeCommit;
    public Boolean EnablePromptForReceipt;
    public Boolean IsActive;

    public void validate() throws Exception {
        if (Utility.isEmpty(PosTerminalId)) fail("Missing PosTerminal ID!");
        if (Utility.isEmpty(FacilityId)) fail("Missing Store ID!");
        if (Utility.isEmpty(TerminalName)) fail("Missing Terminal Name!");
        if (Utility.isEmpty(TareUomId)) fail("Missing Tare Uom ID!");
        if (PaymentLimit == null) fail("Missing Payment Limit!");
        if (!PosTerminalId.startsWith(FacilityId + "-")) fail("Invalid Pos Terminal ID!");
    }



    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Pos Terminal Data Element! " + reason + "  >>  " + this);
    }

    @Override
    public String toString() {
        return "PosTerminalElement{" +
                "PosTerminalId='" + PosTerminalId + '\'' +
                ", TerminalName='" + TerminalName + '\'' +
                ", FacilityId='" + FacilityId + '\'' +
                ", IsActive=" + IsActive +
                '}';
    }
}
