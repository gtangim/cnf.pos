package cnf.pos.cart.resources.elements;

/**
 * Created by russela on 3/30/2015.
 */
public enum PromoReservationOrder {
    RESV_ORDER_CART, RESV_ORDER_MINMAX
}
