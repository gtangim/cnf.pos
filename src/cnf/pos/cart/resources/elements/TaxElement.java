package cnf.pos.cart.resources.elements;

import cnf.pos.util.Utility;

import java.math.BigDecimal;

/**
 * Created by RusselA on 3/29/2015.
 */
public class TaxElement {
    public String TaxId;
    public String ProductStoreId;
    public String TaxCategoryId;
    public String Description;
    public BigDecimal TaxPercent;
    public Boolean IsActive;

    public void validate() throws Exception
    {
        if (Utility.isEmpty(TaxId)) fail("Missing Tax ID!");
        if (Utility.isEmpty(ProductStoreId)) fail("Missing Store ID!");
        if (Utility.isEmpty(TaxCategoryId)) fail("Missing Tax Category ID!");
        if (Utility.isEmpty(Description)) fail("Missing Tax Description!");
        if (TaxPercent==null || TaxPercent.compareTo(BigDecimal.ZERO)==-1
                || TaxPercent.compareTo(new BigDecimal("100"))==1)
            fail("Missing/Invalid Tax Percentage!");
    }


    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Tax Element! " + reason + " >> " + this);
    }

    @Override
    public String toString() {
        return "TaxElement{" +
                "TaxId='" + TaxId + '\'' +
                ", ProductStoreId='" + ProductStoreId + '\'' +
                ", TaxCategoryId='" + TaxCategoryId + '\'' +
                ", Description='" + Description + '\'' +
                ", TaxPercent=" + TaxPercent +
                ", IsActive=" + IsActive +
                '}';
    }
}
