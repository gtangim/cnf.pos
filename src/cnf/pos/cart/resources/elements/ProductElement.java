package cnf.pos.cart.resources.elements;


import javolution.util.FastList;
import javolution.util.FastMap;
import cnf.pos.util.Utility;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 1/19/11
 * Time: 3:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProductElement {
    public String ProductId;
    public String ProductTypeId;
    public String Name;
    public String JournalName;
    public String BrandName;
    public String DepartmentId;
    public String DepartmentName;
    public String SubDepartmentId;
    public String SubDepartmentName;
    public String CategoryId;
    public String CategoryName;
    public String UomId;
    public List<PriceElement> StorePrices;
    public List<FeatureElement> ProductFeatures;
    public List<ProductIdentificationElement> ProductCodes;

    public Boolean IsScalable;
    public Boolean IsReturnable;
    public Boolean IsTaxable;
    public Boolean IsPromoEnabled;
    public Boolean IsPromoAllowed;
    public Boolean IsDiscountAllowed;
    public Boolean IsVirtual;
    public Boolean IsVariant;
    public Boolean IsActive;

    public PriceElement Price = null;
    protected PriceElement genericPrice = null;
    protected Map<String,List<FeatureElement>> featureLookup = null;
    protected boolean hasStorePromoPrice = false;

    public void validate(String storeId) throws Exception
    {
        if (Utility.isEmpty(ProductId)) fail("Missing Product ID!");
        if (Utility.isEmpty(Name) || Utility.isEmpty(JournalName)) fail("Missing Product Name/Description!");
        if (Utility.isEmpty(DepartmentId) || Utility.isEmpty(DepartmentName))
            fail("Missing Product Department!");
        if (Utility.isEmpty(SubDepartmentId) || Utility.isEmpty(SubDepartmentName))
            fail("Missing Product Sub-Department!");
        if (Utility.isEmpty(CategoryId) || Utility.isEmpty(CategoryName))
            fail("Missing Product Category!");
        if (Utility.isEmpty(UomId)) fail("Product Uom ID is missing!");
        if (IsScalable && !UomId.startsWith("WT_")) fail("Scalable product must start with WT_* Uom ID!");

        if (StorePrices==null) StorePrices = FastList.newInstance();
        if (ProductFeatures==null) ProductFeatures = FastList.newInstance();
        if (ProductCodes==null) ProductCodes = FastList.newInstance();
        featureLookup = FastMap.newInstance();
        List<FeatureElement> fList = FastList.newInstance();
        featureLookup.put("STANDARD_FEATURE",fList);
        fList = FastList.newInstance();
        featureLookup.put("REQUIRED_FEATURE",fList);
        fList = FastList.newInstance();
        featureLookup.put("OPTIONAL_FEATURE",fList);
        fList = FastList.newInstance();
        featureLookup.put("CHARGE_FEATURE",fList);
        fList = FastList.newInstance();
        featureLookup.put("SELECTABLE_FEATURE",fList);
        fList = FastList.newInstance();
        featureLookup.put("DISTINGUISHING_FEAT",fList);
        try
        {
            for(PriceElement pr:StorePrices)
            {
                pr.validate();
                if (pr.StoreId.equals(storeId)) Price=pr;
                else if (pr.StoreId.equals("_NA_")) genericPrice = pr;
            }
            if (Price==null || genericPrice==null)
                fail("Product Price for "+ ProductId + " is not setup correctly!");
            if (Price.getActivePrice(BigDecimal.ONE).getPrice().getNetPrice().compareTo(
                    genericPrice.getActivePrice(BigDecimal.ONE).getPrice().getNetPrice())!=0)
                hasStorePromoPrice=true;
            for(FeatureElement f:ProductFeatures)
            {
                f.validate();
                fList = featureLookup.get(f.FeatureApplTypeId);
                if (fList==null) fail("Invalid Feature Application Type in product Feature!  >>  " + f);
                fList.add(f);
            }
            for(ProductIdentificationElement pi:ProductCodes) {pi.ProductId=ProductId; pi.validate();}
        }
        catch (Exception ex)
        {
            fail(ex.getMessage());
        }
        if (Price==null) fail("Product has no price entry for selected store " + storeId + "!");
    }
    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Product Element! " + reason + " >> " + this);
    }

    public List<FeatureElement> getFeatures(String featureTypeId)
    {
        if (featureLookup.containsKey(featureTypeId))  return featureLookup.get(featureTypeId);
        else return null;
    }
    public boolean HasStorePromoPrice(){
        return hasStorePromoPrice;
    }

    @Override
    public String toString() {
        return "ProductElement{" +
                "ProductId='" + ProductId + '\'' +
                ", ProductTypeId='" + ProductTypeId + '\'' +
                ", Name='" + Name + '\'' +
                ", BrandName='" + BrandName + '\'' +
                ", DepartmentId='" + DepartmentId + '\'' +
                ", SubDepartmentId='" + SubDepartmentId + '\'' +
                ", CategoryId='" + CategoryId + '\'' +
                ", UomId='" + UomId + '\'' +
                ", IsActive=" + IsActive +
                '}';
    }
}
