package cnf.pos.cart.resources.elements;

/**
 * Created by RusselA on 3/29/2015.
 */
public enum SCLPrimaryType {
    CL_DISCOUNT, CL_FEATURE, CL_PROMO, CL_TAX

}
