package cnf.pos.cart.resources.elements;


import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/15/11
 * Time: 5:03 PM
 * To change this template use File | Settings | File Templates.
 */
public final class PriceConditionElement {
    public static final String EQ = "0";
    public static final String NEQ = "1";
    public static final String GT = "2";
    public static final String LT = "3";
    public static final String GTE = "4";
    public static final String LTE = "5";


    public String Field;
    public String Op;
    public String Value;
    protected BigDecimal valD;
    protected boolean isSet;

    public PriceConditionElement(){

    }

    /*public PriceConditionElement(ProductPriceCond pc)
    {
        Field = pc.getInputParamEnumId();
        Description =null;
        valD=null;
        isSet = false;
        isSet = false;
        Op =EQ; //default...
        String c = pc.getOperatorEnumId();
        if ("PRC_NEQ".equals(c)) Op =NEQ;
        else if ("PRC_GT".equals(c)) Op =GT;
        else if ("PRC_LT".equals(c)) Op =LT;
        else if ("PRC_GTE".equals(c)) Op =GTE;
        else if ("PRC_LTE".equals(c)) Op =LTE;

        if ("PRIP_LIST_PRICE".equals(Field) || "PRIP_QUANTITY".equals(Field))
            try
            {
                valD=new BigDecimal(pc.getCondValue());
            }
            catch (Exception ex)
            {
                valD=BigDecimal.ZERO;
            }
        else Description =pc.getCondValue();
        if ("PRIP_PARTY_ClASS".equals(Field) || "PRIP_PARTY_GRP_MEM".equals(Field)
                || "PRIP_PARTY_ID".equals(Field) || "PRIP_PROD_CAT_ID".equals(Field)
                || "PRIP_PROD_FEAT_ID".equals(Field) || "PRIP_ROLE_TYPE".equals(Field))isSet=true;
    }*/


    public boolean eval(Map params)
    {
        try
        {
            if (!params.containsKey(Field)) return false;
            int res=0;
            if (isSet)
            {
                // For sets, we ignore the operator...
                Set<String> vals = (Set<String>)params.get(Field);
                if (vals.contains(Value)) return true;
            }
            else if (Value !=null)
                // Text Argument...
                res = Value.compareToIgnoreCase((String)params.get(Field));
            else
                // Number argument...
                res=valD.compareTo((BigDecimal)params.get(Field));

            // Comparison above is in the reverse order, so GT=>LT
            if (Op ==EQ && res==0) return true;
            else if (Op ==NEQ && res!=0) return true;
            else if (Op ==GT && res<0) return true;
            else if (Op ==GTE && res<=0) return true;
            else if (Op ==LT && res>0) return true;
            else if (Op ==LTE && res>=0) return true;
            return false;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public boolean isStaticCond()
    {
        if ("PRIP_CURRENCY_UOMID".equals(Field)) return true;
        if ("PRIP_PROD_CLG_ID".equals(Field)) return true;
        if ("PRIP_PROD_SGRP_ID".equals(Field)) return true;
        if ("PRIP_WEBSITE_ID".equals(Field)) return true;
        return false;
    }
}
