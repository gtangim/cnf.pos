package cnf.pos.cart.resources.elements;

import javolution.util.FastList;
import cnf.pos.util.Utility;

import java.math.RoundingMode;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/21/11
 * Time: 10:04 AM
 * To change this template use File | Settings | File Templates.
 */
public class PromoElement {

    /*
    public static final int CUMULATIVE = 0;
    public static final int EXCLUSIVE = 1;
    protected static int nextIndex = 0;

    protected String Id;
    protected String Name;
    protected String promoType;

    protected boolean isManual;
    protected boolean isCodeRequired;
    protected int useLimitOrder;
    protected int useLimitCustomer;
    protected int useLimitPromotion;
    protected int Index;
    protected int globalConstraint;
    protected int typeConstraint;


    protected FastList<PromoRuleElement> rules;
    protected FastMap<String,PromoRuleElement> ruleLookup;  */


    public String Id;
    public String Name;
    public PromoType PromoType;
    public String ReportingCode;
    public Boolean IsManual;
    public Boolean IsCodeRequired;
    public int UseLimitOrder;
    public int UseLimitCustomer;
    public int UseLimitPromotion;
    public PromoConstraint GlobalConstraint;
    public PromoConstraint TypeConstraint;
    public int Seq;
    public Boolean IsActive;

    public List<PromoRuleElement> Rules;
    public List<String> PromoCodes;
    public List<String> AppliedToStores;


    public void validate() throws Exception
    {
        if (Utility.isEmpty(Id)) fail("Missing Promo ID!");
        if (Utility.isEmpty(Name)) fail("Missing Promo Name/Description!");
        if (PromoType==null) fail("Missing Promo Type!");
        if (GlobalConstraint==null) fail("Missing Global Constraint!");
        if (TypeConstraint==null) fail("Missing Type Constraint!");
        if (Rules==null) {Rules = FastList.newInstance(); IsActive=false;}
        if (PromoCodes==null) PromoCodes = FastList.newInstance();
        if (AppliedToStores==null) {AppliedToStores=FastList.newInstance(); IsActive=false;}

        try
        {
            for(PromoRuleElement pr:Rules) pr.validate();
        }
        catch (Exception ex)
        {
            fail(ex.getMessage());
        }
        if (UseLimitOrder<=0) UseLimitOrder=9999999;
        if (UseLimitCustomer<=0) UseLimitCustomer=9999999;
        if (UseLimitPromotion<=0) UseLimitPromotion=9999999;
    }
    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Promo Element! " + reason + " >> " + this);
    }


    public PromoRuleElement selectRule(Map params,String layer,int scale,RoundingMode rMode) {
        for(PromoRuleElement rule:Rules)
            if (rule.isConditionSatisfied(params,layer,scale,rMode))
                return rule;
        return null;
    }


    public boolean isExclusiveTo(PromoElement promo)
    {
        if (promo==null) return false;
        if (this.PromoType==promo.PromoType)
        {
            // Decision based on type Constraint
            return this.TypeConstraint==PromoConstraint.EXCLUSIVE
                    || promo.TypeConstraint==PromoConstraint.EXCLUSIVE;
        }
        else
        {
            // Decision based on global constraint
            return this.GlobalConstraint==PromoConstraint.EXCLUSIVE
                    || promo.GlobalConstraint==PromoConstraint.EXCLUSIVE;
        }
    }



    public boolean isEmptyPromo()
    {
        return Rules.size()==0;
    }



}
