package cnf.pos.cart.resources.elements;


import cnf.pos.util.Utility;

/**
 * Created by RusselA on 3/27/2015.
 */
public class TcpLinkConfig {
    public String Address;
    public int Port;
    public String LogFile;

    public TcpLinkConfig(){
    }
    public TcpLinkConfig(String address, int port, String logFile)
    {
        Address = address;
        Port = port;
        LogFile = logFile;
    }

    public void validate() throws Exception
    {
        if (Utility.isEmpty(Address)) fail("Tcp Address cannot be empty!");
        if (Port<=0 || Port>100000) fail("Invalid Tcp Port!");
        if (Utility.isEmpty(LogFile)) fail("Log File Location is not specified!");
    }

    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Tcp Api Configuration! " + reason);
    }

}
