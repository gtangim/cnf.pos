package cnf.pos.cart.resources.elements;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;

public class PosState {
    public int DeviceId;                    // 1001
    public String Alias;                    // S3T1
    public int LastSequenceNumber;          // 0010
    public DateTime CurrentOperationDay;    // 2017-11-1
}
