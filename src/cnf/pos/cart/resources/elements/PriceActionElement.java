package cnf.pos.cart.resources.elements;

import cnf.pos.cart.resources.datastructures.AtomicPrice;
import cnf.pos.cart.resources.datastructures.PriceResult;

import java.math.BigDecimal;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/15/11
 * Time: 7:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class PriceActionElement {
    public static final String FLAT = "0";
    public static final String FOL = "1";
    public static final String PFLAT = "2";
    public static final String POAC = "3";
    public static final String POD = "4";
    public static final String POL = "5";
    public static final String POM = "6";
    public static final String WFLAT = "7";
    public static final String NA = "8";


    public String Act;
    public BigDecimal Val;

    public PriceActionElement() {
    }

    /*public PriceActionElement(String action, BigDecimal amount)
    {
        init(action,amount);
    }

    public PriceActionElement(ProductPriceAction pAction)
    {
        init(pAction.getProductPriceActionTypeId(),pAction.getAmount());
    } */

    protected void init(String action, BigDecimal amount)
    {
        Act = NA;
        Val = BigDecimal.ZERO;
        if (action==null || amount==null) return;
        Val =amount;
        if (action.equals("PRICE_FLAT")) Act =FLAT;
        else if (action.equals("PRICE_FOL")) Act =FOL;
        else if (action.equals("PRICE_PFLAT")) Act =PFLAT;
        else if (action.equals("PRICE_POAC")) Act =POAC;
        else if (action.equals("PRICE_POD")) Act =POD;
        else if (action.equals("PRICE_POL")) Act =POL;
        else if (action.equals("PRICE_POM")) Act =POM;
        else if (action.equals("PRICE_WFLAT")) Act =WFLAT;
    }

    public double eval(double currentPrice, PriceElement priceElement, BigDecimal qty, boolean useGrossPrice)
    {
        if (Act ==FLAT) return Val.doubleValue();
        else if (Act ==PFLAT)
        {
            AtomicPrice ap = priceElement.SpecialPromoPrice;
            if (ap==null)
            {
                PriceResult r=priceElement.getActivePrice(qty);
                if (r.isValid()) ap = r.getPrice();
            }
            //BigDecimal p = useGrossPrice?ap.getGrossPrice():ap.getNetPrice();
            return ap.getPrice(useGrossPrice).doubleValue()+ Val.doubleValue();
        }
        else if (Act ==WFLAT)
        {
            AtomicPrice ap = priceElement.WholesalePrice;
            if (ap==null) ap = priceElement.ListPrice;
            //BigDecimal p = useGrossPrice?ap.getGrossPrice():ap.getNetPrice();
            return ap.getPrice(useGrossPrice).doubleValue()+ Val.doubleValue();
        }
        else if (Act ==FOL)
        {
            return currentPrice+ Val.doubleValue();
        }
        else
        {
            AtomicPrice ap = null;
            if (Act ==POD) ap = priceElement.DefaultPrice;
            else if (Act ==POL) ap = priceElement.ListPrice;
            else if (Act ==POAC) ap = new AtomicPrice(priceElement.Cost,priceElement.Cost);
            else if (Act ==POM) ap = ap = new AtomicPrice(priceElement.Cost,priceElement.Cost);
            if (ap==null) ap = priceElement.ListPrice;
            double p = ap.getPrice(useGrossPrice).doubleValue();
            if (Act ==POM) p = priceElement.ListPrice.getPrice(useGrossPrice).doubleValue() - p;
            //BigDecimal mod = p.multiply(Val.movePointLeft(2));
            return currentPrice + (p * Val.doubleValue()/100.0);
        }
    }

    public boolean stopLoop()
    {
        return (Act ==PFLAT || Act ==WFLAT);
    }


}
