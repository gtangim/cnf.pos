package cnf.pos.cart.resources.elements;

/**
 * Created by russela on 3/30/2015.
 */
public enum PromoReservationType {
    RESV_TYPE_VALUE, RESV_TYPE_QUANT, RESV_TYPE_UNIQUE
}
