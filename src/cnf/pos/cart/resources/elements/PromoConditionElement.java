package cnf.pos.cart.resources.elements;

import cnf.pos.cart.resources.PromoEngine;
import javolution.util.FastSet;
import cnf.pos.cart.resources.datastructures.ValueTreeNode;
import cnf.pos.util.Utility;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/15/11
 * Time: 5:03 PM
 * To change this template use File | Settings | File Templates.
 */
public final class PromoConditionElement {

    public int Seq;
    public PromoConditionField Field;
    public PromoComparator Op;
    public String Value;
    public List<String> MemberProducts;


    protected String xS;
    protected BigDecimal xD;
    protected BigDecimal y;
    protected boolean isSet;
    protected boolean useValueTree;
    protected boolean isCartSubTotal;
    protected boolean isProdAmount;
    protected boolean isProdQuant;
    protected boolean isProdTotal;
    protected String fieldStr;
    protected double accumulatorQ;
    protected double accumulatorT;
    protected Set<String> members = null;
    protected boolean isOnSaleCategory = false;

    public void validate() throws Exception
    {
        if (Field==null) fail("Missing Field Type!");
        if (Op==null) fail("Missing Operator!");
        if (Utility.isEmpty(Value)) fail("Condition value must not be empty!");
        init();
    }


    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Promo Condition Element! " + reason + " >> " + this);
    }

    private void init()
    {
        xS=null;
        xD=null;
        y=null;
        isSet = false;
        fieldStr = Field.toString();
        fieldStr = Field.toString();

        if (Field == PromoConditionField.PPIP_ORST_HIST)
        {
            try
            {
                y=new BigDecimal(Value);
            }
            catch (Exception ex)
            {
                y=BigDecimal.ZERO;
            }
        }

        useValueTree=false;
        isCartSubTotal=false;
        isProdAmount=false;
        isProdQuant=false;
        isProdTotal=false;


        if (Field == PromoConditionField.PPIP_ORDER_TOTAL)
        {
            useValueTree=true;
            isCartSubTotal=true;
        }
        else if (Field == PromoConditionField.PPIP_PRODUCT_AMOUNT)
        {
            useValueTree=true;
            isProdAmount=true;
        }
        else if (Field == PromoConditionField.PPIP_PRODUCT_QUANT)
        {
            useValueTree=true;
            isProdQuant=true;
        }
        else if (Field == PromoConditionField.PPIP_PRODUCT_TOTAL)
        {
            useValueTree=true;
            isProdTotal=true;
        }


        if (Field == PromoConditionField.PPIP_NEW_ACCT || Field == PromoConditionField.PPIP_ORDER_TOTAL
                || Field == PromoConditionField.PPIP_ORST_HIST || Field == PromoConditionField.PPIP_ORST_LAST_YEAR
                || Field == PromoConditionField.PPIP_ORST_YEAR || Field == PromoConditionField.PPIP_PRODUCT_AMOUNT
                || Field == PromoConditionField.PPIP_PRODUCT_QUANT || Field == PromoConditionField.PPIP_PRODUCT_TOTAL)
            try
            {
                xD=new BigDecimal(Value);
            }
            catch (Exception ex)
            {
                xD=BigDecimal.ZERO;
            }
        else
        {
            xS=Value;
            isSet=true;
        }
        members = FastSet.newInstance();
        members.addAll(MemberProducts);
        if (members.contains("ONSALE")) isOnSaleCategory = true;
    }

    public boolean IsOnSaleCategory(){
        return isOnSaleCategory;
    }

    public void addMember(String pid){
        if (members.contains(pid)) return;
        members.add(pid);
        MemberProducts.add(pid);
    }

    public void resetAccumulators()
    {
        accumulatorQ=0;
        accumulatorT=0;
    }
    public void accumulate(String productId, double quantity, double subtotal)
    {
        if (members.size()==0 || members.contains(productId))
        {
            accumulatorT+=subtotal;
            accumulatorQ+=quantity;
        }
    }
    public boolean requireSubValuesForCond()
    {
        return members.size()==0;
    }

    public boolean eval(Map params,String layer, int scale, RoundingMode rMode)
    {
        try
        {
            int res=0;
            if (useValueTree)
            {
                ValueTreeNode values = null;
                values = (ValueTreeNode)params.get("VALUES");
                if (values==null) return false;
                double value=0;
                if (!requireSubValuesForCond())
                {
                    accumulatorQ = values.getValue(PromoEngine.PROMO_SUM,PromoEngine.PROMO_QUANTITY);
                    accumulatorT = values.getValue(PromoEngine.PROMO_SUM,PromoEngine.PROMO_AMOUNT);
                }
                if (isCartSubTotal) value = values.getValue(layer);
                else if (isProdAmount) value = accumulatorT;
                else if (isProdTotal) value = accumulatorT;
                else if (isProdQuant) value = accumulatorQ;
                if (Double.isNaN(value)) value=0;
                BigDecimal valueB=new BigDecimal(value).setScale(scale,rMode);
                res = xD.compareTo(valueB);
            }
            else
            {
                if (!params.containsKey(fieldStr)) return false;
                if (isSet)
                {
                    // For sets, we ignore the operator...
                    Set<String> vals = (Set<String>)params.get(fieldStr);
                    if (vals.contains(xS)) return true;
                    else return false;
                }
                else if (xS!=null)
                    // Text Argument...
                    res = xS.compareToIgnoreCase((String)params.get(fieldStr));
                else
                    // Number argument...
                    res=xD.compareTo((BigDecimal)params.get(fieldStr));
            }
            // Comparison above is in the reverse order, so GT=>LT
            if (Op==PromoComparator.EQ && res==0) return true;
            else if (Op==PromoComparator.NEQ && res!=0) return true;
            else if (Op==PromoComparator.GT && res<0) return true;
            else if (Op==PromoComparator.GTE && res<=0) return true;
            else if (Op==PromoComparator.LT && res>0) return true;
            else if (Op==PromoComparator.LTE && res>=0) return true;
            return false;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public boolean isStaticCond()
    {
        return false;
    }

    @Override
    public String toString() {
        return "PromoConditionElement{" +
                "Seq=" + Seq +
                ", Field=" + Field +
                ", Op=" + Op +
                ", Description='" + Value + '\'' +
                '}';
    }
}
