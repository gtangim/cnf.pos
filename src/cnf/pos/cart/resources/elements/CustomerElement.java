package cnf.pos.cart.resources.elements;

import cnf.pos.util.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RusselA on 3/28/2015.
 */
public class CustomerElement {
    public String PartyId;
    public String LocalUserId;
    public String Description;
    public String FirstName;
    public String LastName;
    public String CustomerId;
    public String CustomerPhoneNumber;
    public String Email;
    public String AddressLine1;
    public String AddressLine2;
    public String City;
    public String Provice;
    public String PostalCode;
    public String ExternalId;
    public String ClassificationId;
    public List<String> Groups;
    public List<BillingAccountElement> BillingAccounts;
    public Boolean IsActive;

    public void validate() throws Exception
    {
        if (Utility.isEmpty(PartyId)) fail("Missing Party ID!");
        if (Utility.isEmpty(Description)) fail("Missing Customer Description!");
        if (Utility.isEmpty(FirstName)) fail("Missing Customer First Name!");
        if (Utility.isEmpty(LastName)) LastName="";
        if (Utility.isEmpty(CustomerId) || !Utility.isInteger(CustomerId))
            fail("Missing/Invalid Customer Number");
        if (ClassificationId==null || !ClassificationId.endsWith("_CUSTOMER"))
            fail("Missing/Invalid Customer Classification!");
        if (BillingAccounts==null) BillingAccounts = new ArrayList<BillingAccountElement>();
        try {
            for(BillingAccountElement ba:BillingAccounts) ba.validate();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }


    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Customer Element! " + reason + "  >>  " + this);
    }

    @Override
    public String toString() {
        return "CustomerElement{" +
                "PartyId='" + PartyId + '\'' +
                ", Description='" + Description + '\'' +
                ", FirstName='" + FirstName + '\'' +
                ", LastName='" + LastName + '\'' +
                ", CustomerId='" + CustomerId + '\'' +
                ", ExternalId='" + ExternalId + '\'' +
                ", ClassificationId='" + ClassificationId + '\'' +
                ", IsActive=" + IsActive +
                '}';
    }
}
