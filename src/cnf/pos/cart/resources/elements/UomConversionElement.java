package cnf.pos.cart.resources.elements;

import javolution.util.FastMap;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by RusselA on 3/29/2015.
 */
public class UomConversionElement {
    public List<UomElement> UomList;
    public List<UomConv> Conversions;
    private Map<String,UomElement> uomLookup = null;
    private Map<String, BigDecimal> conv = null;

    public void validate() throws Exception
    {
        try {
            if (UomList == null) UomList = new ArrayList<UomElement>();
            if (Conversions == null) Conversions = new ArrayList<UomConv>();
            uomLookup = FastMap.newInstance();
            for (UomElement u : UomList) {
                u.validate();
                uomLookup.put(u.Id, u);
            }
            conv = FastMap.newInstance();
            for (UomConv uc : Conversions) {
                uc.validate();
                if (!uomLookup.containsKey(uc.From) || !uomLookup.containsKey(uc.To))
                    throw new Exception("Unknown Uom in Conversion Entry >> " + uc);
                conv.put(uc.From + "_" + uc.To, uc.V);
                if (!conv.containsKey(uc.To + "_" + uc.From)) {
                    BigDecimal iFactor = BigDecimal.ONE.divide(uc.V, 10, RoundingMode.HALF_EVEN);
                    conv.put(uc.To + "_" + uc.From, iFactor);
                }
            }
        }
        catch (Exception ex)
        {
            fail(ex.getMessage());
        }
    }

    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Uom Conversion Matrix! " + reason);
    }


    public UomElement getUom(String uomId)
    {
        if (uomLookup.containsKey(uomId)) return uomLookup.get(uomId);
        else return null;
    }

    public BigDecimal getConversionFactor(String fromUomId, String toUomId)
    {
        String key = fromUomId+"_"+toUomId;
        if (conv.containsKey(key)) return conv.get(key);
        else return null;
    }
}
