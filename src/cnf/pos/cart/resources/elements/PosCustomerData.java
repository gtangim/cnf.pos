package cnf.pos.cart.resources.elements;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RusselA on 3/28/2015.
 */
public class PosCustomerData {
    public static String[] SupportedVersions = {"APU0005C", "APU0006C", "APU0007C"};
    public String DataModelVersion;
    public String DataModelAuthor;
    public String GeneratedByApplication;
    public String SnapshotId;
    public String ParentSnapshotId;
    public DateTime CreatedDateTime;
    public List<CustomerElement> Customers;

    public void validate() throws Exception
    {
        versionCheck();
        if (Customers==null) Customers = new ArrayList<CustomerElement>();
        try {
            for (CustomerElement c : Customers) c.validate();
        }
        catch (Exception ex)
        {
            fail(ex.getMessage());
        }
    }

    public void versionCheck() throws Exception
    {
        for(String v:SupportedVersions) if (v.equals(DataModelVersion)) return;
        fail("This POS Software does not support Customer Data Model version " + DataModelVersion);
    }


    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Pos Customer Data Model! " + reason + "  >>  " + this);
    }

    @Override
    public String toString() {
        return "PosCustomerData{" +
                "DataModelVersion='" + DataModelVersion + '\'' +
                ", DataModelAuthor='" + DataModelAuthor + '\'' +
                ", GeneratedByApplication='" + GeneratedByApplication + '\'' +
                ", SnapshotId='" + SnapshotId + '\'' +
                ", ParentSnapshotId='" + ParentSnapshotId + '\'' +
                ", CreatedDateTime=" + CreatedDateTime +
                '}';
    }
}
