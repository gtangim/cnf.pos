package cnf.pos.cart.resources.elements;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RusselA on 3/29/2015.
 */
public class PosCoreData {
    public static String[] SupportedVersions = { "APU0007P" };

    public String DataModelVersion;
    public String DataModelAuthor;
    public String GeneratedByApplication;
    public String SnapshotId;
    public String ParentSnapshotId;
    public DateTime CreatedDateTime;
    public List<CategoryElement> Categories;
    public List<ProductElement> Products;           // Rewrite
    public List<FeatureElement> Features;
    public UomConversionElement UomConversions;
    //public List<PriceRuleElement> PriceRules;       // Ignore
    public List<PromoElement> Promos;               // Rewrite
    public List<TaxElement> Taxes;
    public List<ShoppingCartLayerElement> ShoppingCartLayers;
    public List<ReasonElement> ShoppingCartValueMaps;
    public List<ReasonElement> PosCashInReasons;
    public List<ReasonElement> PosCashOutReasons;
    public List<ReasonElement> PosOverrideReasons;
    public List<ReasonElement> PosSaleDiscountReasons;
    public List<ReasonElement> PosItemDiscountReasons;
    public List<String> PromoEnabledPriceOverrides;


    public void validate(String storeId) throws Exception
    {
        versionCheck();
        if (Categories==null) Categories= new ArrayList<CategoryElement>();
        if (PosCashInReasons==null) PosCashInReasons = new ArrayList<ReasonElement>();
        if (PosCashOutReasons==null) PosCashOutReasons = new ArrayList<ReasonElement>();
        if (PosOverrideReasons==null) PosOverrideReasons = new ArrayList<ReasonElement>();
        if (PromoEnabledPriceOverrides==null) PromoEnabledPriceOverrides = new ArrayList<String>();
        if (ShoppingCartLayers==null || ShoppingCartLayers.size()<1) fail("Shopping Cart Layer data is missing!");
        if (Taxes==null) Taxes = new ArrayList<TaxElement>();
        if (Features==null) Features = new ArrayList<FeatureElement>();
        if (Products==null) Products = new ArrayList<ProductElement>();
        if (Promos==null) Promos = new ArrayList<PromoElement>();
        if (UomConversions==null) fail("Missing Uom Conversion Matrix!");
        try {
            UomConversions.validate();
            for(CategoryElement c:Categories) c.validate();
            for(ReasonElement kvp: PosCashInReasons) kvp.validate();
            for(ReasonElement kvp: PosCashOutReasons) kvp.validate();
            for(ReasonElement kvp: PosOverrideReasons) kvp.validate();
            for(TaxElement t:Taxes) t.validate();
            for(FeatureElement f: Features) f.validate();
            ShoppingCartLayerElement prev = null;
            for(ShoppingCartLayerElement scl:ShoppingCartLayers){
                scl.validate();
                if (prev!=null && scl.LayerId<=prev.LayerId) throw new Exception("Invalid Shopping Cart Layer  >>  "
                        + scl +" after " + prev);
                prev = scl;
            }
            List<PromoElement> filteredPromos = new ArrayList<PromoElement>();
            for(PromoElement promo:Promos){
                promo.validate();
                if (promo.IsActive && promo.AppliedToStores.contains(storeId)) filteredPromos.add(promo);
            }
            Promos=filteredPromos;
            for(ProductElement p:Products) p.validate(storeId);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    public void versionCheck() throws Exception
    {
        for(String v:SupportedVersions) if (v.equals(DataModelVersion)) return;
        fail("This POS Software does not support Customer Data Model version " + DataModelVersion);
    }

    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Pos Core Data Model! " + reason + "  >>  " + this);
    }


    @Override
    public String toString() {
        return "PosCoreData{" +
                "DataModelVersion='" + DataModelVersion + '\'' +
                ", DataModelAuthor='" + DataModelAuthor + '\'' +
                ", GeneratedByApplication='" + GeneratedByApplication + '\'' +
                ", SnapshotId='" + SnapshotId + '\'' +
                ", ParentSnapshotId='" + ParentSnapshotId + '\'' +
                ", CreatedDateTime=" + CreatedDateTime +
                '}';
    }
}
