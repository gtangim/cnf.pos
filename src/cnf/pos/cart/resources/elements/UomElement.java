package cnf.pos.cart.resources.elements;

import cnf.pos.util.Utility;


/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 1/26/11
 * Time: 9:54 AM
 * To change this template use File | Settings | File Templates.
 */
public class UomElement {
    public String Id;
    public String UomType;
    public String Abbrev;
    public String Description;

    public void validate() throws Exception
    {
        if (Utility.isEmpty(Id)) fail("Missing UOM ID!");
        if (Utility.isEmpty(UomType)) fail("Missing Uom Type!");
        if (Utility.isEmpty(Abbrev)) fail("Missing UOM Abbreviation!");
        if (Utility.isEmpty(Description)) fail("Missing UOM Description!");
    }

    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Uom Element! " + reason + " >> " + this);
    }

    @Override
    public String toString() {
        return "UomElement{" +
                "Id='" + Id + '\'' +
                ", UomType='" + UomType + '\'' +
                ", Abbrev='" + Abbrev + '\'' +
                ", Description='" + Description + '\'' +
                '}';
    }
}
