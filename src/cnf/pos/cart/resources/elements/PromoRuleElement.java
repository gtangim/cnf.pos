package cnf.pos.cart.resources.elements;

import javolution.util.FastList;
import cnf.pos.cart.PosShoppingCartItem;
import cnf.pos.cart.PosShoppingCartItemAllocator;
import cnf.pos.cart.resources.ResourceManager;
import cnf.pos.cart.resources.datastructures.PromoApplication;
import cnf.pos.util.Utility;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/15/11
 * Time: 8:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class PromoRuleElement {

    public String Id;
    public BigDecimal ReservationAmount;
    public PromoReservationType ReservationType;
    public PromoReservationOrder ReservationOrder;
    public List<PromoConditionElement> Conditions;
    public List<PromoActionElement> Actions;


    public void validate() throws Exception {
        if (Utility.isEmpty(Id)) fail("Missing Promo Rule ID!");
        if (ReservationType==null) fail("Missing Promo Rule Reservation Type!");
        if (ReservationOrder==null) fail("Missing Promo Rule Reservation Order!");
        if (Conditions==null) Conditions = FastList.newInstance();
        if (Actions==null) Actions = FastList.newInstance();
        if (ReservationAmount==null) ReservationAmount=BigDecimal.ZERO;

        try
        {
            for(PromoConditionElement pce:Conditions) pce.validate();
            for(PromoActionElement pae:Actions) pae.validate();
        }
        catch (Exception ex)
        {
            fail(ex.getMessage());
        }
    }

    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Promo Rule Element! " + reason + " >> " + this);
    }


    private ArrayList<String> codes;
    private int codeInd;
    private void resetCodes(ArrayList<String> codes)
    {
        this.codes=codes;
        codeInd=0;
    }
    private String nextCode()
    {
        if (codes==null || codes.size()==0) return null;
        if (codeInd<codes.size()) return codes.get(codeInd++);
        else return codes.get(codes.size()-1);
    }
    private boolean codeExpired()
    {
        if (codes==null) return true;
        else if (codeInd>=codes.size()) return true;
        else return false;
    }


    public void applyPromo(ResourceManager r,PromoApplication pa, PromoApplication gpa
            , List<PosShoppingCartItem> cartList, List<PosShoppingCartItem> sortedList
            , Set<Integer> scope, boolean useGrossPrice, boolean stackingModel, ArrayList<String> codes, int useLimit)
    {
        if (ReservationAmount!=null && scope!=null && ReservationType!=PromoReservationType.RESV_TYPE_UNIQUE)
        {
            boolean reserveAmount = (ReservationType==PromoReservationType.RESV_TYPE_VALUE);
            boolean isMinMax = (ReservationOrder==PromoReservationOrder.RESV_ORDER_MINMAX);
            double resv = ReservationAmount.doubleValue();
            if (resv<=0) return; // This is an invalid reservation quantity...

            List<PosShoppingCartItem> itemsRaw = cartList;
            if (isMinMax) itemsRaw = sortedList;

            // First Filter by scope...
            List<PosShoppingCartItem> items = FastList.newInstance();
            for(PosShoppingCartItem item:itemsRaw)
                if (scope.contains(item.getCartIndex()))
                    items.add(item);

            //FastList<PosShoppingCartItemAllocator>

            for(PromoActionElement pae : Actions)
            {
                resetCodes(codes);
                int useCount = 0;
                //String key = pae.getGroupKey();
                List<PosShoppingCartItem> filteredItems;
                if (pae.isFilteredProductsOnly())
                {
                    filteredItems = FastList.newInstance();
                    for(PosShoppingCartItem item:items)
                        if (pae.isProductIncluded(item.getProductId()))
                            filteredItems.add(item);
                }
                else filteredItems=items;


                //TODO: Add another case for GWP

                if (pae.isOrderLevel())
                {
                    // apply order level action...
                    // ignore the reservation strategy...
                    // apply this only once...
                    PosShoppingCartItemAllocator curAlloc = new PosShoppingCartItemAllocator(filteredItems,false);
                    pae.applyAction(r,curAlloc,pa,gpa,useGrossPrice,stackingModel,nextCode());
                    int n= curAlloc.size();
                    for(int i=0;i<n;i++) pa.addToScope(curAlloc.getItem(i).getCartIndex(),curAlloc.getAmount(i));
                    useCount++;
                    if (useCount>=useLimit) continue;
                    //if (pae.getAct()==PromoActionElement.ORDER_PERCENT)
                    //    for(int i=0;i<n;i++)
                    //        pa.addToTarget(curAlloc.getItem(i).getCartIndex());
                }
                else if (isMinMax)
                {
                    // follow a min-max reservation strategy...
                    PosShoppingCartItemAllocator scopeAlloc = new PosShoppingCartItemAllocator(filteredItems,false);
                    scopeAlloc.resetAllocation();
                    PosShoppingCartItemAllocator actionAlloc = new PosShoppingCartItemAllocator(filteredItems,true);
                    actionAlloc.resetAllocation();

                    //List<PosShoppingCartItemAllocator> scopes = FastList.newInstance();
                    //List<PosShoppingCartItemAllocator> targets = FastList.newInstance();

                    PosShoppingCartItemAllocator curAlloc = null;
                    while((curAlloc=(reserveAmount?scopeAlloc.reserveByAmount(resv,false)
                            :scopeAlloc.reserveByQuantity(resv,false)))!=null)
                    {
                        double actionQty = pae.getReservationQuantity().doubleValue();
                        PosShoppingCartItemAllocator curTarget =
                                actionAlloc.reserveByQuantity(actionQty,false);
                        if (curTarget==null) break; // We ran out of targets...
                        pae.applyAction(r,curTarget,pa,gpa,useGrossPrice,stackingModel,nextCode());
                        int n= curAlloc.size();
                        for(int i=0;i<n;i++) pa.addToScope(curAlloc.getItem(i).getCartIndex(), curAlloc.getAmount(i));
                        useCount++;
                        if (useCount>=useLimit) break;
                        //n=curTarget.size();
                        //for(int i=0;i<n;i++) pa.addToTarget(curTarget.getItem(i).getCartIndex());
                    }
                }
                else
                {
                    // follow cart order reservation strategy...
                    PosShoppingCartItemAllocator alloc = new PosShoppingCartItemAllocator(filteredItems,false);
                    alloc.resetAllocation();
                    PosShoppingCartItemAllocator curAlloc = null;
                    while((curAlloc=(reserveAmount?alloc.reserveByAmount(resv,true)
                            :alloc.reserveByQuantity(resv,true)))!=null)
                    {
                        double actionQty = pae.getReservationQuantity().doubleValue();
                        curAlloc.resetAllocation();
                        PosShoppingCartItemAllocator curTarget =
                                curAlloc.reserveByQuantity(actionQty,false);
                        int n= curAlloc.size();
                        for(int i=0;i<n;i++) pa.addToScope(curAlloc.getItem(i).getCartIndex(), curAlloc.getAmount(i));
                        if (curTarget!=null)
                        {
                            // Sometimes, a reserved scope may not have enough element
                            // for an action in this allocation strategy...
                            // If this happens then the user screwed up...
                            // still we need to skip that sub block...
                            pae.applyAction(r,curTarget,pa,gpa,useGrossPrice,stackingModel,nextCode());
                            useCount++;
                            if (useCount>=useLimit) break;
                            //n=curTarget.size();
                            //for(int i=0;i<n;i++) pa.addToTarget(curTarget.getItem(i).getCartIndex());
                        }
                    }
                }
            }
        }
        else
        {
            // Apply just once...(Force RESV_TYPE_UNIQUE)
            //useLimit=1;
            boolean isMinMax = (ReservationOrder==PromoReservationOrder.RESV_ORDER_MINMAX);

            List<PosShoppingCartItem> itemsRaw = cartList;
            if (isMinMax) itemsRaw = sortedList;

            // First Filter by scope...
            List<PosShoppingCartItem> items = FastList.newInstance();
            for(PosShoppingCartItem item:itemsRaw)
                if (scope==null || scope.contains(item.getCartIndex()))
                    items.add(item);


            for(PromoActionElement pae : Actions)
            {
                resetCodes(codes);
                List<PosShoppingCartItem> filteredItems;
                if (pae.isFilteredProductsOnly())
                {
                    filteredItems = FastList.newInstance();
                    for(PosShoppingCartItem item:items)
                        if (pae.isProductIncluded(item.getProductId()))
                            filteredItems.add(item);
                }
                else filteredItems=items;


                //TODO: Add another case for GWP

                if (pae.isOrderLevel())
                {
                    // apply order level action...
                    // ignore the reservation strategy...
                    // apply this only once...
                    PosShoppingCartItemAllocator curAlloc = new PosShoppingCartItemAllocator(filteredItems,false);
                    pae.applyAction(r,curAlloc,pa,gpa,useGrossPrice,stackingModel,nextCode());
                    int n= curAlloc.size();
                    for(int i=0;i<n;i++) pa.addToScope(curAlloc.getItem(i).getCartIndex(), curAlloc.getAmount(i));
                    //if (pae.getAct()==PromoActionElement.ORDER_PERCENT)
                    //    for(int i=0;i<n;i++)
                    //        pa.addToTarget(curAlloc.getItem(i).getCartIndex());
                }
                else
                {
                    PosShoppingCartItemAllocator curAlloc = new PosShoppingCartItemAllocator(filteredItems,false);
                    curAlloc.resetAllocation();
                    double actionQty = pae.getReservationQuantity().doubleValue();
                    PosShoppingCartItemAllocator curTarget = curAlloc.reserveByQuantity(actionQty,false);
                    if (curTarget==null) return;
                    pae.applyAction(r,curTarget,pa,gpa,useGrossPrice,stackingModel,nextCode());
                    int n= curAlloc.size();
                    for(int i=0;i<n;i++) pa.addToScope(curAlloc.getItem(i).getCartIndex(), curAlloc.getAmount(i));
                }
            }

        }
    }




    public boolean isConditionSatisfied(Map params,String layer,int scale,RoundingMode rMode)
    {
        for(PromoConditionElement cond:Conditions)
        {
            boolean res = cond.eval(params,layer,scale,rMode);
            if (!res) return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PromoRuleElement{" +
                "Id='" + Id + '\'' +
                ", ReservationAmount=" + ReservationAmount +
                ", ReservationType=" + ReservationType +
                ", ReservationOrder=" + ReservationOrder +
                '}';
    }
}
