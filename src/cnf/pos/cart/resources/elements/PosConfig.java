package cnf.pos.cart.resources.elements;

import cnf.node.entities.PosSettlement;
import org.joda.time.DateTime;
import cnf.pos.util.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RusselA on 3/27/2015.
 */
public class PosConfig {
    public String StoreId;
    public int TerminalNumber;
    public String MachineId;
    public String SnapshotHotFolder;
    public TcpLinkConfig BongoNodeConfig;
    public TcpLinkConfig CustomerScreenConfig;
    public List<String> CustomerPatterns;
    public List<String> PromoCodePatterns;
    public List<String> InputFilters;
    public List<String> PriceLabelPatterns;
    public List<String> WeightLabelPatterns;
    public DateTime InstallDateTime;
    public DateTime LastModifiedDateTime;
    public String LastActionBeforeSave;
    public String ModifiedByApplication;



    public String KeyboardDeviceName;
    public String ScannerDeviceName;
    public String ScaleDeviceName;
    public String CashDrawerDeviceName;
    public String CatDeviceName;
    public String PrinterDeviceName;
    public String CheckScannerDeviceName;
    public String MsrDeviceName;
    public String JournalDeviceName;
    public String LineDisplayDeviceName;

    public void validate() throws Exception{
        if (Utility.isEmpty(StoreId)) fail("Missing Store Id!");
        if (TerminalNumber<=0 || TerminalNumber>200) fail("Missing Pos Terminal Number!");
        if (BongoNodeConfig!=null) BongoNodeConfig.validate();
        if (CustomerScreenConfig!=null) CustomerScreenConfig.validate();
        if (CustomerPatterns==null) CustomerPatterns= new ArrayList<String>();
        if (PromoCodePatterns==null) PromoCodePatterns =  new ArrayList<String>();
        if (InputFilters==null) InputFilters =  new ArrayList<String>();
        if (PriceLabelPatterns==null) PriceLabelPatterns =  new ArrayList<String>();
        if (WeightLabelPatterns==null) WeightLabelPatterns =  new ArrayList<String>();
    }

    public String getTerminalId(){
        return StoreId+"-"+Integer.toString(TerminalNumber);
    }

    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Pos Configuration! " + reason);
    }
}
