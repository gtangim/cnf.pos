package cnf.pos.cart.resources.elements;

/**
 * Created by RusselA on 3/29/2015.
 */
public enum SCLSecondaryType {
    CSL_DS_ITM, CSL_DS_SAL, CSL_FT_CHG, CSL_FT_DIS, CSL_FT_OPT, CSL_FT_REQ, CSL_FT_STD
}
