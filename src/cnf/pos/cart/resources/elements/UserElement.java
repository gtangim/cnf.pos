package cnf.pos.cart.resources.elements;

import bongo.util.Utility;
import org.apache.commons.codec.binary.Hex;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RusselA on 3/26/2015.
 */
public class UserElement {
    public String UserId;
    public String UserNumber;
    public String Password;
    public String Description;
    public String FirstName;
    public String LastName;
    public String PhoneNumber;
    public String EmployeeNumber;
    public String PinNumber;
    public List<String> Roles;
    public Boolean IsActive;

    public void validate() throws Exception {
        if (cnf.pos.util.Utility.isEmpty(UserId)) fail("Missing PosTerminal ID!");
        if (cnf.pos.util.Utility.isEmpty(Password)) fail("Missing User Password!");
        if (cnf.pos.util.Utility.isEmpty(Description)) fail("Missing User description!");
        if (cnf.pos.util.Utility.isEmpty(FirstName)) fail("Missing User First Name!");
        if (cnf.pos.util.Utility.isEmpty(LastName)) fail("Missing User Last Name!");
        if (Roles==null) Roles = new ArrayList<String>();
        for(String r:Roles) if (cnf.pos.util.Utility.isEmpty(r)) fail("Invalid User Role!");
        if (PhoneNumber!=null && !cnf.pos.util.Utility.isInteger(PhoneNumber))
            fail("Invalid User Phone number: " + PhoneNumber);
        if (UserNumber!=null && !cnf.pos.util.Utility.isInteger(UserNumber))
            fail("Invalid User Phone number: " + UserNumber);
        if (PinNumber!=null && !cnf.pos.util.Utility.isIntegerInRange(PinNumber, 0, 999999))
            fail("Invalid User Pin number: " + PinNumber);
        if (EmployeeNumber!=null && !cnf.pos.util.Utility.isInteger(EmployeeNumber))
            fail("Invalid User Employee number: " + EmployeeNumber);

    }



    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Login User Element! " + reason + " (User: " + UserId+")");
    }



    public boolean hasRole(String role)
    {
        for(String r:Roles) if (r.compareToIgnoreCase(role)==0) return true;
        return false;
    }

    public boolean matchesCredential(String userCode, String password) throws Exception
    {
        if (userCode==null || password==null) return false;
        if (!matchesIdentity(userCode)) return false;
        if (cnf.pos.util.Utility.isEmpty(Password)) return false;
        String p = getPasswordHash(password);
        if (p!=null && p.equalsIgnoreCase(Password)) return true;
        p = getLegacyPasswordHash(password);
        if (p!=null && p.equalsIgnoreCase(Password)) return true;
        if (password!=null && password.equals(PinNumber)) return true;
        return false;
    }

    public boolean matchesIdentity(String userCode)
    {
        if (Utility.stringEquals(userCode,UserId)
                || Utility.stringEquals(userCode,UserNumber)
                || Utility.stringEquals(userCode,PhoneNumber)
                || Utility.stringEquals(userCode,EmployeeNumber)
                ) return true;
        else return false;
    }

    private String getPasswordHash(String psw) throws Exception {
        if (psw == null) return null;
        MessageDigest messagedigest = MessageDigest.getInstance("SHA");
        byte[] strBytes = psw.getBytes();

        messagedigest.update(strBytes);
        byte[] digestBytes = messagedigest.digest();
        char[] digestChars = Hex.encodeHex(digestBytes);

        StringBuilder sb = new StringBuilder();
        sb.append("{").append("SHA").append("}");
        sb.append(digestChars, 0, digestChars.length);
        return sb.toString();
    }

    public String getLegacyPasswordHash(String psw) throws Exception {
        if (psw == null) return null;
        MessageDigest messagedigest = MessageDigest.getInstance("SHA");
        byte strBytes[] = psw.getBytes();

        messagedigest.update(strBytes);
        byte digestBytes[] = messagedigest.digest();
        int k = 0;
        char digestChars[] = new char[digestBytes.length * 2];

        for (int l = 0; l < digestBytes.length; l++) {
            int i1 = digestBytes[l];

            if (i1 < 0) {
                i1 = 127 + i1 * -1;
            }
            encodeInt(i1, k, digestChars);
            k += 2;
        }

        return new String(digestChars, 0, digestChars.length);
    }

    public static char[] encodeInt(int i, int j, char digestChars[]) {
        if (i < 16) {
            digestChars[j] = '0';
        }
        j++;
        do {
            digestChars[j--] = hexChar[i & 0xf];
            i >>>= 4;
        } while (i != 0);
        return digestChars;
    }
    private static char[] hexChar = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };


}
