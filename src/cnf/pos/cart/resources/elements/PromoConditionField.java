package cnf.pos.cart.resources.elements;

/**
 * Created by russela on 3/30/2015.
 */
public enum PromoConditionField {
    PPIP_NEW_ACCT, PPIP_ORDER_TOTAL, PPIP_ORST_HIST, PPIP_ORST_LAST_YEAR, PPIP_ORST_YEAR, PPIP_PARTY_CLASS, PPIP_PARTY_GRP_MEM, PPIP_PARTY_ID, PPIP_PRODUCT_AMOUNT, PPIP_PRODUCT_QUANT, PPIP_PRODUCT_TOTAL
}
