package cnf.pos.cart.resources.elements;

import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by RusselA on 3/26/2015.
 */
public class PosSecurityData {
    public static String[] SupportedVersions = {"APU0006S","APU0007S"};
    public String DataModelVersion;
    public String DataModelAuthor;
    public String GeneratedByApplication;
    public String SnapshotId;
    public String ParentSnapshotId;
    public DateTime CreatedDateTime;
    public List<StoreElement> Stores;
    public List<UserElement> Users;
    private StoreElement store = null;
    private PosTerminalElement terminal = null;

    public void validate(String thisStoreId, String thisTerminalId) throws Exception
    {
        versionCheck();
        try {
            for(StoreElement se:Stores) { se.validate(); if (se.StoreId.equals(thisStoreId)) store=se;}
            for(UserElement u:Users) u.validate();
            if (store==null) fail("Unable to find currently designated store " + thisStoreId + " in data file!");
            terminal = store.getTerminal(thisTerminalId);
            if (terminal==null) fail("Unable to find currently designated terminal "
                    + thisTerminalId + " in data file!");
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    public void versionCheck() throws Exception
    {
        for(String v:SupportedVersions) if (v.equals(DataModelVersion)) return;
        fail("This POS Software does not support Security Data Model version " + DataModelVersion);
    }


    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Pos Security Data Model! " + reason + "  >>  " + this);
    }

    @Override
    public String toString() {
        return "PosSecurityData{" +
                "DataModelVersion='" + DataModelVersion + '\'' +
                ", DataModelAuthor='" + DataModelAuthor + '\'' +
                ", GeneratedByApplication='" + GeneratedByApplication + '\'' +
                ", SnapshotId='" + SnapshotId + '\'' +
                ", ParentSnapshotId='" + ParentSnapshotId + '\'' +
                ", CreatedDateTime=" + CreatedDateTime +
                '}';
    }

    public StoreElement getStore() {
        return store;
    }

    public PosTerminalElement getTerminal() {
        return terminal;
    }
}
