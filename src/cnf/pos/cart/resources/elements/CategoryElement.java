package cnf.pos.cart.resources.elements;

import cnf.pos.util.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RusselA on 3/29/2015.
 */
public class CategoryElement {
    public String CategoryId;
    public String CategoryTypeId;
    public String ParentCategoryId;
    public String Name;

    public List<CategoryElement> SubCategories;
    public List<String> MemberProducts;

    public void validate() throws Exception
    {
        if (Utility.isEmpty(CategoryId)) fail("Missing Category ID!");
        if (Utility.isEmpty(CategoryTypeId)) fail("Missing Category Type!");
        if (Utility.isEmpty(Name)) fail("Missing Category Name!");
        if (SubCategories==null) SubCategories = new ArrayList<CategoryElement>();
        if (MemberProducts==null) MemberProducts = new ArrayList<String>();
        try {
            for (CategoryElement s : SubCategories) s.validate();
        }
        catch (Exception ex)
        {
            fail(ex.getMessage());
        }
    }


    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Category Element! " + reason + "  >>  " + this);
    }

    @Override
    public String toString() {
        return "CategoryElement{" +
                "CategoryId='" + CategoryId + '\'' +
                ", CategoryTypeId='" + CategoryTypeId + '\'' +
                ", ParentCategoryId='" + ParentCategoryId + '\'' +
                ", Name='" + Name + '\'' +
                '}';
    }
}
