package cnf.pos.cart.resources.elements;

import cnf.pos.cart.resources.datastructures.AtomicPrice;
import cnf.pos.util.Utility;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/16/11
 * Time: 8:32 PM
 * To change this template use File | Settings | File Templates.
 */
public final class FeatureElement {
    public String FeatureId;
    public String FeatureTypeId;
    public String FeatureApplTypeId;
    public String Description;
    public String FeatureCategoryId;
    public AtomicPrice Price;


    public void validate() throws Exception
    {
            if (Utility.isEmpty(FeatureId)) fail("Missing Feature ID!");
            if (Utility.isEmpty(FeatureTypeId)) fail("Missing Feature Type!");
            if (Utility.isEmpty(FeatureApplTypeId)) fail("Missing Feature Application Type!");
            if (Utility.isEmpty(FeatureCategoryId)) fail("Missing Feature Category!");
            if (Utility.isEmpty(Description)) fail("Missing Feature Description!");
            if (Price==null) fail("Price is Empty!");
        try
        {
            Price.validate();
        }
        catch(Exception ex)
        {
            fail(ex.getMessage());
        }
    }


    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Feature Element! " + reason + "  >>  " + this);
    }

    @Override
    public String toString() {
        return "FeatureElement{" +
                "FeatureId='" + FeatureId + '\'' +
                ", FeatureTypeId='" + FeatureTypeId + '\'' +
                ", FeatureApplTypeId='" + FeatureApplTypeId + '\'' +
                ", Description='" + Description + '\'' +
                ", FeatureCategoryId='" + FeatureCategoryId + '\'' +
                ", Price=" + Price +
                '}';
    }
}
