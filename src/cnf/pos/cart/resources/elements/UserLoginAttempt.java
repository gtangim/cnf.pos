package cnf.pos.cart.resources.elements;

import apu.jpos.util.Utility;
import org.joda.time.DateTime;

/**
 * Created by russela on 4/1/2015.
 */
public class UserLoginAttempt {
    public String UserId;
    public DateTime lastFailedAttempt = null;
    public DateTime lastSuccessfulAttempt = null;
    public int unsuccessfulLoginCount = 0;


    public UserLoginAttempt(){

    }

    public UserLoginAttempt(String userId)
    {
        this.UserId = userId;
    }


    public void markFailed()
    {
        lastFailedAttempt = new DateTime();
        unsuccessfulLoginCount++;
    }

    public void unlock()
    {
        lastFailedAttempt=null;
        unsuccessfulLoginCount=0;
        lastSuccessfulAttempt = new DateTime();
    }

    public boolean isLockedOut(int lockoutThreshold, int lockoutDurationMs)
    {
        if (unsuccessfulLoginCount>lockoutThreshold && lastFailedAttempt!=null
                && Utility.getElapsed(lastFailedAttempt)<=lockoutDurationMs) return true;
        else return false;
    }

    public boolean isStale()
    {
        if (lastFailedAttempt!=null && Utility.getElapsed(lastFailedAttempt)>=48 * 3600 * 1000) return true;
        else if (lastFailedAttempt==null && lastSuccessfulAttempt!=null
                && Utility.getElapsed(lastSuccessfulAttempt)>=48 * 3600 * 1000) return true;
        return false;
    }

}
