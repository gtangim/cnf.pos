package cnf.pos.cart.resources.elements;

/**
 * Created by russela on 3/30/2015.
 */
public enum PromoActionType {
    GWP, ORDER_AMOUNT, ORDER_PERCENT, PROD_AMDISC, PROD_DISC, PROD_PRICE, PROD_SPPRICE, NA
}
