package cnf.pos.cart.resources.elements;

import cnf.pos.util.Utility;

/**
 * Created by RusselA on 3/29/2015.
 */
public class ReasonElement {
    public String ReasonId;
    public String Description;
    public String ReportingCode;
    public void validate() throws Exception
    {
        if (Utility.isEmpty(ReasonId)) fail("Missing ReasonId in ReasonId Description Pair!");
        if (Utility.isEmpty(Description)) fail("Null Description not allowed in ReasonId Description Pair!");
    }


    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Customer Element! " + reason + " :: Faulty KVP: " + this);
    }

    @Override
    public String toString() {
        return "ReasonElement{" +
                "ReasonId='" + ReasonId + '\'' +
                ", Description='" + Description + '\'' +
                ", ReportingCode='" + ReportingCode + '\'' +
                '}';
    }
}
