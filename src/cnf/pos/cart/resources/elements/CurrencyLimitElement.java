package cnf.pos.cart.resources.elements;

import java.math.BigDecimal;

/**
 * Created by RusselA on 3/26/2015.
 */
public class CurrencyLimitElement {
    public String Currency;
    public BigDecimal Limit;

    @Override
    public String toString() {
        return "CurrencyLimitElement{" +
                "Currency='" + Currency + '\'' +
                ", Limit=" + Limit +
                '}';
    }
}
