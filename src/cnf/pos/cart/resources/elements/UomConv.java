package cnf.pos.cart.resources.elements;

import cnf.pos.util.Utility;

import java.math.BigDecimal;

/**
 * Created by russela on 3/30/2015.
 */
public class UomConv {
    public String From;
    public String To;
    public BigDecimal V;


    public void validate() throws Exception
    {
        if (Utility.isEmpty(From)) fail("Missing From UOM ID!");
        if (Utility.isEmpty(To)) fail("Missing To UOM ID!");
        if (V==null) fail("Conversion factor must not be null!");
    }

    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Uom Conversion Entry! " + reason + " >> " + this);
    }

    @Override
    public String toString() {
        return "UomConv{" +
                "From='" + From + '\'' +
                ", To='" + To + '\'' +
                ", V=" + V +
                '}';
    }
}
