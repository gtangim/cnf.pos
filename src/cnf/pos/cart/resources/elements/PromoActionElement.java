package cnf.pos.cart.resources.elements;

import javolution.util.FastSet;
import cnf.pos.cart.PosShoppingCartItem;
import cnf.pos.cart.PosShoppingCartItemAllocator;
import cnf.pos.cart.resources.ResourceManager;
import cnf.pos.cart.resources.datastructures.AtomicPrice;
import cnf.pos.cart.resources.datastructures.PromoAdjustment;
import cnf.pos.cart.resources.datastructures.PromoApplication;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/15/11
 * Time: 7:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class PromoActionElement {
    public int Seq;
    public PromoActionType Action;
    public BigDecimal X;
    public BigDecimal Y;
    public String ProductId;
    public List<String> MemberProducts;


    protected Set<String> members = null;
    protected double xx;
    protected double yy;
    protected boolean filterProducts;
    protected boolean isOnSaleCategory = false;

    public void validate() throws Exception
    {
        if (Action==null) fail("Missing Promo Action Type!");
        init();
    }


    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Promo Action Element! " + reason + " >> " + this);
    }


    private void init()
    {
        if (X==null) X=BigDecimal.ZERO;
        if (Y==null) Y=BigDecimal.ZERO;
        else Y=Y.negate();
        xx=X.doubleValue();
        yy=Y.doubleValue();
        members = FastSet.newInstance();
        members.addAll(MemberProducts);
        if (members.size()==0) filterProducts = false;
        else {
            filterProducts = true;
            if (members.contains("ONSALE")) isOnSaleCategory = true;
        }
    }


    public boolean IsOnSaleCategory(){
        return isOnSaleCategory;
    }

    public void addMember(String pid){
        if (members.contains(pid)) return;
        members.add(pid);
        MemberProducts.add(pid);
    }











    public boolean isOrderLevel()
    {
        return (Action==PromoActionType.ORDER_AMOUNT || Action==PromoActionType.ORDER_PERCENT);
    }


    public BigDecimal getReservationQuantity() {
        return X;
    }

    public boolean isFilteredProductsOnly(){
        return filterProducts;
    }

    public boolean isProductIncluded(String productId)
    {
        return members.contains(productId);
    }

    public void applyAction(ResourceManager r, PosShoppingCartItemAllocator alloc
            , PromoApplication pa, PromoApplication gpa, boolean useGrossPrice, boolean stackingModel, String promoCode)
    {
        // Take each entries of alloc, convert them to adjustment and insert it into pa...
        // Also update the target scope...

        // if it is flat amount then create only one adjustment (ignore alloc)
        // At this level Order_Percent discount and item_percent discount is the same...
        // For item discounts loop through alloc and apply one adjustment for each entry...
        if (Action==PromoActionType.NA) return;

        PromoElement p = pa.getPromo();
        if (Action==PromoActionType.ORDER_AMOUNT)
        {
            PromoAdjustment pad = new PromoAdjustment(-1,p.Id,false,
                    Y.setScale(r.getSettings().getScale(),r.getSettings().getRoundingMode())
                    ,promoCode);
            pa.addAdjustment(pad);
            gpa.addAdjustment(pad);
            // There is no target for this application, it is order level
        }
        else if (Action==PromoActionType.GWP)
        {
            // Only apply to the first element of alloc...
            BigDecimal amount = calculateAmount(r,alloc.getItem(0),0,0,useGrossPrice);
            PromoAdjustment pad = new PromoAdjustment(alloc.getItem(0).getCartIndex(),
                    p.Id,true,amount,promoCode);
            pa.addAdjustment(pad);
            pa.addToTarget(alloc.getItem(0).getCartIndex(), alloc.getAmount(0));
            gpa.addAdjustment(pad);
            if (stackingModel)
            {
                double newValue = alloc.getItem(0).getPrePromoValue() + pad.getAmount().doubleValue();
                alloc.getItem(0).setPromoValue(newValue);
            }
        }
        else
        {
            int n = alloc.size();
            for (int i=0;i<n;i++)
            {
                BigDecimal amount = calculateAmount(r,alloc.getItem(i), alloc.getQty(i)
                        ,alloc.getAmount(i),useGrossPrice);
                PromoAdjustment pad = new PromoAdjustment(alloc.getItem(i).getCartIndex(),
                        p.Id,true,amount,promoCode);
                pa.addAdjustment(pad);
                pa.addToTarget(alloc.getItem(i).getCartIndex(), alloc.getAmount(i));
                gpa.addAdjustment(pad);
                if (stackingModel)
                {
                    double newValue = alloc.getItem(i).getPrePromoValue() + pad.getAmount().doubleValue();
                    alloc.getItem(i).setPromoValue(newValue);
                }
            }
        }
    }

    private BigDecimal calculateAmount(ResourceManager r,PosShoppingCartItem item, double qty, double amount, boolean useGrossPrice)
    {
        double val=0;
        if (Action==PromoActionType.GWP)
        {
            //Ignore amount...
            if (item.getProductId().equals(ProductId))
                val = -item.getPrePromoPrice();
        }
        else if (Action==PromoActionType.PROD_AMDISC || Action==PromoActionType.ORDER_AMOUNT)
        {
            // ignore amount...
            return Y.setScale(r.getSettings().getScale()
                    ,r.getSettings().getRoundingMode());
        }
        else if (Action==PromoActionType.ORDER_PERCENT || Action==PromoActionType.PROD_DISC)
        {
            // Percentage discount...
            val = amount * yy * 0.01;
        }
        else if (Action==PromoActionType.PROD_PRICE)
        {
            val = qty * yy - amount;
        }
        else if (Action==PromoActionType.PROD_SPPRICE)
        {
            AtomicPrice spPrice = item.getPrice().SpecialPromoPrice;
            if (spPrice==null) val = qty * yy - amount;
            else val=qty * spPrice.getPrice(useGrossPrice).doubleValue() - amount;
        }
        return new BigDecimal(val).setScale(r.getSettings().getScale()
                ,r.getSettings().getRoundingMode());
    }

    @Override
    public String toString() {
        return "PromoActionElement{" +
                "Seq=" + Seq +
                ", Action=" + Action +
                ", X=" + X +
                ", Y=" + Y +
                ", ProductId='" + ProductId + '\'' +
                '}';
    }
}
