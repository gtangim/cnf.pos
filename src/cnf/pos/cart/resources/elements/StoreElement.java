package cnf.pos.cart.resources.elements;

import javolution.util.FastMap;
import cnf.pos.util.Utility;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Created by RusselA on 3/26/2015.
 */
public class StoreElement {

    public String StoreId;
    public String StoreGroupId;
    public String StoreName;
    public String CompanyName;
    public String InventoryMethodId;
    public String DefaultCurrency;
    public String DefaultSalesChannelId;

    public String AppTitle;
    public String DefaultUomId;
    public RoundingMode RoundingMode;
    public int CurrencyScale;
    public String CurrencyFormat;
    public String ShortCurrencyFormat;
    public String SystemUserId;
    public String TaxLiteral;
    public Boolean ShowZeroTax;
    public BigDecimal EmployeePricingCostPlusPercent;
    public List<CurrencyLimitElement> CurrencyLimits;
    public String CurrencyLimitWarning;
    public String ReceiptValidation;
    public String WeightLabelUomId;
    public Boolean EnableCalculatePriceLabelQuantity;
    public Boolean IsStackingDiscount;
    public Boolean IsDemo;
    public Boolean PostUserEvents;
    public String OperationDayEnd;
    public int UserLockoutThreshold;
    public int UserLockoutDurationMs;
    public int RoundPenniesTo;
    public String ReceiptTitle;
    public String ReceiptHeader;
    public String ReceiptFooter;
    public Boolean UseGrossPrice;
    public Boolean ShowKeyboardInSavedSale;
    public List<PosTerminalElement> Terminals;
    public Boolean IsActive;

    //public RoundingMode DefaultRoundingMode;
    public Map<String,BigDecimal> currencyLimitLookup = FastMap.newInstance();


    public void validate() throws Exception {
        if (Utility.isEmpty(StoreId)) fail("Missing Store ID!");
        if (Utility.isEmpty(StoreGroupId)) fail("Missing Store Group ID!");
        if (Utility.isEmpty(StoreName)) fail("Missing Store Name!");
        if (Utility.isEmpty(CompanyName)) fail("Missing Company Name!");
        if (Utility.isEmpty(AppTitle)) AppTitle = "CNF POS";
        if (Utility.isEmpty(InventoryMethodId)) fail("Missing Inventory Method ID!");
        if (Utility.isEmpty(DefaultUomId)) fail("Missing Default Uom ID!");
        if (Utility.isEmpty(WeightLabelUomId)) fail("Missing Weight Label Uom ID!");
        if (Utility.isEmpty(CurrencyLimitWarning)) fail("Missing Currency Limit Warning Message!");
        if (!validRegex(ReceiptValidation)) fail("Missing or Invalid Receipt Validation Regex!");
        if (!validCurrency(DefaultCurrency)) fail("Invalid Default Currency: " + DefaultCurrency);
        if (!"POS_SALES_CHANNEL".equals(DefaultSalesChannelId)) fail("Store Sales channel must be POS_SALES_CHANNEL");
        if (!InventoryMethodId.startsWith("INVRO_")) fail("Invalid Inventory Method ID: " + InventoryMethodId);
        if (TaxLiteral==null || TaxLiteral.length()!=1) fail("Invalid Tax Literal!");
        if (EmployeePricingCostPlusPercent==null) fail("Missing Employee Pricing Amount!");
        if (CurrencyLimits==null) CurrencyLimits = new ArrayList<CurrencyLimitElement>();
        if (Terminals==null) Terminals = new ArrayList<PosTerminalElement>();
        if (UseGrossPrice==null) UseGrossPrice=false;

        for(CurrencyLimitElement cl:CurrencyLimits)
            if(validCurrency(cl.Currency) && cl.Limit!=null && cl.Limit.compareTo(BigDecimal.ZERO)==1)
                currencyLimitLookup.put(cl.Currency, cl.Limit);
            else
                fail("Invalid Currency Limit Entry!  >>  " + cl);

        try {
            for(PosTerminalElement pos: Terminals) {
                pos.validate();
                if (!StoreId.equals(pos.FacilityId))
                    throw new Exception("Invalid Pos Terminal in this Store!  >>  " + pos);
                if (!IsActive) pos.IsActive=false;
            }
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    private boolean validCurrency(String currency)
    {
        if (DefaultCurrency==null || DefaultCurrency.length()!=3) return false;
        else return true;
    }

    private boolean validRegex(String regexSource) {
        try {
            Pattern.compile(regexSource);
            return  true;
        } catch (PatternSyntaxException exception) {
            return false;
        }
    }

    public BigDecimal getCurrencyLimit(String currency)
    {
        if (currencyLimitLookup.containsKey(currency)) return currencyLimitLookup.get(currency);
        else return null;
    }

    public PosTerminalElement getTerminal(String terminalId){
        for(PosTerminalElement pos:Terminals) if (pos.PosTerminalId.equals(terminalId)) return pos;
        return null;
    }

    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Pos Store Data Element! " + reason + "  >>  " + this);
    }

    @Override
    public String toString() {
        return "StoreElement{" +
                "StoreId='" + StoreId + '\'' +
                ", StoreGroupId='" + StoreGroupId + '\'' +
                ", StoreName='" + StoreName + '\'' +
                ", IsActive=" + IsActive +
                '}';
    }
}
