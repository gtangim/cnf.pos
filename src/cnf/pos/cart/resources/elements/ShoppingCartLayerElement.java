package cnf.pos.cart.resources.elements;

/**
 * Created by RusselA on 3/29/2015.
 */
public class ShoppingCartLayerElement {
    public int LayerId;
    public SCLPrimaryType PrimaryTypeId;
    public SCLSecondaryType SecondaryTypeId;
    public int ConditionLayerId;
    public int ActionLayerId;

    public void validate() throws Exception
    {
        if (LayerId<1 || LayerId>100) fail("Invalid Shopping Cart Layer ID!");
        if (PrimaryTypeId==null) fail("Missing Primary Type ID!");
        if (ConditionLayerId>LayerId) fail("Invalid Condition Layer ID!");
        if (ActionLayerId>LayerId) fail("Invalid Action Layer ID!");

    }


    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Shopping Cart Layer Element! " + reason + " >> " + this);
    }

    @Override
    public String toString() {
        return "ShoppingCartLayerElement{" +
                "LayerId=" + LayerId +
                ", PrimaryTypeId=" + PrimaryTypeId +
                ", SecondaryTypeId=" + SecondaryTypeId +
                ", ConditionLayerId=" + ConditionLayerId +
                ", ActionLayerId=" + ActionLayerId +
                '}';
    }
}
