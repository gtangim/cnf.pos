package cnf.pos.cart.resources.elements;

import javolution.util.FastList;
import cnf.pos.cart.resources.datastructures.AtomicPrice;
import cnf.pos.cart.resources.datastructures.PriceResult;
import cnf.pos.util.Utility;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/10/11
 * Time: 9:56 AM
 * To change this template use File | Settings | File Templates.
 */
public class PriceElement {

    public String StoreId;
    public AtomicPrice ListPrice;
    public AtomicPrice DefaultPrice;
    public AtomicPrice CompetitivePrice;
    public AtomicPrice WholesalePrice;
    public AtomicPrice PromoPrice;
    public AtomicPrice SpecialPromoPrice;
    public AtomicPrice MinimumPrice;
    public AtomicPrice MaximumPrice;
    public List<BigDecimal> PackagePriceQuantityList;
    public List<AtomicPrice> PackagePriceList;
    public List<BigDecimal> PromoPackagePriceQuantityList;
    public List<AtomicPrice> PromoPackagePriceList;
    public BigDecimal Cost;


    public void validate() throws Exception
    {
        if (Utility.isEmpty(StoreId)) fail("Missing Product Store ID!");
        if (ListPrice==null) fail("Missing List Price!");
        if (PackagePriceList==null) PackagePriceList = FastList.newInstance();
        if (PackagePriceQuantityList==null) PackagePriceQuantityList = FastList.newInstance();
        if (PromoPackagePriceList==null) PromoPackagePriceList = FastList.newInstance();
        if (PromoPackagePriceQuantityList==null) PromoPackagePriceQuantityList = FastList.newInstance();
        if (PackagePriceList.size()!=PackagePriceQuantityList.size()) fail("Invalid Package Price entries!");
        if (Cost==null) fail("Missing Cost in Price Entry!");

        try {
            if (ListPrice!=null) ListPrice.validate();
            if (DefaultPrice!=null) DefaultPrice.validate();
            if (CompetitivePrice!=null) CompetitivePrice.validate();
            if (PromoPrice!=null) PromoPrice.validate();
            if (SpecialPromoPrice!=null) SpecialPromoPrice.validate();
            if (MinimumPrice!=null) MinimumPrice.validate();
            if (MaximumPrice!=null) MaximumPrice.validate();
            for(AtomicPrice p: PackagePriceList) p.validate();
            for(AtomicPrice p: PromoPackagePriceList) p.validate();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }
    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Product Price Entry! " + reason + " >> " + this);
    }




    public PriceResult getActivePrice(BigDecimal quantity)
    {
        if (quantity==null) quantity = BigDecimal.ONE;

        if (ListPrice==null) return new PriceResult("List price not found!");
        AtomicPrice activePrice=ListPrice;
        String priceType="LIST_PRICE";
        String clampType=null;

        // We dont want to use List Price unless there is no default price...
        if (DefaultPrice!=null && DefaultPrice.lessThanEqual(activePrice,false))
        {
            activePrice=DefaultPrice;
            priceType="DEFAULT_PRICE";
        }

        if (CompetitivePrice!=null && CompetitivePrice.lessThan(activePrice,false))
        {
            activePrice=CompetitivePrice;
            priceType="COMPETITIVE_PRICE";
        }

        //if (WholesalePrice!=null && WholesalePrice.lessThan(activePrice,false))
        //{
        //    activePrice=wholesalePrice;
        //    priceType="WHOLESALE_PRICE";
        //}

        AtomicPrice ppr = getPackagePrice(quantity);
        if (ppr!=null && ppr.lessThan(activePrice,false)) {
            activePrice = ppr;
            priceType = "PACKAGE_PRICE";
        }

        if (PromoPrice!=null && PromoPrice.lessThan(activePrice,false))
        {
            activePrice=PromoPrice;
            priceType="PROMO_PRICE";
        }

        AtomicPrice pppr = getPromoPackagePrice(quantity);
        if (pppr!=null && pppr.lessThan(activePrice,false)) {
            activePrice = pppr;
            priceType = "PROMO_PACKAGE_PRICE";
        }


        // Note: We dont use average cost or special promo price in the price Calculation
        //          The Special promo price is used sometimes by a product promo.



        // Now check if a price clamp is necessary...
        if (MinimumPrice!=null && (activePrice.lessThan(MinimumPrice,false)
                || activePrice.lessThan(MinimumPrice,true)))
        {
            activePrice=MinimumPrice;
            clampType="MINIMUM_PRICE";
        }

        if (MaximumPrice!=null && (activePrice.greaterThan(MaximumPrice,false)
                || activePrice.greaterThan(MaximumPrice,true)))
        {
            activePrice=MaximumPrice;
            clampType="MAXIMUM_PRICE";
        }

        return new PriceResult(activePrice,priceType,clampType,null,ListPrice,DefaultPrice);

    }




    private AtomicPrice getPackagePrice(BigDecimal qty) {
        if (PackagePriceList==null || PackagePriceList.size()==0) return null;
        int n = PackagePriceList.size();
        for (int i=n-1;i>=0;i--)
        {
            BigDecimal q = PackagePriceQuantityList.get(i);
            if (q.compareTo(BigDecimal.ZERO)<=0) continue;  // Do not allow an invalid package quantity
            AtomicPrice p = PackagePriceList.get(i);
            if (q.compareTo(qty)<=0) return p;
        }
        return null;
    }

    private AtomicPrice getPromoPackagePrice(BigDecimal qty) {
        if (PromoPackagePriceList==null || PromoPackagePriceList.size()==0) return null;
        int n = PromoPackagePriceList.size();
        for (int i=n-1;i>=0;i--)
        {
            BigDecimal q = PromoPackagePriceQuantityList.get(i);
            if (q.compareTo(BigDecimal.ZERO)<=0) continue;  // Do not allow an invalid package quantity
            AtomicPrice p = PromoPackagePriceList.get(i);
            if (q.compareTo(qty)<=0) return p;
        }
        return null;
    }

    @Override
    public String toString() {
        return "PriceElement{" +
                "StoreId='" + StoreId + '\'' +
                ", ListPrice=" + ListPrice +
                ", DefaultPrice=" + DefaultPrice +
                ", PromoPrice=" + PromoPrice +
                ", Cost=" + Cost +
                '}';
    }
}
