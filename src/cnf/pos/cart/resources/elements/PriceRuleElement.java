package cnf.pos.cart.resources.elements;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/15/11
 * Time: 8:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class PriceRuleElement implements Comparable<PriceRuleElement>{
    public String Id;
    public String Name;
    public boolean IsSale;
    public Integer Index;

    public List<PriceConditionElement> Conditions;
    public List<PriceActionElement> Actions;


    public PriceRuleElement(){

    }
    /*public PriceRuleElement(ProductPriceRule pRule, int index)
    {
        Id = pRule.getProductPriceRuleId();
        Name = pRule.getRuleName();
        IsSale = "Y".equals(pRule.getIsSale());
        Conditions = FastList.newInstance();
        Actions = FastList.newInstance();
        this.Index = new Integer(index);
    } */

    public void addCondition(PriceConditionElement cond)
    {
        Conditions.add(cond);
    }

    public void addAction(PriceActionElement act)
    {
        Actions.add(act);
    }

    public boolean isConditionSatisfied(Map params)
    {
        for(PriceConditionElement cond: Conditions)
        {
            boolean res = cond.eval(params);
            if (!res) return false;
        }
        return true;
    }
    public double eval(PriceElement priceElement, BigDecimal qty, boolean useGrossPrice)
    {
        double currentPrice = priceElement.ListPrice.getPrice(useGrossPrice).doubleValue();
        for (PriceActionElement action: Actions)
        {
            currentPrice = action.eval(currentPrice,priceElement,qty,useGrossPrice);
            if (action.stopLoop()) break;
        }
        return currentPrice;
    }


    public String getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public boolean isSale() {
        return IsSale;
    }

    @Override
    public boolean equals(Object o)
    {
        return Index.equals(((PriceRuleElement)o).Index);
    }

    @Override
    public int hashCode()
    {
        return Index.hashCode();
    }

    public int compareTo(PriceRuleElement priceRuleElement) {
        return Index.compareTo(priceRuleElement.Index);
    }
}
