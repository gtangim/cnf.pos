package cnf.pos.cart.resources.elements;

import cnf.pos.util.Utility;

import java.math.BigDecimal;

/**
 * Created by russela on 3/30/2015.
 */
public class ProductIdentificationElement {
    public String IdType;
    public String ProductCode;
    public BigDecimal Quantity;
    public String ProductId;

    public ProductIdentificationElement(){
    }

    public ProductIdentificationElement(String idType, String productCode, String productId, BigDecimal quantity){
        IdType = idType;
        if (Utility.isEmpty(idType)) IdType="PK";
        ProductCode = productCode;
        ProductId = productId;
        Quantity = quantity;
        if (Quantity==null) Quantity=BigDecimal.ONE;
    }


    public void validate() throws Exception
    {
        if (Utility.isEmpty(IdType)) fail("Missing ID Type!");
        if (Utility.isEmpty(ProductId)) fail("Missing Product Primary ID!");
        if (Utility.isEmpty(ProductCode)) fail("Missing Product Code Description!");
        if (Quantity==null) Quantity = BigDecimal.ONE;
    }

    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Uom Conversion Entry! " + reason + " >> " + this);
    }

    @Override
    public String toString() {
        return "ProductIdentificationElement{" +
                "IdType='" + IdType + '\'' +
                ", ProductCode='" + ProductCode + '\'' +
                ", Quantity=" + Quantity +
                '}';
    }
}
