package cnf.pos.cart.resources.elements;

import cnf.pos.util.Utility;

import java.math.BigDecimal;

/**
 * Created by RusselA on 3/28/2015.
 */
public class BillingAccountElement {
    public String BillingAccountId;
    public String Description;
    public BigDecimal CreditLimit;
    public String Currency;

    public void validate() throws Exception
    {
        if (Utility.isEmpty(BillingAccountId)) fail("Empty Billing Account ID!");
        else if (!Utility.isInteger(BillingAccountId)) fail("Invalid Billing Account ID!");
        if (Utility.isEmpty(Description)) fail("Missing Billing account description!");
        if (CreditLimit==null) fail("Missing Credit Limit!");
        if (Currency==null || Currency.length()!=3) fail("Missing/Invalid Billing Account Currency!");
    }

    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Billing Account Element! " + reason + "  >>  " + this);
    }

    @Override
    public String toString() {
        return "BillingAccountElement{" +
                "BillingAccountId='" + BillingAccountId + '\'' +
                ", Description='" + Description + '\'' +
                ", CreditLimit=" + CreditLimit +
                ", Currency='" + Currency + '\'' +
                '}';
    }
}
