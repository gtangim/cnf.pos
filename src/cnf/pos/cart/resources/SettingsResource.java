package cnf.pos.cart.resources;

import apu.jpos.util.StringUtil;
import com.google.gson.*;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import cnf.pos.util.Debug;
import cnf.pos.cart.resources.elements.*;
import cnf.pos.device.DeviceLoader;
import cnf.pos.util.Utility;
import org.joda.time.*;
import org.joda.time.format.*;

import java.io.File;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 1/26/11
 * Time: 5:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class SettingsResource {
    //public static final String AppVersion = "CNF POS 3.0";

    public static final String module = SettingsResource.class.getName();

    private Gson serializer = new GsonBuilder()
            .registerTypeAdapter(DateTime.class, new JsonSerializer<DateTime>(){
                @Override
                public JsonElement serialize(DateTime json, Type typeOfSrc, JsonSerializationContext context) {
                    return new JsonPrimitive(ISODateTimeFormat.dateTime().print(json));
                }
            })
            .registerTypeAdapter(LocalTime.class, new JsonSerializer<LocalTime>(){
                @Override
                public JsonElement serialize(LocalTime json, Type typeOfSrc, JsonSerializationContext context) {
                    return new JsonPrimitive(ISODateTimeFormat.time().print(json));
                }
            })
            .registerTypeAdapter(DateTime.class, new JsonDeserializer<DateTime>() {
                @Override
                public DateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                    DateTime dt = ISODateTimeFormat.dateTime().parseDateTime(json.getAsString());
                    return dt;
                }
            })
            .registerTypeAdapter(LocalTime.class, new JsonDeserializer<LocalTime>() {
                @Override
                public LocalTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                    LocalTime dt = ISODateTimeFormat.timeParser().parseLocalTime(json.getAsString());
                    return dt;
                }
            })
            .create();
    public static String DataFolder = "./resources/data/";
    public static String StateFileName = "PosState.json";
    protected PosConfig posConfig = null;
    protected PosCoreData coreData = null;
    protected PosSecurityData securityData = null;
    protected BigDecimal tare = BigDecimal.ZERO;
    protected Properties properties;
    protected PosState state = null;
    DateTimeParser[] parsers = {
            DateTimeFormat.forPattern( "h:mm:ssa" ).getParser(),
            DateTimeFormat.forPattern( "h:mm:ss a" ).getParser(),
            DateTimeFormat.forPattern( "h:mma" ).getParser(),
            DateTimeFormat.forPattern( "h:mm a" ).getParser()
    };
    DateTimeFormatter format = new DateTimeFormatterBuilder().append( null, parsers ).toFormatter();

    public SettingsResource(PosConfig cfg, PosCoreData core, PosSecurityData sec) throws Exception {
        //super(session);
        //resourceName = "POS Settings";
        properties = Utility.getProperties(ResourceManager.OldPosPropertyFileName);
        posConfig = cfg;
        // Freemarker Resource...
        String receiptTemplateLocation = properties.getProperty("ReceiptTemplateLocation");
        if (receiptTemplateLocation==null) throw new Exception("Pos Receipt Template Location not found in session!");
        Configuration freemarkerConfig = new Configuration();
        freemarkerConfig.setDirectoryForTemplateLoading(
                new File(receiptTemplateLocation));
        freemarkerConfig.setObjectWrapper(new DefaultObjectWrapper());
        if (DeviceLoader.receipt!=null) DeviceLoader.receipt.setFreemarkerConfig(freemarkerConfig);
        updateDataModel(core, sec);
    }

    public void updateDataModel(PosCoreData core, PosSecurityData sec) throws Exception {
        if (core == null) throw new Exception("Core Data cannot be null!");
        if (sec == null) throw new Exception("Security Data cannot be null!");
        boolean updated = (securityData !=null);
        if (securityData ==null || !securityData.SnapshotId.equals(sec.SnapshotId)
                || !core.SnapshotId.equals(core.SnapshotId)) {
            securityData = sec;
            coreData = core;
            state = loadPosState();
            //checkFiles();
            if (updated) Debug.log("Settings Resource has been updated!");
        }
    }


    public String getPosTerminalId() {
        return posConfig.StoreId + "-" + Integer.toString(posConfig.TerminalNumber);
    }

    public String getPosTerminalPrefix() {
        return posConfig.StoreId + "T" + Integer.toString(posConfig.TerminalNumber);
    }

    public String getLegacyOrderIdPrefix() {
        return posConfig.StoreId + "T" + Integer.toString(posConfig.TerminalNumber) + "X";
    }

    public boolean setTare(String tareValue) {
        try {
            synchronized (this) {
                BigDecimal newTare = new BigDecimal(tareValue);
                if (newTare == null) {
                    tare = BigDecimal.ZERO;
                    return false;
                }
                tare = newTare;
            }
            return true;
        } catch (Exception ex) {
            Debug.logError(ex, "ERROR: Failed to assign a tare value!", module);
            tare = BigDecimal.ZERO;
            return false;
        }

    }

    public StoreElement getStore() {
        return securityData.getStore();
    }


    public PosTerminalElement getTerminal() {
        return securityData.getTerminal();
    }

    public boolean isShowZeroTax() {
        return securityData.getStore().ShowZeroTax;
    }

    public boolean isAlwaysPopDrawer() {
        return securityData.getTerminal().EnableAlwaysPopDrawer;
    }

    public String getCurrency() {
        return securityData.getStore().DefaultCurrency;
    }

    public String getCurrencyLimitWarning() {
        return securityData.getStore().CurrencyLimitWarning;
    }

    public String filterReceipt(String receiptId) {
        if (receiptId==null) return null;
        else if (receiptId.matches(securityData.getStore().ReceiptValidation)) return receiptId;
        else
        {
            try
            {
                long v = Long.parseLong(receiptId);
                if (v<10000000000000L) return null;
                return receiptId;
            }
            catch (Exception ex)
            {
            }
            try
            {
                String rid = Utility.decompressNumber(receiptId);
                return rid;
            }
            catch (Exception ex)
            {
            }
        }
        return null;
    }

    public PosState loadPosState() throws Exception {
        String stateJson = bongo.util.Utility.readFile(DataFolder+StateFileName);
        if (StringUtil.empty(stateJson))
            throw  new Exception("Unable to read the current POS State from the state file '"+StateFileName+"'!");
        PosState posState = serializer.fromJson(stateJson,PosState.class);
        posState.DeviceId = Integer.parseInt(posConfig.MachineId);
        posState.Alias = getPosTerminalPrefix();
        return posState;
    }

    public void savePosState(long documentId) throws Exception{
        int lastSeqId = (int)((documentId / 1000) % 10000);
        state.Alias = getPosTerminalPrefix();
        state.DeviceId = Integer.parseInt(posConfig.MachineId);
        state.CurrentOperationDay = getCurrentOperationDay();
        if (lastSeqId>=0) state.LastSequenceNumber = lastSeqId;
        String json = serializer.toJson(state, PosState.class);
        bongo.util.Utility.stringToFile(DataFolder + StateFileName, json);
    }

    DateTime lastOperationDay = null;
    public DateTime getCurrentOperationDay(){
        DateTime now  = new DateTime();
        now = now.withTime(0,0,0,0);
        if (lastOperationDay!=null){
            long duration = new Duration(lastOperationDay,now).getMillis();
            if (duration<0) now = lastOperationDay;
        }
        lastOperationDay = now;
        return now;
    }

    Random rnd = new Random();
    int lastSeq = -1;
    int lastRand = 0;
    public long getNextDocumentId() throws Exception
    {
        // [TillId 4D] [DATE 6D] [SEQ 4D] [RND 3D]
        int nextSeqId = state.LastSequenceNumber+1;
        if (nextSeqId>9999) nextSeqId=0;
        int rand = rnd.nextInt(999);
        if (lastSeq==nextSeqId) rand=lastRand;
        else lastSeq = nextSeqId;

        PosState lastState = loadPosState();
        if (lastState.CurrentOperationDay.compareTo(getCurrentOperationDay())!=0) nextSeqId=0;
        long id = (long)state.DeviceId * 10000000000000L;
        DateTime dt = new DateTime();
        long dtl = Long.parseLong(dt.toString("yyMMdd"));
        id += dtl * 10000000L;
        id+=nextSeqId * 1000;
        id+= Math.abs(rand);
        return id;
    }

    private Path getOperationDayPath(DateTime operationDay){
        String alias = getPosTerminalPrefix();
        String oDay = operationDay.toString("yyyy_MM_dd");
        String path = bongo.util.Utility.appendDir(DataFolder, alias+ "/"+oDay+"/");
        Path dir = FileSystems.getDefault().getPath( path );
        return dir;
    }
    public void checkFiles() throws Exception{

        int maxSeq=-1;
        //boolean dateInvalid = false;
        long now = Long.parseLong(new DateTime().toString("yyyyMMddHHmmss"));
        Path currentPath = getOperationDayPath(getCurrentOperationDay());
        Path nextPath = getOperationDayPath(getCurrentOperationDay().plusDays(1));
        if (Files.exists(nextPath))
            throw new Exception("It appears that the system clock has been changed and there is already data for the next day!");
        else if (Files.exists(currentPath))
        {
            DirectoryStream<Path> stream = Files.newDirectoryStream(currentPath, "*.json");
            for(Path entry:stream){
                try {
                    String[] fnameParts = entry.getFileName().toString().toLowerCase()
                            .replace(".json","").split("_");
                    long id = Long.parseLong(fnameParts[3]);
                    int seq = (int)((id/1000) % 10000);
                    if (seq>maxSeq) maxSeq=seq;
                    //long fdt = Long.parseLong(fnameParts[2]);
                    //if (fdt>now) dateInvalid = true;
                }
                catch (Exception ex) {}
                //if (dateInvalid)
                //    throw  new Exception("The datetime in file '"+entry.getFileName().toString()+"' exceeds the current system datetime!");
            }
            if (maxSeq!=state.LastSequenceNumber)
                throw  new Exception("The last transaction sequence number in PosState.json doesnt match the files in the data folder!");
        }
        else if (state.LastSequenceNumber>=0)
            throw  new Exception("The last transaction sequence number in PosState.json doesnt match the files in the data folder!");
    }

    /*DateTime lastCheck = null;
    public boolean checkTime(){
        if (lastCheck==null)
        {
            lastCheck = new DateTime();
            return true;
        }
        DateTime now = new DateTime();
        if (lastCheck.isAfter(now)) return false;
        else if (now.getMillis()-lastCheck.getMillis()>60000) return false;
        lastCheck = now;
        return true;
    }*/



    public BigDecimal getCurrencyLimit(String currency) {
        return securityData.getStore().getCurrencyLimit(currency);
    }

    public int getScale() {
        return securityData.getStore().CurrencyScale;
    }

    public RoundingMode getRoundingMode() {
        return securityData.getStore().RoundingMode;
    }

    public String getSystemUserId() {
        return securityData.getStore().SystemUserId;
    }

    public static String getModule() {
        return module;
    }

    public List<ReasonElement> getPaidInReasons() {
        return coreData.PosCashInReasons;
    }

    public List<ReasonElement> getPaidOutReasons() {
        return coreData.PosCashOutReasons;
    }


    public BigDecimal getTare() {
        BigDecimal rtare = null;
        synchronized (this) {
            rtare = tare;
        }
        return rtare;
    }

    public String getTareUom() {
        return securityData.getTerminal().TareUomId;
    }


    public List<String> getPriceLabelPatterns() {
        return posConfig.PriceLabelPatterns;
    }

    public List<String> getWeightLabelPatterns() {
        return posConfig.WeightLabelPatterns;
    }

    public List<String> getInputFilters() {
        return posConfig.InputFilters;
    }

    public List<String> getPromoCodePatterns() {
        return posConfig.PromoCodePatterns;
    }

    public List<String> getCustomerPatterns() {
        return posConfig.CustomerPatterns;
    }

    public String getTaxLiteral() {
        return securityData.getStore().TaxLiteral;
    }

    public List<ReasonElement> getOverrideReasons() {
        return coreData.PosOverrideReasons;
    }
    public List<ReasonElement> getSaleDiscountReasons() {
        return coreData.PosSaleDiscountReasons;
    }
    public List<ReasonElement> getItemDiscountReasons() {
        return coreData.PosItemDiscountReasons;
    }

    public boolean isOverridePromoAllowed(String overrideType) {
        return coreData.PromoEnabledPriceOverrides.contains(overrideType);
    }

    public String getDefaultUom() {
        return securityData.getStore().DefaultUomId;
    }

    public String getWeightLabelUom() {
        return securityData.getStore().WeightLabelUomId;
    }

    public BigDecimal getEmployeePricingFactor() {
        StoreElement store = securityData.getStore();
        if (store.EmployeePricingCostPlusPercent == null) return BigDecimal.ONE;
        else return store.EmployeePricingCostPlusPercent.movePointLeft(2).add(BigDecimal.ONE);
    }

    public int getAutoLockTerminalMs() {
        return securityData.getTerminal().AutoLockTerminalMs;
    }

    public boolean isOnScreenKeyboard() {
        return securityData.getTerminal().EnableOnScreenKeyboard;
    }

    public boolean isAllowPosCashback() {
        return securityData.getTerminal().AllowPosCashBack;
    }

    public boolean alwaysPrintCustomerPaymentSlips() {
        return securityData.getTerminal().AlwaysPrintCustomerPaymentSlips;
    }

    public boolean alwaysPrintMerchantPaymentSlips() {
        return securityData.getTerminal().AlwaysPrintMerchantPaymentSlips;
    }

    public String getCurrencyFormat() {
        return securityData.getStore().CurrencyFormat;
    }

    public String getShortCurrencyFormat() {
        return securityData.getStore().ShortCurrencyFormat;
    }

    public String getStartTab() {
        return "sale";  // This is not a constant behavior in the POS.
    }

    public List<ShoppingCartLayerElement> getCartLayers() {
        return coreData.ShoppingCartLayers;
    }

    //public HashMap<String, String> getMap() {
    //    return map;
    //}

    public boolean printReceiptBeforeCommit() {
        return securityData.getTerminal().EnablePrintReceiptBeforeCommit;
    }

    public boolean calculatePriceLabelQuantity() {
        return securityData.getStore().EnableCalculatePriceLabelQuantity;
    }

    public boolean promptForReceipt() {
        return securityData.getTerminal().EnablePromptForReceipt;
    }

    public boolean isStackingDiscount() {
        return securityData.getStore().IsStackingDiscount;
    }

    public BigDecimal getQuantityLimitCount() {
        return securityData.getTerminal().QuantityLimitCount;
    }

    public BigDecimal getQuantityLimitWeight() {
        return securityData.getTerminal().QuantityLimitWeight;
    }

    public BigDecimal getPaymentLimit() {
        return securityData.getTerminal().PaymentLimit;
    }

    public Properties getProperties() {
        return properties;
    }

    public int getUserLockoutThreshold(){
        return securityData.getStore().UserLockoutThreshold;
    }

    public int getUserLockoutDurationMs(){
        return securityData.getStore().UserLockoutDurationMs;
    }

    public int getAutoCloseIntervalMs() {return 10000;} // 60 Seconds...

    public int getPennyRounding() {return securityData.getStore().RoundPenniesTo;}

    public boolean isUserEventsPostEnabled() {return securityData.getStore().PostUserEvents;}

    //public boolean performPinPadCloseBatch() {return false;}
}
