package cnf.pos.cart.resources;

import javolution.util.FastList;
import cnf.pos.util.Debug;
import cnf.pos.cart.resources.elements.PosCoreData;
import cnf.pos.cart.resources.elements.TaxElement;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/7/11
 * Time: 10:53 AM
 * To change this template use File | Settings | File Templates.
 */
public class TaxResource {

    protected PosCoreData coreData = null;
    protected List<TaxElement> storeTaxes;

    public TaxResource(PosCoreData core, String storeId) throws Exception
    {
        updateDataModel(core, storeId);
    }

    public void updateDataModel(PosCoreData core, String storeId) throws Exception
    {
        boolean updated = (coreData!=null);
        if (core==null) throw new Exception("Pos Core Data cannot be null!");
        if (coreData==null || !coreData.SnapshotId.equals(core.SnapshotId)) {
            coreData = core;
            storeTaxes = FastList.newInstance();
            for (TaxElement t : coreData.Taxes) if (t.IsActive && t.ProductStoreId.equals(storeId)) storeTaxes.add(t);
            if (updated) Debug.log("Tax Resource has been updated!");
        }
    }


    public List<TaxElement> getStoreTaxes()
    {
        return storeTaxes;
    }





}
