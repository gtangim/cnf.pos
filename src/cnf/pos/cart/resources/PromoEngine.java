package cnf.pos.cart.resources;

import cnf.pos.cart.resources.elements.PromoConditionElement;
import javolution.util.FastList;
import javolution.util.FastMap;
import javolution.util.FastSet;
import cnf.pos.cart.PosShoppingCartItem;
import cnf.pos.cart.resources.datastructures.PromoApplication;
import cnf.pos.cart.resources.datastructures.ValueTreeNode;
import cnf.pos.cart.resources.elements.PromoElement;
import cnf.pos.cart.resources.elements.PromoRuleElement;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 3/8/11
 * Time: 2:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class PromoEngine {
    public static final String PROMO_AMOUNT = "pa";
    public static final String PROMO_QUANTITY = "pq";
    public static final String PROMO_SUM = "psum";

    protected ResourceManager r;
    protected FastList<PosShoppingCartItem> items;
    protected FastList<PromoApplication> promoApplications;
    protected PromoApplication globalPromoApplications;

    // Internal resources...
    protected ArrayList<PosShoppingCartItem> sortedItems;
    protected FastSet<Integer> fullScope;
    protected double[] condValues;
    protected double[] actValues;
    protected ValueTreeNode values;
    protected Map params;
    protected String cLayer;
    protected String aLayer;
    protected int n;
    protected Set<String> categories;
    protected FastMap<String,ArrayList<String>> promoCodes = null;
    protected boolean stackingModel=true;


    public PromoEngine(FastList<PosShoppingCartItem> items, ResourceManager resource)
    {
        this.items=items;
        this.r = resource;
        stackingModel = r.getSettings().isStackingDiscount();
    }


    public void prepareDataStructures()
    {
        int ind=0;
        fullScope=FastSet.newInstance();
        n=items.size();
        condValues = new double[n];
        actValues = new double[n];
        //categories = r.getMemberships().getComplexCategories();
        double sumA=0;
        double sumQ=0;

        List<PromoConditionElement> subValuedConditions = FastList.newInstance();
        List<PromoElement> promos = r.getPromos().getPromos();
        for(PromoElement promo:promos) {
            ArrayList<String> codes = promoCodes.get(promo.Id);
            if (promo.IsCodeRequired && (codes==null || codes.size() == 0)) continue;
            for(PromoRuleElement rule:promo.Rules)
                for(PromoConditionElement cond: rule.Conditions)
                    if (cond.requireSubValuesForCond()) {
                        cond.resetAccumulators();
                        subValuedConditions.add(cond);
                    }
        }

        for(PosShoppingCartItem item:items)
        {
            item.setCartIndex(ind);
            String index = Integer.toString(ind);
            double cValue = values.getValue(cLayer, index);
            double aValue = values.getValue(aLayer, index);
            if(item.isPromoAllowed())
            {
                fullScope.add(ind);
                item.setPromoValue(aValue);
                /*for(String cat:categories)
                    if (r.getMemberships().isMemberComplex(cat,item.getProductId()))
                    {
                        double totV = values.getValue(cat, PROMO_AMOUNT);
                        if (Double.isNaN(totV)) totV=0;
                        totV+=cValue;
                        values.setValue(cat,PROMO_AMOUNT,totV);
                        double totQ = values.getValue(cat,PROMO_QUANTITY);
                        if (Double.isNaN(totQ)) totQ=0;
                        totQ+=item.getQuantity().doubleValue();
                        values.setValue(cat,PROMO_QUANTITY,totQ);
                    }*/
            }
            double qty = item.getQuantity().doubleValue();
            sumA+=cValue;
            sumQ+=qty;
            for(PromoConditionElement cond: subValuedConditions) cond.accumulate(item.getProductId(),qty,cValue);

            ind++;
        }
        values.setValue(PROMO_SUM,PROMO_AMOUNT,sumA);
        values.setValue(PROMO_SUM,PROMO_QUANTITY,sumQ);
        sortedItems = new ArrayList<PosShoppingCartItem>();
        //for(PosShoppingCartItem item:items)
        //    if (item.isPromoAllowed()) sortedItems.add(item);
        sortedItems.addAll(items);
        Collections.sort(sortedItems);
        promoApplications = new FastList<PromoApplication>();
        globalPromoApplications = new PromoApplication(null);
    }

    public void applyPromo(Map params, String condLayer, String actLayer, boolean useGrossPrice)
    {
        this.params=params;
        this.values=(ValueTreeNode)params.get("VALUES");
        this.promoCodes = (FastMap<String,ArrayList<String>>)params.get("CODES");
        this.aLayer = actLayer;
        this.cLayer = condLayer;
        prepareDataStructures();

        // Now iterate all applicable promotions...
        List<PromoElement> promos = r.getPromos().getPromos();
        for(PromoElement promo:promos)
        {
            ArrayList<String> codes = promoCodes.get(promo.Id);
            if (codes==null) codes = new ArrayList<String>();
            if (promo.IsCodeRequired && codes.size()==0) continue;
            PromoRuleElement rule= promo.selectRule(params,condLayer
                    ,r.getSettings().getScale(),r.getSettings().getRoundingMode());
            if (rule!=null)
            {
                int useLimit = promo.UseLimitOrder;
                if (promo.IsManual)
                {
                    if (useLimit>codes.size()) useLimit=codes.size();
                }

                // Apply this promo...
                PromoApplication pa = new PromoApplication(promo);

                FastSet<Integer> conflictScope = FastSet.newInstance();
                for(PromoApplication x:promoApplications)
                    if (x.getPromo().isExclusiveTo(pa.getPromo()))
                        conflictScope.addAll(x.getScope());

                // Now apply each Actions of the promo rule...
                //if (rule.getReservationAmount()!=null)
                //{
                    // Apply promo using reservation strategy...
                    FastSet<Integer> resedueScope = FastSet.newInstance();
                    resedueScope.addAll(fullScope);
                    resedueScope.removeAll(conflictScope);
                    rule.applyPromo(r,pa,globalPromoApplications,items,sortedItems,resedueScope
                            ,useGrossPrice,stackingModel,codes,useLimit);

                /*}
                else
                {
                    // Apply promo to entire cart...
                    if (conflictScope.size()==0)
                    {
                        // We can only apply this promo if there is no other promo
                        // already applied to the cart that conflicts with the current promo
                        rule.applyPromo(r,pa,globalPromoApplications,items,sortedItems,null,useGrossPrice,codes,useLimit);
                    }
                }*/
                if (!pa.isEmpty()) {
                    pa.distributePromo();
                    promoApplications.add(pa);
                }
            }
        }

    }


    public FastList<PromoApplication> getPromoApplications() {
        return promoApplications;
    }

    public PromoApplication getGlobalPromoApplications() {
        return globalPromoApplications;
    }
}
