package cnf.pos.cart.resources.datastructures;

import javolution.util.FastList;

import java.util.HashSet;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/17/11
 * Time: 2:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class StringTreeNode {
    protected String name;
    protected FastList<StringTreeNode> children;

    public StringTreeNode(String name)
    {
        this.name = name;
        children = FastList.newInstance();
    }

    public HashSet<StringTreeNode> getChildren(boolean recurse)
    {
        HashSet<StringTreeNode> c = new HashSet<StringTreeNode>();
        c.addAll(children);
        if (recurse)
            for (StringTreeNode x : children)
                c.addAll(x.getChildren(recurse));
        return c;
    }

    public FastList<String> getChildrenNames(boolean recurse)
    {
        HashSet<StringTreeNode> c = getChildren(recurse);
        FastList<String> names = FastList.newInstance();
        for(StringTreeNode x:c) names.add(x.name);
        return names;
    }

    public void addChild(StringTreeNode child)
    {
        children.add(child);
    }
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o)
    {
        if (o instanceof String) return name.equals((String)o);
        else if (o instanceof StringTreeNode) return name.equals(((StringTreeNode)o).name);
        else return false;
    }

    @Override
    public int hashCode()
    {
        return name.hashCode();
    }
}
