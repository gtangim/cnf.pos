package cnf.pos.cart.resources.datastructures;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/11/11
 * Time: 11:29 AM
 * To change this template use File | Settings | File Templates.
 */
public final class AtomicPrice {
    // All comparison and price selection should be based on netprice...

    public BigDecimal NetPrice;
    public BigDecimal GrossPrice;

    public AtomicPrice(BigDecimal netPrice, BigDecimal grossPrice)
    {
        if (netPrice==null) netPrice = BigDecimal.ZERO.setScale(2,RoundingMode.HALF_EVEN);
        this.NetPrice = netPrice;
        this.GrossPrice = grossPrice;
        if (this.GrossPrice ==null) this.GrossPrice =netPrice;
    }
    public AtomicPrice(){
    }


    public boolean greaterThan(AtomicPrice b,boolean useGrossPrice)
    {
        return getPrice(useGrossPrice).compareTo(b.getPrice(useGrossPrice))>0;
    }
    public boolean greaterThanEqual(AtomicPrice b,boolean useGrossPrice)
    {
        return getPrice(useGrossPrice).compareTo(b.getPrice(useGrossPrice))>=0;
    }
    public boolean equal(AtomicPrice b,boolean useGrossPrice)
    {
        return getPrice(useGrossPrice).compareTo(b.getPrice(useGrossPrice))==0;
    }
    public boolean lessThan(AtomicPrice b,boolean useGrossPrice)
    {
        return getPrice(useGrossPrice).compareTo(b.getPrice(useGrossPrice))<0;
    }
    public boolean lessThanEqual(AtomicPrice b,boolean useGrossPrice)
    {
        return getPrice(useGrossPrice).compareTo(b.getPrice(useGrossPrice))<=0;
    }


    public BigDecimal getNetPrice() {
        return NetPrice;
    }

    public BigDecimal getGrossPrice() {
        return GrossPrice;
    }

    public BigDecimal getPrice(boolean useGrossPrice)
    {
        if (useGrossPrice) return GrossPrice;
        else return NetPrice;
    }

    public double getNetPriceDouble()
    {
        return NetPrice.doubleValue();
    }
    public double getGrossPriceDouble()
    {
        return GrossPrice.doubleValue();
    }

    public void validate() throws Exception
    {
        if (NetPrice==null || GrossPrice==null) fail("Price value not set!");
    }

    private void fail(String reason) throws Exception
    {
        throw new Exception("Invalid Atomic Price! " + reason);
    }

    @Override
    public String toString() {
        return NetPrice.toString();
    }
}
