package cnf.pos.cart.resources.datastructures;

import java.math.BigDecimal;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/15/11
 * Time: 10:50 AM
 * To change this template use File | Settings | File Templates.
 */
public final class PriceResult {
    AtomicPrice price;
    String priceRuleId;
    String priceType;
    String clampType;
    boolean isValid;
    String priceError;
    //
    AtomicPrice listPrice;
    AtomicPrice defaultPrice;
    public PriceResult(AtomicPrice price, String priceType, String clampType, String priceRuleId
            , AtomicPrice listPrice, AtomicPrice defaultPrice)
    {
        this.price=price;
        this.priceType=priceType;
        this.clampType=clampType;
        this.priceRuleId=priceRuleId;
        isValid=true;
        priceError=null;
        this.listPrice = listPrice;
        this.defaultPrice = defaultPrice;
    }

    public PriceResult(String priceError)
    {
        price=null;
        priceType=null;
        clampType=null;
        isValid=false;
        this.priceError=priceError;
        listPrice = null;
    }

    public AtomicPrice getPrice() {
        return price;
    }

    public String getPriceType() {
        return priceType;
    }

    public String getClampType() {
        return clampType;
    }

    public boolean isValid() {
        return isValid;
    }

    public String getPriceError() {
        return priceError;
    }

    public String getPriceRuleId() {
        return priceRuleId;
    }

    public AtomicPrice getListPrice() {
        return listPrice;
    }

    public BigDecimal getPriceSaving(boolean useGrossPrice) {
        BigDecimal difference = BigDecimal.ZERO;
        AtomicPrice refPrice = listPrice;
        if (defaultPrice!=null)  refPrice=defaultPrice;

        if (refPrice!=null && price!=null) {
            if (refPrice.greaterThan(price, useGrossPrice)) {
                difference = refPrice.getPrice(useGrossPrice)
                        .subtract(price.getPrice(useGrossPrice))
                        .setScale(2, BigDecimal.ROUND_HALF_EVEN);
            }
        }
        return difference;
    }
}
