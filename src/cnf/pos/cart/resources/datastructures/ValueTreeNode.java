package cnf.pos.cart.resources.datastructures;

import javolution.util.FastMap;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/17/11
 * Time: 2:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class ValueTreeNode {
    protected static int nextIndex;

    protected double value;
    protected FastMap<String,ValueTreeNode> children;
    protected int level;
    protected Integer index;

    public ValueTreeNode(double value)
    {
        this.value = value;
        children = null;
        level=0;
        index = nextIndex++;
    }

    public ValueTreeNode(double value, int level)
    {
        this.value = value;
        this.children = null;
        this.level=level;
        index = nextIndex++;
    }

    @Override
    public ValueTreeNode clone()
    {
        ValueTreeNode temp = new ValueTreeNode(value,level);
        if (children!=null)
        {
            temp.children=FastMap.newInstance();
            temp.children.putAll(children);
        }
        return temp;
    }


    public void addChild(String key, ValueTreeNode child)
    {
        if (child==null) return;
        if (children==null) children=FastMap.newInstance();
        child.level = level+1;
        children.put(key, child);
    }
    public void addChildValue(String key, double val)
    {
        if (children==null) children=FastMap.newInstance();
        children.put(key,new ValueTreeNode(val,level+1));
    }

    public ValueTreeNode getChild(String key)
    {
        if (children==null) children=FastMap.newInstance();
        return children.get(key);
    }

    public double getValue() {
        return value;
    }

    public double getValue(String key0)
    {
        if (children==null) children=FastMap.newInstance();
        ValueTreeNode temp;
        temp=children.get(key0);
        if (temp==null) return Double.NaN;
        return temp.getValue();
    }

    public double getValue(String key0, String key1)
    {
        if (children==null) children=FastMap.newInstance();
        ValueTreeNode temp;
        temp=children.get(key0);
        if (temp==null) return Double.NaN;
        return temp.getValue(key1);
    }

    public double getValue(String key0, String key1, String key2)
    {
        if (children==null) children=FastMap.newInstance();
        ValueTreeNode temp;
        temp=children.get(key0);
        if (temp==null) return Double.NaN;
        return temp.getValue(key1,key2);
    }

    public double getValue(String key0, String key1, String key2, String key3)
    {
        if (children==null) children=FastMap.newInstance();
        ValueTreeNode temp;
        temp=children.get(key0);
        if (temp==null) return Double.NaN;
        return temp.getValue(key1,key2,key3);
    }

    public double getValue(String key0, String key1, String key2, String key3, String key4)
    {
        if (children==null) children=FastMap.newInstance();
        ValueTreeNode temp;
        temp=children.get(key0);
        if (temp==null) return Double.NaN;
        return temp.getValue(key1,key2,key3,key4);
    }

    public void setValue(double value) {
        this.value = value;
    }

    public void setValue(String key0, double value) {
        if (children==null) children=FastMap.newInstance();
        ValueTreeNode v = children.get(key0);
        if (v==null) children.put(key0,new ValueTreeNode(value));
        else v.setValue(value);
    }

    public void setValue(String key0, String key1, double value) {
        if (children==null) children=FastMap.newInstance();
        ValueTreeNode temp;
        temp=children.get(key0);
        if (temp==null)
        {
            temp = new ValueTreeNode(0);
            children.put(key0,temp);
        }
        temp.setValue(key1,value);
    }

    public void setValue(String key0, String key1, String key2, double value) {
        if (children==null) children=FastMap.newInstance();
        ValueTreeNode temp;
        temp=children.get(key0);
        if (temp==null)
        {
            temp = new ValueTreeNode(0);
            children.put(key0,temp);
        }
        temp.setValue(key1,key2,value);
    }

    public void setValue(String key0, String key1, String key2, String key3, double value) {
        if (children==null) children=FastMap.newInstance();
        ValueTreeNode temp;
        temp=children.get(key0);
        if (temp==null)
        {
            temp = new ValueTreeNode(0);
            children.put(key0,temp);
        }
        temp.setValue(key1,key2,key3,value);
    }

    public void setValue(String key0, String key1, String key2, String key3, String key4, double value) {
        if (children==null) children=FastMap.newInstance();
        ValueTreeNode temp;
        temp=children.get(key0);
        if (temp==null)
        {
            temp = new ValueTreeNode(0);
            children.put(key0,temp);
        }
        temp.setValue(key1,key2,key3,key4,value);
    }

    @Override
    public boolean equals(Object o)
    {
        if (o instanceof String) return getUniqueKey().equals((String)o);
        else if (o instanceof ValueTreeNode) return getUniqueKey().equals(((ValueTreeNode)o).getUniqueKey());
        else return false;
    }

    @Override
    public int hashCode()
    {
      return index.hashCode();
    }

    public String getUniqueKey()
    {
        return Integer.toString(index);
    }
}
