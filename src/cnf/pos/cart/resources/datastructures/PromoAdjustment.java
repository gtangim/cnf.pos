package cnf.pos.cart.resources.datastructures;

import java.math.BigDecimal;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 3/8/11
 * Time: 2:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class PromoAdjustment {
    int index;
    String promoId;
    boolean isItemPromo;
    BigDecimal amount;
    double taxableNet;
    String promoCode;

    public PromoAdjustment(int index, String promoId, boolean isItemPromo, BigDecimal amount, String promoCode)
    {
        this.index=index;
        this.promoId=promoId;
        this.isItemPromo=isItemPromo;
        this.amount=amount;
        this.promoCode=promoCode;

    }

    public int getIndex() {
        return index;
    }

    public String getPromoId() {
        return promoId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public boolean isItemPromo() {
        return isItemPromo;
    }

    public double getTaxableNet() {
        return taxableNet;
    }

    public void setTaxableNet(double taxableNet) {
        this.taxableNet = taxableNet;
    }

    public String getDisplayPromoCode() {
        if (promoCode==null) return null;
        else return " ["+promoCode+"]";
    }
    public String getPromoCode()
    {
        return promoCode;
    }
}
