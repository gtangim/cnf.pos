package cnf.pos.cart.resources.datastructures;

import javolution.util.FastMap;
import cnf.pos.cart.resources.elements.ProductElement;
import cnf.pos.cart.resources.elements.ProductIdentificationElement;
import cnf.pos.util.Utility;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/23/11
 * Time: 11:48 AM
 * To change this template use File | Settings | File Templates.
 */
public class AtomicProductInput {
    String source;
    String sku;
    String inputValue;
    String prodId;
    String parentProdId;
    BigDecimal price;
    BigDecimal qty;
    String uomId;
    String itemType;
    boolean selected=false;
    List<ProductIdentificationElement> selectableProductIds;
    List<ProductElement> selectableProducts;

    public AtomicProductInput(String source, String inputValue, String sku, String prodId, BigDecimal price, BigDecimal qty)
    {
        this.source = source;
        this.sku = sku;
        this.inputValue = inputValue;
        this.prodId = prodId;
        this.price = price;
        this.qty = qty;
        itemType = null;
        parentProdId = null;
        selected=false;
    }

    public String getSource() {
        return source;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getProdId() {
        return prodId;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public String getUomId() {
        return uomId;
    }

    public void setUomId(String uomId) {
        this.uomId = uomId;
    }

    public List<ProductElement> getSelectableProducts() {
        return selectableProducts;
    }

    public void setSelectableProducts(List<ProductElement> selectableProducts) {
        this.selectableProducts = selectableProducts;
    }

    public List<ProductIdentificationElement> getSelectableProductIds() {
        return selectableProductIds;
    }

    public void setSelectableProductIds(List<ProductIdentificationElement> selectableProductIds) {
        this.selectableProductIds = selectableProductIds;
    }

    public Map getProductSelectionMap()
    {
        FastMap sMap = FastMap.newInstance();
        if (selectableProducts!=null)
        {
            for (ProductElement p : selectableProducts)
            {
                sMap.put(p.ProductId,p.ProductId + " - " + p.Name + " [" + p.BrandName + "]");
            }
        }
        return sMap;
    }

    public void selectProduct(String productId)
    {
        if (Utility.isNotEmpty(productId))
        {
            this.prodId=null;
            for(int i=0;i<selectableProductIds.size();i++)
                if (selectableProductIds.get(i).ProductId.equals(productId))
                {
                    selectProduct(i);
                    selected=true;
                    return;
                }
        }
        else if (selectableProducts!=null && selectableProducts.size()>0)
            selectProduct(0);
        else this.prodId=null;
    }

    public void selectProduct(int ind)
    {
        BigDecimal qty = selectableProductIds.get(ind).Quantity;
        this.prodId = selectableProductIds.get(ind).ProductId;
        if (qty!=null && qty.compareTo(BigDecimal.ONE)>0) this.qty = qty;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getParentProdId() {
        return parentProdId;
    }

    public void setParentProdId(String parentProdId) {
        this.parentProdId = parentProdId;
    }

    public boolean isSelected() {
        return selected;
    }

    public String getInputValue() {
        return inputValue;
    }
}
