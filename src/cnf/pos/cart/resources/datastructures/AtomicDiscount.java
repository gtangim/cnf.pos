package cnf.pos.cart.resources.datastructures;

import cnf.pos.util.Debug;
import cnf.pos.cart.resources.ResourceManager;
import cnf.pos.cart.resources.SettingsResource;
import cnf.pos.util.Utility;

import java.math.BigDecimal;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/21/11
 * Time: 4:55 PM
 * To change this template use File | Settings | File Templates.
 */
public final class AtomicDiscount {
    public static final String module = AtomicDiscount.class.getName();

    public static final int PERCENT = 0;
    public static final int VALUE = 1;

    protected BigDecimal amount;
    protected BigDecimal amtB;
    protected double amtD;
    protected int discType;
    protected boolean isValid;
    protected String reasonId;
    protected String reportingCode;

    protected ResourceManager resource;


    public AtomicDiscount(BigDecimal amount, int discType, String reasonId, String reportingCode, ResourceManager resource)
    {
        this.reportingCode = reportingCode;
        this.reasonId = reasonId;
        this.discType = discType;
        this.amount = amount;
        amtB = amount.negate();
        if (discType==PERCENT) amtB = amtB.movePointLeft(2);
        amtD = amtB.doubleValue();
        isValid = true;
        this.resource = resource;
    }

    public AtomicDiscount(String disc, String reasonId, String reportingCode, ResourceManager resource)
    {
        this.reportingCode = reportingCode;
        isValid = true;
        discType=VALUE;
        if (disc.endsWith("%"))
        {
            discType=PERCENT;
            disc=disc.substring(0,disc.length()-1);
        }
        try
        {
            if (discType==VALUE) amount = getAmount(disc);
            else amount = new BigDecimal(disc);
            if (amount==null) {isValid=false; return;}
            //if (discType==VALUE) amount = amount.movePointLeft(2);
            amtB = amount.negate();
            if (discType==PERCENT) amtB=amtB.movePointLeft(2);
        }
        catch(Exception ex)
        {
            isValid = false;
            return;
        }
        amtD = amtB.doubleValue();
        this.reasonId = reasonId;
        this.resource = resource;
    }

    public String getReasonId() {
        return reasonId;
    }

    public String getReportingCode() {
        return reportingCode;
    }

    public BigDecimal getPercent()
    {
        if (discType==PERCENT) return amount;
        else return null;
    }

    public double calculateDiscount(double value)
    {
        if (discType==VALUE) return amtD;
        else return value*amtD;
    }

    public BigDecimal calculateDiscount(BigDecimal value)
    {
        double retVal = calculateDiscount(value.doubleValue());
        SettingsResource settings = resource.getSettings();
        return new BigDecimal(retVal).setScale(settings.getScale(),settings.getRoundingMode());
    }


    public BigDecimal calculateRoundedDiscount(double value)
    {
        double retVal = calculateDiscount(value);
        SettingsResource settings = resource.getSettings();
        return new BigDecimal(retVal).setScale(settings.getScale(),settings.getRoundingMode());
    }

    public String getDiscountText()
    {
        return (discType==PERCENT?amount.toString()+"%": Utility.formatPrice(amount));
    }

    public boolean isValid() {
        return isValid;
    }

    private BigDecimal getAmount(String amtStr)
    {
        BigDecimal amount=null;
        try
        {
            if (Utility.isNotEmpty(amtStr)) {
                amtStr=amtStr.trim();
                int dotIndex=amtStr.indexOf('.');
                if (amtStr.equals(".")) amtStr="0";
                else if (dotIndex==0) amtStr="0"+amtStr;
                else if (dotIndex==amtStr.length()-1) amtStr=amtStr+"0";
                try {
                    amount = new BigDecimal(amtStr);
                } catch (NumberFormatException e) {
                    Debug.logError("Invalid number for amount : " + amtStr, module);
                    return null;
                }
                if (dotIndex<0)
                {
                    // Move points when amount entered without a '.' (in pennies)
                    amount = amount.movePointLeft(2); // convert to decimal
                    Debug.log("Set amount / 100 : " + amount, module);
                }
            }
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
            amount=null;
        }
        return amount;
    }

}
