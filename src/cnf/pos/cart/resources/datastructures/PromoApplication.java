package cnf.pos.cart.resources.datastructures;

import javolution.util.FastList;
import javolution.util.FastMap;
import javolution.util.FastSet;
import cnf.pos.cart.resources.elements.PromoElement;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 3/8/11
 * Time: 2:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class PromoApplication {
    protected PromoElement promo;
    protected FastMap<Integer,FastList<PromoAdjustment>> adjustments;
    protected FastMap<Integer,PromoAdjustment> distributedAdjustments;
    protected FastSet<Integer> scope;
    protected FastMap<Integer,Double> scopeSourceValue;
    protected FastSet<Integer> targets;
    protected FastList<String> promoCodes;

    //protected double orderTotal;
    //protected double itemTotal;
    protected double total;

    public PromoApplication(PromoElement promo)
    {
        this.promo=promo;
        adjustments=FastMap.newInstance();
        scope=FastSet.newInstance();
        targets=FastSet.newInstance();
        promoCodes=FastList.newInstance();
        scopeSourceValue=FastMap.newInstance();
        distributedAdjustments = FastMap.newInstance();
        //orderTotal=itemTotal=total=0;
        total=0;
    }

    public boolean isEmpty()
    {
        return adjustments.size()==0;
    }


    public void addAdjustment(PromoAdjustment adjustment)
    {
        int ind = adjustment.getIndex();
        FastList<PromoAdjustment> adjList = adjustments.get(ind);
        if (adjList==null)
        {
            adjList = FastList.newInstance();
            adjustments.put(ind,adjList);
        }
        adjList.add(adjustment);
        double val = adjustment.getAmount().doubleValue();

        //if (adjustment.isItemPromo) itemTotal+=Val;
        //else orderTotal+=Val;

        total+=val;
    }


    public void addToScope(int index, double sourceValue)
    {
        scope.add(index);
        scopeSourceValue.put(index, sourceValue);
    }

    public void addToTarget(int index, double sourceValue)
    {
        addToScope(index,sourceValue);
        targets.add(index);
    }

    public void distributePromo(){
        PromoAdjustment firstPA = null;
        if (adjustments.size()>0) {
            BigDecimal remainingAmount = BigDecimal.ZERO;
            for (List<PromoAdjustment> adjLineEntries : adjustments.values()) {
                if (adjLineEntries != null && adjLineEntries.size() > 0)
                    for (PromoAdjustment adj : adjLineEntries) {
                        remainingAmount = remainingAmount.add(adj.amount);
                        if (firstPA == null) firstPA = adj;
                    }
            }
            String pcode = firstPA.promoCode;

            double totalSourceValue = 0;
            for (double val : scopeSourceValue.values()) totalSourceValue += val;

            // now distribute the promo...


            int maxIndex = 0;
            double maxSourceValue = -1000000000.0;
            BigDecimal distributedAmount = BigDecimal.ZERO;
            for (Integer ind : scope) {
                double sourceValue = scopeSourceValue.get(ind);
                if (sourceValue >= maxSourceValue) {
                    maxSourceValue = sourceValue;
                    maxIndex = ind;
                }
                double amountDouble = total * sourceValue / totalSourceValue;
                BigDecimal amount = new BigDecimal(amountDouble).setScale(2, RoundingMode.HALF_EVEN);
                PromoAdjustment pa = new PromoAdjustment(ind, promo.Id, firstPA.isItemPromo, amount, pcode);
                remainingAmount = remainingAmount.subtract(amount);
                distributedAdjustments.put(ind,pa);
            }

            distributedAdjustments.get(maxIndex).amount = distributedAdjustments.get(maxIndex)
                    .amount.add(remainingAmount);

        }
    }


    public PromoElement getPromo() {
        return promo;
    }

    public FastList<PromoAdjustment> getAdjustments(int index) {
        return adjustments.get(index);
    }

    public PromoAdjustment getDistributedAdjustment(int index){
        return distributedAdjustments.get(index);
    }

    public FastSet<Integer> getScope() {
        return scope;
    }

    public FastSet<Integer> getTargets() {
        return targets;
    }

    public FastList<String> getPromoCodes() {
        return promoCodes;
    }

    //public double getOrderTotal() {
    //    return orderTotal;
    //}

    //public double getItemTotal() {
    //    return itemTotal;
    //}

    public double getTotal() {
        return total;
    }
}
