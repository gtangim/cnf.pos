package cnf.pos.cart;

import cnf.pos.util.GeneralException;
import javolution.util.FastMap;
import cnf.pos.cart.resources.ResourceManager;
import cnf.pos.util.Utility;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/21/11
 * Time: 10:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class PosShoppingCartPayment {
    public static final int AUTO_SELECT=-1;
    public static final int PAYMENT=1;
    public static final int REFUND=2;

    public static final int STAT_APPROVED=0;
    public static final int STAT_VOIDED=1;
    public static final int STAT_CANCELLED=2;
    public static final int STAT_DECLINED=3;

    public static final String module = PosShoppingCartPayment.class.getName();
    protected static int nextIndex = 0;

    // General...
    protected BigDecimal inputAmount;
    protected String inputAmountUomId;
    protected String payType;
    protected String additionalDescription;
    protected String reversalDescription;
    protected String displayText;


    // Card Related...
    protected String cardCompany;
    protected String cardSuffix;
    protected String authCode;
    protected String expiry;
    protected String verificationMethod;
    protected String invoiceId;
    protected boolean isCATPayment;
    //protected int status;
    protected int transactionType;
    protected int transactionStatus;
    protected String referenceNumber;
    protected String operation;
    protected String responseCode;


    // internal...
    protected ResourceManager resource;
    protected BigDecimal amount;
    protected String amountUomId;
    protected int index;
    protected int catOperationIndex = 0;
    protected Map<String,String> properties = FastMap.newInstance();
    //protected boolean implicitCardDetection = false;

    public String serialized()
    {
        String ret = operation + "," + amount.toString()+","+getPayTypeExtended()+","+inputAmount.toString()+inputAmountUomId+","
                +referenceNumber+","+authCode+","+expiry+","+invoiceId+","+isCATPayment()+
                ","+isActive()+","+additionalDescription+","+reversalDescription;
        ret = ret.replace("null","#NULL");
        return ret;
    }


    public PosShoppingCartPayment(ResourceManager resource, String payType, BigDecimal amount, String amountUomId,
            String refNum, String authCode, String expiry, String verificationMethod, String invoiceId,
            String additionalDescription, boolean isCATPayment)
            throws GeneralException
    {
        String currency = resource.getSettings().getCurrency();
        if (resource==null) throw new GeneralException("Payment needs Data Resources!");
        if (payType==null) throw new GeneralException("Must provide a payment type!");
        if (amount==null) throw new GeneralException("Amount not specified!");
        if (amountUomId==null) amountUomId = currency;

        this.resource=resource;
        this.payType=payType;
        if ("INTERAC".equals(payType)) this.payType = "DEBIT_CARD";
        else if ("VISA".equals(payType)) this.payType = "VISA_CARD";
        else if ("MASTERCARD".equals(payType)) this.payType = "MASTER_CARD";
        else if ("AMEX".equals(payType)) this.payType = "AMEX_CARD";
        else if ("JCB".equals(payType)) this.payType = "JCB_CARD";
        else if ("DISCOVER".equals(payType)) this.payType = "DISCOVER_CARD";


        this.inputAmount=amount.setScale(resource.getSettings().getScale()
                ,resource.getSettings().getRoundingMode());
        this.inputAmountUomId=amountUomId;
        if (!amountUomId.equals(currency))
        {
            this.amount=resource.getConversions().convert(inputAmount,inputAmountUomId,currency);
            if (this.amount==null) throw new GeneralException("Cannot convert amount currency "+inputAmountUomId
                    +" to default currency " + currency+"!");
            this.amount=this.amount.setScale(resource.getSettings().getScale()
                ,resource.getSettings().getRoundingMode());
            this.amountUomId=currency;
        }
        else
        {
            this.amount=inputAmount;
            this.amountUomId=inputAmountUomId;
        }

        this.referenceNumber=refNum;
        if ("DEBIT_CARD".equals(payType) || "CREDIT_CARD".equals(payType))
        {
            // Split up the card number from the refNum and identify companyId...
            if (refNum.length()!=16) throw new GeneralException("Card Number must be 16 digits, you provided: " + refNum+ "!");
            cardCompany = refNum.substring(0,6);
            cardSuffix = refNum.substring(12);
            //implicitCardDetection = true;
        }
        else if (this.payType.endsWith("_CARD"))
        {
            String[] cardIdComp = refNum.split("\\*");
            cardCompany = cardIdComp[0];
            cardSuffix = cardIdComp[1];
        }
        if (verificationMethod==null) verificationMethod="";
        this.authCode=authCode;
        this.expiry=expiry;
        this.verificationMethod = verificationMethod;
        this.invoiceId=invoiceId;
        this.isCATPayment=isCATPayment;
        this.additionalDescription=additionalDescription;

        if (!Utility.isEmpty(additionalDescription))
        {
            int asiIndex = additionalDescription.indexOf("ASI");
            if (asiIndex>0)
            {
                String asi = additionalDescription.substring(asiIndex+3,additionalDescription.length());
                String[] asiComp = asi.split("\\|");
                if (asiComp!=null && asiComp.length>0)
                    for(String asiElement:asiComp)
                    {
                        if(!Utility.isEmpty(asiElement))
                        {
                            String[] asiField = asiElement.split("\\:");
                            if (asiField.length>=2) properties.put(asiField[0],asiField[1]);
                        }
                    }
            }
        }

        this.transactionType=PAYMENT;
        if (amount.compareTo(BigDecimal.ZERO)==-1) this.transactionType=REFUND;

        if (this.transactionType==PAYMENT) operation="PURCHASE";
        else operation = "REFUND";

        this.transactionStatus=STAT_APPROVED;

        // Setup display text...

        /*if ("DEBIT_CARD".equals(payType) || "CREDIT_CARD".equals(payType))
        {
            displayText="*"+this.cardSuffix+"/"+this.invoiceId+"/"+this.authCode;
        } */
        if (this.payType.endsWith("_CARD"))
        {
            displayText = refNum;
        }
        else if ("VENDOR_COUPON".equals(this.payType) && Utility.isNotEmpty(refNum))
            displayText=refNum;
        else if (Utility.isNotEmpty(refNum)) displayText="REF#"+refNum;
        else displayText="";

        if (!this.amountUomId.equals(this.inputAmountUomId))
            displayText=displayText + "["+ inputAmount.toString()+
                    resource.getConversions().getUom(inputAmountUomId).Abbrev
                    +"@"+resource.getConversions().getConversionFactor(
                    inputAmountUomId,this.amountUomId
            ) + "]";


        index = nextIndex++;
    }

    public String getConversion()
    {
        if (!this.amountUomId.equals(this.inputAmountUomId))
            return inputAmount.toString()+
                    resource.getConversions().getUom(inputAmountUomId).Abbrev
                    +"@"+resource.getConversions().getConversionFactor(
                    inputAmountUomId,this.amountUomId
            );
        else return null;
    }

    /*public void setStatus(int status)
    {
        if (status<0 || status>4)
        {
            // Auto Assign Status...
            if (amount.doubleValue()<0) status=REFUNDED;
            else status=PAID;
        }
        else this.status = status;
    }*/

    public BigDecimal getApplicableAmount()
    {
        if (transactionStatus==STAT_APPROVED) return amount;
        else return BigDecimal.ZERO.setScale(resource.getSettings().getScale()
                ,resource.getSettings().getRoundingMode());
    }

    public boolean isActive()
    {
        if (transactionStatus==STAT_APPROVED) return true;
        else return false;
    }

    public boolean isForeignCurrencyPayment()
    {
        if (!inputAmountUomId.equals(amountUomId)) return true;
        else return false;
    }


    public BigDecimal getInputAmount() {
        return inputAmount;
    }

    public String getInputAmountUomId() {
        return inputAmountUomId;
    }

    public String getPayType() {
        return payType;
    }

    public String getAdditionalDescription() {
        return additionalDescription;
    }

    public String getDisplayText() {
        return displayText;
    }

    public String getCardCompany() {
        return cardCompany;
    }

    public String getCardSuffix() {
        return cardSuffix;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public String getAuthCode() {
        return authCode;
    }

    public String getExpiry() {
        return expiry;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public boolean isCATPayment() {
        return isCATPayment;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public String getProperty(String key)
    {
        if (properties.containsKey(key))
        {
            String val = properties.get(key);
            if (!Utility.isEmpty(val)) return val;
            else return null;
        }
        else return null;
    }

    public String getStatusItemId()
    {
        if (transactionStatus==STAT_APPROVED)
        {
            if (transactionType==PAYMENT)
            {
                if (isCATPayment) return "PAYMENT_AUTHORIZED";
                else return "PAYMENT_RECEIVED";
            }
            else if (transactionType==REFUND) return "PAYMENT_REFUNDED";
            else return "PAYMENT_CANCELLED";
        }
        else if (transactionStatus==STAT_DECLINED) return "PAYMENT_DECLINED";
        else if (transactionStatus==STAT_CANCELLED) return "PAYMENT_CANCELLED";
        else if (transactionStatus==STAT_VOIDED) return "PAYMENT_CANCELLED";
        else return "PAYMENT_CANCELLED";
    }


    public String getStatusText()
    {
        if (transactionStatus==STAT_APPROVED)
        {
            if (transactionType==PAYMENT) return "Paid";
            else if (transactionType==REFUND) return "Refunded";
            else return "Voided";
        }
        else if (transactionStatus==STAT_DECLINED) return "Declined";
        else if (transactionStatus==STAT_CANCELLED) return "Cancelled";
        else if (transactionStatus==STAT_VOIDED) return "Voided";
        else return "Voided";
    }

    /*public String getPaymentTitle()
    {
        if (status==PAID) return "PURCHASE";
        else if (status==REFUNDED) return "REFUND";
        else if (status==DECLINED) return "VOID";
        else return "Voided";
    }*/

    public String getPayTypeExtended()
    {
        String pType = payType;
        if (pType.equals("CREDIT_CARD"))
        {
            String mc = this.referenceNumber;
            if (mc.matches("^4\\d{5}.*")) pType="VISA_CARD";
            else if (mc.matches("^5[1-5]\\d{4}.*")) pType="MASTER_CARD";
            else pType="OTHER_CREDIT_CARD";
        }
        return pType;
    }

    public String getPaymentMethodTypeShortText()
    {
        if ("CASH".equals(payType)) return "CASH";
        else if ("DEBIT_CARD".equals(payType)) return "INTERAC";
        else if ("CREDIT_CARD".equals(payType)) return "CRE CRD";
        else if ("DONATION".equals(payType)) return "DON";
        else if ("GIFT_CARD".equals(payType)) return "GFT CRD";
        else if ("GIFT_CERTIFICATE".equals(payType)) return "GFT CER";
        else if ("MONEY_ORDER".equals(payType)) return "MON ORD";
        else if ("PERSONAL_CHECK".equals(payType)) return "CHEQUE";
        else if ("VENDOR_COUPON".equals(payType)) return "V. COUPON";
        else if ("EXT_BILLACT".equals(payType)) return "BILL ACC";
        else if ("VISA_CARD".equals(payType)) return "VISA";
        else if ("MASTER_CARD".equals(payType)) return "MASTERCARD";
        else if ("AMEX_CARD".equals(payType)) return "AMEX";
        else if ("JCB_CARD".equals(payType)) return "JCB";
        else if ("DISCOVER".equals(payType)) return "DISCOVER";
        else return payType;
    }

    public String getPaymentMethodTypeText()
    {
        if ("CASH".equals(payType)) return "CASH";
        else if ("DEBIT_CARD".equals(payType)) return "DEBIT CARD";
        else if ("CREDIT_CARD".equals(payType)) return "CREDIT CARD";
        else if ("DONATION".equals(payType)) return "DONATION";
        else if ("GIFT_CARD".equals(payType)) return "GIFT CARD";
        else if ("GIFT_CERTIFICATE".equals(payType)) return "GIFT CERTIFICATE";
        else if ("MONEY_ORDER".equals(payType)) return "MONEY ORDER";
        else if ("PERSONAL_CHECK".equals(payType)) return "CHEQUE";
        else if ("VENDOR_COUPON".equals(payType)) return "VENDOR COUPON";
        else if ("EXT_BILLACT".equals(payType)) return "BILL TO ACCOUNT";
        else if ("VISA_CARD".equals(payType)) return "VISA CARD";
        else if ("MASTER_CARD".equals(payType)) return "MASTER CARD";
        else if ("AMEX_CARD".equals(payType)) return "AMEX CARD";
        else if ("JCB_CARD".equals(payType)) return "JCB CARD";
        else if ("DISCOVER".equals(payType)) return "DISCOVER CARD";
        else return payType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getUnsignedAmount() {
        if (amount.compareTo(BigDecimal.ZERO)==-1) return amount.negate();
        else return amount;
    }

    public String getAmountUomId() {
        return amountUomId;
    }

    public int getIndex() {
        return index;
    }

    public int getCatOperationIndex() {
        return catOperationIndex;
    }

    public void setCatOperationIndex(int catOperationIndex) {
        this.catOperationIndex = catOperationIndex;
    }

    public String getVerificationMethod() {
        return verificationMethod;
    }

    public String getOperation() {
        return operation;
    }

    /*public void setOperation(String operation) {
        this.operation = operation;
    }*/

    public int getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(int transactionStatus) {
        if (this.transactionStatus!=STAT_VOIDED
                && transactionStatus==STAT_VOIDED)
        {
            operation +=" CORRECTION";
        }
        if (this.transactionStatus==STAT_VOIDED && transactionStatus==STAT_APPROVED) return;
        this.transactionStatus = transactionStatus;
    }

    public int getTransactionType() {
        return transactionType;
    }

    public String getReversalDescription() {
        return reversalDescription;
    }

    public void setReversalDescription(String reversalDescription) {
        this.reversalDescription = reversalDescription;
    }
}
