package cnf.pos.cart;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 3/10/11
 * Time: 1:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class PosShoppingCartItemAllocator {
    protected PosShoppingCartItem[] items=null;
    protected double[] value=null;
    protected double[] qty=null;
    //protected int[] resvSegment=null;       // Index
    //protected double[] resvOffset=null;     // qty
    //protected boolean isReverse=false;
    //protected double resv=1.0;
    //protected boolean isAmountResv=false;


    //protected int multiplier=0;

    protected double totalValue;
    protected double totalQty;
    protected int n;
    //protected int nA;

    protected int curSegment;
    protected double curOffset;


    public PosShoppingCartItemAllocator(List<PosShoppingCartItem> items, boolean reverseList)
    {
        this.items = new PosShoppingCartItem[items.size()];
        this.items = items.toArray(this.items);
        init(reverseList);
    }


    public PosShoppingCartItemAllocator(PosShoppingCartItemAllocator parent, int startSegment, double startOffset,
                                        int endSegment, double endOffset, boolean reverseList)
    {
        if (startOffset>=parent.qty[startSegment])
        {
            startSegment++;
            startOffset=0;
        }
        if (endOffset==0.0)
        {
            endSegment--;
            endOffset=parent.qty[endSegment];
        }
        if (endOffset>=parent.qty[endSegment])
        {
            endOffset=parent.qty[endSegment];
        }



        this.n = endSegment-startSegment+1;
        this.items = new PosShoppingCartItem[n];
        this.qty = new double[n];
        this.value = new double[n];

        if (startSegment==endSegment)
        {
            this.items[0]=parent.items[startSegment];
            this.qty[0]=endOffset-startOffset;
            this.value[0]=parent.value[startSegment] * this.qty[0] / parent.qty[startSegment];
        }
        else
        {
            int ind = 0;
            this.items[ind]=parent.items[startSegment];
            this.qty[ind]=parent.qty[startSegment]-startOffset;
            this.value[ind]=parent.value[startSegment] * this.qty[ind] / parent.qty[startSegment];
            this.totalQty+=this.qty[ind];
            this.totalValue+=this.value[ind];
            ind++;
            for(int i=startSegment+1;i<endSegment;i++)
            {
                this.items[ind]=parent.items[i];
                this.qty[ind]=parent.qty[i];
                this.value[ind]=parent.value[i];
                this.totalQty+=this.qty[ind];
                this.totalValue+=this.value[ind];
                ind++;
            }

            this.items[ind]=parent.items[endSegment];
            this.qty[ind]=endOffset;
            this.value[ind]=parent.value[endSegment] * this.qty[ind] / parent.qty[endSegment];
            this.totalQty+=this.qty[ind];
            this.totalValue+=this.value[ind];
            //ind++;
            //init(reverseList);
        }
        if (reverseList)
        {
            int n2 = n/2;
            int i2=n-1;
            for (int i=0;i<n2;i++)
            {
                PosShoppingCartItem temp = items[i];
                items[i]=items[i2];
                items[i2]=temp;
                double temp2 = qty[i];
                qty[i]=qty[i2];
                qty[i2]=temp2;
                temp2 = value[i];
                value[i]=value[i2];
                value[i2]=temp2;
                i2--;
            }
        }

    }


    public void resetAllocation()
    {
        curSegment=0;
        curOffset=0;
    }


    public PosShoppingCartItemAllocator reserveByQuantity(double qtyToReserve, boolean reverseList)
    {
        if (qtyToReserve<=0) return null;
        if (curSegment>=n) return null;
        double qtyReserved = 0;

        int oldSegment = curSegment;
        double oldOffset = curOffset;

        while(qtyReserved<qtyToReserve && curSegment<n)
        {
            // Reserve One item segment
            double curQty = qty[curSegment]-curOffset;
            qtyReserved+=curQty;
            curSegment++;
            curOffset=0;
        }
        if (qtyReserved<qtyToReserve) return null; // Not enough left to reserve...
        // Now calculate the multiplier and resedue;
        //int mult = (int)Math.floor(qtyReserved/qtyToReserve);
        double remainder = qtyReserved - qtyToReserve;
        //double remainder = qtyReserved - (qtyToReserve * (double)mult);

        if (remainder>0)
        {
            curSegment--;
            curOffset = qty[curSegment]-remainder;
        }

        PosShoppingCartItemAllocator alloc = new PosShoppingCartItemAllocator(this,oldSegment,oldOffset
                ,curSegment,curOffset,reverseList);
        //alloc.setMultiplier(mult);
        return alloc;
    }

    public PosShoppingCartItemAllocator reserveByAmount(double amountToReserve, boolean reverseList)
    {
        if (amountToReserve<=0) return null;
        if (curSegment>=n) return null;
        double amountReserved = 0;

        int oldSegment = curSegment;
        double oldOffset = curOffset;

        while(amountReserved<amountToReserve && curSegment<n)
        {
            // Reserve One item segment
            double curQty = qty[curSegment]-curOffset;
            double curVal = value[curSegment] * curQty / qty[curSegment];
            amountReserved+=curVal;
            curSegment++;
            curOffset=0;
        }
        if (amountReserved<amountToReserve) return null; // Not enough left to reserve...
        // Now calculate the multiplier and resedue;
        //int mult = (int)Math.floor(amountReserved/amountToReserve);
        //double remainder = amountReserved - (amountToReserve * (double)mult);
        double remainder = amountReserved - amountToReserve;
        if (remainder>0)
        {
            curSegment--;
            double remainderQty = qty[curSegment] * remainder / value[curSegment];
            curOffset = qty[curSegment]-remainderQty;
        }

        PosShoppingCartItemAllocator alloc = new PosShoppingCartItemAllocator(this,oldSegment,oldOffset
                ,curSegment,curOffset,reverseList);
        //alloc.setMultiplier(mult);
        return alloc;
    }



    protected void init(boolean reverseList)
    {
        n=this.items.length;
        if (reverseList)
        {
            int n2 = n/2;
            int i2=n-1;
            for (int i=0;i<n2;i++)
            {
                PosShoppingCartItem temp = items[i];
                items[i]=items[i2];
                items[i2]=temp;
                i2--;
            }
        }
        //nA=numActions;
        value = new double[n];
        qty = new double[n];
        for(int i=0;i<n;i++)
        {
            value[i]=this.items[i].getPrePromoValue();
            qty[i]=this.items[i].getQuantity().doubleValue();
            totalValue+=value[i];
            totalQty+=qty[i];
        }
    }

    //public int getMultiplier() {
    //    return multiplier;
    //}

    //public void setMultiplier(int multiplier) {
    //    this.multiplier = multiplier;
    //}

    public double getTotalValue() {
        return totalValue;
    }

    public double getTotalQty() {
        return totalQty;
    }

    public int size() {
        return n;
    }

    public PosShoppingCartItem getItem(int ind)
    {
        return items[ind];
    }
    public double getQty(int ind)
    {
        return qty[ind];
    }
    public double getAmount(int ind)
    {
        return value[ind];
    }


}
