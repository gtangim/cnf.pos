package cnf.pos.cart;

import javolution.util.FastList;
import javolution.util.FastSet;
import cnf.pos.util.GeneralException;
import cnf.pos.cart.resources.ResourceManager;
import cnf.pos.cart.resources.datastructures.AtomicDiscount;
import cnf.pos.cart.resources.datastructures.AtomicPrice;
import cnf.pos.cart.resources.datastructures.PriceResult;
import cnf.pos.cart.resources.elements.*;
import cnf.pos.util.Utility;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/21/11
 * Time: 4:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class PosShoppingCartItem  implements Comparable<PosShoppingCartItem> {
    public static final String BASE_PRICE = "bpr";
    public static final String BASE_PRICE_PRE = "bprp";
    public static final String SUB_TOTAL = "sto";
    public static final String SUB_TOTAL_PRE = "stop";
    public static final String ITEM_DISCOUNT = "idc";
    public static final String SUB_ITEM_PREFIX = "  - ";

    protected static int nextIndex = 0;
    protected ResourceManager resource;
    protected int index;
    protected int cartIndex;
    protected String sku;
    protected String inputProductCode;
    //protected boolean taxable;
    protected String productId;
    //protected String description;
    protected String parentProductId;
    protected BigDecimal quantity;
    protected boolean useGrossPrice;
    protected HashMap params;

    protected PriceResult selectedPrice;
    protected BigDecimal selectedSubtotal;
    protected BigDecimal cost;
    protected boolean noPriceRule;
    protected boolean allowPromo;
    protected boolean allowDiscount;
    protected double prePromoValue;
    protected double prePromoPrice;

    // Discount and override
    protected AtomicDiscount itemDiscount;
    //protected BigDecimal itemDiscountValue;
    //protected BigDecimal overrideAmount;

    protected BigDecimal overridePrice;
    protected BigDecimal overrideSubtotal;
    protected String overrideReason;
    protected String overrideReportingCode;
    protected boolean subTotalOverride;
    protected boolean isSelected;
    protected boolean isPriceLabel=false;

    // Product Info
    protected ProductElement prod;
    //protected FastSet<String> categories;


    // Tax Settings
    protected double totalTaxRate; // fraction
    protected FastSet<String> applicableTaxRates;

    // Features
    protected FastSet<String> optionalFeatures;
    protected HashMap<String,BigDecimal> values;


    public PosShoppingCartItem(ResourceManager resource, Map params,
                                    String sku, String inputProductCode, String prodId, String parentProdId, BigDecimal qty,
                                    BigDecimal initialPrice, boolean isSelected, String itemType,
                                    boolean useGrossPrice)
        throws GeneralException
    {
        this.resource=resource;
        this.sku=sku;
        this.inputProductCode = inputProductCode;
        this.productId = prodId;
        this.parentProductId = parentProdId;
        this.quantity = qty;
        this.useGrossPrice=useGrossPrice;
        this.noPriceRule=false;
        if (itemType!=null && itemType.toLowerCase().contains("pl item")) isPriceLabel=true;
        prod = resource.getProducts().getProduct(prodId);
        if (prod==null) throw new GeneralException("Product not found: "+prodId+"!");
        this.cost = prod.Price.Cost;
        if (cost==null) throw new GeneralException("Standard cost is not initialized for : "+prodId+"!");
        if (isPriceLabel && prod.IsScalable && resource.getSettings().calculatePriceLabelQuantity())
        {
            BigDecimal unitPrice = prod.Price.getActivePrice(BigDecimal.ONE).getPrice().getPrice(useGrossPrice);
            initializePrices(unitPrice,params);
            if (qty.compareTo(BigDecimal.ZERO)<0) selectedSubtotal = initialPrice.negate();
            else selectedSubtotal = initialPrice;
        }
        else initializePrices(initialPrice,params);

        checkQuantity(qty);


        this.index=nextIndex++;
        itemDiscount=null;
        //itemDiscountValue=null;
        //overrideAmount=null;
        overrideReason=null;
        overridePrice=null;
        overrideSubtotal=null;
        overrideReportingCode=null;
        subTotalOverride=false;
        this.isSelected = isSelected;

        //categories = resource.getMemberships().getCategories(prod.getProductId(),true);

        // Determine Taxes
        initializeTax();
        // Setup optional features...
        resetValues();
        optionalFeatures = FastSet.newInstance();

        setupOverridePromoAllowed();
    }

    protected void checkQuantity(BigDecimal q) throws GeneralException
    {
        //BigDecimal q = quantity;
        if (q.compareTo(BigDecimal.ZERO)==-1) q=q.negate();
        if (isScalable())
        {
            BigDecimal conv = resource.getConversions().convert(q,prod.UomId,"WT_kg");
            if (conv.compareTo(resource.getSettings().getQuantityLimitWeight())==1)
                throw  new GeneralException("Quantity limit exceeded! ");
        }
        else
        {
            if (q.compareTo(resource.getSettings().getQuantityLimitCount())==1)
                throw  new GeneralException("Quantity limit exceeded! ");
        }

    }

    protected void initializeTax()
    {
        applicableTaxRates = FastSet.newInstance();
        totalTaxRate = 0;
        if (prod.IsTaxable)
        {
            //BigDecimal taxSum = BigDecimal.ZERO;
            // TODO: Implement tax application restrictions in TaxAuthorityRateProduct
            List<TaxElement> tList = resource.getTaxes().getStoreTaxes();
            for(TaxElement t:tList)
            {
                if (t.TaxPercent==null) continue;
                applicableTaxRates.add(t.TaxId);
                double tRate = t.TaxPercent.movePointLeft(2).doubleValue();
                totalTaxRate+=tRate;
            }
            //totalTaxRate = taxSum.movePointLeft(2).doubleValue();
        }
    }



    protected void initializePrices(BigDecimal initialPrice, Map params) throws GeneralException
    {
        String prodId = prod.ProductId;
        if (initialPrice!=null)
        {
            // We need to validate the product price structure first
            // otherwise the order processing will fail during "finish_order"...
            PriceResult validatePrice = prod.Price.getActivePrice(quantity);
            if (!validatePrice.isValid())
                throw new GeneralException("Invalid Price for: "
                    + prodId + ".\r\n"+validatePrice.getPriceError());

            // Now Assign the preselcted price and ignore the priceStructure
            selectedPrice = new PriceResult(new AtomicPrice(initialPrice,initialPrice)
                    ,"DEFAULT_PRICE",null,null,null,null);
            noPriceRule = true;
        }

        if (!noPriceRule)
        {
            selectedPrice = prod.Price.getActivePrice(quantity);
            if (!selectedPrice.isValid()) throw new GeneralException("Invalid Price for: "
                    + prodId + ".\r\n"+selectedPrice.getPriceError());

            PriceElement pre = prod.Price;

            //Disable price rule...
            /*this.params=(HashMap)params.clone();
            params = PriceRuleResource.setProductParams(params,prodId,quantity,pre,
                    null,null,useGrossPrice);

            PriceResult res = resource.getPriceRules().applyRules(params);
            if (res!=null && res.isValid() && res.getPrice().lessThan(selectedPrice.getPrice(),useGrossPrice))
                selectedPrice=res;*/
        }
    }


    public void setQuantity(Map params, BigDecimal qty) throws Exception
    {
        checkQuantity(qty);
        if (qty!=null) quantity=qty;
        if (subTotalOverride && overrideSubtotal!=null)
        {
            int c1 = quantity.compareTo(BigDecimal.ZERO);
            int c2 = overrideSubtotal.compareTo(BigDecimal.ZERO);
            if (c1!=c2) overrideSubtotal = overrideSubtotal.negate();
            if (c1!=0) overridePrice = overrideSubtotal.divide(quantity,
                    resource.getSettings().getScale(),resource.getSettings().getRoundingMode());
        }
        else if (overridePrice!=null) {
            overrideSubtotal = quantity.multiply(overridePrice)
                    .setScale(resource.getSettings().getScale(),resource.getSettings().getRoundingMode());
        }

        initializePrices(null,params);
        initializeTax();
        resetValues();
    }

    public boolean isPromoAllowed() {
        return allowPromo;
    }

    public void setPromoAllowed(boolean allowPromo) {
        this.allowPromo = allowPromo;
    }

    public boolean isDiscountAllowed() {
        return allowDiscount;
    }

    public void setDiscountAllowed(boolean allowDiscount) {
        this.allowDiscount = allowDiscount;
    }

    public String getSku() {
        return sku;
    }

    public String getInputProductCode() {
        return inputProductCode;
    }

    public String getProductId() {
        return productId;
    }

    public String getParentProductId() {
        return parentProductId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public boolean isUseGrossPrice() {
        return useGrossPrice;
    }

    public PriceResult getSelectedPrice() {
        return selectedPrice;
    }

    public AtomicDiscount getItemDiscount() {
        return itemDiscount;
    }

    /*public BigDecimal getOverrideAmount() {
        return overrideAmount;
    }*/

    public BigDecimal getOverridePrice() {
        return overridePrice;
    }

    public BigDecimal getOverrideSubtotal() {
        return overrideSubtotal;
    }

    public String getOverrideReason() {
        return overrideReason;
    }

    public String getOverrideReportingCode(){
        return overrideReportingCode;
    }

    public boolean isSubTotalOverride() {
        return subTotalOverride;
    }


    public String getDescription() {
        return prod.Name;
    }

    public boolean isTaxable() {
        return prod.IsTaxable;
    }

    public double getTotalTaxRate() {
        return totalTaxRate;
    }

    public FastSet<String> getApplicableTaxRates() {
        return applicableTaxRates;
    }

    public FastSet<String> getOptionalFeatures() {
        return optionalFeatures;
    }

    public double getPrePromoValue() {
        return prePromoValue;
    }

    public void setPromoValue(double promoValue) {
        this.prePromoValue=promoValue;
        this.prePromoPrice = promoValue/quantity.doubleValue();
    }

    public double getPrePromoPrice() {
        return prePromoPrice;
    }

    public int getCartIndex() {
        return cartIndex;
    }

    public void setCartIndex(int cartIndex) {
        this.cartIndex = cartIndex;
    }

    public ProductElement getProduct()
    {
        return prod;
    }
    public PriceElement getPrice()
    {
        return prod.Price;
    }

    public int getIndex() {
        return index;
    }

    public UomElement getProductUom()
    {
        return resource.getConversions().getUom(prod.UomId);
    }

    public String getTaxable()
    {
        if (prod.IsTaxable) return resource.getSettings().getTaxLiteral();
        else return " ";
    }

    public boolean isSelected()
    {
        return isSelected;
    }

    public boolean isScalable()
    {
        if (isPriceLabel && !resource.getSettings().calculatePriceLabelQuantity())  return false;
        else return prod.IsScalable;
    }

    public String getReceiptProductId()
    {
        if (isSelected) return sku+"/"+productId;
        else return sku;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public boolean isPriceLabel() {
        return isPriceLabel;
    }

    // *********************** Get Amounts *****************************

    public BigDecimal getBasePriceBeforeOverride()
    {
        BigDecimal bp=values.get(BASE_PRICE_PRE);
        if (bp==null)
        {
            bp = selectedPrice.getPrice().getPrice(useGrossPrice);
            values.put(BASE_PRICE_PRE,bp);
        }
        return bp;
    }
    public BigDecimal getBasePrice()
    {
        BigDecimal bp=values.get(BASE_PRICE);
        if (bp==null)
        {
            bp = selectedPrice.getPrice().getPrice(useGrossPrice);
            /*if (overridePrice!=null && subTotalOverride==false) bp=overridePrice;
            else if (overrideSubtotal!=null && subTotalOverride==true)
            {
                double oa = overrideAmount.doubleValue();
                oa = oa/quantity.doubleValue();
                bp = new BigDecimal(oa).setScale(resource.getSettings().getScale()
                    ,resource.getSettings().getRoundingMode());
            }*/
            if (overrideReason!=null && overridePrice!=null) bp = overridePrice;
            values.put(BASE_PRICE,bp);
        }
        return bp;
    }

    public String getRate()
    {
        if (resource.getSettings().calculatePriceLabelQuantity() && isPriceLabel && prod.IsScalable)
            return getBasePrice().toString()+"/"
                    +resource.getConversions().getUom(prod.UomId).Abbrev;
        else if (isPriceLabel==false && prod.IsScalable) return getBasePrice().toString()+"/"
                +resource.getConversions().getUom(prod.UomId).Abbrev;
        else return getBasePrice().setScale(resource.getSettings().getScale()
                ,resource.getSettings().getRoundingMode()).toString();
    }

    public BigDecimal getSubtotalBeforeOverride()
    {
        BigDecimal value = values.get(SUB_TOTAL_PRE);
        if (value==null)
        {
            double val=0.0;
            if (selectedSubtotal!=null) value=selectedSubtotal;
            else
            {
                double q = quantity.doubleValue();
                double bp = getBasePriceBeforeOverride().doubleValue();
                val = q*bp;
            }

            if (value==null) value = new BigDecimal(val).setScale(resource.getSettings().getScale()
                    ,resource.getSettings().getRoundingMode());
            values.put(SUB_TOTAL_PRE,value);
        }
        return value;
    }
    public BigDecimal getSubtotal()
    {
        BigDecimal value = values.get(SUB_TOTAL);
        if (value==null)
        {
            double val=0.0;
            /*if (overrideAmount!=null && subTotalOverride) Val = overrideAmount.doubleValue();
            else if (selectedSubtotal!=null) value=selectedSubtotal;*/
            if (overrideReason!=null && overrideSubtotal!=null) val = overrideSubtotal.doubleValue();
            else
            {
                double q = quantity.doubleValue();
                double bp = getBasePrice().doubleValue();
                val = q*bp;
            }

            if (value==null) value = new BigDecimal(val).setScale(resource.getSettings().getScale()
                ,resource.getSettings().getRoundingMode());
            values.put(SUB_TOTAL,value);
        }
        return value;
    }
    public double calculateTaxableNet(double value)
    {
        if (useGrossPrice) return value/(1.0+totalTaxRate);
        else return value;
    }




    // *********************** Price Override *****************************
    protected void setupOverridePromoAllowed()
    {
        if (prod!=null)
        {
            allowPromo=prod.IsPromoAllowed;
            allowDiscount=prod.IsDiscountAllowed;
        }
    }
    public void clearOverride()
    {
        subTotalOverride=false;
        overrideReason=null;
        //overrideAmount=null;
        overridePrice=null;
        overrideSubtotal=null;
        overrideReportingCode = null;
        resetValues();
        setupOverridePromoAllowed();
    }

    public void overridePrice(BigDecimal value, String reason, String reportingCode)
    {
        if (value!=null && Utility.isNotEmpty(reason))
        {
            if (value.compareTo(BigDecimal.ZERO)==-1) value=value.negate(); // Only positive prices...
            //overrideAmount = value;
            overrideReportingCode = reportingCode;
            overridePrice = value;
            overrideSubtotal = quantity.multiply(overridePrice)
                    .setScale(resource.getSettings().getScale(),resource.getSettings().getRoundingMode());
            overrideReason = reason;
            subTotalOverride = false;
            resetValues();
            setupOverridePromoAllowed();
            if (!resource.getSettings().isOverridePromoAllowed(reason))
                allowPromo=false;
        }
    }

    public void overrideSubtotal(BigDecimal value, String reason, String reportingCode)
    {
        if (value!=null && Utility.isNotEmpty(reason))
        {
            if (quantity.compareTo(BigDecimal.ZERO)==-1 && value.compareTo(BigDecimal.ZERO)==1)
                value = value.negate();
            else if (quantity.compareTo(BigDecimal.ZERO)==-1 && value.compareTo(BigDecimal.ZERO)==1)
                value = value.negate();

            overrideReportingCode = reportingCode;
            overrideSubtotal = value;
            overridePrice = value.divide(quantity,
                    resource.getSettings().getScale(),resource.getSettings().getRoundingMode());
            overrideReason = reason;
            subTotalOverride = true;
            resetValues();
            setupOverridePromoAllowed();
            if (!resource.getSettings().isOverridePromoAllowed(reason))
                allowPromo=false;
        }
    }
    public String getPriceOverrideText()
    {
        if (overrideSubtotal!=null && subTotalOverride)
            return SUB_ITEM_PREFIX+"Sub Total Overridden";
        else if (overridePrice!=null && !subTotalOverride)
            return SUB_ITEM_PREFIX +"Unit Price Overridden";
        else return null;
    }




    // *********************** Item Discount *****************************
    public void setDiscount(String disc, String reasonId, String reportingCode) throws GeneralException
    {
        AtomicDiscount discount = new AtomicDiscount(disc, reasonId, reportingCode, resource);
        if (!discount.isValid()) throw new GeneralException("Invalid discount amount: "+disc);
        this.itemDiscount = discount;
        resetValues();
    }
    public void clearDiscount()
    {
        this.itemDiscount=null;
        resetValues();
    }
    public String getItemDiscountText()
    {
        if (itemDiscount==null) return null;
        else return SUB_ITEM_PREFIX+"Item Discount: " + itemDiscount.getDiscountText();
    }
    public BigDecimal getItemDiscountAmount()
    {
        return values.get(ITEM_DISCOUNT);
    }

    public BigDecimal getItemDiscountAmount(double layerValue)
    {
        if (itemDiscount==null) return null;
        BigDecimal value = values.get(ITEM_DISCOUNT);
        if (value==null)
        {
            if (layerValue==Double.NaN) layerValue = getSubtotal().doubleValue();
            value = itemDiscount.calculateRoundedDiscount(layerValue);
            values.put(ITEM_DISCOUNT,value);
        }
        return value;
    }


    // *********************** Item Discount *****************************
    public boolean addOptionalFeature(String featureId)
    {
        // Efficiency is not an issue here...
        if (!optionalFeatures.contains(featureId))
                //&& prod.getFeatures("OPTIONAL_FEATURE").contains(featureId))
        {
            optionalFeatures.add(featureId);
            return true;
        }
        return false;
    }

    public List<FeatureElement> getFeatures(String featureType)
    {
        if ("OPTIONAL_FEATURE".equals(featureType))
        {
            /*List<FeatureElement> l1 = prod.getFeatures(featureType);
            FastList<FeatureElement> l2 = FastList.newInstance();
            for(FeatureElement fae : l1)
                if (optionalFeatures.contains(fae.FeatureId))
                    l2.add(fae);
            return l2;*/
            FastList<FeatureElement> l = FastList.newInstance();
            for(String featureId: optionalFeatures) {
                FeatureElement f = resource.getProducts().getOptionalFeature(featureId);
                if (f != null) l.add(f);
            }
            return l;
        }
        else return prod.getFeatures(featureType);
    }

    public BigDecimal getFeatureDisplayValue(FeatureElement fae)
    {
        BigDecimal val = values.get(fae.FeatureId);
        if (val==null)
        {
            double fRate = fae.Price.getPrice(useGrossPrice).doubleValue();
            double v = fRate * quantity.doubleValue();
            val = new BigDecimal(v).setScale(resource.getSettings().getScale()
                    ,resource.getSettings().getRoundingMode());
        }
        return val;
    }

    public String getFeatureDisplayName(FeatureElement fae)
    {
        return SUB_ITEM_PREFIX + fae.Description;
    }

    // *********************** Value Cache Management *****************************
    public BigDecimal getValue(String valueId)
    {
        return values.get(valueId);
    }
    public void resetValues()
    {
        if (values ==null)
            values = new HashMap<String, BigDecimal>();
        else values.clear();
    }


    // *********************** Value Cache Management *****************************

    public int compareTo(PosShoppingCartItem item) {
        // The following is sorting, and we reversed
        // the logic to make it sort from max to min
        if (prePromoPrice==item.prePromoPrice) return 0;
        if (prePromoPrice<item.prePromoPrice) return 1;
        else return -1;
    }
}
