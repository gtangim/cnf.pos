/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.event;

import cnf.jpos.factory.CnfJposServiceFactory;
import javolution.util.FastMap;
import net.xoetrope.xui.XProjectManager;
import cnf.pos.util.*;
import cnf.pos.PosTransaction;
import cnf.pos.cart.resources.ResourceManager;
import cnf.pos.cart.resources.elements.UserElement;
import cnf.pos.component.Input;
import cnf.pos.device.DeviceLoader;
import cnf.pos.device.impl.FreemarkerReceipt;
import cnf.pos.screen.PaidInOut;
import cnf.pos.screen.PosScreen;
import cnf.pos.screen.RefundIdentificationSelector;

import java.math.BigDecimal;
import java.util.Map;

public class ManagerEvents {


    public static final String module = ManagerEvents.class.getName();
    public static boolean mgrLoggedIn = false;
    //static DecimalFormat priceDecimalFormat = new DecimalFormat("#,##0.00");

    // scales and rounding modes for BigDecimal math
    //public static final int scale = UtilNumber.getBigDecimalScale("order.decimals");
    //public static final int rounding = UtilNumber.getBigDecimalRoundingMode("order.rounding");
    //public static final BigDecimal ZERO = (BigDecimal.ZERO).setScale(scale, rounding);

    


/*    public static synchronized void modifyPriceSelectedItem(PosScreen pos)
    {
        Input input = pos.getInput();
        String value = input.value();
        
        if (UtilValidate.isNotEmpty(value)) {
        	pos.getInput().clear();
        	pos.getOutput().print(null);
            BigDecimal price = BigDecimal.ZERO;
            try {
                price = new BigDecimal(value);
                PosTransaction trans = PosTransaction.getCurrentTx(pos.getSession());
                price = price.movePointLeft(2);
                trans.modifyPriceSelected(price);        	
            } catch (NumberFormatException e) {
                pos.showDialog("dialog/error/exception", "Invalid Price");
            }
        }
    }  */

    
    /*
    public static synchronized void modifyPrice(PosScreen pos) {
        PosTransaction trans = PosTransaction.getCurrentTx(pos.getSession());
        String sku = null;
        try {
            sku = MenuEvents.getSelectedItem(pos);
        } catch (ArrayIndexOutOfBoundsException e) {
        }

        if (sku == null) {
            pos.getOutput().print(UtilProperties.getMessage(PosTransaction.stringResource,"PosInvalidSelection",Locale.getDefault()));
            //pos.getJournal().refresh(pos);
            pos.getInput().clear();
        }

        Input input = pos.getInput();
        String value = input.value();
        if (UtilValidate.isNotEmpty(value)) {
            BigDecimal price = ZERO;
            boolean parsed = false;
            try {
                price = new BigDecimal(value);
                parsed = true;
            } catch (NumberFormatException e) {
            }

            if (parsed) {
                price = price.movePointLeft(2);
                trans.modifyPrice(sku, price);

                // re-calc tax
                trans.calcTax();
            }
        }

        // refresh the other components
        pos.refresh();
    }*/

    public static void about(PosScreen pos)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("           POS Version:  CNF POS V" + ResourceManager.Version + "\r\n");
        //sb.append("    Bongo Sync Version:  " + ResourceManager.Resource.getNode().getVersion() + "\r\n");
        sb.append("   JPOS Driver Version:  Cnf Generic JPOS V" + CnfJposServiceFactory.Version + "\r\n");
        sb.append("    Comm Proxy Version:  COM Surrogate V" + CnfJposServiceFactory.CommSurrogateVersion + "\r\n");
        sb.append("     Core Data Version:  " + ResourceManager.Resource.getProducts().getVersion() + "\r\n");
        sb.append(" Customer Data Version:  " + ResourceManager.Resource.getCustomers().getVersion() + "\r\n");
        sb.append(" Security Data Version:  " + ResourceManager.Resource.getSecurity().getVersion() + "\r\n");
        sb.append("          Developed by:  Dr. Russel Ahmed Apu\r\n");
        pos.getTerminalHelper().showInfo("About CNF POS", sb.toString());
    }

    public static void triggerRefundMode(PosScreen pos)
    {
        if (!mgrLoggedIn) {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Enter Refund Mode"
                                    ,"Manager must be logged in to enter refund mode!");
            return;
        }
    	
    	PosScreen.getActiveScreen().getOutput().setProgress("Entering Refund Mode...");
        String value = pos.getInput().value();    	
    	if (value!=null && !value.isEmpty())
    	{
    		ManagerEvents.refundMode(pos); 
    	}
    	else
    	{
    		pos.getInput().clear();
    		pos.getInput().setFunction("REFUND");
    		pos.getOutput().print("Enter Invoice# or ID:");
    	}
        PosScreen.getActiveScreen().getOutput().setProgress(null);
    }

    public static void refundMode(PosScreen pos)
    {
        refundMode(pos,null);
    }
    public static void refundMode(PosScreen pos, String value)
    {
        if (!mgrLoggedIn) {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Enter Refund Mode"
                                    ,"Manager must be logged in to enter refund mode!");
            return;
        }

        Input input = pos.getInput();
        String src = "SCN";
        if (value==null) {value = input.value();src="KEY";}
        input.clear();
        pos.getOutput().print(null);
        
        if (Utility.isNotEmpty(value)) {
            RefundIdentificationSelector sel = new RefundIdentificationSelector(pos);
            int res = sel.openDlg();
            if (res!=RefundIdentificationSelector.ID_CANCELLED)
            {
                //XuiSession session = pos.getSession();
                PosTransaction trans = PosTransaction.getCurrentTx();
                if (res==RefundIdentificationSelector.ID_RECEIPT)
                {
                    String receiptId = trans.getResource().getSettings().filterReceipt(value);
                    if (receiptId!=null)
                        trans.enterRefundMode(src,"RECEIPT",receiptId);
                    else
                    {
                      pos.getTerminalHelper().showError("Error: Enter Refund Mode"
                                    ,"You entered an invalid receipt number: " + value+"!");
                    }
                }
                else if (res==RefundIdentificationSelector.ID_LICENSE)
                {
                    try
                    {
                        trans.getCart().setCustomerExternalId(value);
                        trans.enterRefundMode(src,"LICENSE",value);
                    }
                    catch (Exception ex)
                    {
                        pos.getTerminalHelper().showError("Error: Enter Refund Mode"
                                      ,"Failed to enter refund mode!",ex);
                    }
                }
                else
                {
                    if (pos.getPromoHelper().setCustomer("KEY",value))
                        trans.enterRefundMode(src,"CUSTOMER",value);
                    else pos.getTerminalHelper().showError("Error: Enter Refund Mode"
                                    ,"You entered a customer Id that doesn't exist: " + value+"!");
                }
            }
        }
    	
    }

    /*
    public static void voidOrder(PosScreen pos) {
        if (!mgrLoggedIn) {
            pos.showDialog("dialog/error/mgrnotloggedin");
            return;
        }

        XuiSession session = pos.getSession();
        PosTransaction trans = PosTransaction.getCurrentTx(session);
        if (!trans.isOpen()) {
            pos.showDialog("dialog/error/terminalclosed");
            return;
        }

        Output output = pos.getOutput();
        Input input = pos.getInput();
        boolean lookup = false;

        if (input.isFunctionSet("VOID")) {
            lookup = true;
        } else if (UtilValidate.isNotEmpty(input.value())) {
            lookup = true;
        }

        if (lookup) {
            PosTerminalState state = trans.getResource().getSettings().getState();
            Timestamp openDate = state.getOpenedDate();

            String orderId = input.value();
            GenericValue orderHeader = null;
            try {
                orderHeader = session.getDelegator().findByPrimaryKey("OrderHeader", UtilMisc.toMap("orderId", orderId));
            } catch (GenericEntityException e) {
                Debug.logError(e, module);
            }
            if (orderHeader == null) {
                input.clear();
                pos.showDialog("dialog/error/ordernotfound");
                return;
            } else {
                Timestamp orderDate = orderHeader.getTimestamp("orderDate");
                //if (orderDate.after(openDate)) {
                    LocalDispatcher dispatcher = session.getDispatcher();
                    Map<String, Object> returnResp = null;
                    try {
                        returnResp = dispatcher.runSync("quickReturnOrder", UtilMisc.<String, Object>toMap("orderId", orderId,
                                                        "returnHeaderTypeId", "CUSTOMER_RETURN", "userLogin", session.getUserLogin()));
                    } catch (GenericServiceException e) {
                        Debug.logError(e, module);
                        pos.showDialog("dialog/error/exception", e.getMessage());
                        pos.refresh();
                        return;
                    }
                    if (returnResp != null && ServiceUtil.isError(returnResp)) {
                        pos.showDialog("dialog/error/exception", ServiceUtil.getErrorMessage(returnResp));
                        pos.refresh();
                        return;
                    }

                    // todo print void receipt

                    trans.setTxAsReturn((String) returnResp.get("returnId"));
                    input.clear();
                    pos.showDialog("dialog/error/salevoided");
                    pos.refresh();
                //} else {
                //    input.clear();
                //    pos.showDialog("dialog/error/ordernotfound");
                //    return;
                //}
            }
        } else {
            input.setFunction("VOID");
            output.print(UtilProperties.getMessage(PosTransaction.stringResource,"PosVoid",Locale.getDefault()));
        }
    }*/

    /*public static void printLoggedReceits(PosScreen pos) {
        if (!mgrLoggedIn) {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Print Logged Receipts"
                                    ,"Manager not logged in!");
            return;
        }
        //PosTransaction.PrintLoggedReceits();
        pos.refresh();
    } */

    /*
    public static void reprintLastTx(PosScreen pos) {
        PosScreen.getActiveScreen().getOutput().setProgress("Printing last receipt...");
        if (!mgrLoggedIn) {
            pos.showDialog("dialog/error/mgrnotloggedin");
            return;
        }
        DeviceLoader.receipt.reprintReceipt(true);
        pos.refresh();
        PosScreen.getActiveScreen().getOutput().setProgress(null);
    }
    */

    public static void popDrawer(PosScreen pos) {
        if (!mgrLoggedIn) {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Open Drawer"
                                    ,"Manager login required to open cash drawer!");
        } else {
            PosTransaction trans = PosTransaction.getCurrentTx();
            trans.popDrawer(true);
            pos.getOutput().setProgress(null);
            pos.refresh();
        }
    }

    /*public static void resetXui(PosScreen pos) {
        if (!mgrLoggedIn) {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Reset XUI"
                                    ,"Manager not logged in!");
        } else {
            XProjectManager.getCurrentProject().getPageManager().reset();
            pos.refresh();
        }
    }*/


    public static void shutdown(PosScreen pos) {
        PosScreen.getActiveScreen().getOutput().setProgress("Closing Application...");
        if (!mgrLoggedIn) {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Application Shutdown"
                                    ,"Manager must be logged in to exit this application!");
        } else {

            if (pos.getTerminalHelper().clearedAllTx("You are attempting to exit the POS Session")) {
                PosTransaction tx = PosTransaction.getCurrentTx();
                /*try {
                    tx.getResource().getNode().ping("OFFLINE");
                } catch (Exception ex) {
                }*/
                if (tx.getResource().getNode().postCashierLogoutToDataSyncQueue()) {
                    pos.getOutput().print("Pos is shutting down...");
                    //tx.closeAllActiveTransactions();
                    tx.getResource().getNode().clearCustomerScreen();
                    System.exit(0);
                }
            }
        }
    }


    public static void paidOut(PosScreen pos) {
        paidOutAndIn(pos, "OUT");
    }

    public static void paidIn(PosScreen pos) {
        paidOutAndIn(pos, "IN");
    }

    public static void paidOutAndIn(PosScreen pos, String type) {
        if (!mgrLoggedIn) {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: pay In/Out"
                    , "Cannot pay IN/OUT because manager is not logged in!");
            return;
        }

        PosTransaction trans = PosTransaction.getCurrentTx();


        PaidInOut PaidInOut = new PaidInOut(trans.getResource(), pos, type);
        Map mapInOut = PaidInOut.openDlg();
        if (mapInOut.get("amount")!=null) {
            String amount = (String) mapInOut.get("amount");
            BigDecimal amt = pos.getPaymentHelper().getAmount(amount);
            if (amt == null) return;
            mapInOut.put("value",amt);
            String reason = (String) mapInOut.get("reason");
            String reasonText = (String) mapInOut.get("reasonText");
            String comment = (String) mapInOut.get("reasonComment");

            boolean res = pos.getTerminalHelper().confirm("Pay " + type.toLowerCase() + " confirmation",
                    "You are about to pay " + type.toLowerCase() + " with the following details:"
                            + "\r\nAmount: " + pos.getTerminalHelper().formatCurrency(amt)
                            + "\r\nReason: " + reasonText
                            + "\r\nComment: " + comment,
                    "Are you sure you want to finish paying " + type.toLowerCase() + "?");

            if (res) trans.paidInOut(type, mapInOut);
        } else pos.getOutput().setHint("Pay " + type + " Cancelled!");
    }


    public static void toggleTrainingMode(PosScreen pos)
    {
        pos.getTerminalHelper().toggleTrainingMode();
    }
    public static void unlockUser(PosScreen pos)
    {
        unlockUser(pos,null);
    }
    public static void unlockUser(PosScreen pos,String val)
    {
        pos.getOutput().print(null);
        pos.getInput().clear();
        if (!mgrLoggedIn) {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Unlock User"
                    ,"Manager must be logged in to unlock a user!");
            return;
        }
        //String Val = pos.getInput().value();
        if (val!=null && !val.isEmpty()) {
            pos.getInput().clearInput();
            UserElement u = null;
            try {
                u = ResourceManager.Resource.getSecurity().getUser(val);
            } catch (Exception e) {
                pos.getTerminalHelper().showError("Error: Unlock User", "User login with Id " + val + " not found!");
            }
            if (u == null)
                pos.getTerminalHelper().showError("Error: Unlock User", "User login with Id " + val + " not found!");
            else {
                boolean unlock = pos.getTerminalHelper().confirm("Unlock User?"
                        , "You are about to unlock user " + u.Description + "?"
                        + "\r\nAfter unlocking the user will be able to log in to the POS!"
                        , "Do you want to unlock this user?");
                if (unlock) {
                    ResourceManager.Resource.getSecurity().unlockUser(u.UserId);
                    pos.getOutput().setHint("User Unlocked!");
                }
            }
        }
        else
        {
            pos.getInput().clear();
            pos.getInput().setFunction("ULUNLOCK");
            pos.getOutput().print("Enter user ID:");
        }
    }

    public static void triggerExternalCustomerId(PosScreen pos) {
        if (!mgrLoggedIn) {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Input Driver License#"
                                    ,"Manager not logged in!");
        } else {
            pos.getCustomerHelper().selectExternalCustomerId();
        }
    }

    private static void printCashInOutReport(PosTransaction trans, String exchangeType, BigDecimal amount, String reason, String comment)
    {
        Map<String, String> reportMap = FastMap.newInstance();
        String reportTemplate = "cashinout.txt";
        reportMap.put("term", "Term:");
        reportMap.put("draw", "Dr:");
        reportMap.put("clerk", "Clerk:");
        reportMap.put("trans", "Trans Id:");
        reportMap.put("exchangeType", exchangeType);
        reportMap.put("exchangeAmount", Utility.formatPrice(amount));
        reportMap.put("exchangeReason", reason);
        reportMap.put("exchangeComment", comment);
       	FreemarkerReceipt receipt = DeviceLoader.receipt;
       	if (receipt!=null)
       	{
       		if (receipt.isEnabled()) {
       			//receipt.printReport(trans, reportTemplate, reportMap);
       		}
       	}

    }





}
