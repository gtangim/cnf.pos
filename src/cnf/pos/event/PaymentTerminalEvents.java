/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
package cnf.pos.event;

import cnf.pos.cart.resources.ResourceManager;
import cnf.pos.util.Debug;
import cnf.pos.PosTransaction;
import cnf.pos.device.DeviceLoader;
import cnf.pos.device.impl.PaymentTerminal.CardType;
import cnf.pos.screen.PosScreen;

import java.math.BigDecimal;

public class PaymentTerminalEvents {


    public static final String module = PaymentTerminalEvents.class.getName();

    private static void processCATResponse(PosScreen pos, PosTransaction trans)
    {
    	/*// First check if it was debit or credit....
    	// Some payment terminals doesnt allow the POS selecting the cardtype, it would depend on what clerk swipes
    	CardType media = DeviceLoader.paymentTerminal.getPaymentMedia();
        String tt = "CREDIT_CARD";
        if (media==CardType.DEBIT) tt = "DEBIT_CARD";

        //String ref = DeviceLoader.paymentTerminal.getPartialCardId() + "/" + DeviceLoader.paymentTerminal.getInvoiceId() + "/" + DeviceLoader.paymentTerminal.getAuthCode();
    	//BigDecimal due = trans.addPayment(tt,"Crd:"+DeviceLoader.paymentTerminal.getPartialCardId() + ", Auth:" + DeviceLoader.paymentTerminal.getAuthCode() + ", Inv:"+DeviceLoader.paymentTerminal.getInvoiceId(), amount,ref
        //		, DeviceLoader.paymentTerminal.getAuthCode());
        //trans.setAdditionalPaymentInfo(tt,ref, DeviceLoader.paymentTerminal.getTransactionResponse());

        PaymentTerminal term = DeviceLoader.paymentTerminal;
        BigDecimal amount = new BigDecimal(term.getAmount());
        amount = amount.movePointLeft(2);

        try
        {
            trans.getCart().addPayment(amount.toString(),trans.isRefundMode(),
                tt,"",term.getCardCompanyId(),term.getPartialCardId()
                ,term.getAuthCode(),term.getExpiry(), term.getTransactionResponse());
        }
        catch (GeneralException ex)
        {
            pos.showDialog("dialog/error/exception", "Payment Error!"+ex.getMessage());
        }

        if (!trans.isRefundMode() && trans.getCart().getDue().compareTo(BigDecimal.ZERO)<=0)
        	PaymentEvents.processSale(pos);
    */
    }

    public static void payCard(PosScreen pos, CardType cardType, String cardTypeText)
    {
        PosTransaction trans = PosTransaction.getCurrentTx();

        if (DeviceLoader.paymentTerminal==null)
            pos.getOutput().print("CAT driver not found!");
        else
        {
            try {
                BigDecimal amount = pos.getPaymentHelper().processAmount(null,"Card",true,true);
                if (amount==null) throw new Exception("Invalid amount!");
                Debug.log("Processing ["+cardTypeText+"] Amount : " + amount, module);
                int txid = (int)ResourceManager.Resource.getSecurity().getNextTxId();
                //txid = txid*100 + nextID();
                if (amount.compareTo(BigDecimal.ZERO)>0)
                {
                    if (DeviceLoader.paymentTerminal.sale(cardType, txid, amount, BigDecimal.ZERO))
                        processCATResponse(pos,trans);
                    else pos.getOutput().setHint("Transaction failed!");
                }
                else if (amount.compareTo(BigDecimal.ZERO)<0)
                {
                    if (DeviceLoader.paymentTerminal.refundSale(cardType, txid, amount.negate(), BigDecimal.ZERO))
                        processCATResponse(pos,trans);
                    else pos.getOutput().setHint("Transaction failed!");
                }
                else PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Payment Terminal"
                                    ,"Cannot apply a 0 (Zero) payment/refund to the card!");

            } catch (Exception e) {
                PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Payment Terminal"
                                        ,"Amount Error!",e);
            }
        }
        pos.refresh();

    }
    
    public static void payDebit(PosScreen pos) {
        payCard(pos,CardType.DEBIT,"Debit");
    }



    public static void payCredit(PosScreen pos) {
        payCard(pos,CardType.CREDIT,"Credit");
    }
    
    public static void payDemoCard(PosScreen pos,String cType,String cTypeText)
    {
        try{
            PosTransaction trans = PosTransaction.getCurrentTx();

            int cardid = (int)(Math.random()*100000);
            int authid = (int)(Math.random()*10000000);
            int invid = (int)(Math.random()*10000000);
            String cardid_s = Integer.toString(cardid);
            String authid_s = Integer.toString(authid);
            String invid_s = Integer.toString(invid);

            BigDecimal amount = pos.getPaymentHelper().processAmount(null,"Card",true,true);
            if (amount==null) throw new Exception("Invalid amount!");
            if (amount.compareTo(BigDecimal.ZERO)==0)
            {
                PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Payment Terminal"
                    ,"Cannot apply a 0 (Zero) payment/refund to the card!");
                return;
            }
            Debug.log("Processing ["+cTypeText+"] Amount : " + amount, module);
            int txid = (int)ResourceManager.Resource.getSecurity().getNextTxId();

            //trans.getCart().addPayment(amount.toString(),trans.isRefundMode(),
            //        cType,cTypeText,"45101",cardid_s,authid_s,"0101", "DEMO PAYMENT!");

            if (!trans.isRefundMode() && trans.getCart().getDue().compareTo(BigDecimal.ZERO)<=0)
                PaymentEvents.processSale(pos);
            pos.refresh();
        }
        catch (Exception e) {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Payment Terminal"
                ,"Amount Error!",e);
        }
    }

    public static void payDemoDebit(PosScreen pos)
    {
        payDemoCard(pos,"DEBIT_CARD","Debit");
    }

    public static void payDemoCredit(PosScreen pos)
    {
        payDemoCard(pos,"CREDIT_CARD","Visa");
    }
    
    public static void payExternalCard(PosScreen pos, String cType,String cTypeText)
    {
        /*try{
            PosTransaction trans = PosTransaction.getCurrentTx(pos.getSession());

            int cardid = 2999;
            int authid = 999999;
            int invid = (int)(Math.random()*10000000);
            String cardid_s = Integer.toString(cardid);
            String authid_s = Integer.toString(authid);
            String invid_s = Integer.toString(invid);

            BigDecimal amount = processAmount(trans, pos, null);
            if (amount.compareTo(BigDecimal.ZERO)==0)
            {
                pos.showDialog("dialog/error/exception", "Cannot add zero payment/refund!");
                return;
            }
            Debug.log("Processing ["+cTypeText+"] Amount : " + amount, module);
            int txid = Integer.parseInt(ExtractNumbers(trans.getTransactionId()));
            txid = txid*100 + nextID();

            trans.getCart().addPayment(amount.toString(),trans.isRefundMode(),
                    cType,cTypeText,"45101",cardid_s,authid_s,"0101", "EXTERNAL PAYMENT!");


            if (!trans.isRefundMode() && trans.getCart().getDue().compareTo(BigDecimal.ZERO)<=0)
                PaymentEvents.processSale(pos);
            pos.refresh();
        }
        catch (GeneralException e) {
            pos.showDialog("dialog/error/exception", "Amount Error!");
        }  */

    }

    public static void payExternalDebit(PosScreen pos)
    {
        payDemoCard(pos,"DEBIT_CARD","Debit");
    }

    public static void payExternalCredit(PosScreen pos)
    {
        payDemoCard(pos,"CREDIT_CARD","Visa");
    }
    
    
    public static void voidSelectedPayment(PosScreen pos)
    {
        /*PosTransaction trans = PosTransaction.getCurrentTx(pos.getSession());
        ExtendedJournal j = pos.getJournal();
        
        if (j.getSelectedIndexType()==JournalEntryType.PAYMENT)
        {
        	//TODO: Fix this Index method....
            //int ind = j.getSelectedIndexInt();
        	int ind=0;
            if (ind!=-1)
        	{
        		PosShoppingCartPayment pi = trans.getCart().getPayment(ind);
        		String ttype = pi.getPayType();
    			if (ttype==null) ttype = "";
    			ttype = ttype.toLowerCase();
    			if (ttype.contains("cash") || ttype.contains("personal") || ttype.contains("gift"))
    			{
    				trans.clearPayment(ind);
    				//trans.popDrawer();
    				pos.refresh();
    				return;
    			}
    			else if (ttype.contains("credit") || ttype.contains("debit"))
    			{
    				if (trans.isRefundMode())
    				{
    	                pos.showDialog("dialog/error/exception", "Cannot void payment in refund mode!");    				
    					return;
    				}
    				BigDecimal amount = pi.getAmount();
    				if (amount.compareTo(BigDecimal.ZERO)<0)
    					amount = amount.negate();
    	            int txid = Integer.parseInt(ExtractNumbers(trans.getTransactionId()));
    	            txid = txid*100 + nextID();
    	            CardType ct;
    	            if (ttype.contains("credit")) ct=CardType.CREDIT;
    	            else ct = CardType.DEBIT;
    	            if (DeviceLoader.paymentTerminal.voidSale(ct, txid, amount, BigDecimal.ZERO,pi.getRefNum()))
    	            {	            	
        				trans.clearPayment(ind);
    	            }
    				pos.refresh();
    				return;    				
    			}
        		
        	}
        }    */
        
        /*
        String idx = journal.getSelectedIdx();
        if (UtilValidate.isNotEmpty(idx))
        {
        	int ind = -1;
        	try
        	{
        		ind=Integer.parseInt(idx);
        		if (ind>-1)
        		{
        			String ttype = journal.getSelectedRowCell(1);
        			if (ttype==null) ttype = "";
        			ttype = ttype.toLowerCase();
        			String txId = journal.getSelectedRowCell(0);
        			if (ttype.contains("cash") || ttype.contains("personal") || ttype.contains("gift"))
        			{
        				trans.clearPayment(ind);
        				pos.refresh();
        				return;
        			}
        			else if (ttype.toLowerCase().contains("credit") && UtilValidate.isEmpty(txId))
        			{
        				trans.clearPayment(ind);
        				pos.refresh();
        				return;
        			}
        			else if (ttype.contains("debit") || ttype.contains("credit"))
        			{
        				String amt_s = journal.getSelectedRowCell(4);
        				try
        				{
        					BigDecimal amt = new BigDecimal(amt_s);
        			    	if (amt.compareTo(BigDecimal.ZERO)<0) amt = amt.negate();
        					if (DeviceLoader.paymentTerminal.voidSale(ttype.contains("debit")?CardType.DEBIT:CardType.CREDIT
        							, nextID(), amt, BigDecimal.ZERO, txId))
        					{
        						trans.clearPayment(ind);
        						pos.refresh();
            					return;
        					}
        					else
        					{
            	        		pos.getOutput().print("Void Unsuccessful!");
        						return;
        					}
        				}
        				catch(Exception e)
        				{
        	        		pos.getOutput().print("Invalid amount in selected!");
        	        		return;
        				}
        			}        			
        		}
        	}
        	catch(Exception ex)
        	{
        		pos.getOutput().print("Internal error (ParseInt)!");
        		return;
        	}
        }
		pos.getOutput().print("Select a payment first!");   
		*/
    }
    
    


    private static String ExtractNumbers(String id)
    {
    	StringBuilder sb = new StringBuilder();
    	for(int i=0;i<id.length();i++)
    		if (Character.isDigit(id.charAt(i))) sb.append(id.charAt(i));
    	if (sb.length()==0) sb.append("0");
    	return sb.toString();    	
    }
    
    protected static int nid = 0;
    protected static int nextID(){nid++; nid=nid%100; return nid;}
}
