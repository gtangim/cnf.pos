/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.event;

import cnf.pos.cart.resources.ResourceManager;
import cnf.pos.cart.resources.elements.UserElement;
import cnf.pos.screen.PosScreen;
import cnf.pos.component.InputWithPassword;
import cnf.pos.component.Output;
import cnf.pos.PosTransaction;
import cnf.pos.util.Debug;
import cnf.pos.util.Utility;

public class SecurityEvents {


    public static final String module = SecurityEvents.class.getName();

    public static void login(PosScreen pos, String textInput) {
        pos.setWaitCursor();
        String[] func = pos.getInput().getFunction("LOGIN");
        if (func == null) {
            pos.getInput().setFunction("LOGIN", "");
        }
        baseLogin(pos, false, false, textInput);
        pos.setNormalCursor();
    }

    public static void logout(PosScreen pos) {
        if (pos.getTerminalHelper().clearedAllTx("You are attempting to logout from the POS")) {
            pos.setWaitCursor();
            PosTransaction trans = PosTransaction.getCurrentTx();
            if (trans.getResource().getNode().postCashierLogoutToDataSyncQueue()) {
                ResourceManager.Resource.getSecurity().logout();
                pos.showPage("pospanel");
                PosScreen.getActiveScreen().setLock(true);
                pos.setNormalCursor();
                trans.getResource().getNode().clearCustomerScreen();
            }
        }
    }

    public static void mgrLoginTab(PosScreen pos, String tab, String textInput) {
        pos.setWaitCursor();
        UserElement user = ResourceManager.Resource.getSecurity().getUser();
        if (user!=null && user.hasRole("MANAGER")) {
            ManagerEvents.mgrLoggedIn = true;
            pos.setTab(tab);
            PosScreen.currentScreen.getInput().clear();
        } else {
            String[] func = pos.getInput().getFunction("MGRLOGINTAB");
            if (func == null) {
                pos.getInput().setFunction("NEXTVALUE", tab);
                pos.getInput().setFunction("MGRLOGINTAB", "");
            }
            baseLogin(pos, true, true, textInput);
        }
        pos.setNormalCursor();
    }


    public static void mgrLoginPage(PosScreen pos, String newPage, String textInput) {
        pos.setWaitCursor();
        //XuiSession session = pos.getSession();
        UserElement user = ResourceManager.Resource.getSecurity().getUser();
        if (user!=null && user.hasRole("MANAGER")) {
            ManagerEvents.mgrLoggedIn = true;
            PosScreen newPos = pos.showPage(newPage);
            PosScreen.currentScreen.getInput().clear();
        } else {
            String[] func = pos.getInput().getFunction("MGRLOGIN");
            if (func == null) {
                pos.getInput().setFunction("NEXTVALUE", newPage);
                pos.getInput().setFunction("MGRLOGIN", "");
            }
            baseLogin(pos, true, false, textInput);
        }
        pos.setNormalCursor();
    }

    public static void lock(PosScreen pos) {
        pos.setLock(true);
    }

    public static void baseLogin(PosScreen pos, boolean mgr, boolean isTab, String text) {
        //XuiSession session = pos.getSession();
        Output output = pos.getOutput();
        InputWithPassword input = pos.getInput();

        String loginFunc = mgr ? "MGRLOGIN" : "LOGIN";
        if (mgr && isTab) loginFunc = "MGRLOGINTAB";
        String[] func = input.getLastFunction();
        //String text = input.value();
        if (text==null) text = "";
        if (func != null && func[0].equals(loginFunc)) {
            if (Utility.isEmpty(func[1]) && Utility.isEmpty(text)) {
                output.print("Enter User ID:");
                input.setFunction(loginFunc, text);
                input.setPasswordInput(false);
            } else if (Utility.isEmpty(func[1])) {
                output.print("Enter Password:");
                input.setFunction(loginFunc, text);
                input.setPasswordInput(true);
            } else {
                input.setPasswordInput(false);
                String username = func[1];
                String password = text;
                if (!mgr) {
                    boolean passed = false;
                    boolean retainTx = false;
                    if (ResourceManager.Resource.getSecurity().getUser()==null) {
                        // This is a fresh login user Credential....
                        try {
                            pos.setLock(true);
                            ResourceManager.Resource.getSecurity().authenticate(username,password);
                            if (!ResourceManager.Resource.getNode().postCashierLoginToDataSyncQueue())
                                throw new Exception("Queue failed!");
                            passed = true;
                        } catch (Exception e) {
                            input.clear();
                            ResourceManager.Resource.getSecurity().logout();
                            input.setFunction(loginFunc);
                            output.setHint(e.getMessage());
                            output.print("Enter User ID:");
                            pos.setLock(true);
                        }
                    }
                    else
                    {
                        // This is a locked terminal, please validate using same user or manager
                        try {
                            if (PosTransaction.transactionCompleted()) retainTx = false;
                            else retainTx = true;
                            UserElement uProxy = ResourceManager.Resource.getSecurity().authenticate(username,password);
                            if (uProxy==null) throw new Exception("Login Failed!");
                            if (uProxy.UserId.equals(ResourceManager.Resource.getSecurity().getUser().UserId))
                                passed=true;
                            else if (uProxy.hasRole("MANAGER"))
                                passed=true;
                            else throw new Exception("Access Denied!");

                        } catch (Exception e) {
                            input.clear();
                            input.setFunction(loginFunc);
                            output.setHint(e.getMessage());
                            output.print("Enter User ID:");
                            pos.setLock(true);
                        }
                    }

                    if (passed) {
                        input.clear();
                        pos.setLock(false);
                        pos.refresh();
                        PosScreen.getActiveScreen().getOutput().print(null);
                        if (!retainTx) PosTransaction.getNewTransaction();
                        return;
                    }
                } else {
                    UserElement mgrUl = null;
                    try {
                        mgrUl = ResourceManager.Resource.getSecurity().authenticate(username,password);
                    } catch (Exception e) {
                        output.setHint(e.getMessage());
                        output.print(null);
                        input.clear();
                    }
                    if (mgrUl != null) {
                        if (!mgrUl.hasRole("MANAGER")) {
                            output.setHint("User is not a valid manager!");
                            output.print(null);
                            input.clear();
                        } else {
                            ManagerEvents.mgrLoggedIn = true;
                            String next = input.getFunction("NEXTVALUE")[1];
                            input.clear();
                            if (isTab) pos.setTab(next);
                            else pos.showPage(next);
                        }
                    }
                }
            }
        } else {
            Debug.log("Login function called but not prepared as a function!", module);
        }
    }
}
