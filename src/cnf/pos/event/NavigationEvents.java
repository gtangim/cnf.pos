/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.event;

import cnf.pos.PosTransaction;
import cnf.pos.config.ButtonEventConfig;
import cnf.pos.screen.PosScreen;
import cnf.pos.util.Utility;

import java.awt.*;

public class NavigationEvents {


    public static void navigateTab(PosScreen pos, AWTEvent event)
    {
        String value=null;
        String buttonName = ButtonEventConfig.getButtonName(event);
        if (Utility.isNotEmpty(buttonName)) {
            if (buttonName.startsWith("NAV.")) {
                if (PosTransaction.getCurrentTx()!=null) {
                    PosTransaction.getCurrentTx().getCart().setPaymentProcessing(false);
                }
                String target = buttonName.substring(4);
                if (target.toLowerCase().contains("manager"))
                    SecurityEvents.mgrLoginTab(pos,target, null);
                else if (target.toLowerCase().contains("pay"))
                {
                    if (PosTransaction.getCurrentTx()!=null) {
                        PosTransaction.getCurrentTx().getCart().setPaymentProcessing(true);
                    }
                    ManagerEvents.mgrLoggedIn=false;
                    pos.setTab(target);
                }
                else
                {
                    ManagerEvents.mgrLoggedIn=false;
                    pos.setTab(target);
                }
                if (PosTransaction.getCurrentTx()!=null) {
                    pos.getResource().getNode().postSalesToSecondScreen(PosTransaction.getCurrentTx().getCart());
                }
            }
            else PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Nav Button"
                    ,"invalid button "+buttonName+". Must start with \"NAV.\".");
        }

    }

}


