/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.event;

import net.xoetrope.xui.helper.SwingWorker;
import cnf.pos.util.Debug;
import cnf.pos.PosCardSettlementInfo;
import cnf.pos.PosTransaction;
import cnf.pos.cart.resources.ResourceManager;
import cnf.pos.cart.resources.datastructures.AtomicProductInput;
import cnf.pos.component.ExtendedJournal;
import cnf.pos.component.Input;
import cnf.pos.component.InputWithPassword;
import cnf.pos.component.Output;
import cnf.pos.config.ButtonEventConfig;
import cnf.pos.device.DeviceLoader;
import cnf.pos.device.impl.PaymentTerminal;
import cnf.pos.screen.PosScreen;
import cnf.pos.screen.Settlement;
import cnf.pos.uihelper.PaymentUIHelper;
import cnf.pos.util.Utility;
import org.joda.time.DateTime;

import java.awt.*;
import java.math.BigDecimal;
import java.net.URI;
import java.util.Map;

public class MenuEvents {

    public static final String module = MenuEvents.class.getName();

    // scales and rounding modes for BigDecimal math
    //public static final int scale = 2;
    //public static final int rounding = UtilNumber.getBigDecimalRoundingMode("order.rounding");
    //public static final BigDecimal ZERO = (BigDecimal.ZERO).setScale(scale, rounding);

    // extended number events
    public static void triggerClear(PosScreen pos) {
        // clear the pieces
        String[] totalFunc = pos.getInput().getFunction("TOTAL");
        String[] paidFunc = pos.getInput().getFunction("PAID");
        if (paidFunc != null) {
            pos.getInput().clear();
            pos.showStartupPage();
            PosTransaction trans = PosTransaction.getCurrentTx();
            trans.reset();
        } else {
            if (Utility.isEmpty(pos.getInput().value())) {
                pos.getInput().clear();
            }
            if (totalFunc != null) {
                pos.getInput().setFunction("TOTAL", totalFunc[1]);
            }
        }

        // refresh the current screen
        pos.refresh();

        // clear out the manual locks
        if (!pos.isLocked()) {
            pos.getInput().setLock(false);
            pos.getButtons().setLock(false);
        } else {
            // just re-call set lock
            pos.setLock(true);
        }
    }

    public static void toggleQtySign(PosScreen pos) {
        pos.getTerminalHelper().toggleQtySign();
    }
    public static void triggerQty(PosScreen pos) {
        PosScreen.getActiveScreen().getOutput().setProgress("Modifying Item Quantity...");
        String value = pos.getInput().value();    	
    	if (value!=null && !value.isEmpty())
    	{
    		pos.getProductHelper().modifyQuantitySelected();
    	}
    	else
    	{
    		pos.getInput().clear();
    		pos.getInput().setFunction("QTY");
    		pos.getOutput().print("Enter Quantity:");
    	}
    }

    public static void triggerPriceCheck(PosScreen pos) {
        PosScreen.getActiveScreen().getOutput().setProgress("Check Price (please scan)...");
        String value = pos.getInput().value();
        if (value!=null && !value.isEmpty())
        {
            pos.getProductHelper().showPrice(value);
            pos.getInput().clear();
            PosScreen.getActiveScreen().getOutput().clear();
        }
        else
        {
            pos.getInput().clear();
            pos.getInput().setFunction("PRICE");
            pos.getOutput().print("Enter/Scan Product:");
        }
    }
    public static void triggerCustomerPhone(PosScreen pos) {
        PosScreen.getActiveScreen().getOutput().setProgress("Enter Customer Phone#...");
        String value = pos.getInput().value();
        if (value!=null && !value.isEmpty())
        {
            pos.getPromoHelper().setCustomerByPhone("KEY", value);
            pos.getInput().clear();
            PosScreen.getActiveScreen().getOutput().clear();
        }
        else
        {
            pos.getInput().clear();
            pos.getInput().setFunction("PHONE");
            pos.getOutput().print("Enter Cust. Phone#:");
        }
    }

    public static boolean checkToken(String token){
        try
        {
            long v = Long.parseLong(token);
            v = 999999999L-v;
            DateTime dt = new DateTime();
            long dayOfYear = dt.getDayOfYear();
            long divisor = 19719 - dayOfYear;
            if (v%divisor==0) return true;
            else return false;
        }
        catch (Exception ex){
            return false;
        }
    }

    public static void triggerLoyaltyToken(PosScreen pos){
        PosScreen.getActiveScreen().getOutput().setProgress("Enter Loyalty Token#...");
        String value = pos.getInput().value();
        if (value!=null && !value.isEmpty())
        {
            if (checkToken(value)) pos.getPromoHelper().setUnfilteredPromoCode("KEY", "55400009925");
            else pos.getTerminalHelper().showError("Invalid Token#", "The token number you entered is not valid!");
            pos.getInput().clear();
            PosScreen.getActiveScreen().getOutput().clear();
        }
        else
        {
            pos.getInput().clear();
            pos.getInput().setFunction("LOYALTY");
            pos.getOutput().print("Enter Loyalty Tkn#:");
        }
    }


    public static void tare(PosScreen pos) {
        PosScreen.getActiveScreen().getOutput().setProgress("Setting Tare...");
        String value = pos.getInput().value();    	
    	if (value!=null && !value.isEmpty())
    	{
    		PosTransaction trans = PosTransaction.getCurrentTx();
    		if (!trans.setTare(value,"KEY"))
                PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Setting Tare"
                                        ,"You attempted to set an invalid tare value!\r\n"
                + "You have entered: " + value);
    	}
        PosScreen.getActiveScreen().getOutput().setProgress(null);
    }
    
    public static void tareScale(PosScreen pos)
    {
        PosScreen.getActiveScreen().getOutput().setProgress("Setting Tare...");
		PosTransaction trans = PosTransaction.getCurrentTx();
    	if (!trans.setTareFromScale())
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Setting Tare"
                                    ,"Unable to get the tare value from the scale!");
        PosScreen.getActiveScreen().getOutput().setProgress(null);
    }
    public static void triggerEnter(PosScreen pos, AWTEvent event)
    {
    	pos.getInput().appendString("\n");
    }

    
    public static void addItemToCart(AtomicProductInput p)
    {
        PosScreen pos = PosScreen.getActiveScreen();
        pos.getProductHelper().addItem(p);
    }


    public static void addItemFromButton(PosScreen pos, AWTEvent event) {
        String value=null;
        String buttonName = ButtonEventConfig.getButtonName(event);
        if (Utility.isNotEmpty(buttonName)) {
            if (buttonName.startsWith("SKU.")) {
                value = buttonName.substring(4);
                AtomicProductInput p = pos.getProductHelper().getFilteredInput("BTN", value);
                if (p==null)
                    PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Add Item"
                                            ,"Product Not found!\r\n" +
                                            "Input SKU: "+value);
                else addItemToCart(p);
            }
            else if (buttonName.startsWith("INP.")){
                pos.getInput().clear();
                if (!PosTransaction.transactionCompleted()) {
                    String[] inpList = buttonName.substring(4).split("\\|");
                    pos.getInput().appendString(inpList);
                }
                else {
                    pos.getInput().appendString("\n");
                }
            }
            else
                PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Add Item"
                                        ,"invalid button "+buttonName+". Must start with \"SKU.\" or \"INP.\".");
        }
    }

    public static void addItemFromInput(PosScreen pos, AWTEvent event)
    {
        String value = pos.getInput().value();
        AtomicProductInput p = pos.getProductHelper().getFilteredInput("KEY",value);
        if (p==null)
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Add Item"
                                    ,"Product Not found!\r\n" +
                                    "Input SKU: "+value);
        else addItemToCart(p);
    }




    /*
    public static void modifyQtySelectedItem(PosScreen pos)
    {
        PosScreen.getActiveScreen().getOutput().setProgress("Modifying item quanity...");
        Input input = pos.getInput();
        String value = input.value();
        
        if (UtilValidate.isNotEmpty(value)) {
        	pos.getInput().clear();
        	pos.getOutput().print(null);
            BigDecimal quantity = BigDecimal.ONE;
            try {
                quantity = new BigDecimal(value);
                PosTransaction trans = PosTransaction.getCurrentTx();
                if (!trans.isRefundMode() && quantity.compareTo(BigDecimal.ZERO)<=0)
                {
                    pos.showDialog("dialog/error/exception", "Invalid Quantity");
                    return;
                }
                trans.modifyQtySelected(quantity);        	
            } catch (NumberFormatException e) {
                pos.showDialog("dialog/error/exception", "Invalid Quantity");
            }
        }
    } */

    






    public static void triggerSaleDiscount(PosScreen pos) {
        PosScreen.getActiveScreen().getOutput().setProgress("Setting discount...");
        String value = pos.getInput().value();
    	if (value!=null && !value.isEmpty())
    	{
    		pos.getDiscountHelper().setSaleDiscount();
    	}
    	else
    	{
    		pos.getInput().clear();
    		pos.getInput().setFunction("SALEDISC");
    		pos.getOutput().print("Enter Sale Discount:");
    	}
    }
    public static void triggerItemDiscount(PosScreen pos) {
        PosScreen.getActiveScreen().getOutput().setProgress("Setting item discount...");
        String value = pos.getInput().value();
    	if (value!=null && !value.isEmpty())
    	{
            pos.getDiscountHelper().setSelectedItemDiscount();
        }
    	else
    	{
    		pos.getInput().clear();
    		pos.getInput().setFunction("ITEMDISC");
    		pos.getOutput().print("Enter Item Discount:");
    	}
    }
    public static void clearAllDiscount(PosScreen pos) {
        pos.getDiscountHelper().clearSaleDiscount(true);
    }
    public static void clearItemDiscount(PosScreen pos) {
        pos.getDiscountHelper().clearSelectedItemDiscount();
    }
    public static void clearSaleDiscount(PosScreen pos) {
        pos.getDiscountHelper().clearSaleDiscount(false);
    }
    public static void clearPromos(PosScreen pos) {
        pos.getPromoHelper().clearPromos();
    }
    public static void clearCustomer(PosScreen pos) {
        pos.getPromoHelper().clearCustomer();
    }






    public static void voidItem(PosScreen pos) {
        PosTransaction trans = PosTransaction.getCurrentTx();
        
        pos.getProductHelper().voidSelectedItem();
        pos.refresh();
    }

    public static void flipChart(PosScreen pos)
    {
        pos.getTerminalHelper().flipChart();
    }

    public static void voidAll(PosScreen pos) {
        pos.getTerminalHelper().voidSale();
    }

    public static void saveSale(PosScreen pos) {
        pos.getTerminalHelper().saveSale();
    }

    public static void loadSale(PosScreen pos) {
        pos.getTerminalHelper().loadSale();
    }

    public static String getSelectedItem(PosScreen pos) {
        ExtendedJournal journal = pos.getJournal();
        
        //return journal.getSelectedSku();
        return "";
    }

    public static void reprintLastTx(PosScreen pos) {
        pos.getOutput().setProgress("Printing last receipt...");
        PosTransaction trans = PosTransaction.getCurrentTx();
        if (trans.printLastReceipt())
            pos.getOutput().setHint("Reprinted last receipt!");
        PosScreen.getActiveScreen().getOutput().setProgress(null);
        pos.refresh();
    }
    public static void printCurrentTx(PosScreen pos) {
        pos.getOutput().setProgress("Printing last receipt...");
        PosTransaction trans = PosTransaction.getCurrentTx();
        if (trans.printCurrentReceipt())
            pos.getOutput().setHint("Printed current Trans!");
        PosScreen.getActiveScreen().getOutput().setProgress(null);
        pos.refresh();
    }




    // ********************** Helper Methods ****************************


    public static void processEnter(PosScreen pos, AWTEvent event) {
        //PosScreen.lockUI();
        if (!PosScreen.tryLockUI(100))
        {
            pos.getTerminalHelper().playWarning();
            return;
        }
        try {
            // enter key maps to various different events; depending on the function
            pos.setActivity();
            Input input = pos.getInput();
            //Debug.log("Enter key pressed, processing input data["+input.value()+"]...", module);
            PosTransaction trans = PosTransaction.getCurrentTx();
            if (ResourceManager.Resource.getSecurity().getUser()!=null
                    && !pos.isLocked() && PosTransaction.transactionCompleted())
            {
                // Create transaction if empty...
                String value = input.value();
                trans = PosTransaction.getNewTransaction();
                pos.setNormalCursor();
                pos.setLock(false);
                pos.showStartupPage();
                input.clear();
                // Carry the input forward...
                if (value==null || value.trim().isEmpty()) return;
                pos = PosScreen.getActiveScreen();
                //input.appendString(value+"\n"); // Dont accept input or scan after the tx-end this will cause bugs
            }

            String[] lastFunc = input.getLastFunction();
            if (lastFunc != null) {
                if ("MGRLOGIN".equals(lastFunc[0])) {
                    SecurityEvents.mgrLoginPage(pos,input.getFunction("NEXTVALUE")[1],input.value());
                } else if ("MGRLOGINTAB".equals(lastFunc[0])) {
                    SecurityEvents.mgrLoginTab(pos,input.getFunction("NEXTVALUE")[1], input.value());
                } else if ("LOGIN".equals(lastFunc[0])) {
                    SecurityEvents.login(pos, input.value());
                } else if ("ULUNLOCK".equals(lastFunc[0])) {
                    ManagerEvents.unlockUser(pos, input.value());
                } else if ("PAID_IN".equals(lastFunc[0])) {
                    ManagerEvents.paidOutAndIn(pos, "IN");
                } else if ("PAID_OUT".equals(lastFunc[0])) {
                    ManagerEvents.paidOutAndIn(pos, "OUT");
                } else if ("DEBIT".equals(lastFunc[0])) {
                    PaymentEvents.payManualDebit(pos);
                } else if ("CREDIT".equals(lastFunc[0])) {
                    PaymentEvents.payManualCredit(pos);
                } else if ("GIFTCARD".equals(lastFunc[0])) {
                    PaymentEvents.payManualGift(pos);
                } else if ("CHECK".equals(lastFunc[0])) {
                    PaymentEvents.payCheck(pos);
                } else if ("GIFTCERT".equals(lastFunc[0])) {
                    PaymentEvents.payGiftCert(pos);
                /*} else if ("COUPON".equals(lastFunc[0])) {
                    PaymentEvents.payVendorCoupon(pos);*/
                } else if ("SKU".equals(lastFunc[0])) {
                    MenuEvents.addItemFromInput(pos, event);
                } else if ("PROMOCODE".equals(lastFunc[0])) {
                    PromoEvents.addPromoCode(pos);
                }
                else if ("QTY".equals(lastFunc[0]))
                {
                    pos.getProductHelper().modifyQuantitySelected();
                }
                else if ("PRICE".equals(lastFunc[0]))
                {
                    pos.getProductHelper().showPrice(input.value());
                    input.clear();
                }
                else if ("PHONE".equals(lastFunc[0]))
                {
                    pos.getPromoHelper().setCustomerByPhone("KEY", input.value());
                    input.clear();
                }
                else if ("LOYALTY".equals(lastFunc[0]))
                {
                    if (checkToken(input.value())) pos.getPromoHelper().setUnfilteredPromoCode("KEY", "55400009925");
                    else pos.getTerminalHelper().showError("Invalid Token#", "The token number you entered is not valid!");
                    input.clear();
                }
                else if ("MOD".equals(lastFunc[0]))
                {
                    pos.getOverrideHelper().modifyPriceSelected(false);
                }
                else if ("STMOD".equals(lastFunc[0]))
                {
                    pos.getOverrideHelper().modifyPriceSelected(true);
                }
                else if ("ITEMDISC".equals(lastFunc[0]))
                {
                    pos.getDiscountHelper().setSelectedItemDiscount();
                }
                else if ("SALEDISC".equals(lastFunc[0]))
                {
                    pos.getDiscountHelper().setSaleDiscount();
                }
                else if ("REFUND".equals(lastFunc[0]))
                {
                    ManagerEvents.refundMode(pos);
                }
            } else if (input.value().length() > 1) {
                String val = input.value();
                // First See if this is a function
                if (val.startsWith("--"))
                {
                    val = val.substring(2);
                    trans.setTare(val, "KEY");
                    return;
                }
                else if (val.toLowerCase().startsWith("http://"))
                {
                    input.clear();
                    Desktop.getDesktop().browse(new URI(val));
                    return;
                }

                if (val.length()>16)
                {
                    PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Add Item"
                            ,"The value entered is invalid because it is too long!\r\n" +
                            "Input: "+val);
                    input.clear();
                    return;
                }
                // Find Customer or Promo...
                if (pos.getPromoHelper().setCustomer("KEY",val)
                        || pos.getPromoHelper().setPromoCode("KEY",val))
                    return;

                // Find Product...

                AtomicProductInput p = pos.getProductHelper().getFilteredInput("KEY",val);
                if (p==null)
                    PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Add Item"
                                            ,"Product Not found!\r\n" +
                                            "Input SKU: "+val);
                else addItemToCart(p);
                //Debug.log("Finished item entry...");
                if (input.value().length()>0)
                {
                    input.clear();
                }
            }
        } catch (Exception e) {
            Debug.log(e,"Unable to process enter!",module);
        } finally {
            PosScreen.unlockUI();
        }
    }

    public static void queueScanData(String value)
    {
        final String thisStr = value;
        final SwingWorker worker = new SwingWorker() {
            @Override
            public Object construct() {
                //PosScreen.lockUI();
                if (!PosScreen.tryLockUI(100))
                {
                    PosScreen.getActiveScreen().getTerminalHelper().playWarning();
                    return null;
                }
                try {
                    processScan(thisStr);
                } catch (Exception e) {
                    Debug.logError(e,"Unable to process scanner data!",module);
                } finally {
                    PosScreen.unlockUI();
                }
                return null;
            }
        };
        worker.start();
    }

    private static void processScan(String value) {
        PosScreen.lockUI();
        try {
            // enter key maps to various different events; depending on the function
            PosScreen pos = PosScreen.getActiveScreen();
            InputWithPassword input = pos.getInput();
            String[] lastFunc = pos.getInput().getLastFunction();
            pos.setActivity();
            pos.getInput().clearInput();
            //if ("LOGIN".equals(lastFunc[0])) pos.getInput().setFunction("LOGIN", "");
            //else if ("MGRLOGIN".equals(lastFunc[0])) pos.getInput().setFunction("MGRLOGIN", "");
            //else if ("MGRLOGINTAB".equals(lastFunc[0])) pos.getInput().setFunction("MGRLOGINTAB", "");
            Debug.log("Data from scanner ["+value+"]", module);
            PosTransaction trans = PosTransaction.getCurrentTx();
            if (ResourceManager.Resource.getSecurity().getUser()!=null
                    && !pos.isLocked() && PosTransaction.transactionCompleted())
            {
                // Create transaction if empty...
                trans = PosTransaction.getNewTransaction();
                pos.setNormalCursor();
                pos.setLock(false);
                pos.showStartupPage();
                // Carry the input forward...
                if (value==null || value.trim().isEmpty()) return;
                pos = PosScreen.getActiveScreen();
                pos.getInput().clear();
                lastFunc=null;
            }

            if (lastFunc != null) {
                Debug.log("Processing Scanner input for function: " + lastFunc[0]);
                /*if ("COUPON".equals(lastFunc[0])) {
                    PosScreen.getActiveScreen().getOutput().setProgress("Adding Payment...");
                    Output output = pos.getOutput();
                    if (pos.getTerminalHelper().TermClosed()) return;
                    pos.getPaymentHelper().payVendorCoupon("SCN",value);
                    pos.getPaymentHelper().autoFinishOrder();
                }*/
                if ("REFUND".equals(lastFunc[0]))
                {
                    ManagerEvents.refundMode(pos,value);
                }
                else if ("MGRLOGIN".equals(lastFunc[0])) {
                    SecurityEvents.mgrLoginPage(pos,input.getFunction("NEXTVALUE")[1],value);
                } else if ("MGRLOGINTAB".equals(lastFunc[0])) {
                    SecurityEvents.mgrLoginTab(pos,input.getFunction("NEXTVALUE")[1], value);
                } else if ("LOGIN".equals(lastFunc[0])) {
                    SecurityEvents.login(pos, value);
                } else if ("ULUNLOCK".equals(lastFunc[0])) {
                    ManagerEvents.unlockUser(pos, value);
                }
                else if ("PRICE".equals(lastFunc[0]))
                {
                    pos.getProductHelper().showPrice(value);
                }
                else input.clear();
            } else if (value.length() > 1) {
                if (value.startsWith("--"))
                {
                    value = value.substring(2);
                    trans.setTare(value,"SCN");
                    return;
                }

                // Find Customer or Promo...
                if (pos.getPromoHelper().setCustomer("SCN",value)
                        || pos.getPromoHelper().setPromoCode("SCN",value))
                    return;

                // Find Product...

                AtomicProductInput p = pos.getProductHelper().getFilteredInput("SCN",value);
                if (p==null)
                    PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Add Item"
                            ,"Product Not found!\r\n" +
                            "Input SKU: "+value);
                else addItemToCart(p);
                Debug.log("Finished item entry...");
            }
        } catch (Exception e) {
            Debug.log(e,"Unable to process enter!",module);
        } finally {
            PosScreen.unlockUI();
        }
    }



    public static void triggerPriceOverride(PosScreen pos) {
        PosScreen.getActiveScreen().getOutput().setProgress("Modifying item price...");
        String value = pos.getInput().value();
    	if (value!=null && !value.isEmpty())
    	{
    		//ManagerEvents.modifyPriceSelectedItem(pos);
    	    pos.getOverrideHelper().modifyPriceSelected(false);
        }
    	else
    	{
    		pos.getInput().clear();
    		pos.getInput().setFunction("MOD");
    		pos.getOutput().print("Enter Price:");
    	}
    }

    public static void triggerSubtotalOverride(PosScreen pos) {
        PosScreen.getActiveScreen().getOutput().setProgress("Modifying item subtotal...");
        String value = pos.getInput().value();
    	if (value!=null && !value.isEmpty())
    	{
    		//ManagerEvents.modifyPriceSelectedItem(pos);
    	    pos.getOverrideHelper().modifyPriceSelected(true);
        }
    	else
    	{
    		pos.getInput().clear();
    		pos.getInput().setFunction("STMOD");
    		pos.getOutput().print("Enter Item Subtotal:");
    	}
    }

    public static void triggerClearOverride(PosScreen pos)
    {
        pos.getOverrideHelper().clearSelectedOverride();
    }

    public static void triggerEmployeeBulkPricing(PosScreen pos)
    {
        pos.getProductHelper().selectEmployeePricing();
    }


    public static void test1(PosScreen pos)
    {
        // This is occasionally needed to simulate test code...
        PosScreen.lockUI();
        try
        {
            for(int i=0;i<70;i++)
            {
                Debug.logInfo("Test 1....",module);
                PosScreen.getActiveScreen().getOutput()
                        .setProgress("Test1 Waiting..." + Integer.toString(i));
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
            }
        }
        finally{
            PosScreen.unlockUI();
        }
    }

    public static void test2(PosScreen pos)
    {
        // This is occasionally needed to simulate test code...
        for(int i=0;i<10;i++)
        {
            Debug.logInfo("Test 2....",module);
            PosScreen.getActiveScreen().getOutput()
                    .setProgress("Test2 Waiting..." + Integer.toString(i));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }


}
