/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.event;

import cnf.pos.PosTransaction;
import cnf.pos.component.Input;
import cnf.pos.component.Output;
import cnf.pos.screen.PosScreen;
import cnf.pos.util.Utility;

public class PromoEvents {


    public static final String module = PromoEvents.class.getName();

    public static void addPromoCode(PosScreen pos) {
        PosTransaction trans = PosTransaction.getCurrentTx();
        Input input = pos.getInput();
        String[] lastFunc = input.getLastFunction();
        if (lastFunc == null || !"PROMOCODE".equals(lastFunc[0])) {
            Output output = pos.getOutput();
            input.setFunction("PROMOCODE");
            output.print("Enter Promo Code:");
        } else if ("PROMOCODE".equals(lastFunc[0])) {
            String promoCode = input.value();
            if (Utility.isNotEmpty(promoCode)) {
                /*String result = trans.addProductPromoCode(promoCode);
                if (result != null) {
                    pos.showDialog("dialog/error/exception", result);
                    input.clearFunction("PROMOCODE");
                } else {
                    input.clearFunction("PROMOCODE");
                    pos.getPromoStatusBar().addPromoCode(promoCode);
                    //NavigationEvents.showPosScreen(pos);
                    //pos.showStartupPage();
                    pos.refresh();
                }*/
            }
        }
    }

    public static void clientProfile(PosScreen pos) {
        PosTransaction trans = PosTransaction.getCurrentTx();
        //trans.clientProfile(pos);
    }
}
