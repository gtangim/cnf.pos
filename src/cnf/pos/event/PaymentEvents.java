/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.event;

import cnf.pos.PosTransaction;
import cnf.pos.cart.PosShoppingCart;
import cnf.pos.cart.PosShoppingCartPayment;
import cnf.pos.cart.resources.elements.BillingAccountElement;
import cnf.pos.component.Input;
import cnf.pos.component.Output;
import cnf.pos.device.impl.PaymentTerminal;
import cnf.pos.screen.PosScreen;
import cnf.pos.util.Utility;

public class PaymentEvents {


    public static final String module = PaymentEvents.class.getName();


    public static void payDebitCard(PosScreen pos)
    {
        pos.getPaymentHelper().payWithCATerminal(PaymentTerminal.CardType.DEBIT);
        pos.getPaymentHelper().autoFinishOrder();
    }
    public static void payCreditCard(PosScreen pos)
    {
        pos.getPaymentHelper().payWithCATerminal(PaymentTerminal.CardType.CREDIT);
        pos.getPaymentHelper().autoFinishOrder();
    }
    public static void payGiftCard(PosScreen pos)
    {
        pos.getPaymentHelper().payWithCATerminal(PaymentTerminal.CardType.GIFT);
        pos.getPaymentHelper().autoFinishOrder();
    }


    public static void payCash(PosScreen pos)
    {
        pos.getPaymentHelper().payCash(null);
        pos.getPaymentHelper().autoFinishOrder();
    }

    public static void payUSCash(PosScreen pos)
    {
        pos.getPaymentHelper().payCash("USD");
        pos.getPaymentHelper().autoFinishOrder();
    }

    public static void payToBillingAccount(PosScreen pos)
    {
        boolean success = pos.getPaymentHelper().payBillingAccount(false);
        if (success) pos.getPaymentHelper().autoFinishOrder();
    }

    public static void payDonation(PosScreen pos)
    {
        try {
            PosTransaction trans = PosTransaction.getCurrentTx();
            if (trans.getCart().isRefundPaymentNeeded())
            {
                pos.getTerminalHelper().showError("Error: Refund Customer"
                        , "This payment method cannot be chosen for a refund!\r\n");
                return;
            }
            boolean success = pos.getPaymentHelper().payBillingAccount(true);
            if (success)
            {
                pos.getPaymentHelper().autoFinishOrder();
                PosShoppingCart cart = trans.getCart();
                if (!cart.isReadOnly()) throw new Exception("Billing account sale did not autofinish!");
                PosTransaction roaTrans = PosTransaction.getNewTransaction();
                roaTrans.setTransactionMode(3); // Switch the transaction to ROA Mode...
                roaTrans.getCart().setCustomer(cart.getCustomer());
                roaTrans.getCart().setRoaMode(true,cart.getBillingAccountId());
                roaTrans.getCart().addPayment(new PosShoppingCartPayment(
                        roaTrans.getResource(),"DONATION",cart.getGrandTotal(),null,cart.getBillingAccountId(),null,
                        null,null,null,null,false
                ));
                roaTrans.setReceiptPrint(true);
                roaTrans.finalizeSale();
                PosTransaction.setTransaction(trans);
                PosTransaction.setRoaReprintMode();
            }
        } catch (Exception e) {
            pos.getTerminalHelper().showError("Error: Register Donation","Unable to process this sale as donation!"
            +"\r\nPlease contact IT immediately (take a screenshot).",e);
        }
    }

    public static void payVendorCoupon(PosScreen pos) {
        /*if (PosTransaction.getCurrentTx().getCart().isRefundPaymentNeeded())
        {
            pos.getTerminalHelper().showError("Error: Refund Customer"
                    , "This payment method cannot be chosen for a refund!\r\n");
            return;
        }*/
        PosScreen.getActiveScreen().getOutput().setProgress("Adding Payment...");
        Output output = pos.getOutput();
        pos.getPaymentHelper().payVendorCoupon("KEY","0000");
        pos.getPaymentHelper().autoFinishOrder();

        /*String[] info = getFuncValues(pos,"COUPON");
        switch (info.length) {
            case 0:
                output.print("Enter Coupon#");
                break;
           case 1:
                pos.getPaymentHelper().payVendorCoupon("KEY",info[0]);
                pos.getPaymentHelper().autoFinishOrder();
                break;
        }*/
    }


    public static void payManualCredit(PosScreen pos) {
        PosScreen.getActiveScreen().getOutput().setProgress("Adding Payment...");
        Output output = pos.getOutput();
        String[] info = getFuncValues(pos,"CREDIT");
        switch (info.length) {
            case 0:
                output.print("Enter Amount:");
                break;
            case 1:
                output.print("Enter Card#");
                break;
            case 2:
                output.print("Enter Expiry(MMYY):");
                break;
            case 3:
                output.print("Enter invoice#");
                break;
            case 4:
                pos.getPaymentHelper().payAdvanced("CREDIT_CARD",null,info);
                pos.getPaymentHelper().autoFinishOrder();
                break;
        }
    }


    public static void payManualDebit(PosScreen pos) {
        PosScreen.getActiveScreen().getOutput().setProgress("Adding Payment...");
        Output output = pos.getOutput();
        String[] info = getFuncValues(pos,"DEBIT");
        switch (info.length) {
            case 0:
                output.print("Enter Amount:");
                break;
            case 1:
                output.print("Enter Card#");
                break;
            case 2:
                output.print("Enter Invoice#");
                break;
           case 3:
                pos.getPaymentHelper().payAdvanced("DEBIT_CARD",null,info);
                pos.getPaymentHelper().autoFinishOrder();
                break;
        }
    }

    public static void payCheck(PosScreen pos) {
        if (PosTransaction.getCurrentTx().getCart().isRefundPaymentNeeded())
        {
            pos.getTerminalHelper().showError("Error: Refund Customer"
                    , "This payment method cannot be chosen for a refund!\r\n");
            return;
        }
        PosScreen.getActiveScreen().getOutput().setProgress("Adding Payment...");
        Output output = pos.getOutput();
        String[] info = getFuncValues(pos,"CHECK");
        switch (info.length) {
            case 0:
                output.print("Enter Amount:");
                break;
            case 1:
                output.print("Enter Ref#");
                break;
           case 2:
                pos.getPaymentHelper().payAdvanced("PERSONAL_CHECK",null,info);
                pos.getPaymentHelper().autoFinishOrder();
                break;
        }
    }

    public static void payGiftCert(PosScreen pos) {
        PosScreen.getActiveScreen().getOutput().setProgress("Adding Payment...");
        Output output = pos.getOutput();
        PosShoppingCart cart = PosTransaction.getCurrentTx().getCart();
        if (cart.isRefundPaymentNeeded())
        {
            pos.getTerminalHelper().showError("Error: Refund Customer"
                    , "Refunding customer in Gift Certificate is not allowed!\r\n"
                        +"You can however add a gift certificate to the order and click Cash to finish!");
            return;
        }
        String[] info = getFuncValues(pos,"GIFTCERT");
        switch (info.length) {
            case 0:
                output.print("Enter Amount:");
                break;
            case 1:
                output.print("Enter Ref#");
                break;
           case 2:
                pos.getPaymentHelper().payAdvanced("GIFT_CERTIFICATE",null,info);
                pos.getPaymentHelper().autoFinishOrder();
                break;
        }
    }



    public static void payManualGift(PosScreen pos) {
        PosScreen.getActiveScreen().getOutput().setProgress("Adding Payment...");
        Output output = pos.getOutput();
        String[] info = getFuncValues(pos,"GIFTCARD");
        switch (info.length) {
            case 0:
                output.print("Enter Amount:");
                break;
            case 1:
                output.print("Enter Card#");
                break;
           case 2:
                pos.getPaymentHelper().payAdvanced("GIFT_CARD",null,info);
                pos.getPaymentHelper().autoFinishOrder();
                break;
        }
    }



    private static String[] getFuncValues(PosScreen pos, String fnc)
    {
        Output output = pos.getOutput();
        Input input = pos.getInput();
        if (!input.isFunctionSet(fnc)) {
            input.setFunction(fnc);
        }
        String[] func = input.getFunction(fnc);
        String lastValue = input.value();
        if (Utility.isNotEmpty(lastValue)) {
            if (Utility.isNotEmpty(func[1])) {
                func[1] = func[1] + "|";
            }
            func[1] = func[1] + lastValue;
            input.setFunction(fnc, func[1]);
        }

        String[] info = new String[0];
        if (Utility.isNotEmpty(func[1])) {
            info = func[1].split("\\|");
        }
        return info;
    }


    public static void voidSelectedPayment(PosScreen pos)
    {
        PosScreen.getActiveScreen().getOutput().setProgress("Void Payment...");
        Output output = pos.getOutput();
        pos.getPaymentHelper().voidPayment();

    }




    /*
    public static void payAccount(PosScreen pos) {
        PosTransaction trans = PosTransaction.getCurrentTx(pos.getSession());
        //trans.clearPayment("CASH");

        // all cash transactions are NO_PAYMENT; no need to check
        try {
            BigDecimal amount = processAmount(trans, pos, null);
            Debug.log("Processing [Account] Amount : " + amount, module);
            //TODO: Add code here...

            // add the payment
            //trans.addPayment("ACCOUNT", amount, null, null);
        } catch (GeneralException e) {
            // errors handled
        }
        clearInputPaymentFunctions(pos);
        pos.refresh();
    }

    public static void payManual(PosScreen pos) {
        PosTransaction trans = PosTransaction.getCurrentTx(pos.getSession());
        //trans.clearPayment("CASH");
        //TODO: Add code here...
        // all cash transactions are NO_PAYMENT; no need to check
        try {
            BigDecimal amount = processAmount(trans, pos, null);
            Debug.log("Processing [Manual Card] Amount : " + amount, module);

            // add the payment
            //trans.addPayment("ACCOUNT", amount, null, null);
        } catch (GeneralException e) {
            // errors handled
        }
        clearInputPaymentFunctions(pos);
        pos.refresh();
    } */
    
    /*private static void processNoPayment(PosScreen pos, String paymentMethodTypeId) {
        PosTransaction trans = PosTransaction.getCurrentTx(pos.getSession());

        try {
            BigDecimal amount = processAmount(trans, pos, null);
            Debug.log("Processing [" + paymentMethodTypeId + "] Amount : " + amount, module);

            // add the payment
            trans.addPayment(paymentMethodTypeId, "", amount, null, null);
        } catch (GeneralException e) {
            // errors handled
        }
        clearInputPaymentFunctions(pos);
        pos.refresh();
    }
    
    private static void processExternalPayment(PosScreen pos, String paymentMethodTypeId, String amountStr) {
        PosTransaction trans = PosTransaction.getCurrentTx(pos.getSession());
        Input input = pos.getInput();
        String refNum = input.value();
        if (refNum == null) {
            pos.getOutput().print(UtilProperties.getMessage(PosTransaction.stringResource,"PosRefNum",Locale.getDefault()));
            return;
        }
        input.clearInput();

        try {
            BigDecimal amount = processAmount(trans, pos, amountStr);
            Debug.log("Processing [" + paymentMethodTypeId + "] Amount : " + amount, module);

            // add the payment
            trans.addPayment(paymentMethodTypeId, "",amount, refNum, null);
        } catch (GeneralException e) {
            // errors handled
        }
        clearInputPaymentFunctions(pos);
        pos.refresh();
    } */


    public static void processSale(PosScreen pos) {
        PosTransaction trans = PosTransaction.getCurrentTx();
        if (trans.getCart().canFinalize())
            pos.getPaymentHelper().autoFinishOrder();
        else
        {
            pos.getTerminalHelper().showError("Error: Finish Sale"
                                    ,"Unable to finalize sale!\r\nSale is not complete!");
        }
    }

    public static void triggerRoaMode(PosScreen pos)
    {
        try {
            PosTransaction trans = PosTransaction.getCurrentTx();
            PosShoppingCart cart = trans.getCart();
            if (trans.isRoaMode()) throw new Exception("You are already in the ROA mode!");
            if (cart.size()>0) throw new Exception("You must have an empty shopping cart to enter ROA mode!\r\n"
                +"Either finish this transaction or void it before entering ROA mode!");
            if (cart.getPayments().size()>0)
                throw new Exception("Cart must have no payments before entering ROA Mode!");
            if (cart.getCustomer()==null)
                throw new Exception("You must scan the customer loyalty card first!");
            if (cart.getCustomer().BillingAccounts.size()==0)
                throw new Exception("The customer you scanned has no billing account!");
            BillingAccountElement ba = pos.getCustomerHelper().getCustomerBillingAccount();
            if (ba==null) return;
            trans.enterRoaMode(ba);
        } catch (Exception e) {
            pos.getTerminalHelper().showError("Error: ROA Mode"
                                    ,"Unable to enter Receive of Account (ROA) mode!",e);
        }

    }
}
