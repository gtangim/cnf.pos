package cnf.pos.uihelper;

import cnf.node.entities.PosAct;
import cnf.pos.cart.resources.elements.ReasonElement;
import cnf.pos.screen.ReasonSelector;
import cnf.pos.util.Debug;
import cnf.pos.cart.PosShoppingCartItem;
import cnf.pos.component.ExtendedJournal;
import cnf.pos.component.Input;
import cnf.pos.util.Utility;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/23/11
 * Time: 10:08 AM
 * To change this template use File | Settings | File Templates.
 */
public class DiscountUIHelper extends BaseUIHelper {

    public static final String module = DiscountUIHelper.class.getName();

    public DiscountUIHelper()
    {
        super();
    }


    public void setSelectedItemDiscount()
    {
        try
        {
            refresh();
            pos.getOutput().setProgress("Adding Item Discount...");
            PosShoppingCartItem item = pos.getTerminalHelper().getSelectedJournalItem();
            if (!item.isDiscountAllowed())
                throw new Exception("Discount is not allowed on this item!");
            Input input = pos.getInput();
            String value = input.value();
            if (Utility.isNotEmpty(value)) {
                ReasonSelector dlg = new ReasonSelector("Cashier Item Discount", pos
                        ,r.getSettings().getItemDiscountReasons());
                ReasonElement res = dlg.openDlg();
                if (res==null)
                {
                    clearFunction();
                    return;
                }
                pos.getInput().clear();
                pos.getOutput().print(null);
                item.setDiscount(value, res.ReasonId, res.ReportingCode);
                String v = value;
                trans.history(new PosAct("KEY","idisc",item.getItemDiscount().getDiscountText()));

                pos.getOutput().setHint("Added item discount.");
                pos.getTerminalHelper().updateJournal(false);
            }
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
            pos.getTerminalHelper().showError("Error: Item Discount"
                    , "Unable to set the item discount!",ex);
        }

        clearFunction();
    }


    public void setSaleDiscount()
    {
        try
        {
            refresh();
            pos.getOutput().setProgress("Adding Sale Discount...");
            Input input = pos.getInput();
            String value = input.value();
            if (Utility.isNotEmpty(value)) {
                ReasonSelector dlg = new ReasonSelector("Cashier Order Discount", pos
                        ,r.getSettings().getSaleDiscountReasons());
                ReasonElement res = dlg.openDlg();
                if (res==null)
                {
                    clearFunction();
                    return;
                }
                pos.getInput().clear();
                pos.getOutput().print(null);
                trans.getCart().setDiscount(value, res.ReasonId, res.ReportingCode);
                pos.getOutput().setHint("Added sale discount.");
                trans.history(new PosAct("KEY","sdisc",trans.getCart().getSaleDiscount().getDiscountText()));
                pos.getTerminalHelper().updateJournal(false);
            }
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
            pos.getTerminalHelper().showError("Error: Sale Discount"
                    , "Unable to set the sale discount!",ex);
        }

        clearFunction();
    }


    public void clearSelectedItemDiscount()
    {
        try
        {
            refresh();
            pos.getOutput().setProgress("Clearing Item Discount...");
            ExtendedJournal j = pos.getJournal();
            PosShoppingCartItem item = pos.getTerminalHelper().getSelectedJournalItem();
            item.clearDiscount();
            int ind = trans.getCart().getItemIndex(item);
            trans.history(new PosAct("BTN","cidisc",Integer.toString(ind)));
            pos.getOutput().setHint("Item discount removed.");
            pos.getTerminalHelper().updateJournal(false);
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
            pos.getTerminalHelper().showError("Error: Clear Item Discount"
                    , "Unable to clear the item discount!\r\nHint: You must select an item first!",ex);
        }

        clearFunction();
    }

    public void clearSaleDiscount(boolean clearItemDiscounts)
    {
        try
        {
            refresh();
            pos.getOutput().setProgress("Clearing Sale Discount...");
            trans.getCart().clearDiscount(clearItemDiscounts);
            if (clearItemDiscounts) trans.history(new PosAct("BTN","cdisc"));
            else trans.history(new PosAct("BTN","csdisc"));
            pos.getOutput().setHint("Sale discount removed.");
            pos.getTerminalHelper().updateJournal(false);
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
            pos.getTerminalHelper().showError("Error: Clear Discounts"
                    , "Unable to clear the discounts!",ex);
        }

        clearFunction();
    }

}
