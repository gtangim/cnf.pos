package cnf.pos.uihelper;

import cnf.node.entities.*;
import cnf.pos.PosCardSettlementInfo;
import cnf.pos.cart.resources.ResourceManager;
import cnf.pos.cart.resources.SecurityResource;
import cnf.pos.cart.resources.datastructures.AtomicProductInput;
import cnf.pos.cart.resources.elements.ProductElement;
import cnf.pos.cart.resources.elements.ProductIdentificationElement;
import cnf.pos.device.DeviceLoader;
import cnf.pos.device.impl.PaymentTerminal;
import cnf.pos.util.*;
import cnf.pos.PosTransaction;
import cnf.pos.PosTransactionEntry;
import cnf.pos.cart.PosShoppingCart;
import cnf.pos.cart.PosShoppingCartItem;
import cnf.pos.cart.PosShoppingCartPayment;
import cnf.pos.component.*;
import cnf.pos.screen.*;
import javolution.util.FastList;
import javolution.util.FastMap;
import org.joda.time.DateTime;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;
import java.awt.Toolkit;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/23/11
 * Time: 10:10 AM
 * To change this template use File | Settings | File Templates.
 */
public class TerminalUIHelper extends BaseUIHelper {

    public static final String module = TerminalUIHelper.class.getName();
    Clip beep;
    Clip error;
    Clip warning;
    Clip question;



    public TerminalUIHelper()
    {
        super();
        beep = loadClip("resources/sounds/beep.wav");
        error = loadClip("resources/sounds/error.wav");
        warning = loadClip("resources/sounds/warning.wav");
        question = loadClip("resources/sounds/question.wav");
    }

    public Clip loadClip( String filename )
    {
        Clip in = null;

        try
        {
            AudioInputStream audioIn = AudioSystem.getAudioInputStream( UtilURL.fromResource(filename) );
            in = AudioSystem.getClip();
            in.open( audioIn );
        }
        catch( Exception e )
        {
            Debug.logError(e,"Unable to load sound file: " + filename + "!");
        }

        return in;
    }

    public void playBeep()
    {
        try {
            if (beep.isRunning()) beep.stop();
            beep.setFramePosition(0);
            beep.start();
        } catch (Exception e) {
            Debug.logError(e,"Unable to play sound file!");
        }
    }
    public void playError()
    {
        try {
            if (error.isRunning()) error.stop();
            error.setFramePosition(0);
            error.start();
        } catch (Exception e) {
            Debug.logError(e, "Unable to play sound file!");
        }
    }
    public void playWarning()
    {
        try {
            if (warning.isRunning()) warning.stop();
            warning.setFramePosition(0);
            warning.start();
        } catch (Exception e) {
            Debug.logError(e, "Unable to play sound file!");
        }
    }
    public void playQuestion()
    {
        try {
            if (question.isRunning()) question.stop();
            question.setFramePosition(0);
            question.start();
        } catch (Exception e) {
            Debug.logError(e,"Unable to play sound file!");
        }
    }

    public void loadSale()
    {
        try
        {
            refresh();
            pos.getOutput().print(null);
            pos.getOutput().setProgress("Loading Saved Sale...");
            if (!trans.getCart().isEmpty())
                throw new Exception("Error: Shopping cart must be empty before loading sale!");
            LoadSale loader = new LoadSale(pos);
            PosTransactionEntry selectedTrans = loader.openDlg();
            if (selectedTrans!=null)
            {
                PosTransaction.loadTx(selectedTrans);
                pos.getOutput().setHint("Restored Saved Sale.");
                refresh();
                pos.getTerminalHelper().updateJournal(true);
            }
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
            pos.getTerminalHelper().showError("Error: Load Transaction"
                    , "Unable to load the selected saved transaction!", ex);
        }

        clearFunction();
    }
    public void flipChart()
    {
        try
        {
            refresh();
            pos.getOutput().print(null);
            pos.getOutput().setProgress("Loading Saved Sale...");
            Map<String, String> source = r.getProducts().getFlipProducts();
            playQuestion();
            FlipChart flip = new FlipChart(source,trans,pos);
            String selection = flip.openDlg();
            if (selection==null) return;
            String[] ids = selection.split("_");
            AtomicProductInput p = new AtomicProductInput("BTN",ids[0],ids[0],ids[1],null,null);
            List<ProductIdentificationElement> iList = FastList.newInstance();
            List<ProductElement> pList = FastList.newInstance();
            pList.add(r.getProducts().getProduct(ids[1]));
            ProductIdentificationElement pi = new ProductIdentificationElement();
            pi.ProductId = ids[1];
            pi.IdType = "SKU";
            pi.ProductCode = ids[0];
            iList.add(pi);
            p.setSelectableProductIds(iList);
            p.setSelectableProducts(pList);
            p.setItemType("Item");
            pos.getProductHelper().addItem(p);
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
            pos.getTerminalHelper().showError("Error: Load Transaction"
                    , "Unable to load the selected saved transaction!", ex);
        }

        clearFunction();
    }

    public void saveSale()
    {
        try
        {
            refresh();
            pos.getOutput().print(null);
            pos.getOutput().setProgress("Saving Sale...");
            if (trans.getCart().isEmpty())
                throw new Exception("Error: Cannot save empty shopping cart!");
            else if (trans.getCart().getPayments().size()>0)
                throw new Exception("Error: Cannot save/park with payment information (do finish or void)!");
            SaveSale saver = new SaveSale(pos);
            PosTransactionEntry selectedTrans = saver.openDlg();
            if (selectedTrans!=null)
            {
                PosTransaction.saveTx(selectedTrans);
                pos.getOutput().setHint("Saved Sale.");
            }
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
            pos.getTerminalHelper().showError("Error: Save Transaction"
                    , "Unable to save the current transaction!", ex);
        }

        clearFunction();
    }


    public BigDecimal getPosQuantity()
    {
        refresh();
        Input input = pos.getInput();
        String[] func = input.getFunction("QTY");
        // check for quantity
        BigDecimal quantity = BigDecimal.ONE;
        if (func != null && "QTY".equals(func[0])) {
            try {
                quantity = new BigDecimal(func[1]);
            } catch (NumberFormatException e) {
                quantity = BigDecimal.ONE;
            }
        }
        return quantity;
    }


    public void clearInput()
    {
        try
        {
            refresh();
            Input input = pos.getInput();
            input.clearLastFunction();
            //input.clearFunction("QTY");
            input.clearInput();
        }
        catch (Exception ex){}
    }

    public void clearOutput()
    {
        // refresh the others
        try
        {
            refresh();
            pos.getOutput().setProgress(null);
            pos.getOutput().print(null);
            pos.refresh();
        }
        catch (Exception ex){}

    }

    public void selectLastItem()
    {
        try
        {
            refresh();
            ExtendedJournal j = pos.getJournal();
            j.selectLastItem("itm_");
        }
        catch(Exception ex)
        {
            // Silently record the problem...
            // Dont bother the user because, if there is a UI errors it may occur very frequently
            Debug.logError("****** Error: Unable to refresh screen! " + ex.getMessage(), module);

        }
    }

    public void updateJournal(boolean scrollToBottom)
    {
        try
        {
            refresh();
            trans.getCart().refreshCartTotals();
            ExtendedJournal j = pos.getJournal();
            j.reloadJournalFromCart(pos,trans.getCart(),scrollToBottom);
            r.getNode().postSalesToSecondScreen(trans.getCart());
        }
        catch(Exception ex)
        {
            // Silently record the problem...
            // Dont bother the user because, if there is a UI errors it may occur very frequently
            Debug.logError("****** Error: Unable to refresh screen! " + ex.getMessage(), module);

        }
    }


    public PosShoppingCartItem getSelectedJournalItem() throws GeneralException
    {
        refresh();
        if (pos==null) throw new GeneralException("Unable to find primary UI screen!");
        ExtendedJournal j = pos.getJournal();
        if (j==null) throw new GeneralException("Unable to find item journal!");
        if (j.getSelectedIndexType()== ExtendedJournal.JournalEntryType.ITEM)
        {
            int ind = j.getSelectedIndexInt();
            if (ind<0) throw new GeneralException("Please Select Item");
            if (ind>=trans.getCart().getItems().size())
                throw new GeneralException("Selection is out of range!");
            PosShoppingCartItem item = trans.getCart().getItems().get(ind);
            if (item==null) throw new GeneralException("Please Select Item");
            return item;
        }
        else throw new GeneralException(("Please Select Item"));
    }


    public void showError(String title, String message)
    {
        try
        {
            refresh();
            pos.getOutput().setProgress("Error: Please wait...");
            //Toolkit tk = Toolkit.getDefaultToolkit();
            //tk.beep();
            playError();
            Debug.logError(title+" - "+message,module);
            CommonDialog dlg = new CommonDialog(PosScreen.getActiveScreen(),"dialog/error/quickerror");
            dlg.openDlg(title,message,null,null);
            Debug.logInfo("***** Ended Error Dialog ********",module);
            pos.getOutput().setProgress(null);
        }
        catch (Exception ex)
        {
            pos.getOutput().setProgress(null);
            Debug.logError("****** Error: Unable to Show error dialog! " + ex.getMessage(), module);
        }
    }
    public void showError(String title, String message, Exception e)
    {
        try
        {
            refresh();
            pos.getOutput().setProgress("Error: Please wait...");
            //Toolkit tk = Toolkit.getDefaultToolkit();
            //tk.beep();
            playError();
            //CommonDialog dlg = new CommonDialog(PosScreen.getActiveScreen(),"dialog/error/detailederror");
            CommonDialog dlg = new CommonDialog(PosScreen.getActiveScreen(),"dialog/error/quickerror");
            Debug.log(e);
            Debug.logError(title+" - "+message,module);
            dlg.openDlg(title,message+"\r\nThis error occurred because: "+e.getMessage(),null,null);
            Debug.logInfo("***** Ended Error Dialog ********",module);
            pos.getOutput().setProgress(null);

        }
        catch (Exception ex)
        {
            pos.getOutput().setProgress(null);
            Debug.logError("****** Error: Unable to Show error dialog! " + ex.getMessage(), module);
        }
    }

    public void showInfo(String title, String message)
    {
        try
        {
            refresh();
            CommonDialog dlg = new CommonDialog(PosScreen.getActiveScreen(),"dialog/infopopup");
            Debug.logInfo(title+" - "+message+". ",module);
            String res = dlg.openDlg(title,message,null,null);
            Debug.logInfo("***** Ended Info Dialog ********",module);
        }
        catch (Exception ex)
        {
            Debug.logError("****** Error: Unable to Show confirm dialog! " + ex.getMessage(), module);
        }
    }


    public String getReceiptOption(){
        try
        {
            refresh();
            playQuestion();
            CommonDialog dlg = new CommonDialog(PosScreen.getActiveScreen(),"dialog/receiptoption");
            Debug.logInfo("Print Receipt? - Please c",module);
            String res = dlg.openDlg("Receipt Option?",
                    "Would the customer like to get a receipt?\r\n"+
                            "Would they like to have a hardcopy (print) or sent electronically via email?"
                    ,null,"Please choose one of the following option for Receipt:...");
            Debug.log("User chose: "+res);
            Debug.logInfo("***** Ended Confirm Dialog ********",module);
            if (Utility.isEmpty(res)) return "NA";
            return res.toUpperCase();
        }
        catch (Exception ex)
        {
            Debug.logError("****** Error: Unable to Show confirm dialog! " + ex.getMessage(), module);
        }
        return "NA";
    }


    public boolean confirm(String title, String message, String confirmQuestion)
    {
        return confirm(title,message,confirmQuestion,false);
    }
    public boolean confirm(String title, String message, String confirmQuestion, boolean defaultValue)
    {
        try
        {
            refresh();
            playQuestion();
            CommonDialog dlg = new CommonDialog(PosScreen.getActiveScreen(),"dialog/quickconfirm");
            Debug.logInfo(title+" - "+message+". "+confirmQuestion,module);
            String res = dlg.openDlg(title,message,null,confirmQuestion);
            Debug.log("User chose: "+res);
            Debug.logInfo("***** Ended Confirm Dialog ********",module);
            if (Utility.isEmpty(res)) return defaultValue;
            else if ("ok".equalsIgnoreCase(res)) return true;
            else return false;
        }
        catch (Exception ex)
        {
            Debug.logError("****** Error: Unable to Show confirm dialog! " + ex.getMessage(), module);
        }
        return false;
    }
    public boolean confirm(String title, String message,String info,String confirmQuestion)
    {
        return confirm(title,message, info,confirmQuestion,false);
    }
    public boolean confirm(String title, String message,String info,String confirmQuestion, boolean defaultValue)
    {
        try
        {
            refresh();
            playQuestion();
            CommonDialog dlg = new CommonDialog(PosScreen.getActiveScreen(),"dialog/detailedconfirm");
            Debug.logInfo(title+" - "+message+". "+confirmQuestion,module);
            String res = dlg.openDlg(title,message,info,confirmQuestion);
            Debug.log("User chose: "+res);
            Debug.logInfo("***** Ended Confirm Dialog ********",module);
            if (Utility.isEmpty(res)) return defaultValue;
            else if ("ok".equalsIgnoreCase(res)) return true;
            else return false;
        }
        catch (Exception ex)
        {
            Debug.logError("****** Error: Unable to Show confirm dialog! " + ex.getMessage(), module);
        }
        return false;
    }

    public String formatCurrency(BigDecimal amount)
    {
        if (amount==null) return "0";
        return formatCurrency(amount.doubleValue());
    }

    public String formatCurrency(double amount)
    {
        try
        {
            refresh();
            String currencyFormat="0.00";
            if (r!=null)
            {
                currencyFormat = r.getSettings().getCurrencyFormat();
            }
            DecimalFormat df = new DecimalFormat(currencyFormat);
            String aText = df.format(amount);
            return aText;
        }
        catch(Exception ex)
        {
            Debug.logError(ex,"***** Error: Unable to format currency!",module);
            return Double.toString(amount);
        }
    }

    public String getKeyboardInput(String oldValue, String message, boolean forcePopup)
    {
        refresh();
        boolean popup=false;
        if (forcePopup) popup=true;
        if (r.getSettings().isOnScreenKeyboard()) popup=true;
        if (popup)
        {
            Keyboard key = new Keyboard("dialog/keyboard",message,oldValue);
            String value = key.openDlg();
            if (value==null) return "";
            else return value;
        }
        else return oldValue;
    }
    public String getNumpadInput(String oldValue, String message, boolean forcePopup)
    {
        refresh();
        boolean popup=false;
        if (forcePopup) popup=true;
        if (r.getSettings().isOnScreenKeyboard()) popup=true;
        if (popup)
        {
            Keyboard key = new Keyboard("dialog/numerickeypad",message,oldValue);
            String value = key.openDlg();
            if (value==null) return "";
            else return value;
        }
        else return oldValue;
    }
    public String getNumpadInput2(String oldValue, String message, boolean forcePopup)
    {
        refresh();
        boolean popup=false;
        if (forcePopup) popup=true;
        if (r.getSettings().isOnScreenKeyboard()) popup=true;
        if (popup)
        {
            Keyboard key = new Keyboard("dialog/numerickeypad2",message,oldValue);
            String value = key.openDlg();
            if (value==null) return "";
            else return value;
        }
        else return oldValue;
    }

    public void toggleTrainingMode()
    {
        refresh();
        try {
            String msg = "You are about to leave the training mode";
            if (!r.getSecurity().isTrainingMode()) msg = "You are about to enter the training mode";

            if (clearedAllTx(msg)) {
                r.getSecurity().toggleTrainingMode();
                PosTransaction.getNewTransaction();
                pos.setTrainingDisplay(r.getSecurity().isTrainingMode());
                pos.refresh();
                pos.getOutput().setHint("Training mode is " + r.getSecurity().getTrainingMode());
                trans.refreshOutput(pos);
            }
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
            pos.getTerminalHelper().showError("Error: Training Mode","Unable to switch to Training Mode!");
        }
    }

    public boolean isTrainingMode()
    {
        refresh();
        if (r!=null) return r.getSecurity().isTrainingMode();
        else return false;
    }


    private void forceVoidSavedTx(PosTransaction tx)
    {
        PosShoppingCart savedCart = tx.getCart();
        BigDecimal payTotal = savedCart.getPaymentTotal();

        if (!savedCart.isEmpty()) {
            try {
                r.getNode().postSalesVoidToDataSyncQueue(savedCart,tx.getActionHistory());
            } catch (Exception ex) {
                Debug.logError(ex, module);
            }
        }
    }

    public void voidSale()
    {
        refresh();
        PosShoppingCart cart = trans.getCart();
        BigDecimal payTotal = cart.getPaymentTotal();

        if (!cart.isEmpty())
        {
            if (payTotal.compareTo(cart.ZERO)!=0)
                showError("Error: Void Sale"
                            ,"You must remove all payments first\r\n" +
                            "Currently there are " + cart.getActivePayments().size()
                            + " payments");
            else
            {
        	    boolean doVoid = confirm("Void Entire Transaction?"
                        ,"You are about to void this entire transaction!"
                        ,"Do you really want to void this transaction?");
                if (doVoid)
                {
                    try {
                        trans.voidSale(pos);
                        pos.showStartupPage();
                        pos.refresh();
                        pos.getOutput().setHint("Transaction Voided!");
                    }
                    catch (Exception ex)
                    {
                        Debug.logError(ex,module);
                        showError("Error: Void Sale","Unable to void the current transaction!",ex);
                    }
                }
            }
        }
        else pos.getOutput().setHint("Transaction is empty!");

    }

    public void toggleQtySign()
    {
        refresh();
        if (trans.isRefundMode())
        {
            trans.getCart().toggleQuantitySign();
            String sgn = (trans.getCart().isQtyNegative() ? "-" : "+");
            pos.getOutput().setHint("Qty is now " + sgn + "ve");
            trans.history(new PosAct("BTN","qtytoggle",sgn));
            trans.refreshOutput(pos);
        }
        else showError("Error: Quantity Sign Toggle"
                ,"You can only use this function in Refund Mode!");
    }

    public void forceClearAllTxSilent() throws Exception {
        refresh();
        LifoSet<PosTransactionEntry> savedTx = PosTransaction.getSavedTransactions();
        if (savedTx.size() > 0) {
            for (PosTransactionEntry txEntry : savedTx)
                forceVoidSavedTx(txEntry.getTx());
            savedTx.clear();
        }
        if (trans != null && trans.isOngoingTransaction()) {
            List<PosShoppingCartPayment> payments = trans.getCart().getActivePayments();
            if (payments.size() > 0)
                throw new Exception("Currently there are " + payments.size()
                        + " payments");
            trans.voidSale(pos);
        }
    }
    public boolean clearedAllTx(String message)
    {
        try
        {
            refresh();
            LifoSet<PosTransactionEntry> savedTx = PosTransaction.getSavedTransactions();
            if (savedTx.size()>0) {
                boolean voidActive = confirm("Void Saved Transactions?"
                        ,message + ",\r\n"+
                                " but there are " + savedTx.size()+" saved transactions!",
                        "Do you want to void these transaction(s) and proceed?");
                if (voidActive)
                {
                    for(PosTransactionEntry txEntry : savedTx)
                        forceVoidSavedTx(txEntry.getTx());
                    savedTx.clear();
                    //return true;
                }
                else return false;
            }
            if (trans!=null && trans.isOngoingTransaction())
            {
                boolean voidActive = confirm("Void Current Transaction?"
                        ,message + ",\r\n"+
                         " but there is an active ongoing transaction!",
                        "Do you want to void this transaction and proceed?");
                if (voidActive)
                {
                    try {
                        List<PosShoppingCartPayment> payments = trans.getCart().getActivePayments();
                        if (payments.size()>0)
                            throw new Exception("Currently there are " + payments.size()
                                + " payments");
                        trans.voidSale(pos);
                    }
                    catch (Exception ex)
                    {
                        showError("Error: Void Sale","Unable to void the current transaction!"
                                , ex);
                        return false;
                    }
                    return true;
                }
                else return false;
            }

            return true;
        }
        catch (Exception ex)
        {
            showError("Clear Transactions","Unable to clear all transactions!", ex);
            return false;
        }
    }





}
