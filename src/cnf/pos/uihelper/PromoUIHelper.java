package cnf.pos.uihelper;

import apu.jpos.util.StringUtil;
import cnf.node.entities.PosAct;
import cnf.pos.util.Debug;
import cnf.pos.util.GeneralException;
import cnf.pos.cart.resources.elements.CustomerElement;
import cnf.pos.cart.resources.elements.PromoElement;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/23/11
 * Time: 10:08 AM
 * To change this template use File | Settings | File Templates.
 */
public class PromoUIHelper extends BaseUIHelper {

    public static final String module = PromoUIHelper.class.getName();

    public PromoUIHelper()
    {
        super();
    }


    private void assignSelectedCustomer(CustomerElement c, String sourceTrigger, String idScanned) throws Exception {

        if (!StringUtil.empty(c.LocalUserId)){
            if (!pos.getTerminalHelper().confirm("Verify Employee Address"
                    , "Please verify the employee address with the information below:",
                    "Street Address: " + c.AddressLine1 + " ******************************\r\n"
                    + " Postal Code: *** " + c.PostalCode,
                    "Does this information match with the employee photo ID?"))
            {
                pos.getOutput().setHint("Customer scan cancelled!");
                return;
            }
        }
        trans.getCart().setCustomer(c);
        trans.history(new PosAct(sourceTrigger,"cust", c.PartyId, idScanned));
        pos.getInput().clear();
        pos.getOutput().setHint("Customer Scanned!");
        pos.getTerminalHelper().updateJournal(true);
        pos.getTerminalHelper().playBeep();
    }

    public boolean setCustomer(String sourceTrigger, String value)
    {
        refresh();
        try
        {
            pos.getOutput().print(null);
            pos.getOutput().setProgress("Assigning customer to sale...");
            CustomerElement c = pos.getCustomerHelper().getCustomer(value);
            if (c==null)
            {
                // not a party code...
                clearFunction();
                return false;
            }
            assignSelectedCustomer(c,sourceTrigger, value);
        }
        catch(GeneralException pex)
        {
            pos.getTerminalHelper().showError("Error: Set Customer"
                ,"Unable to assign a customer to this sale!\r\n", pex);
            clearFunction();
            return true;
        }
        catch(Exception ex)
        {
            // this is a party code buy party doesn't exist!
            Debug.logError(ex, module);
            pos.getTerminalHelper().showError("Error: Set Customer"
                ,"Unable to assign a customer to this sale!",ex);
            clearFunction();
            return true;
        }
        clearFunction();
        return true;
    }

    public boolean setCustomerByPhone(String sourceTrigger, String phoneNumber)
    {
        refresh();
        try
        {
            pos.getOutput().print(null);
            CustomerElement c = pos.getCustomerHelper().getCustomerByPhone(phoneNumber);
            if (c==null)
            {
                // not a party code...
                clearFunction();
                return false;
            }
            assignSelectedCustomer(c, sourceTrigger, phoneNumber);
        }
        catch(GeneralException pex)
        {
            pos.getTerminalHelper().showError("Error: Set Customer"
                    ,"Unable to assign a customer to this sale!\r\n", pex);
            clearFunction();
            return true;
        }
        catch(Exception ex)
        {
            Debug.logError(ex, module);
            pos.getTerminalHelper().showError("Error: Set Customer"
                    ,"Unable to assign a customer to this sale!",ex);
            clearFunction();
            return true;
        }
        clearFunction();
        return true;
    }




    public boolean setPromoCode(String triggerSource, String value)
    {
        try
        {
            refresh();
            pos.getOutput().print(null);
            pos.getOutput().setProgress("Applying Promo Code...");
            String promoCodeId = getPromoCode(value);
            if (promoCodeId==null)
            {
                // Not a promo...
                clearFunction();
                return false;
            }
            if (!trans.getCart().addPromoCode(promoCodeId)) throw new Exception("Invalid Promo Code!");
            trans.history(new PosAct(triggerSource,"promo",value));
            pos.getOutput().setHint("Applied Promo: " + promoCodeId);
            pos.getTerminalHelper().updateJournal(true);
            pos.getTerminalHelper().playBeep();
        }
        catch (Exception ex)
        {
            Debug.logError(ex, module);
            pos.getTerminalHelper().showError("Error: Set Promo Code"
                ,"Unable to apply a promo code to this sale!",ex);
        }

        clearFunction();
        return true;
    }
    public boolean setUnfilteredPromoCode(String triggerSource, String value)
    {
        try
        {
            refresh();
            if (!trans.getCart().addPromoCode(value)) throw new Exception("Invalid Promo Code!");
            trans.history(new PosAct(triggerSource,"promo",value));
            pos.getTerminalHelper().updateJournal(true);
            pos.getTerminalHelper().playBeep();
        }
        catch (Exception ex)
        {
            Debug.logError(ex, module);
            pos.getTerminalHelper().showError("Error: Set Promo Code"
                    ,"Unable to apply a promo code to this sale!",ex);
        }

        clearFunction();
        return true;
    }

    public void clearPromos()
    {
        try
        {
            refresh();
            pos.getOutput().print(null);
            pos.getOutput().setProgress("Resetting Promos");
            if (!trans.getCart().isRefund()) trans.getCart().setCustomer(null);
            trans.getCart().clearPromoCodes();
            trans.history(new PosAct("BTN","cpromo"));
            pos.getOutput().setHint("Cleared Order Promos!");
            pos.getTerminalHelper().updateJournal(true);
        }
        catch (Exception ex)
        {
            Debug.logError(ex, module);
            pos.getTerminalHelper().showError("Error: Clear Promo Codes"
                ,"Unable to clear promo codes from this sale!",ex);
        }
        clearFunction();
    }

    public void clearCustomer()
    {
        try
        {
            refresh();
            pos.getOutput().print(null);
            pos.getOutput().setProgress("Resetting Customer");
            trans.getCart().setCustomer(null);
            trans.history(new PosAct("BTN","ccust"));
            pos.getOutput().setHint("Cleared Customer!");
            pos.getTerminalHelper().updateJournal(true);
        }
        catch (Exception ex)
        {
            Debug.logError(ex, module);
            pos.getTerminalHelper().showError("Error: Clear Customer"
                ,"Unable to remove the customer from this sale!",ex);
        }
        clearFunction();
    }


    public String getPromoCode(String value) throws GeneralException
    {
        refresh();
        List<String> promoCodePatterns = r.getSettings().getPromoCodePatterns();
        for(String promoCodePattern:promoCodePatterns)
        {
            Pattern pcPattern = Pattern.compile(promoCodePattern);
            Matcher m = pcPattern.matcher(value);
            if (m.find())
            {
                String val = null;
                if (m.groupCount()>0) val=m.group(1);
                else val=m.group(0);
                PromoElement p = r.getPromos().getPromoCodePromo(val);
                if (p!=null) return val;
                else throw new GeneralException("Promo Code not found!");
            }
        }
        return null;
    }

}
