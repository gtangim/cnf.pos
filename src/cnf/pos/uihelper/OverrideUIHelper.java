package cnf.pos.uihelper;

import cnf.node.entities.PosAct;
import cnf.pos.cart.resources.elements.ReasonElement;
import cnf.pos.screen.ReasonSelector;
import cnf.pos.util.Debug;
import cnf.pos.cart.PosShoppingCartItem;
import cnf.pos.component.*;
import cnf.pos.util.Utility;

import java.math.BigDecimal;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/23/11
 * Time: 10:08 AM
 * To change this template use File | Settings | File Templates.
 */
public class OverrideUIHelper extends BaseUIHelper {

    public static final String module = OverrideUIHelper.class.getName();

    public OverrideUIHelper()
    {
        super();
    }

    public void modifyPriceSelected(boolean overrideSubtotal)
    {
        BigDecimal value=null;
        try
        {
            refresh();
            value = pos.getPaymentHelper().getAmount(pos.getInput().value());
            if (value==null)
                throw new NumberFormatException("Invalid Amount Error!");
            //if (value.compareTo(BigDecimal.ZERO)==-1) value=value.negate();
            pos.getOutput().setProgress("Modifying Item Price...");
            PosShoppingCartItem item = pos.getTerminalHelper().getSelectedJournalItem();
            if (item==null) throw new Exception("You must select an item first!");
            int ind = trans.getCart().getItemIndex(item);
            ReasonSelector dlg = new ReasonSelector("Price Override", pos,r.getSettings().getOverrideReasons());
            ReasonElement res = dlg.openDlg();
            if (res==null)
            {
                clearFunction();
                return;
            }
            pos.getInput().clear();
            pos.getOutput().print(null);
            //if (item.getQuantity().compareTo(BigDecimal.ZERO)==-1) value = value.negate();
            if (overrideSubtotal)
            {
                item.overrideSubtotal(value,res.ReasonId, res.ReportingCode);
                trans.history(new PosAct("KEY","stotovr",Integer.toString(ind)
                        ,value.toString(),res.ReasonId));
            }
            else
            {
                item.overridePrice(value,res.ReasonId, res.ReportingCode);
                trans.history(new PosAct("KEY","priceovr",Integer.toString(ind)
                        ,value.toString(),res.ReasonId));
            }
            pos.getOutput().setHint("Item price changed.");
            pos.getTerminalHelper().updateJournal(false);
        }
        catch (NumberFormatException nex)
        {
            Debug.logError(nex, module);
            pos.getTerminalHelper().showError("Error: Price Override"
                    , "Unable to set a price override!\r\nInvalid price: "+pos.getInput().value(),nex);
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
            pos.getTerminalHelper().showError("Error: Price Override"
                    , "Unable to set a price override!\r\nHint: You must select an item first!",ex);
        }

        clearFunction();
    }


    public void clearSelectedOverride()
    {
        try
        {
            refresh();
            pos.getOutput().setProgress("Clearing Item Override...");
            ExtendedJournal j = pos.getJournal();
            PosShoppingCartItem item = pos.getTerminalHelper().getSelectedJournalItem();
            if (item==null) throw new Exception("You must select an item first!");
            int ind = trans.getCart().getItemIndex(item);
            item.clearOverride();
            trans.history(new PosAct("KEY","clearovr",Integer.toString(ind)));
            pos.getOutput().setHint("Item override cleared.");
            pos.getTerminalHelper().updateJournal(false);
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
            pos.getTerminalHelper().showError("Error: Clear Price Override"
                    , "Unable to clear the price override!\r\nHint: You must select an item first!",ex);
        }

        clearFunction();
    }




}
