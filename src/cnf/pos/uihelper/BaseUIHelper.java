package cnf.pos.uihelper;

import cnf.pos.PosTransaction;
import cnf.pos.cart.resources.ResourceManager;
import cnf.pos.screen.PosScreen;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/23/11
 * Time: 10:03 AM
 * To change this template use File | Settings | File Templates.
 */
public class BaseUIHelper {
    protected ResourceManager r;
    //protected XuiSession session;
    protected PosScreen pos;
    protected PosTransaction trans;

    public void BaseUIHelper()
    {
        refresh();
    }

    public void refresh()
    {
        pos = PosScreen.getActiveScreen();
        if (pos!=null) trans = PosTransaction.getCurrentTx();
        r = ResourceManager.Resource;
    }

    protected void clearFunction()
    {
        try
        {
            pos.getTerminalHelper().clearInput();
            pos.getTerminalHelper().clearOutput();
        }
        catch (Exception ex)
        {}
    }

}
