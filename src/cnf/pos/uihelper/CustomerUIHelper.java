package cnf.pos.uihelper;

import cnf.node.entities.PosAct;
import cnf.pos.util.Debug;
import cnf.pos.util.GeneralException;
import cnf.pos.cart.PosShoppingCart;
import cnf.pos.cart.resources.elements.BillingAccountElement;
import cnf.pos.cart.resources.elements.CustomerElement;
import cnf.pos.screen.SelectAccount;
import cnf.pos.util.Utility;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/23/11
 * Time: 10:08 AM
 * To change this template use File | Settings | File Templates.
 */
public class CustomerUIHelper extends BaseUIHelper {

    public static final String module = CustomerUIHelper.class.getName();

    public CustomerUIHelper()
    {
        super();
    }

    public CustomerElement getCustomer(String customerCode) throws GeneralException
    {
        refresh();
        List<String> customerPatterns = r.getSettings().getCustomerPatterns();
        for(String customerPattern:customerPatterns)
        {
            Pattern csPattern = Pattern.compile(customerPattern);
            Matcher m = csPattern.matcher(customerCode);
            if (m.find())
            {
                String val = null;
                if (m.groupCount()>0) val=m.group(1);
                else val=m.group(0);
                CustomerElement c = r.getCustomers().getCustomer(val);
                if (c!=null) return c;
                else throw new GeneralException("Customer not found!");
            }
        }
        return null;
    }

    public CustomerElement getCustomerByPhone(String phoneNumber) throws GeneralException
    {
        refresh();
        CustomerElement c = r.getCustomers().getCustomerByPhone(phoneNumber);
        if (c!=null) return c;
        else throw new GeneralException("Customer not found!");
    }

    public void selectExternalCustomerId()
    {
        refresh();
        try
        {
            pos.getOutput().setProgress("Assigning Driver License#");
            String eid = pos.getTerminalHelper().getKeyboardInput(trans.getCart().getCustomerExternalId()
                ,"Please Enter the customer's driver license number:",true);
            if (Utility.isNotEmpty(eid))
            {
                trans.getCart().setCustomerExternalId(eid);
                trans.history(new PosAct("KEY","custdl",eid));
                pos.getOutput().setHint("DL#"+eid);
            }
            else
            {
                trans.getCart().setCustomerExternalId(null);
                trans.history(new PosAct("KEY","clearcustdl"));
                pos.getOutput().setHint("Cleared DL#");
            }
        }
        catch (Exception ex)
        {
            Debug.logError(ex, module);
            pos.getTerminalHelper().showError("Error: Input Driver License#"
                ,"Unable to set customer Driver License#!",ex);
        }
        pos.getTerminalHelper().clearInput();
        pos.getTerminalHelper().clearOutput();
    }

    public BillingAccountElement getCustomerBillingAccount()
    {
        refresh();
        try
        {
            if (trans==null) throw new Exception("No active transaction found!");
            PosShoppingCart cart = trans.getCart();
            if (cart==null) throw new Exception("Unable to locate shopping cart object!");
            CustomerElement cust = cart.getCustomer();
            if (cust==null) throw new Exception("You must scan a customer who \r\nhas a valid billing account!");
            List<BillingAccountElement> accounts = cust.BillingAccounts;
            if(accounts.size()<1) throw new Exception("Customer " + cust.Description
                    + " has no billing account!");
            if (accounts.size()==1) return (BillingAccountElement)accounts.get(0);
            SelectAccount accSelect= new SelectAccount(pos,accounts);
            BillingAccountElement ba = accSelect.openDlg();
            return ba;
        }
        catch (Exception ex)
        {
            Debug.logError(ex, module);
            pos.getTerminalHelper().showError("Error: Billing Account"
                ,"Unable to select customer billing account!",ex);
        }
        pos.getTerminalHelper().clearInput();
        pos.getTerminalHelper().clearOutput();
        return null;
    }
}
