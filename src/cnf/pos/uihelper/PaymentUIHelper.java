package cnf.pos.uihelper;

import cnf.node.entities.PosAct;
import jpos.CATConst;
import cnf.pos.util.Debug;
import cnf.pos.cart.PosShoppingCart;
import cnf.pos.cart.PosShoppingCartPayment;
import cnf.pos.cart.resources.elements.BillingAccountElement;
import cnf.pos.component.*;
import cnf.pos.device.DeviceLoader;
import cnf.pos.device.impl.PaymentTerminal;
import cnf.pos.screen.PosScreen;
import cnf.pos.util.Utility;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/23/11
 * Time: 10:08 AM
 * To change this template use File | Settings | File Templates.
 */
public class PaymentUIHelper extends BaseUIHelper {

    public static final String module = PaymentUIHelper.class.getName();

    public PaymentUIHelper()
    {
        super();
    }

    public void payVendorCoupon(String sourceTrigger, String couponId)
    {
        try
        {
            refresh();
            if (!trans.getCart().isPayable()) throw new Exception("You cannot pay until items are added to the cart!");
            pos.getOutput().setProgress("Adding Payment...");
            //VendorCoupon vc = r.getPrices().getCoupon(couponId);
            //VendorCoupon vc = null;
            BigDecimal amount = null;
            //if (vc==null)
            //{
            String amtStr = pos.getTerminalHelper().getNumpadInput(null,"Enter vendor coupon value:",true);
            amount = processAmount(amtStr,null,"VENDOR_COUPON",true,true);
            /*}
            else amount = vc.getAmount();*/
            if (amount!=null && amount.compareTo(BigDecimal.ZERO)!=0)
            {
                PosShoppingCartPayment pay = new PosShoppingCartPayment(
                    r,"VENDOR_COUPON",amount
                        ,null,couponId,null,null,null,null
                        ,"UNKNOWN",false);
                trans.getCart().addPayment(pay);
                trans.history(new PosAct(sourceTrigger,"pay",pay.serialized()));
                pos.getTerminalHelper().updateJournal(true);
            }
        }
        catch (Exception ex)
        {
            Debug.logError(ex, module);
            pos.getTerminalHelper().showError("Error: Vendor Coupon"
                    , "Unable to pay with a vendor coupon!",ex);
        }
        pos.getOutput().print(null);
        pos.getInput().clear();
        pos.getOutput().print(null);
        clearFunction();
    }

    public void payCash(String currency)
    {
        try
        {
            refresh();
            if (!trans.getCart().isPayable()) throw new Exception("You cannot pay until items are added to the cart!");
            if (trans.getCart().isRefundPaymentNeeded() && currency!=null
                    && !currency.equals(r.getSettings().getCurrency()))
                throw new Exception("You can only refund a customer in the native currency ("
                        + r.getSettings().getCurrency()+")!");
            pos.getOutput().setProgress("Adding Payment...");

            BigDecimal amount = processAmount(currency,"CASH",true,true);
            if (amount!=null)
            {
                if (currency==null && amount.compareTo(BigDecimal.ZERO)==0)
                    throw new Exception("Invalid payment (Hint: tap CASH to finish)!");
                PosShoppingCartPayment pay = new PosShoppingCartPayment(
                    r,"CASH",amount
                        ,currency,null,null,null,null,null,null,false
                );
                if (currency==null) currency=r.getSettings().getCurrency();
                //trans.getCart().removePayments("CASH",currency);
                trans.getCart().addPayment(pay);
                trans.history(new PosAct("KEY","pay",pay.serialized()));
                pos.getTerminalHelper().updateJournal(true);
            }
        }
        catch (Exception ex)
        {
            Debug.logError(ex, module);
            pos.getTerminalHelper().showError("Error: Currency Payment"
                    , "Unable to make this cash payment!",ex);
        }
        pos.getOutput().print(null);
        clearFunction();
    }

    public boolean payBillingAccount(boolean isDonation)
    {
        refresh();
        try
        {
            if (!trans.getCart().isPayable()) throw new Exception("You cannot pay until items are added to the cart!");
            pos.getOutput().setProgress("Paying to Billing Account...");
            BillingAccountElement ba = pos.getCustomerHelper().getCustomerBillingAccount();
            if (ba!=null)
            {
                if (trans.getCart().getPayments().size()!=0)
                    throw new Exception("You cannot split payment while \r\nbilling to a customer!");
                // Billing account must pay in full amount!
                BigDecimal amount = trans.getCart().getDue();
                if (amount!=null && amount.compareTo(BigDecimal.ZERO)!=0)
                {
                    boolean doBill = pos.getTerminalHelper().confirm("Pay to Billing Account?",
                            "You are about to bill this transaction to the customer:\r\n"
                                    +"Amount: " + pos.getTerminalHelper().formatCurrency(amount)+"\r\n"
                                    +"Customer: " + trans.getCart().getCustomer().Description+"\r\n"
                                    +"Account: [" + ba.BillingAccountId+"] " + ba.Description +"\r\n"
                            ,"Are you sure you want to bill to this account?"
                    );
                    if (doBill)
                    {
                        PosShoppingCartPayment baPayment = new PosShoppingCartPayment(r,
                            "EXT_BILLACT",amount,null,ba.BillingAccountId,null,null,null,null,null,false);
                        trans.getCart().addPayment(baPayment);
                        trans.history(new PosAct("KEY","pay",baPayment.serialized()));
                        trans.getCart().setDonation(isDonation);
                        pos.getTerminalHelper().updateJournal(true);
                        return true;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Debug.logError(ex, module);
            pos.getTerminalHelper().showError("Error: Billing Account Payment"
                    , "Unable to pay to the billing account!",ex);
        }
        pos.getTerminalHelper().clearOutput();
        clearFunction();
        return false;
    }


    public void payAdvanced(String paymentMethodType, String currency, String[] values)
    {
        try
        {
            refresh();
            if (!trans.getCart().isPayable()) throw new Exception("You cannot pay until items are added to the cart!");
            pos.getOutput().setProgress("Adding Payment...");
            //BigDecimal amount = new BigDecimal(values[0]).movePointLeft(2).setScale(
            //        r.getSettings().getScale(), r.getSettings().getRoundingMode()
            //);
            if (!(values.length>=2 && values.length<=4))
                throw new Exception("Invalid number of arguments!");
            String refNum = values[1];
            String expiry = null;
            String invoiceId = null;
            String cardCompany = null;
            String cardSuffix = null;
            if ("DEBIT_CARD".equals(paymentMethodType) || "CREDIT_CARD".equals(paymentMethodType))
            {
                cardCompany = refNum.substring(0,6);
                cardSuffix = refNum.substring(refNum.length()-4);
                refNum = cardCompany+"******"+cardSuffix;
            }

            if (values.length>3)
            {
                expiry=values[2];
                invoiceId=values[3];
                if (expiry.length()>4) expiry = expiry.substring(0,4);
                if (invoiceId.length()>10) invoiceId = "*"+invoiceId.substring(invoiceId.length()-10);
            }
            else if (values.length==3) invoiceId=values[2];
            BigDecimal amount = processAmount(values[0],null,paymentMethodType,true,true);
            if (amount!=null && amount.compareTo(BigDecimal.ZERO)!=0)
            {
                //int status = PosShoppingCartPayment.PAID;
                //if (amount.compareTo(trans.getCart().ZERO)<0) status=PosShoppingCartPayment.REFUNDED;
                String response = "MANUAL_ENTRY";
                if (invoiceId!=null) response+=",I"+invoiceId;
                if (cardCompany!=null) response+=",C"+cardCompany;
                if (expiry!=null) expiry+=",ASI"+expiry;
                if (currency==null) currency=r.getSettings().getCurrency();
                PosShoppingCartPayment pay = new PosShoppingCartPayment(
                    r,paymentMethodType,amount
                        ,currency,refNum,"99999",expiry,null,invoiceId,response,false
                );
                trans.getCart().addPayment(pay);
                trans.history(new PosAct("KEY","pay",pay.serialized()));
                pos.getTerminalHelper().updateJournal(true);
            }
        }
        catch (NumberFormatException e) {
            Debug.logError(e, module);
            pos.getTerminalHelper().showError("Error: Payment (Advanced)"
                                ,"Invalid Amount number format!\r\nUser Entered: "+values[0]);
        }
        catch (Exception ex)
        {
            Debug.logError(ex, module);
            pos.getTerminalHelper().showError("Error: Payment (Advanced)"
                                ,"Unable to proces this payment!",ex);
        }
        pos.getOutput().print(null);
        pos.getInput().clear();
        clearFunction();
    }


    public void payWithCATerminal(PaymentTerminal.CardType cardType)
    {
        try
        {
            refresh();
            if (!trans.getCart().isPayable()) throw new Exception("You cannot pay until items are added to the cart!");
            pos.getOutput().setProgress("Adding Payment...");
            PaymentTerminal pinpad = DeviceLoader.paymentTerminal;
            if (pinpad==null) throw new Exception("CAT JPos driver not found!");

            BigDecimal amount = processAmount(null, "Card", false, true);
            if (amount==null) return;
            if (amount.compareTo(BigDecimal.ZERO)==0) return;
            BigDecimal due = trans.getCart().getDue();
            if (!trans.getCart().isRefund() && !r.getSettings().isAllowPosCashback())
            {
                // Cashback is disabled in the POS (pinpad)
                // We can only charge upto the subtotal
                if (amount.compareTo(due)>0)
                {
                    boolean doCharge = pos.getTerminalHelper().confirm("No Cashback Allowed!",
                            "You can only perform cashback using the PIN-PAD on this POS.\r\n"
                            +"System will not allow you to charge "
                                    + pos.getTerminalHelper().formatCurrency(amount)+".\r\n"
                            +"You can only charge upto " + pos.getTerminalHelper().formatCurrency(due)+".\r\n"
                            ,"Do you want to charge "+pos.getTerminalHelper().formatCurrency(due)+"?");
                    if (doCharge) amount=due;
                    else return;
                }
            }

            Debug.log("Processing ["+cardType.toString()+"] Amount : " + amount, module);
            //int txid = Integer.parseInt(ExtractNumbers(trans.getTransactionId()));
            //txid = txid*100 + nextID();
            if (amount.compareTo(trans.getCart().ZERO)>0)
            {
                if (amount.compareTo(due)==0)
                {
                    boolean doCashBack = pos.getTerminalHelper().confirm("Want Cashback?",
                            "Cashback will only be allowed if you insert a Debit Card.\r\n"
                                    +"If you click yes make sure you obtain an Interac/Debit card from the customer? "
                            ,"Does the customer wish to receive a cashback? ");
                    if (doCashBack) pinpad.setAllowCashBack(true);
                }

                if (pinpad.sale(cardType, nextID(), amount, trans.getCart().ZERO))
                {
                    processCATResponse(true);
                    pos.getOutput().setHint("Transaction approved!");
                }
                else
                {
                    processCATResponse(false);
                    pos.getOutput().setHint("Transaction failed!");
                }
            }
            else if (amount.compareTo(trans.getCart().ZERO)<0)
            {
                if (pinpad.refundSale(cardType, nextID(), amount.negate(), trans.getCart().ZERO))
                {
                    processCATResponse(true);
                    pos.getOutput().setHint("Transaction approved!");
                }
                else {
                    processCATResponse(false);
                    pos.getOutput().setHint("Transaction failed!");
                }
            }
            else PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Payment Terminal"
                    ,"Cannot apply a 0 (Zero) payment/refund to the card!");

            pos.getTerminalHelper().updateJournal(true);

        }
        catch (Exception ex)
        {
            Debug.logError(ex, module);
            pos.getTerminalHelper().showError("Error: Payment Terminal"
                ,"Unable to process this payment using the payment terminal!",ex);
        }
        pos.getOutput().print(null);
        pos.getInput().clear();
        clearFunction();
    }


    public void voidPayment()
    {
        try
        {
            refresh();
            pos.getOutput().setProgress("Void Payment...");
            if (pos.getJournal().getSelectedIndexType()== ExtendedJournal.JournalEntryType.PAYMENT)
            {
                int index = pos.getJournal().getSelectedIndexInt();
                PosShoppingCartPayment payment = trans.getCart().getPayment(index);
                if (payment==null) throw new Exception("Selected payment not found in Cart!");
                if ("CASH".equals(payment.getPayType()) || "DONATION".equals(payment.getPayType())
                        || "VENDOR_COUPON".equals(payment.getPayType()))
                    trans.getCart().clearPayment(index);
                else if (payment.isCATPayment())
                {
                    if (payment.getTransactionStatus()!=PosShoppingCartPayment.STAT_APPROVED)
                        throw new Exception("You can only void an approved payment!");
                    PaymentTerminal pinpad = DeviceLoader.paymentTerminal;
                    if (pinpad!=null)
                    {
                        BigDecimal amt = payment.getAmount();
                        if (amt.compareTo(BigDecimal.ZERO)==-1) amt = amt.negate();
                        boolean voidResult =pinpad.voidSale(PaymentTerminal.CardType.DEBIT,
                                payment.getCatOperationIndex(),amt,BigDecimal.ZERO);
                        processCATResponse(voidResult);
                        if (voidResult) pos.getOutput().setHint("Payment Voided!");
                        else pos.getOutput().setHint("Payment Void Failed!");
                    }
                    else throw new Exception("No Card Acceptor Terminal device is present!");
                }
                else
                {
                    payment.setTransactionStatus(PosShoppingCartPayment.STAT_VOIDED);
                    pos.getOutput().setHint("Payment Voided!");
                }
                pos.getTerminalHelper().updateJournal(true);
            }
            else throw new Exception("Please Select a payment!");
        }
        catch (Exception ex)
        {
            pos.getTerminalHelper().showError("Error: Payment Terminal"
                ,"Unable to void this payment!", ex);
        }
        pos.getOutput().print(null);
        pos.getInput().clear();
        clearFunction();
    }

    public BigDecimal processAmount(String currency, String method,boolean confirm, boolean showFormatError)
    {
        BigDecimal amount=null;
        try
        {
            refresh();
            Input input = pos.getInput();
            String amtStr = input.value();
            amount=processAmount(amtStr, currency, method, confirm, showFormatError);
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
            //pos.getTerminalHelper().showError("Error: Process Payment"
            //    ,"Unable to process this payment amount!",ex);
            amount=null;
        }
        return amount;
    }

    public BigDecimal getAmount(String amtStr)
    {
        return getAmount(amtStr,false);
    }

    public BigDecimal getAmount(String amtStr, boolean allowNegative)
    {
        BigDecimal amount=null;
        try
        {
            refresh();
            if (Utility.isNotEmpty(amtStr)) {
                if (!allowNegative) amtStr=amtStr.trim().replace("-","");
                int dotIndex=amtStr.indexOf('.');
                if (amtStr.equals(".")) amtStr="0";
                else if (dotIndex==0) amtStr="0"+amtStr;
                else if (dotIndex==amtStr.length()-1) amtStr=amtStr+"0";
                try {
                    amount = new BigDecimal(amtStr);
                } catch (NumberFormatException e) {
                    Debug.logError("Invalid number for amount : " + amtStr, module);
                    return null;
                }
                if (dotIndex<0)
                {
                    // Move points when amount entered without a '.' (in pennies)
                    amount = amount.movePointLeft(2); // convert to decimal
                    Debug.log("Set amount / 100 : " + amount, module);
                }
                BigDecimal cmp = amount;
                if (cmp.compareTo(BigDecimal.ZERO)==-1) cmp = cmp.negate();
                if (cmp.compareTo(pos.getSettings().getPaymentLimit())==1)
                    throw new Exception("Payment limit exceeded!");
            }
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
            amount=null;
        }
        return amount;
    }

    public BigDecimal processAmount(String amtStr, String currency, String method,
                                    boolean confirm, boolean showFormatError)
    {
        BigDecimal amount=null;
        BigDecimal nativeAmount=null;
        try
        {
            refresh();
            pos.getOutput().setProgress("Adding Payment...");
            Input input = pos.getInput();
            if (Utility.isEmpty(amtStr) && trans.isRoaMode())
            {
                amtStr = pos.getTerminalHelper().getNumpadInput(null,"Enter amount:",true);
                if (Utility.isEmpty(amtStr))
                {
                    clearFunction();
                    return null;
                }
            }
            if (Utility.isNotEmpty(amtStr))
            {
                amount = getAmount(amtStr);
                if (amount==null)
                {
                    if (showFormatError)
                    {
                        pos.getOutput().setHint("Invalid Amount!");
                        input.clearInput();
                        pos.getTerminalHelper().showError("Error: Payment"
                            ,"You have entered an invalid amount: " + amtStr);
                    }
                    pos.getOutput().setProgress(null);
                    return null;
                }
                if (trans.getCart().isRefundPaymentNeeded()) amount = amount.negate();
                if (currency!=null)
                {
                    nativeAmount = r.getConversions().convert(amount,currency,r.getSettings().getCurrency());
                    if (nativeAmount==null)
                    {
                        if (showFormatError)
                            pos.getTerminalHelper().showError("Error: Currency Payment"
                        ,"Unable to pay this currency, cannot convert " + currency +
                        " to " + r.getSettings().getCurrency());
                        return null;
                    }
                }
                else nativeAmount=amount;
            } else if (currency!=null)
            {
                nativeAmount = trans.getCart().getFinishAmount(currency, method);
                double fcAmountD = r.getConversions().convert(nativeAmount.doubleValue()
                        ,r.getSettings().getCurrency()
                        ,currency);
                if (Double.isNaN(fcAmountD))
                {
                    if (showFormatError)
                        pos.getTerminalHelper().showError("Error: Currency Payment"
                    ,"Unable to pay this currency, cannot convert " + currency +
                    " to " + r.getSettings().getCurrency());
                    return null;
                }
                else
                {
                    if (fcAmountD>=0)
                        amount = new BigDecimal(fcAmountD).setScale(r.getSettings().getScale(), RoundingMode.UP);
                    else amount = new BigDecimal(fcAmountD).setScale(r.getSettings().getScale(), RoundingMode.DOWN);
                    nativeAmount = r.getConversions().convert(amount,currency,r.getSettings().getCurrency());
                    nativeAmount = nativeAmount.setScale(r.getSettings().getScale(), r.getSettings().getRoundingMode());
                }
            }
            else {
                Debug.log("Amount is empty; assumption is full amount : " +
                        trans.getCart().getFinishAmount(currency,method).toString(), module);
                nativeAmount = amount = trans.getCart().getFinishAmount(currency,method);
            }
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
            if (showFormatError)
                pos.getTerminalHelper().showError("Error: Payment"
                    ,"Unable to process this payment amount!",ex);
            pos.getOutput().setProgress(null);
            return null;
        }
        clearFunction();
        if (r.getSecurity().isTrainingMode() && trans.getCart().canFinishWithAmount(nativeAmount,currency, method))
        {
            boolean proceedTraining = pos.getTerminalHelper().confirm("Proceed in Traning Mode?",
                    "You are currently in training mode!"
                            +"\r\nIf you continue, no sales will be recorded.",
                    "Do you want to finish this transaction in training mode?");
            if (!proceedTraining) return null;
        }
        if (confirm && trans.getCart().canFinishWithAmount(nativeAmount,currency, method))
        {
            String payType=(trans.getCart().isRefundPaymentNeeded()?"Refund":"Pay");
            String title = "#payType #payMethod";
            String currencyText = "#currencyAmount (#currencyType)";
            if (Utility.isEmpty(currency)) currencyText="#amount";
            String msg = "You are about to #payType #currencyText!\r\n#conversion";

            title = title.replace("#payType",payType).replace("#payMethod",method);

            msg = msg.replace("#currencyText",currencyText);
            msg = msg.replace("#payType",payType.toLowerCase()).replace("#payMethod",method.toLowerCase());
            BigDecimal nAmt = nativeAmount;
            if (nAmt.compareTo(BigDecimal.ZERO)==-1) nAmt = nAmt.negate();
            msg= msg.replace("#amount",pos.getTerminalHelper().formatCurrency(nAmt));
            if (currency!=null)
            {
                BigDecimal amt = amount;
                if (amt.compareTo(BigDecimal.ZERO)==-1) amt = amt.negate();
                msg = msg.replace("#currencyAmount"
                        ,Utility.formatPrice(amt)).replace("#currencyType",currency);
                BigDecimal cRate = r.getConversions().getConversionFactor(currency, r.getSettings().getCurrency());
                msg = msg.replace("#conversion","1 "+currency+" = " + cRate.stripTrailingZeros().toPlainString() + " "
                        + r.getConversions().getUom(r.getSettings().getCurrency()).Abbrev);
            }
            else msg = msg.replace("#conversion","");
            boolean pay = pos.getTerminalHelper().confirm(title,msg
                    , "Do you want to " + payType.toLowerCase() + " this amount?");
            if (!pay) return null;
        }

        String curr = currency;
        if (curr==null) curr = r.getSettings().getCurrency();
        BigDecimal limit = r.getSettings().getCurrencyLimit(curr);
        if (!confirm) limit=null;
        if (limit!=null && limit.compareTo(amount)<=0)
        {
            String payType=(trans.getCart().isRefundPaymentNeeded()?"Refund":"Pay");
            String title = "#payType #payMethod: Exceeds Currency Limit";
            String currencyText = "#currencyAmount (#currencyType)";
            if (Utility.isEmpty(currency)) currencyText="#amount";

            title = title.replace("#payType",payType).replace("#payMethod",method);
            String msg = r.getSettings().getCurrencyLimitWarning();

            boolean pay = pos.getTerminalHelper().confirm(title,msg
                    , "Do you want to proceed with this payment?");
            if (!pay) return null;
        }

        return amount;
    }

    public void autoFinishOrder()
    {
        refresh();
        PosShoppingCart cart = trans.getCart();

        if (cart.canFinalize())
        {
            if (r.getSettings().promptForReceipt() && !trans.isPrintOptionSelected())
            {
                if (trans.getCart().getCustomer()!=null && trans.getCart().getCustomer().Email!=null){
                    String opt = pos.getTerminalHelper().getReceiptOption();
                    if ("PRN".equals(opt)) trans.setReceiptPrint(true);
                    else trans.setReceiptPrint(false);
                    trans.getCart().setReceiptOption(opt);
                }
                else
                {
                    boolean printReceipt = pos.getTerminalHelper().confirm("Print Receipt?"
                            , "If you click \"YES\" then a paper receipt will be printed for the customer.\n"
                                    +"If you click \"NO\" paper will be saved!"
                            , "Do you want to print the receipt?",true);
                    trans.setReceiptPrint(printReceipt);
                    if (printReceipt) trans.getCart().setReceiptOption("PRN");
                    else trans.getCart().setReceiptOption("NA");
                }


            }

            if (cart.isRefund() && !cart.isOrderRefundValid())
            {
                pos.getTerminalHelper().showError("Error:Finish Sale","Cannot finish this refund!\r\n"
                        +"You must scan a valid receipt or a customer Id card or enter a driver license#");
                return;
            }

            pos.getOutput().setProgress("Finishing Sale...");
            pos.setWaitCursor();
            pos.setMenuLock(true);


            BigDecimal change = trans.getFinalChange();
            if (change!=null)
            {
                //clearFunction();
                pos.getInput().setFunction("PAID");
                Output output = pos.getOutput();
                String chg = "CHANGE: " +Utility.formatPrice(change);
                output.setProgress(chg);
                output.setHint(chg);
                output.print(chg);
                output.setAmount(change,true);
            }
            else
            {
                pos.setNormalCursor();
                pos.setMenuLock(false);
                clearFunction();
            }
            if (!trans.finalizeSale())
            {
                Output output = pos.getOutput();
                output.setProgress(null);
                output.setHint("Error: Order processing error!");
                output.print(null);
                output.setAmount(cart.getGrandTotal(),false);
            }

        }
    }





    // ************************* Internal Methods **************************


    protected void processCATResponse(boolean cardApproved) throws Exception
    {
        PosShoppingCartPayment oldPayment = null;
        refresh();
        PaymentTerminal pinpad = DeviceLoader.paymentTerminal;
        if (pinpad.getTransactionType()!=CATConst.CAT_TRANSACTION_VOID && nid!=pinpad.getSequenceNumber())
            throw new Exception("POS and Pinpad is out of sequence!"
                    +"\r\nPinpad returned response "
                    +pinpad.getSequenceNumber()+" while the POS is expecting "
                    + nid);
        int txType = pinpad.getTransactionType();
        if (txType==-1) return;
        boolean isRefund=false;
        if (txType==CATConst.CAT_TRANSACTION_SALES) isRefund=false;
        else if (txType==CATConst.CAT_TRANSACTION_REFUND) isRefund=true;
        else if (txType==CATConst.CAT_TRANSACTION_VOID)
        {
            int index = pos.getJournal().getSelectedIndexInt();
            oldPayment = trans.getCart().getPayment(index);
            if (oldPayment!=null)
                if (oldPayment.getTransactionType()==PosShoppingCartPayment.REFUND) isRefund=true;
        }
        else return;

        String cardType = pinpad.getCardCompanyId();
        BigDecimal amount = new BigDecimal(pinpad.getAmount());
        amount = amount.movePointLeft(2);
        if (isRefund) amount=amount.negate();

        int status= PosShoppingCartPayment.STAT_APPROVED;
        String lastError = pinpad.getLastError();
        if (lastError==null) lastError="";
        lastError=lastError.toLowerCase();
        if (!cardApproved){
            if (pinpad.isDeclined()) status=PosShoppingCartPayment.STAT_DECLINED;
            else if (lastError!=null && lastError.contains("declined")) status=PosShoppingCartPayment.STAT_DECLINED;
            else if (lastError!=null && lastError.contains("cancelled")) status=PosShoppingCartPayment.STAT_CANCELLED;
            else return;
        }
        //else if (isRefund) status=PosShoppingCartPayment.REFUNDED;



        String cardId = "???????*****0000";
        if (!Utility.isEmpty(pinpad.getAccountNumber())) cardId = pinpad.getAccountNumber();
        PosShoppingCartPayment payment = new PosShoppingCartPayment(r,cardType,amount,null,
                cardId,pinpad.getAuthCode(),pinpad.getExpiry(),pinpad.getVerification()
                ,pinpad.getTransactionId()+"-"+pinpad.getInvoiceId()
                ,pinpad.getTransactionResponse(),true);

        payment.setCatOperationIndex(pinpad.getSequenceNumber());
        if (txType==CATConst.CAT_TRANSACTION_VOID) payment.setTransactionStatus(PosShoppingCartPayment.STAT_VOIDED);
        payment.setTransactionStatus(status);

        payment.setResponseCode(pinpad.getResponseCode());
        if (cardApproved && oldPayment!=null)
        {
            oldPayment.setReversalDescription(pinpad.getTransactionResponse());
            oldPayment.setTransactionStatus(PosShoppingCartPayment.STAT_VOIDED);
        }

        if (txType!=CATConst.CAT_TRANSACTION_VOID) trans.getCart().addPayment(payment);
        trans.history(new PosAct("CAT","pay",payment.serialized()));

        if (status!=PosShoppingCartPayment.STAT_CANCELLED
                && r.getSettings().promptForReceipt() && !trans.isPrintOptionSelected())
        {

            if (trans.getCart().getCustomer()!=null && trans.getCart().getCustomer().Email!=null){
                String opt = pos.getTerminalHelper().getReceiptOption();
                if ("PRN".equals(opt)) trans.setReceiptPrint(true);
                else trans.setReceiptPrint(false);
                trans.getCart().setReceiptOption(opt);
            }
            else
            {
                boolean printReceipt = pos.getTerminalHelper().confirm("Print Receipt?"
                        , "If you click \"YES\" then a paper receipt will be printed for the customer.\n"
                                +"If you click \"NO\" paper will be saved!"
                        , "Do you want to print the receipt?",true);
                trans.setReceiptPrint(printReceipt);
                if (printReceipt) trans.getCart().setReceiptOption("PRN");
                else trans.getCart().setReceiptOption("NA");
            }
        }

        //if (r.getSettings().isPrintPaymentSlips())
        trans.printPaymentSlips(payment);
        //autoFinishOrder();
    }




    private static String ExtractNumbers(String id)
    {
    	StringBuilder sb = new StringBuilder();
    	for(int i=0;i<id.length();i++)
    		if (Character.isDigit(id.charAt(i))) sb.append(id.charAt(i));
    	if (sb.length()==0) sb.append("0");
    	return sb.toString();
    }
    protected static int nid = 0;
    public static int nextID()
    {
        nid++;
        return nid;
    }
    public static void resetSequence(){nid=0;}


}
