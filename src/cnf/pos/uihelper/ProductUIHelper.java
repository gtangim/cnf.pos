package cnf.pos.uihelper;

import cnf.node.entities.PosAct;
import cnf.pos.cart.PosShoppingCart;
import cnf.pos.cart.resources.ResourceManager;
import cnf.pos.util.*;
import cnf.pos.cart.PosShoppingCartItem;
import cnf.pos.cart.resources.datastructures.AtomicProductInput;
import cnf.pos.cart.resources.elements.PriceElement;
import cnf.pos.cart.resources.elements.ProductElement;
import cnf.pos.cart.resources.elements.ProductIdentificationElement;
import cnf.pos.component.Input;
import cnf.pos.device.DeviceLoader;
import cnf.pos.screen.SelectProduct;

import java.awt.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 2/23/11
 * Time: 10:08 AM
 * To change this template use File | Settings | File Templates.
 */
public class ProductUIHelper extends BaseUIHelper {

    public static final String module = ProductUIHelper.class.getName();

    public ProductUIHelper()
    {
        super();
    }

    public boolean getPriceLabel(AtomicProductInput p)
    {
        try
        {
            refresh();
            List<String> plPatterns = r.getSettings().getPriceLabelPatterns();
            for(String plPattern:plPatterns)
            {
                Pattern plItemPattern = Pattern.compile(plPattern);
                Matcher m = plItemPattern.matcher(p.getSku());
                if (m.find())
                {
                    p.setSku(m.group(1));
                    p.setPrice(new BigDecimal(m.group(2)).movePointLeft(2));
                    p.setItemType("PL Item");
                    p.setQty(BigDecimal.ONE);
                    return true;
                }
            }
        }
        catch (Exception ex) {}
        return false;
    }
    public boolean getWeightLabel(AtomicProductInput p)
    {
        try
        {
            refresh();
            List<String> wlPatterns = r.getSettings().getWeightLabelPatterns();
            for(String wlPattern:wlPatterns)
            {
                Pattern wlItemPattern = Pattern.compile(wlPattern);
                Matcher m = wlItemPattern.matcher(p.getSku());
                if (m.find())
                {
                    p.setSku(m.group(1));
                    p.setQty(new BigDecimal(m.group(2)));
                    p.setUomId(r.getSettings().getWeightLabelUom());
                    p.setItemType("WL Item");
                    return true;
                }
            }
        }
        catch (Exception ex) {}
        return false;
    }
    public boolean isScalable(AtomicProductInput p)
    {
        try
        {
            refresh();
            ProductElement prod = r.getProducts().getProduct(p.getProdId());
            return prod.IsScalable;
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
        }
        return false;
    }

    public boolean getScalableProduct(AtomicProductInput p)
    {
        try
        {
            refresh();
            ProductElement prod = r.getProducts().getProduct(p.getProdId());
            if (!prod.IsScalable) return false;

            if (DeviceLoader.scale==null)
            {
                //pos.getTerminalHelper().showError("Error: Scale Device"
                //    ,"Unable to get weight!\r\nScale device driver is missing!");
                Toolkit tk = Toolkit.getDefaultToolkit();
                tk.beep();
                String qtyStr = pos.getTerminalHelper().getNumpadInput(null,"Please enter weight (Grams):",true);
                BigDecimal qty=null;
                try
                {
                    qty = new BigDecimal(qtyStr);
                    if (qty.compareTo(BigDecimal.ZERO)<=0)
                        throw new Exception("Weight is out of range (must be greater than 0)!");
                }
                catch (Exception ex)
                {
                    Debug.logError(ex,module);
                    pos.getTerminalHelper().showError("Error: Scale Device"
                        ,"You have entered an invalid weight: " + qtyStr+"!",ex);
                    return false;
                }
                BigDecimal nativeQty = r.getConversions().convert(qty
                        ,"WT_g",prod.UomId);
                if (nativeQty==null)
                {
                    pos.getTerminalHelper().showError("Error: Scale Device"
                        ,"Unable to get weight!\r\nUnable to convert weight from WT_g to "
                            + prod.UomId);
                    return false;
                }
                BigDecimal tare = trans.getTare();
                BigDecimal nativeTare = r.getConversions().convert(tare
                        ,r.getSettings().getTareUom(),prod.UomId);
                if (nativeTare==null)
                {
                    pos.getTerminalHelper().showError("Error: Scale Device"
                        ,"Unable to get weight!\r\nUnable to convert weight from "+r.getSettings().getTareUom()
                            +" to " + prod.UomId);
                    return false;
                }
                p.setQty(nativeQty.subtract(nativeTare));
                return true;
            }
            else if (Utility.isEmpty(DeviceLoader.scale.getUOMText()))
            {
                pos.getTerminalHelper().showError("Error: Scale Device"
                    ,"Unable to get weight!\r\nScale Unit of Measure (UOM) is missing in driver!");
                return false;
            }
            else
            {
                pos.getOutput().setProgress("Place product on scale...");
                BigDecimal q = DeviceLoader.scale.getWeight(10000);
                BigDecimal nativeQty = r.getConversions().convert(q
                        ,DeviceLoader.scale.getUOMText(),prod.UomId);
                if (nativeQty==null)
                {
                    pos.getTerminalHelper().showError("Error: Scale Device"
                        ,"Unable to get weight!\r\nUnable to convert weight from "+DeviceLoader.scale.getUOMText()
                            +" to " + prod.UomId);
                    return false;
                }
                BigDecimal tare = trans.getTare();
                BigDecimal nativeTare = r.getConversions().convert(tare
                        ,r.getSettings().getTareUom(),prod.UomId);
                if (nativeTare==null)
                {
                    pos.getTerminalHelper().showError("Error: Scale Device"
                        ,"Unable to get weight!\r\nUnable to convert weight from "+r.getSettings().getTareUom()
                            +" to " + prod.UomId);
                    return false;
                }
                p.setQty(nativeQty.subtract(nativeTare));
                return true;
            }

        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
        }
        return false;

    }




    public void convertProdUom(AtomicProductInput p)
    {
        try
        {
            refresh();
            ProductElement prod = r.getProducts().getProduct(p.getProdId());
            if (Utility.isEmpty(prod.UomId) || Utility.isEmpty(p.getUomId())) return;
            else if (prod.UomId.equals(p.getUomId())) return;
            BigDecimal amount = r.getConversions().convert(p.getQty(),p.getUomId(),prod.UomId);
            if (amount==null) throw new GeneralException("Unable to convert weight from "+p.getUomId()
                    +" to " + prod.UomId);
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
            pos.getTerminalHelper().showError("Error: Scale Device"
                ,"Unable to convert weight!",ex);
        }
    }

    public AtomicProductInput getFilteredInput(String triggerSource, String value)
    {
        try
        {
            refresh();
            List<String> inputFilters = r.getSettings().getInputFilters();
            for(String inputFilter:inputFilters)
            {
                Pattern plItemPattern = Pattern.compile(inputFilter);
                Matcher m = plItemPattern.matcher(value);
                if (m.find())
                {
                    String val = null;
                    if (m.groupCount()>0) val=m.group(1);
                    else val=m.group(0);
                    AtomicProductInput p = new AtomicProductInput(triggerSource,value,val,null,null,null);
                    if (productExists(p)) return p;
                }
            }
        }
        catch (Exception ex){}
        return null;
    }


    public boolean productExists(AtomicProductInput p)
    {
        try
        {
            refresh();
            // Check for Priced Item (i.e. Repackaged Bulk with price label)
            p.setItemType("Item");
            if (!getPriceLabel(p)) getWeightLabel(p);

            List<ProductIdentificationElement> ids = r.getProducts().getProductIdListByCode(p.getSku());
            List<ProductElement> prods = r.getProducts().getProductListByCode(p.getSku());
            p.setSelectableProductIds(ids);
            p.setSelectableProducts(prods);
            if (prods!=null && prods.size()>0) return true;
        }
        catch (Exception ex){}
    	return false;
    }

    public void modifyQuantitySelected()
    {
        String value=null;
        try
        {
            refresh();
            pos.getOutput().setProgress("Modifying Item Quantity...");
            PosShoppingCartItem item = pos.getTerminalHelper().getSelectedJournalItem();
            if (item.isPriceLabel())
            {
                pos.getInput().clear();
                pos.getOutput().clear();
                pos.getOutput().setProgress(null);
                Debug.logWarning("Disallowed quantity override on price label item!",module);
                pos.getTerminalHelper().showError("Error: Modify Quantity"
                        ,"Quantity override is not allowed on Price Labels!");
                return;
            }
            Input input = pos.getInput();
            value = input.value();
            if (Utility.isNotEmpty(value)) {
                pos.getInput().clear();
                pos.getOutput().print(null);
                BigDecimal qty = BigDecimal.ZERO;
                qty = new BigDecimal(value);
                int cmp = qty.compareTo(BigDecimal.ZERO);
                if (cmp==0) throw new Exception("You cannot enter a zero quanity!");
                else if (qty.compareTo(BigDecimal.ZERO)==-1) qty = qty.negate();

                if (trans.getCart().isRefund() && trans.getCart().isQtyNegative())
                    qty = qty.negate();
                item.setQuantity(trans.getCart().constructPriceRuleParams(),qty);
                trans.history(new PosAct("KEY", "qty"
                        , Integer.toString(trans.getCart().getItemIndex(item)), qty.toString()));
                pos.getOutput().setHint("Item quantity changed.");
                pos.getTerminalHelper().updateJournal(false);
            }
        }
        catch (NumberFormatException nex)
        {
            Debug.logError(nex, module);
            pos.getTerminalHelper().showError("Error: Modify Quantity"
                ,"Invalid Quantity!\r\nUser Entered: " + value);
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
            pos.getTerminalHelper().showError("Error: Modify Quantity"
                ,"Unable to modify quantity!\r\nHint: You must select an item first!",ex);
        }

        clearFunction();
    }

    public void showPrice(String value)
    {
        refresh();
        if (Utility.isEmpty(value)) {
            clearFunction();
            pos.getOutput().clear();
            pos.getInput().clear();
            return;
        }
        try
        {
            AtomicProductInput p = pos.getProductHelper().getFilteredInput("KEY",value);
            if (p==null)
                pos.getTerminalHelper().showError("Error: Add Item"
                        ,"Product Not found!\r\n" +
                        "Input SKU/UPC: "+value);
            else if (consolidateItem(p))
            {
                ProductElement prod = r.getProducts().getProduct(p.getProdId());
                BigDecimal unitPrice = prod.Price.getActivePrice(BigDecimal.ONE)
                        .getPrice().getPrice(trans.getCart().isUseGrossPrice());
                String uom = "unit";
                if (prod.UomId!=null) uom = r.getConversions().getUom(prod.UomId).Abbrev;
                if ("ea".equals(uom)) uom="unit";
                boolean enterProd = pos.getTerminalHelper().confirm("Product Price Check"
                        ,(p.getQty().compareTo(BigDecimal.ONE)==0?"Price of Product " : "Price of "
                        + p.getQty() + " " + uom + " of product ")
                        + prod.Name + " [" + p.getInputValue()+"] is "
                        + Utility.formatPrice(unitPrice) +" / " + uom,
                        "Do you want to enter this item to the shopping cart?", false);
                if (enterProd) addItem(p);
            }

        }
        catch (Exception ex)
        {
            pos.getTerminalHelper().showError("Error: Price Check"
                    ,"Unable to check price for Id: " + value+"!", ex);
        }

        clearFunction();
        pos.getOutput().setProgress(null);
    }



    public void voidSelectedItem()
    {
        try
        {
            refresh();
            pos.getOutput().setProgress("Voiding Item...");
            PosShoppingCartItem item = pos.getTerminalHelper().getSelectedJournalItem();
            int ind = trans.getCart().getItemIndex(item);
            trans.getCart().removeItem(item);
            pos.getOutput().setHint("Item removed.");
            pos.getTerminalHelper().updateJournal(false);
            pos.getTerminalHelper().selectLastItem();
            trans.history(new PosAct("BTN", "voiditem", Integer.toString(ind)));
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
            pos.getTerminalHelper().showError("Error: Void Item"
                ,"Unable to void item!\r\nHint: You must select an item first!",ex);
        }

        clearFunction();
    }

    public void selectEmployeePricing()
    {
        try
        {
            refresh();
            pos.getOutput().setProgress("Setting Employee Pricing...");
            PosShoppingCartItem item = pos.getTerminalHelper().getSelectedJournalItem();
            if (item==null) throw new Exception("You must select an item first!");
            String uom="each";
            if (item.getProductUom()!=null) uom=item.getProductUom().Description;
            String msg = "Enter Cost/#uom for #sku:";
            msg= msg.replace("#uom",uom).replace("#sku",item.getSku());
            String priceInput = pos.getTerminalHelper().getNumpadInput(null,msg,true);
            if (Utility.isNotEmpty(priceInput))
            {
                BigDecimal price = pos.getPaymentHelper().getAmount(priceInput);
                if (price==null) throw new Exception("You entered an invalid cost: " + priceInput);
                BigDecimal pr = price.multiply(r.getSettings().getEmployeePricingFactor());
                item.overridePrice(pr,"POR_EMPLOYEE", "90");
                trans.history(new PosAct("BTN", "priceovr"
                        , Integer.toString(trans.getCart().getItemIndex(item)), pr.toString(), "POR_EMPLOYEE"));
                pos.getOutput().setHint("Employee pricing set!");
            }
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
            pos.getTerminalHelper().showError("Error: Employee Pricing"
                ,"Unable to set employee pricing!",ex);
        }
        pos.getTerminalHelper().updateJournal(false);
        clearFunction();
    }

    public boolean consolidateItem(AtomicProductInput p)
    {
        try
        {
            refresh();
            // ************** Check Terminal State ****************
            pos.getOutput().setProgress("Adding Item...");
            if (trans==null) throw new Exception("Pos is not ready to accept products!");


            // ************** Product Verification ****************
            if (p==null)
            {
                pos.getTerminalHelper().showError("Error: Add Item"
                        ,"Product Not Found!");
                pos.getTerminalHelper().clearInput();
                pos.getTerminalHelper().clearOutput();
                return false;
            }

            // ************** Select Product ****************
            if (p.getSelectableProducts().size()>1)
            {
                SelectProduct selectProduct = new SelectProduct(p.getProductSelectionMap(),
                        trans,pos);
                String sel = selectProduct.openDlg();
                if (Utility.isNotEmpty(sel)) p.selectProduct(sel);
                else
                {
                    pos.getTerminalHelper().clearInput();
                    pos.getTerminalHelper().clearOutput();
                    return false;
                }
            }
            else p.selectProduct(null);

            // *********** Product Validation ****************
            if (p.getSku()==null || p.getProdId()==null)
            {
                throw new Exception("Unexpected product error! Product Id/sku not set!");
            }
            ProductElement prod = r.getProducts().getProduct(p.getProdId());
            if (prod==null) throw new GeneralException("Error getting product from DB!");
            if (!prod.IsActive) throw new GeneralException("Product " + prod.ProductId+" is not active!");
            if (prod.IsVariant) throw new GeneralException("Product " + prod.ProductId+
                    "is a variant. It is currently not supported.");
            if (prod.IsVirtual) throw new GeneralException("Product " + prod.ProductId+
                    "is a virtual. It is currently not supported.");
            if (!("FINISHED_GOOD".equals(prod.ProductTypeId)))
                throw new GeneralException("Product "+prod.ProductId+" is a "+prod.ProductTypeId
                        +", which is not supported!");

            // TODO: Add function for configurable items

            // ************ Check for scalable products ********************
            //   Also check for price Label and price label calulate weight property
            if (r.getSettings().calculatePriceLabelQuantity() && p.getItemType().equals("PL Item") && isScalable(p))
            {
                PriceElement pr = prod.Price;
                if (pr==null || !pr.getActivePrice(BigDecimal.ONE).isValid())
                    throw new GeneralException("Product "+prod.ProductId
                            +" doesn't have a valid price in the system!");
                BigDecimal unitPrice = pr.getActivePrice(BigDecimal.ONE).getPrice()
                        .getPrice(trans.getCart().isUseGrossPrice());
                if (unitPrice==null && unitPrice.compareTo(BigDecimal.ZERO)<=0)
                    throw new GeneralException("Product "+prod.ProductId
                            +" doesn't have a valid price in the system!");
                p.setQty(p.getPrice().divide(unitPrice,6,BigDecimal.ROUND_HALF_UP));
            }
            else if (p.getQty()==null && isScalable(p))
            {
                if (!getScalableProduct(p)) return false;
            }
            else if (p.getQty()==null) p.setQty(pos.getTerminalHelper().getPosQuantity());

            convertProdUom(p);
            return true;
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
            pos.getTerminalHelper().showError("Error: Add Item"
                    ,"Unable to add item!",ex);
            return false;
        }
        finally {
            pos.getOutput().setProgress(null);
        }
    }

    public void addItem(AtomicProductInput p)
    {
        try
        {
            if (consolidateItem(p)) {
                // **********-** Now Add the product ********************
                boolean wasEmpty = trans.getCart().isEmpty();
                PosShoppingCartItem item = trans.getCart().addItem(p);
                trans.history(new PosAct(p.getSource(), "additem", p.getInputValue()
                        , p.getProdId(), p.getQty().toString()));
                trans.setTare("0", null);
                pos.getOutput().setHint("Added " + p.getItemType());
                pos.getTerminalHelper().updateJournal(true);
                pos.getTerminalHelper().selectLastItem();
                trans.setStaleTransaction();
                if (trans.isRefundMode())
                {
                    if (!pos.getTerminalHelper().confirm("Item Resalable?", "An Item is resalable if it is returned in a condition that would\r\n"
                    +"Allow us to put the product back on the shelf for resale.", "Is the Item Resalable?"))
                    {
                        // Add a discard feature to this product...
                        item.addOptionalFeature("DISCARD"); // To indicate item is not resalable.
                        trans.history(new PosAct(p.getSource(), "discarded"
                                , Integer.toString(trans.getCart().getItemIndex(item))
                                , p.getProdId()));
                        pos.getTerminalHelper().updateJournal(true);
                    }
                }
                clearFunction();
                pos.getTerminalHelper().playBeep();
                if (!trans.isRefundMode() && trans.getCart().getCustomer()==null && wasEmpty)
                {
                    boolean res = pos.getTerminalHelper().confirm("CNF Membership?"
                            ,"Is the customer part of CNF Membership?"
                            ,"Click YES to enter the customer phone number now:" );
                    if (res){
                        pos.getInput().clear();
                        pos.getInput().setFunction("PHONE");
                        pos.getOutput().print("Enter Cust. Phone#:");
                    }

                }
            }
        }
        catch (Exception ex)
        {
            Debug.logError(ex,module);
            pos.getTerminalHelper().showError("Error: Add Item"
                ,"Unable to add item!",ex);
            clearFunction();
        }
        finally {
            pos.getOutput().setProgress(null);
        }



    }



}
