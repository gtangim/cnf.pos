package cnf.pos;

import javolution.util.FastList;
import javolution.util.FastMap;
import jpos.CATConst;
import cnf.pos.cart.PosShoppingCartPayment;
import cnf.pos.cart.resources.ResourceManager;
import cnf.pos.util.Utility;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 6/29/14
 * Time: 6:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class PosCardSettlementInfo {

    private Map<String,BigDecimal> summary;
    private List<PosShoppingCartPayment> payments;
    private String result;
    private int logTransactionCountDifference;
    private BigDecimal logTransactionValueDifference;
    private int serverTransactionCountDifference;
    private BigDecimal serverTransactionValueDifference;


    public PosCardSettlementInfo(ResourceManager resource, String catDailyLog) throws Exception
    {
        result="OK;";
        logTransactionCountDifference=0;
        logTransactionValueDifference=BigDecimal.ZERO;
        serverTransactionCountDifference=0;
        serverTransactionValueDifference=BigDecimal.ZERO;
        summary= FastMap.newInstance();
        payments= FastList.newInstance();
        addValue("DEBIT_CARD",BigDecimal.ZERO);
        addValue("VISA_CARD",BigDecimal.ZERO);
        addValue("MASTER_CARD",BigDecimal.ZERO);
        addValue("AMEX_CARD",BigDecimal.ZERO);
        addValue("JCB_CARD",BigDecimal.ZERO);
        addValue("DISCOVER_CARD",BigDecimal.ZERO);

        if (!Utility.isEmpty(catDailyLog))
        {
            String[] logElements = catDailyLog.split("[\\r\\n]+");
            //int lastStat = -1;
            for(String element:logElements) {
                String[] fields = element.split("\\,");
                if ("BALANCE".equals(fields[0]) && fields.length >= 6) {
                    result = fields[1];
                    logTransactionCountDifference = Integer.parseInt(fields[2]);
                    logTransactionValueDifference = new BigDecimal(fields[3]).movePointLeft(2);
                    serverTransactionCountDifference = Integer.parseInt(fields[4]);
                    serverTransactionValueDifference = new BigDecimal(fields[5]).movePointLeft(2);
                } else if (fields.length == 13) {
                    int status = Integer.parseInt(fields[1]);

                    PosShoppingCartPayment pay = null;

                    pay = new PosShoppingCartPayment(resource,
                            fields[0], new BigDecimal(fields[9]).movePointLeft(2)
                            , resource.getSettings().getCurrency(), fields[8], fields[6], null, null
                            , fields[3] + "-" + fields[5], fields[12], true);

                    String t = pay.getPayTypeExtended();
                    BigDecimal amt = pay.getAmount();

                    if (status == CATConst.CAT_TRANSACTION_SALES)
                        addValue(t, amt);
                    else if (status == CATConst.CAT_TRANSACTION_REFUND || status == CATConst.CAT_TRANSACTION_VOID)
                        subtractValue(t, amt);
                    else throw new Exception("Invalid Entry found in the Payment Terminal Daily Log!");
                }
            }
        }
    }


    private void addValue(String payType, BigDecimal value)
    {
        if (!summary.containsKey(payType)) summary.put(payType,value);
        else summary.put(payType,summary.get(payType).add(value));
    }
    private void subtractValue(String payType, BigDecimal value)
    {
        if (!summary.containsKey(payType)) summary.put(payType,value);
        else summary.put(payType,summary.get(payType).add(value));
    }

    public Map<String, BigDecimal> getPaymentSummary() {
        return summary;
    }

    public BigDecimal getPaymentTotal(String paymentName)
    {
        if (summary.containsKey(paymentName)) return summary.get(paymentName);
        else return BigDecimal.ZERO;
    }

    public List<PosShoppingCartPayment> getPayments() {
        return payments;
    }

    public String getResult() {
        return result;
    }

    public String getResultText()
    {
        String res = "";
        if ("OK".equals(result))
            //res="Payment Terminal is in Balance with the bank records!";
            res="";
        else if ("LOG_ERROR".equals(result))
            res="Error: Payment Terminal is out of balance due to Terminal error!";
        else if ("HOST_ERROR".equals(result))
            res="Error: Payment Terminal is out of balance due to Moneris Server error!";
        else res="Error: Payment terminal is out of balance because of an unknown error!";
        return res;
    }

    public int getLogTransactionCountDifference() {
        return logTransactionCountDifference;
    }

    public BigDecimal getLogTransactionValueDifference() {
        return logTransactionValueDifference;
    }

    public int getServerTransactionCountDifference() {
        return serverTransactionCountDifference;
    }

    public BigDecimal getServerTransactionValueDifference() {
        return serverTransactionValueDifference;
    }

    public boolean isBalanced()
    {
        return "OK".equals(result);
    }
}
