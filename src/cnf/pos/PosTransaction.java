/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos;

import cnf.node.entities.*;
import cnf.pos.cart.resources.elements.*;
import javolution.util.FastList;
import javolution.util.FastMap;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import cnf.pos.util.*;
import cnf.pos.cart.PosShoppingCart;
import cnf.pos.cart.PosShoppingCartItem;
import cnf.pos.cart.PosShoppingCartPayment;
import cnf.pos.cart.resources.ResourceManager;
import cnf.pos.cart.resources.SettingsResource;
import cnf.pos.cart.resources.datastructures.PromoAdjustment;
import cnf.pos.component.ExtendedJournal;
import cnf.pos.component.Output;
import cnf.pos.device.DeviceLoader;
import cnf.pos.screen.PosScreen;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class PosTransaction implements Serializable {

    public static final String module = PosTransaction.class.getName();
    //public static Map lastReceipt = null;
    //public static Map lastRoa = null;
    public static List<Map> lastReceiptModels = null;
    public static List<String> lastReceiptTemplates = null;

    public static boolean printBillWithReceipt=false;
    public static boolean printRoaWithReceipt=false;
    private static PosTransaction currentTx = null;
    private static LifoSet<PosTransactionEntry> savedTx = new LifoSet<PosTransactionEntry>();

    //public static final int NO_PAYMENT = 0;
    //public static final int INTERNAL_PAYMENT = 1;
    //public static final int EXTERNAL_PAYMENT = 2;
    //private static PrintWriter defaultPrintWriter = new Log4jLoggerWriter(Debug.getLogger(module));
    //protected PosTerminalLog txLog = null;
    //protected String transactionId = null;
    //protected XuiSession session = null;
    //private GenericValue shipAddress = null;
    //public int refundIdx = 0;
    //protected boolean dbTrans = false;
    //protected Repository db;

    protected int drawerIdx = 0;
    protected int transactionMode=0;
    protected boolean complete=false;
    protected BigDecimal tare = BigDecimal.ZERO;
    protected long seqId;
    protected boolean printReceipt = true;
    protected boolean printOptionSelected = false;


    protected PosShoppingCart cart;
    protected List<PosAct> actionHistory;
    protected ResourceManager resource;

    //protected Map receiptModel;

    protected List<Map> receiptModels = FastList.newInstance();
    protected List<String> receiptTemplates = FastList.newInstance();

    // ******************* Initialize & Status Methods ********************

    public PosTransaction() {
        this.resource = ResourceManager.Resource;
        try {
            resource.manifestDataModels();
        } catch (Exception e) {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Unable to refresh data model"
                    ,"An error has occurred while trying to reload the POS Data model from Bongo Node!",e);
        }
        try
        {
            seqId = resource.getNode().allocateTransactionSequence();
            String num = Utility.compressNumber(seqId);
            String decom = Utility.decompressNumber(num);
            this.cart = new PosShoppingCart("POS_SALES_CHANNEL");
            this.actionHistory = new ArrayList<PosAct>();
        }
        catch (Exception ex)
        {
            error("FATAL ERROR: Unable to initialize shopping cart! "
            +"\r\nApplication must quit!\r\n\r\nDetails:", ex);
            System.exit(1);
        }
        currentTx = this;
        Debug.log("******************** NEW TRANSACTION ************************");
        Debug.log("***** Transaction Number: "+Long.toString(seqId)+" ******");
        this.reset();
        resource.getNode().postSalesToSecondScreen(cart);
    }

    //private static int nextTrainID=0;
    //private static String getNextTrainID(ResourceManager r)
    //{
    //    return "*"+r.getSettings().getLegacyOrderIdPrefix()
    //            +Utility.padString(Integer.toString(nextTrainID++),4,false,'0');
    //}

    public void reset()
    {
    	ExtendedJournal j= PosScreen.getActiveJournal();
    	j.clear();
    	j.getTable().repaint();
    	j.initializeStyles();
    	this.transactionMode=0;
    	this.complete=false;
        PosScreen.getActiveScreen().getOutput().setTxMode(transactionMode,cart.isQtyNegative());
        PosScreen.getActiveScreen().getOutput().setTillStatus(Output.TILL_STATUS_READY);
    	PosScreen.getActiveScreen().getOutput().setAmount(cart.ZERO);
        PosScreen.getActiveScreen().refresh();
        PosScreen.getActiveScreen().setTab(resource.getSettings().getStartTab());
    }

    public String getStat()
    {
        Date dt = new Date();
        String dts = " | "
    	+ DateFormat.getDateInstance(DateFormat.SHORT).format(dt) + " "
    	+ DateFormat.getTimeInstance(DateFormat.SHORT).format(dt);

        SettingsResource settings = resource.getSettings();
        UserElement sRep = resource.getSecurity().getUser();
        String sRepName = "N/A";
        if (sRep!=null) sRepName = sRep.FirstName;

        String stat =  "Till:"
    		+settings.getPosTerminalPrefix()
                +" | Tx: "+seqId
                +" | SRep:"+sRepName
                +" | Cust:"+cart.getCustomerDisplayName();
    	return stat+dts+" | Tare: "+tare.toString()+" "
                +resource.getConversions().getUom(resource.getSettings().getTareUom()).Abbrev;
    }

    public BigDecimal getFinalChange()
    {
        BigDecimal change = null;
        if (cart.getDue()==null) return null;
        change = cart.getDue().negate();
        return change;
    }
    public boolean finalizeSale()
    {
        boolean success=false;
        try
        {
            if (!cart.isReadOnly())
            {
                seqId = resource.getNode().allocateTransactionSequence();
                history(new PosAct("AUT", "finalize"));
                Map dataModel = constructReceiptDataModel();
                boolean isRoa = (transactionMode==3);
                //if (transactionMode==3) lastRoa=receiptModel;
                //else lastReceipt=receiptModel;
                lastReceiptModels = receiptModels;
                lastReceiptTemplates = receiptTemplates;
                if (resource.getSettings().printReceiptBeforeCommit())
                    processReceiptAndDrawer(dataModel);
                cart.finalizeOrder();
                finalizeTx();
                if (isRoa)
                {
                    PosReceiveOfAccount roa = resource.getNode().createRoa(cart);
                    if (roa==null) throw new Exception("Unable to create Receive of Account data for submission!");
                    if (!resource.getNode().postRoaToDataSyncQueue(roa))
                        throw new Exception("Unable to queue ROA data to the server!");
                }
                else
                {
                    if (!resource.getNode().postSalesToDataSyncQueue(cart,actionHistory))
                        throw new Exception("Unable to queue data to the server!");
                }
                if (!resource.getSettings().printReceiptBeforeCommit())
                    processReceiptAndDrawer(dataModel);
                success=true;
                resource.getNode().postSalesToSecondScreen(cart);
            }
            else error("Error: This transaction is already processed! You may have pressed the sale button multiple times!");
        }
        catch (Exception ex)
        {
            error("Error: Failed to finalize the sale!", ex);
            return false;
        }
        return success;
    }

    public void refreshOutput(PosScreen pos) {
    	Output output = pos.getOutput();
    	if (transactionMode>3)
    	{
    		output.setTillStatus(Output.TILL_STATUS_COMPLETE);
    		output.setAmount(cart.getDue(),true);
    	}
    	else
    	{
    		output.setTxMode(transactionMode,cart.isQtyNegative());
    		output.setTillStatus(Output.TILL_STATUS_READY);
            boolean showChange = false;
            if (cart.getPayments().size()>0) showChange = true;
            output.setAmount(cart.getDue(),showChange);
    	}
    }




    // ******************* Basic Operations Methods ********************

    public boolean setTare(String tareValue, String triggerSource)
    {
    	try
    	{
    		BigDecimal t = new BigDecimal(tareValue);
    		if (t.compareTo(BigDecimal.ZERO)<0) return false;
    		tare = t;
            PosScreen.getActiveScreen().getOutput().setHint("Tare value = " + tare + " "
                    + resource.getConversions().getUom(resource.getSettings().getTareUom()).Abbrev);
            PosScreen.getActiveScreen().refresh();
            history(new PosAct(triggerSource, "tare", tare.toString()));
    		return true;
    	}
    	catch(Exception ex)
    	{
    		return false;
    	}
    }

    public boolean setTareFromScale()
    {
    	try
    	{
    		if (DeviceLoader.scale!=null)
    		{
    			tare = DeviceLoader.scale.getWeight(10000);
                //tare = PosScreen.getActiveScreen().getTerminalHelper()
                tare = resource.getConversions().convert(tare,DeviceLoader.scale.getUOMText(),
                        resource.getSettings().getTareUom());
                PosScreen.getActiveScreen().getOutput().setHint("Tare value = " + tare + " "
                        + resource.getConversions().getUom(resource.getSettings().getTareUom()).Abbrev);
    			history(new PosAct("SCL","tare",tare.toString()));
                return true;
    		}
    		else return false;
    	}
    	catch(Exception ex)
    	{
    		tare = BigDecimal.ZERO;
    		return false;
    	}
    }

    public BigDecimal getTare()
    {
        if (tare==null) return BigDecimal.ZERO;
        else return tare;
    }

    public String getTotalDueMessage(){
    	BigDecimal due = cart.getDue();
    	String msg = "Please Pay Amount: "+Utility.formatPrice(due);
    	if (due.compareTo(cart.ZERO)<0)
    		msg = "Please Refund Amount: "+Utility.formatPrice(due.negate());
    	return msg;
    }
    public String getTotalDueShortMessage(){
    	BigDecimal due = cart.getDue();
    	String msg = "(Pay: "+Utility.formatPrice(due)+")";
    	if (due.compareTo(cart.ZERO)<0)
    		msg = "(Refund: "+Utility.formatPrice(due.negate())+")";
    	return msg;
    }



    public void voidSale(PosScreen pos) throws Exception
    {
        PosScreen.getActiveScreen().getOutput().setProgress("Clearing Transaction...");
        history(new PosAct("BTN", "voidtx"));
        resource.getNode().postSalesVoidToDataSyncQueue(cart,actionHistory);
        currentTx = null;
        PosScreen.getActiveScreen().getOutput().setHint("Reset Tx!");
        PosScreen.getActiveScreen().getOutput().setProgress(null);
    }


    public void finalizeTx()
    {
        PosScreen.getActiveScreen().getOutput().setProgress(null);
        transactionMode=4;
        refreshOutput(PosScreen.getActiveScreen());
    }


    public void paidInOut(String type, Map cashTxDetails)
    {

        try {
            addTransactionInfoToDataModel(cashTxDetails);
            cashTxDetails.put("title","CASH PAID " + type);
            BigDecimal value = (BigDecimal) cashTxDetails.get("value");
            String reasonId = (String) cashTxDetails.get("reason");
            String comment = (String) cashTxDetails.get("reasonComment");
            PosCashTransfer tx = resource.getNode().createCashTransfer(type,reasonId,comment,value);
            if (tx==null) throw new Exception("Unable to create cash transfer entry!");
            if (!resource.getNode().postCashTransferToDataSyncQueue(tx))
                throw new Exception("Unable to queue Cash Transfer to the server!");

        } catch (Exception e) {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Cash Transfer Failed"
                    , "Cash Transfer Failed!",e);
        }

        try {
            if (DeviceLoader.receipt!=null)
                DeviceLoader.receipt.printTemplate("cashinout.ftl",cashTxDetails);
            PosScreen.getActiveScreen().getOutput().setProgress("Paying In/Out...");
            popDrawer(false);
            currentTx = null;
            PosScreen.getActiveScreen().getOutput().setProgress(null);
            PosScreen.getActiveScreen().getOutput().setHint("Cash paid " + type.toLowerCase() + "!");
        } catch (Exception e) {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Failed to print!"
                    , "Cash Transfer was successful but the pos was unable to print!",e);
        }

    }


    public void popDrawer(boolean isManualPop)
    {
        if (resource.getSecurity().isTrainingMode())
        {
            PosScreen.getActiveScreen().getOutput().setHint("Drawer Access Not Allowed!");
            return;
        }
        PosScreen.getActiveScreen().getOutput().setProgress("Opening Drawer...");
        if (DeviceLoader.drawer!=null && DeviceLoader.drawer.length>drawerIdx && DeviceLoader.drawer[drawerIdx]!=null)
        {
        	DeviceLoader.drawer[drawerIdx].openDrawer();
            PosScreen.getActiveScreen().getOutput().setHint("Drawer has been opened.");
            Debug.log("Drawer is opened!");
            if (isManualPop) history(new PosAct("BTN","drawer"));
        }
    }

    public boolean printCardSettlementInfo(PosCardSettlementInfo cardInfo)
    {
        try
        {
            if (cardInfo==null) throw new Exception("Card Settlement data is not available!");
            if (DeviceLoader.receipt!=null && DeviceLoader.receipt.hasPrinter())
            {
                Map cardData = constructCardDataModel(cardInfo);
                if (!DeviceLoader.receipt.printTemplate("cards.ftl",cardData)) return false;
            }
            return true;
        }
        catch(Exception ex)
        {
            error("Error: Unable to print the card Settlement Data!",ex);
            return false;
        }
    }

    /*public boolean printLastRoa()
    {
        try
        {
            if (lastRoa==null) throw new Exception("No Receive of Account transaction found!");
            if (DeviceLoader.receipt!=null)
            {
                lastRoa.put("isOld",true);
                DeviceLoader.receipt.printTemplate("roa.ftl",lastRoa);
                return true;
            }
        }
        catch(Exception ex)
        {
            error("Error: Unable to print the last ROA!",ex);
        }
        return false;
    }*/

    public boolean printLastReceipt()
    {
        try
        {
            if(lastReceiptModels==null || lastReceiptTemplates==null) throw new Exception("No Receipts found!");
            if (DeviceLoader.receipt!=null)
            {
                for(int i=0;i<lastReceiptTemplates.size();i++)
                {
                    Map rModel = lastReceiptModels.get(i);
                    String rName = lastReceiptTemplates.get(i);
                    rModel.put("isOld",true);
                    DeviceLoader.receipt.printTemplate(rName,rModel);
                }
                return true;
            }
        }
        catch(Exception ex)
        {
            error("Error: Unable to print the last receipt!",ex);
        }
        return false;
    }
    public boolean printCurrentReceipt()
    {
        try
        {
            if (DeviceLoader.receipt!=null && !cart.isEmpty())
            {
                Map rData = constructReceiptDataModel();
                if (rData==null) throw new Exception("Unable to construct data!");
                rData.put("isTemp",true);
                DeviceLoader.receipt.printTemplate("receipt.ftl",rData);
            }
        }
        catch(Exception ex)
        {
            error("Error: Unable to print the current partial receipt!",ex);
        }
        return false;
    }

    public static void setRoaReprintMode()
    {
        printRoaWithReceipt=true;
    }

    public void setReceiptPrint(boolean enabled)
    {
        printReceipt = enabled;
        printOptionSelected = true;
    }

    public boolean isPrintOptionSelected() {
        return printOptionSelected;
    }

    public void printPaymentSlips(PosShoppingCartPayment payment)
    {
        Map paymentModel = constructPaymentSlipModel(payment);
        receiptModels.add(paymentModel);
        receiptTemplates.add("paymentslip.ftl");
        if (DeviceLoader.receipt!=null)
            DeviceLoader.receipt.printTemplate("paymentslip.ftl",paymentModel);
        paymentModel.put("printCustomer",true);
        //paymentModel.put("printMerchant",true);
    }
    public void processReceiptAndDrawer(Map dataModel)
    {
        try
        {
            boolean hasCash = false;

            List<PosShoppingCartPayment> cash = cart.getPayment("CASH");
            if (cash.size()>0) hasCash=true;

            if (resource.getSettings().isAlwaysPopDrawer() || (hasCash || cart.getDue().compareTo(cart.ZERO)!=0))
                this.popDrawer(false);
            PosScreen.getActiveScreen().getOutput().setProgress("Finishing Sale....(Printing)");
            if (transactionMode==3) {
                receiptModels.add(dataModel);
                receiptTemplates.add("roa.ftl");
                if (printReceipt && DeviceLoader.receipt!=null)
                    DeviceLoader.receipt.printTemplate("roa.ftl",dataModel);
            }
            else
            {
                receiptModels.add(dataModel);
                receiptTemplates.add("receipt.ftl");
                if (printReceipt && DeviceLoader.receipt!=null)
                    DeviceLoader.receipt.printTemplate("receipt.ftl",dataModel);
                if (Utility.isNotEmpty(cart.getBillingAccountId())) {
                    receiptModels.add(dataModel);
                    receiptTemplates.add("billaccount.ftl");
                    if (printReceipt && DeviceLoader.receipt!=null)
                        DeviceLoader.receipt.printTemplate("billaccount.ftl", dataModel);
                }
            }
        }
        catch(Exception ex)
        {
            error("Printer Error: Unable to print receipt!\r\nDetails:"+ex.getMessage());
        }
        if (dataModel!=null)
        {
            if (transactionMode<3)
            {
                printRoaWithReceipt=false;
                if (Utility.isNotEmpty(cart.getBillingAccountId())) printBillWithReceipt=true;
                else printBillWithReceipt=false;
            }
        }
    }


    public void setStaleTransaction()
    {
        if (transactionMode==0)
            transactionMode=1;
        PosScreen pos = PosScreen.getActiveScreen();
        if (pos!=null) refreshOutput(pos);
    }

    public boolean isOngoingTransaction()
    {
        if (transactionMode==0 || transactionMode==4) return false;
        else return true;
    }


    public void enterRefundMode(String triggerSource, String idType,String id)
    {
    	if (transactionMode==0 || cart.isEmpty())
    	{
            String originalOrderId = id;
            if (!idType.equals("RECEIPT")) id = resource.getSettings().getLegacyOrderIdPrefix()+"00000";

    		this.transactionMode=2;
    		try
            {
                cart.setParentOrderId(originalOrderId);
            }
            catch(Exception ex)
            {
                error("Error: Unable to enter the refund mode!",ex);
            }
            PosScreen.getActiveScreen().getOutput().setHint("Entered Refund Mode");
    		PosScreen.getActiveScreen().getOutput().setProgress("Refunding Receipt: " + originalOrderId);
    		PosScreen.getActiveScreen().refresh();
            resource.getNode().postSalesToSecondScreen(cart);
            history(new PosAct(triggerSource,"refund",idType,id));
    	}
    	else PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Refund Mode"
                ,"Cannot enter refund mode because you must start with an empty transaction!");
    }

    public void enterRoaMode(BillingAccountElement ba)
    {
        cart.setRoaMode(true, ba.BillingAccountId);
        this.transactionMode=3;
        PosScreen.getActiveScreen().getOutput().setHint("Entered ROA Mode");
        Debug.log("Entered Receive Of Account (ROA) Mode!");
        PosScreen.getActiveScreen().getOutput().setProgress(null);
        PosScreen.getActiveScreen().refresh();
        resource.getNode().postSalesToSecondScreen(cart);
        history(new PosAct("BTN", "roa", ba.BillingAccountId));
    }
    public void setTransactionMode(int txMode)
    {
        this.transactionMode=txMode;
    }


    private void appendPayment(StringBuilder sb, String title, BigDecimal value,boolean allowNegative)
    {
        sb.append(Utility.padString(title+": ",20,false,' '));
        if (value==null) value=new BigDecimal("0.00");
        if (!allowNegative && value.compareTo(BigDecimal.ZERO)==-1) value=value.negate();
        sb.append(Utility.padString(
                PosScreen.getActiveScreen().getTerminalHelper().formatCurrency(value),10,false,' '));
        sb.append("\r\n");
    }
    public String getCloseValues(Map<String,BigDecimal> values) throws Exception
    {
        StringBuilder sb = new StringBuilder();
        appendPayment(sb,"Cash",values.get("cash"),false);
        appendPayment(sb,"Gift Card",values.get("giftCard"),false);
        appendPayment(sb,"Gift Certificate",values.get("giftCert"),false);
        appendPayment(sb,"Money Order",values.get("moneyOrder"),false);
        appendPayment(sb,"Cheque",values.get("cheque"),false);
        appendPayment(sb,"Vendor Coupons",values.get("vendorCoupon"),false);
        appendPayment(sb,"Billing Accounts",values.get("billingAccount"),true);
        appendPayment(sb,"Others",values.get("other"),true);
        appendPayment(sb,"Debit Card",values.get("debit"),true);
        appendPayment(sb,"Visa Card",values.get("visa"),true);
        appendPayment(sb,"Master Card",values.get("master"),true);
        appendPayment(sb,"Amex Card",values.get("amex"),true);
        appendPayment(sb,"JCB Card",values.get("jcb"),true);
        appendPayment(sb,"Discover Card",values.get("discover"),true);
        appendPayment(sb,"Other Card",values.get("otherCC"),true);

        return sb.toString();
    }
    BigDecimal getAmount(BigDecimal value, boolean allowNegative)
    {
        if (value==null)  return new BigDecimal("0.00");
        else if (!allowNegative && value.compareTo(BigDecimal.ZERO)==-1) return value.negate();
        else return value;
    }


    //private BigDecimal runningPayCalculatedTotal;
    //private BigDecimal runningPayDeclaredTotal;

    /*
        The first six digits are the Issuer Identification Number...

            '*CARD TYPES            *PREFIX           *WIDTH
            'American Express       34, 37            15
            'Diners Club            300 to 305, 36    14
            'Carte Blanche          38                14
            'Discover               6011              16
            'EnRoute                2014, 2149        15
            'JCB                    3                 16
            'JCB                    2131, 1800        15
            'Master Card            51 to 55          16
            'Visa                   4                 13, 16
    */
    private FastMap getSummaryMap(PosSettlementDeclare declareEntry)
    {
        FastMap res = FastMap.newInstance();
        res.put("title",declareEntry.DeclareType.replace("EXT_BILLACT","BILLING_ACCOUNTS").replace('_',' '));
        res.put("type",declareEntry.DeclareType);
        res.put("saleValue",declareEntry.CalculatedValue);
        res.put("declaredValue",declareEntry.DeclaredValue);
        res.put("diffValue",declareEntry.ErrorValue);
        if (declareEntry.CalculatedValue.compareTo(BigDecimal.ZERO)==0
                && declareEntry.DeclaredValue.compareTo(BigDecimal.ZERO)==0)
            res.put("hasData",false);
        else
            res.put("hasData",true);
        return res;
    }


    public Map constructCardDataModel(PosCardSettlementInfo cardInfo) throws Exception
    {
        FastMap root = FastMap.newInstance();
        addTotalsHeaderDataModel(root);
        root.put("message",cardInfo.getResultText());
        root.put("logCountDifference",cardInfo.getLogTransactionCountDifference());
        root.put("logValueDifference",cardInfo.getLogTransactionValueDifference());
        root.put("serverCountDifference",cardInfo.getServerTransactionCountDifference());
        root.put("serverValueDifference",cardInfo.getServerTransactionValueDifference());
        List payList = FastList.newInstance();
        for(Map.Entry<String,BigDecimal> pay : cardInfo.getPaymentSummary().entrySet())
        {
            Map p = FastMap.newInstance();
            p.put("payType",pay.getKey().replace("_"," "));
            p.put("value",pay.getValue());
            payList.add(p);
        }
        root.put("payList",payList);
        return root;
    }

    public void addTransactionInfoToDataModel(Map root)
    {
        root.put("receipt",true);
        root.put("sequenceNumber",seqId);
        if (!cart.isRoaMode())
        {
            root.put("orderId",Long.toString(seqId));
            root.put("orderIdBarcode",Utility.compressNumber(seqId));
        }
        else
        {
            root.put("orderId",null);
            root.put("orderIdBarcode",null);
        }
        root.put("parentOrderId",cart.getParentOrderId());
        root.put("transactionId",Long.toString(seqId));
        root.put("drawerNo",getDrawerNumber());
        root.put("userId",cart.getSalesRep().UserNumber);
        root.put("salesRep",cart.getSalesRep().Description);
        root.put("salesRepFirstName",cart.getSalesRep().FirstName);
        if (cart.getCustomer()!=null) {
            root.put("customer",cart.getCustomer().Description);
            root.put("customerFirstName",cart.getCustomer().FirstName);
            root.put("customerLastName",cart.getCustomer().LastName);
            root.put("customerPhoneNumber",cart.getCustomer().CustomerPhoneNumber);
            root.put("customerId",cart.getCustomer().CustomerId);
        }
        else
        {
            root.put("customer","NA");
            root.put("customerFirstName","NA");
            root.put("customerLastName","NA");
            root.put("customerPhoneNumber","NA");
            root.put("customerId","NA");
        }
        root.put("orderDate",DateTimeFormat.forPattern("MM/dd/yyyy HH:mm:ss").print(cart.getOrderDate()));
        root.put("date", DateTimeFormat.forPattern("MM/dd/yyyy HH:mm:ss").print(new DateTime()));
        root.put("billingAccount",cart.getBillingAccountId());
        root.put("trainingMode",resource.getSecurity().isTrainingMode());
    }

    public void addTotalsHeaderDataModel(Map root)
    {
        root.put("drawerNo",getDrawerNumber());
        root.put("userId",cart.getSalesRep().UserNumber);
        root.put("salesRep",cart.getSalesRep().FirstName);
        root.put("orderDate", (new DateTime()).toString());
        root.put("trainingMode",resource.getSecurity().isTrainingMode());
    }

    private String maskCardNumber(String cardNumber)
    {
        StringBuilder sb = new StringBuilder(20);
        int n = cardNumber.length();
        for (int i=0;i<n;i++)
        {
            if (n-i<=4) sb.append(cardNumber.charAt(i));
            else sb.append('*');
        }
        return sb.toString();
    }

    private Map<String,String> payTranslator = null;
    private Map<String,String> toFrench = null;
    public void constructPaymentHelper()
    {
        if (payTranslator==null)
        {
            payTranslator = FastMap.newInstance();
            payTranslator.put("ACT_0","FLASH DEFAULT");
            payTranslator.put("ACT_1","INTERAC CHEQUING");
            payTranslator.put("ACT_2","INTERAC SAVINGS");
            payTranslator.put("ACT_4",null);
        }

        if (toFrench==null)
        {
            toFrench = FastMap.newInstance();
            toFrench.put("PURCHASE","ACHAT");
            toFrench.put("REFUND","REMISE D'ACHAT");
            toFrench.put("PURCHASE CORRECTION","CORRECTION D'ACHAT");
            toFrench.put("REFUND CORRECTION","CORRECTION DE REMISE");
            toFrench.put("INTERAC CHEQUING","INTERAC CHEQUE");
            toFrench.put("INTERAC SAVINGS","INTERAC EPARGNE");
            toFrench.put("FLASH DEFAULT","FLASH DEFAULT");
        }
    }

    private String payField(String fieldTitle, String fieldValue)
    {
        String key = fieldTitle.toUpperCase()+"_"+fieldValue;
        if (payTranslator.containsKey(key)) return payTranslator.get(key);
        else return fieldValue;
    }

    /*private String languageField(String text, String lang)
    {
        if (lang!=null && lang.toLowerCase().startsWith("f"))
        {
            if (toFrench.containsKey(text.toUpperCase())) return toFrench.get(text.toUpperCase());
            else return text;
        }
        else return text;
    }*/

    private String french(String text)
    {
        if (text==null) return "";
        if (toFrench.containsKey(text.toUpperCase())) return toFrench.get(text.toUpperCase());
        else return text;
    }

    private Map getPaymentModel(PosShoppingCartPayment payment)
    {

        Map pay = Utility.toMap(
                "shortText",payment.getPaymentMethodTypeShortText()
                ,"text",payment.getPaymentMethodTypeText()
                ,"displayText",payment.getDisplayText()
                ,"status",payment.getStatusText()
                ,"amount",payment.getUnsignedAmount()
        );
        pay.put("cardType",payment.getPayTypeExtended());
        pay.put("paymentType",payment.getOperation());
        pay.put("paymentTypeFr",french(payment.getOperation()));
        if (payment.getPayTypeExtended().equals("DEBIT_CARD"))
            pay.put("accountType",payField("ACT",payment.getProperty("ACT")));
        else pay.put("accountType",payment.getPaymentMethodTypeShortText());
        pay.put("accountTypeFr",french((String)pay.get("accountType")));
        String cashBack = payment.getProperty("CSB");
        if (cashBack!=null)
        {
            BigDecimal cashBackValue = new BigDecimal(cashBack);
            pay.put("cashBack",cashBackValue);
            pay.put("subAmount",payment.getAmount().subtract(cashBackValue));
        }

        pay.put("signCustomer",false);
        pay.put("signMerchant",false);


        if (payment.isCATPayment())
        {
            pay.put("cardNumber",maskCardNumber(payment.getReferenceNumber()));
            String dt = payment.getProperty("TXD");
            if (dt!=null) dt = dt.replaceAll("\\.",":");
            else
            {
                DateTime now = new DateTime();
                DateTimeFormatter dtf = DateTimeFormat.forPattern("dd MMM yyyy HH.mm.ss");
                dt = dtf.print(now);
            }
            pay.put("paymentDate",dt);
            String[] id = payment.getInvoiceId().split("\\-");
            pay.put("transactionNumber",id[0]);
            pay.put("slipNumber",id[1]);
            pay.put("authCode",payment.getAuthCode());
            if (!Utility.isEmpty(payment.getAuthCode()))
            {
                pay.put("approved",true);
                String verification = payment.getProperty("VER");
                boolean sig = false;
                if (!Utility.isEmpty(verification))
                {
                    if (verification.toLowerCase().contains("signature")) sig=true;
                }
                if (sig && ("VISA_CARD".equals(payment.getPayTypeExtended())
                        || "PURCHASE".equals(payment.getOperation())
                        || "REFUND CORRECTION".equals(payment.getOperation())))
                    pay.put("signCustomer",true);

                if (!"VISA_CARD".equals(payment.getPayTypeExtended())
                        && !"DEBIT_CARD".equals(payment.getPayTypeExtended())
                        && ("PURCHASE CORRECTION".equals(payment.getOperation())
                        || "REFUND".equals(payment.getOperation())))
                    pay.put("signMerchant",true);
            }

            if (payment.getOperation().contains("CORRECTION")) pay.put("reversal",true);
            pay.put("emvApplicationId",payment.getProperty("AID"));
            pay.put("emvApplicationName",payment.getProperty("ANM"));
            pay.put("emvTsi",payment.getProperty("TSI"));
            pay.put("emvTvr",payment.getProperty("TVR"));

            String respCode = payment.getResponseCode();
            String respCode24 = payment.getProperty("B24");
            if (respCode==null || respCode24==null) {
                respCode="";
                respCode24="";
            }
            pay.put("respCode",respCode);
            pay.put("respCode24",respCode24);
            pay.put("condCode",payment.getProperty("CND"));


            pay.put("cvm",payment.getProperty("VER"));
            pay.put("csi",payment.getProperty("SWI"));
            pay.put("language",payment.getProperty("LAN"));

            if (printReceipt) {
                pay.put("printCustomer", resource.getSettings().alwaysPrintCustomerPaymentSlips());
                pay.put("printMerchant", resource.getSettings().alwaysPrintMerchantPaymentSlips());
            }
            else
            {
                pay.put("printCustomer", false);
                pay.put("printMerchant", false);
            }

            // Override any print option when signature is required
            boolean sc = (Boolean)pay.get("signCustomer");
            boolean sm = (Boolean)pay.get("signMerchant");
            if (sm) pay.put("printCustomer",true);
            if (sc) pay.put("printMerchant",true);
        }


        FastList payDesc = FastList.newInstance();
        String pt = payment.getPayType();
            /*if ("DEBIT_CARD".equals(pt))
            {
                payDesc.add(UtilMisc.toMap("title","Card#","text",payment.getReferenceNumber()));
                payDesc.add(UtilMisc.toMap("title","Invoice#","text",payment.getInvoiceId()));
                payDesc.add(UtilMisc.toMap("title","Auth Code:","text",payment.getAuthCode()));
            }
            else if (pt.endsWith("_CARD"))*/
        if (pt.endsWith("_CARD"))
        {
            payDesc.add(Utility.toMap("title","Card#","text",maskCardNumber(payment.getReferenceNumber())));
            String[] id = payment.getInvoiceId().split("\\-");
            String iid = "";
            if (id.length>0) iid = id[0];
            payDesc.add(Utility.toMap("title","Invoice#","text",iid));
            //payDesc.add(UtilMisc.toMap("title","Expiry Date:","text",payment.getExpiry()));
            payDesc.add(Utility.toMap("title","Auth Code:","text",payment.getAuthCode()));
        }
        else if ("GIFT_CARD".equals(pt))
        {
            payDesc.add(Utility.toMap("title","Card#","text","*"+payment.getCardSuffix()));
            payDesc.add(Utility.toMap("title","Invoice#","text",payment.getInvoiceId()));
        }
        else if ("MONEY_ORDER".equals(pt)
                || "GIFT_CERTIFICATE".equals(pt)
                || "PERSONAL_CHECK".equals(pt)
                || "VENDOR_COUPON".equals(pt)
                )
        {
            payDesc.add(Utility.toMap("title","Ref#","text",payment.getReferenceNumber()));
        }
        else if ("EXT_BILLACT".equals(pt))
        {
            payDesc.add(Utility.toMap("title","Customer#","text",cart.getCustomer().PartyId));
            payDesc.add(Utility.toMap("title","Name:","text",cart.getCustomer().FirstName));
            payDesc.add(Utility.toMap("title","Account#","text",payment.getReferenceNumber()));
        }

        String conv = payment.getConversion();
        if (conv!=null) payDesc.add(Utility.toMap(
                "title","Currency Rate:","text",conv));

        pay.put("descriptions",payDesc);
        return pay;
    }

    public Map constructPaymentSlipModel(PosShoppingCartPayment payment)
    {
        constructPaymentHelper();
        Map root = getPaymentModel(payment);
        addTotalsHeaderDataModel(root);
        return root;
    }


    public Map constructReceiptDataModel() throws Exception
    {
        FastMap root = FastMap.newInstance();
        PosShoppingCart cart = getCart();

        constructPaymentHelper();
        addTransactionInfoToDataModel(root);

        root.put("subtotal",cart.getSubTotal());
        root.put("grandTotal",cart.getGrandTotal());
        root.put("change",cart.getDue().negate());
        if (cart.getPriceSavings().compareTo(BigDecimal.ZERO)!=0)
            root.put("priceSavings",cart.getPriceSavings().negate());
        if (cart.getDiscountSavings().compareTo(BigDecimal.ZERO)!=0)
            root.put("discountSavings",cart.getDiscountSavings().negate());
        if (cart.getPromoSavings().compareTo(BigDecimal.ZERO)!=0)
            root.put("promoSavings",cart.getPromoSavings().negate());
        if (cart.getTotalSavings().compareTo(BigDecimal.ZERO)!=0)
            root.put("totalSavings",cart.getTotalSavings().negate());

        FastList items = FastList.newInstance();
        for(PosShoppingCartItem psci : cart.getItems())
        {
            FastMap item = FastMap.newInstance();
            item.put("productId",psci.getProductId());
            item.put("productSku",psci.getSku());
            item.put("productDisplayId",psci.getReceiptProductId());
            item.put("isTaxable",psci.getTaxable());
            item.put("quantity",psci.getQuantity());
            item.put("basePrice",psci.getBasePrice());
            item.put("itemTotalBeforeOverride",psci.getSubtotalBeforeOverride());
            item.put("itemTotal",psci.getSubtotal());
            item.put("description",psci.getDescription());
            item.put("uomShortName",psci.getProductUom().Abbrev);
            item.put("uomLongName",psci.getProductUom().Description);
            item.put("isScalable", psci.isScalable());
            item.put("isSelected", psci.isSelected());

            if (psci.getOverrideReason()!=null) {
                item.put("override",Utility.toMap("text", psci.getPriceOverrideText()
                        , "price", psci.getOverridePrice()
                        , "value", psci.getOverrideSubtotal()
                        , "valueReduction", psci.getOverrideSubtotal().subtract(psci.getSubtotalBeforeOverride())
                        , "reason", psci.getOverrideReason()));
            } else {
              if (! psci.getSelectedPrice().getPriceSaving(cart.isUseGrossPrice()).equals(BigDecimal.ZERO)) {
                  item.put("inf_sale",Utility.toMap("text","*SALE* you saved "
                          + PosScreen.getActiveScreen().getTerminalHelper().formatCurrency(psci.getSelectedPrice()
                          .getPriceSaving(cart.isUseGrossPrice()).multiply(psci.getQuantity()))));

                }
            }

            FastList features = FastList.newInstance();
            FastList featuresZero = FastList.newInstance();
            for(String featureType:cart.getFeatureTypeNames())
            {
                List<FeatureElement> fList = psci.getFeatures(featureType);
                for(FeatureElement fae:fList)
                {
                    BigDecimal v = psci.getFeatureDisplayValue(fae);
                    if (v!=null && v.abs().compareTo(new BigDecimal("0.0001"))>0)
                        features.add(Utility.toMap("text"
                                ,psci.getFeatureDisplayName(fae),"value",v));
                    else featuresZero.add(fae.Description);
                }
            }
            item.put("chargedFeatures",features);
            item.put("features",featuresZero);

            if (psci.getItemDiscountAmount()!=null)
                item.put("discount", Utility.toMap("text",psci.getItemDiscountText()
                        ,"value",psci.getItemDiscountAmount()));

            FastList promos = FastList.newInstance();
            for (PromoAdjustment pa : cart.getPromoAdjustments(psci.getCartIndex()))
            {
                String pcode = pa.getDisplayPromoCode();
                if (pcode==null) pcode="";
                promos.add(Utility.toMap("text",cart.getPromoName(pa.getPromoId())
                        ,"value",pa.getAmount(),"code",pcode));
            }
            item.put("promos",promos);

            items.add(item);
        }
        root.put("items",items);

        if (cart.getDiscountAmount()!=null)
            root.put("discount", Utility.toMap("text",cart.getDiscountText()
                    ,"value",cart.getDiscountAmount()));

        FastList promos = FastList.newInstance();
        for (PromoAdjustment pa : cart.getPromoAdjustments(-1))
        {
            String pcode = pa.getDisplayPromoCode();
            if (pcode==null) pcode="";
            else pcode = " " + pcode;
            promos.add(Utility.toMap("text",cart.getPromoName(pa.getPromoId())
                    ,"value",pa.getAmount(),"code",pcode));
        }
        root.put("promos",promos);

        FastList taxes = FastList.newInstance();
        List<TaxElement> tList = cart.getResource().getTaxes().getStoreTaxes();
        for(TaxElement t:tList)
        {
            BigDecimal taxValue = cart.getTax(t.TaxId);
            if (cart.getResource().getSettings().isShowZeroTax()
                    || !taxValue.equals(cart.ZERO))
                taxes.add(Utility.toMap("text",t.Description,"value",taxValue));
        }
        root.put("taxes",taxes);

        FastList payments = FastList.newInstance();
        for(PosShoppingCartPayment payment: cart.getPayments())
        {
            Map pay = getPaymentModel(payment);
            payments.add(pay);
        }
        root.put("payments",payments);

        return root;
    }










    // *******************  Access Methods ********************

    public int getDrawerNumber() {
        return drawerIdx + 1;
    }
    public boolean txFinished() {return cart.isReadOnly();}
    public PosShoppingCart getCart() {
        return cart;
    }
    public ResourceManager getResource()
    {
        return resource;
    }
    public static synchronized boolean transactionCompleted() {
    	if (currentTx==null) return true;
        else if (currentTx.txFinished()) return true;
        else return false;
    }
    public static synchronized  PosTransaction getNewTransaction()
    {
        currentTx=null;
        return getCurrentTx();
    }
    public static synchronized void setTransaction(PosTransaction tx)
    {
        currentTx=tx;
        tx.refreshOutput(PosScreen.getActiveScreen());
        PosScreen.getActiveScreen().getTerminalHelper().updateJournal(true);
    }
    public List<PosAct> getActionHistory(){
        return actionHistory;
    }

    public boolean isRefundMode() {
        return this.transactionMode==2;
    }
    public boolean isRoaMode()
    {
        return this.transactionMode==3;
    }
    public static synchronized PosTransaction getCurrentTx() {
        if (currentTx == null) {
            if (ResourceManager.Resource.getSecurity().getUser() != null) {
                currentTx = new PosTransaction();
            }
        }
        return currentTx;
    }





    public void error(String err,Exception ex)
    {
        try
        {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Pos Operation", err, ex);
            Debug.logError(ex, module);
        }
        catch(Exception e)
        {
        }
    }
    public void error(String err)
    {
        try
        {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Pos Operation", err);
        }
        catch(Exception e)
        {
        }
    }


    // TODO: In future we should try to make this persist in the DB...
    // But be aware that doings so has some difficult issues to solve
    // in terms of data-sync and cleanup...
    public static void saveTx(PosTransactionEntry te)
    {
        te.getTx().history(new PosAct("BTN","park",(new DateTime()
                .toString(DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z")))));
        savedTx.push(te);
        currentTx = null;
    }

    public static void loadTx(PosTransactionEntry te)
    {
        savedTx.remove(te);
        currentTx = te.getTx();
        currentTx.history(new PosAct("BTN","park",(new DateTime()
                .toString(DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z")))));
    }

    public static LifoSet<PosTransactionEntry> getSavedTransactions()
    {
        return savedTx;
    }

    public void history(PosAct action)
    {
        if (action.S==null) return; // omit action as this is part of another action.
        Debug.log(action.toString());
        actionHistory.add(action);
        setStaleTransaction();
    }



}
