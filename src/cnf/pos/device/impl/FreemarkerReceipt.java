/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.device.impl;

import apu.jpos.util.Utility;
import freemarker.template.Configuration;
import freemarker.template.Template;
import jpos.JposConst;
import jpos.JposException;
import jpos.POSPrinter;
import jpos.POSPrinterConst;
import cnf.pos.util.*;
import cnf.pos.device.GenericDevice;
import cnf.pos.screen.PosScreen;

import java.io.*;
import java.util.Map;

public class FreemarkerReceipt extends GenericDevice {

    public static final String module = FreemarkerReceipt.class.getName();

    protected static final String receipt_log_file = "receipts.txt";

    protected POSPrinter printer = null;

    protected Configuration freemarkerConfig=null;

    public boolean printTemplate(String templateName, Map dataModel)
    {
        boolean txStarted = false;
        try
        {
            Debug.log("Printing receipt:"+templateName);
            checkState();
            if (!ftlInitialized()) return false;
            if (printer==null) return false;
            printer.transactionPrint(POSPrinterConst.PTR_S_RECEIPT,POSPrinterConst.PTR_TP_TRANSACTION);
            txStarted=true;

            //Map root = constructReceiptDataModel(trans);
            //lastReceipt=dataModel;
            Template template = freemarkerConfig.getTemplate(templateName);
            StringWriter out = new StringWriter();
            template.process(dataModel,out);
            out.flush();
            String result = out.toString();
            String[] elements = result.split("###");
            for (int i=0;i<elements.length;i++)
            {
                if (elements[i].startsWith("BARCODE:"))
                {
                    String bCode=elements[i].substring(8);
                    if (cnf.pos.util.Utility.isNotEmpty(bCode))
                    this.printBarcode(bCode);
                }
                else this.print(elements[i]);
            }
            return true;
        }
        catch (Exception ex)
        {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Receipt Printer"
                    , "Unable to print the receipt!" , ex);
            Debug.logError(ex, module);
            return false;
        }
        finally {
            try {
                if (txStarted)
                    printer.transactionPrint(POSPrinterConst.PTR_S_RECEIPT,POSPrinterConst.PTR_TP_NORMAL);
            } catch (JposException e) {
                Debug.logError(e, module);
            }

        }

    }













    private boolean ftlInitialized() {
        if (freemarkerConfig == null) {
            // printer is not ready
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Receipt Printer"
                    , "Freemarker is not initialized properly!\r\nFreemarker is required to print receipt templates!");
            return false;
        }
        return true;
    }




    // *************** PRINTER JPOS DRIVER *************************

    public FreemarkerReceipt(String deviceName, int timeout) {
    	super(deviceName,timeout);
        if (deviceName==null)
        {
        	deviceName="NULL_PRINTER";
        	this.deviceName=deviceName;
        }
        else
        {
        	try
        	{
        		this.printer = new POSPrinter();
        	    this.control = this.printer;
            }
        	catch(Exception ex)
        	{
        		control=null;
                printer=null;
        	}
        }
    }

    @Override
    protected void initialize() throws JposException {
        Debug.logInfo("Receipt [" + control.getPhysicalDeviceName() + "] Claimed : " + control.getClaimed(), module);
        // set map mode to metric - all dimensions specified in 1/100mm units
        // unit = 1/100 mm - i.e. 1 cm = 10 mm = 10 * 100 units
        if (printer!=null)
        	printer.setMapMode(POSPrinterConst.PTR_MM_METRIC);
    }

    public void print(String p) throws Exception {
        try {
            if (!Utility.appendStringToFile(this.receipt_log_file,p,1024*1024*256))
                throw new Exception("Could not write to receipt log file!");
            //FileUtil.appendToTextFile(p, this.receipt_log_file);
            Debug.logInfo(p,module);
        } catch (Exception e) {
            Debug.logError(e, module);
        }
        if (printer!=null)
            printer.printNormal(POSPrinterConst.PTR_S_RECEIPT, p);
    }


    public void printBarcode(String barcode) throws Exception {
        // print the orderId bar code (Code 3 of 9) centered (1cm tall, 6cm wide)
        try {
            if (!Utility.appendStringToFile(this.receipt_log_file,"Barcode:"+barcode,1024*1024*256))
                throw new Exception("Could not write to receipt log file!");
        	//FileUtil.appendToTextFile("Barcode:"+barcode, this.receipt_log_file);
            Debug.logInfo("Barcode:"+barcode,module);
        } catch (Exception e) {
            Debug.logError(e, module);
        }
        if (control!=null)
            ((POSPrinter) control).printBarCode(POSPrinterConst.PTR_S_RECEIPT, barcode, POSPrinterConst.PTR_BCS_Code93,
                80, 2, POSPrinterConst.PTR_BC_CENTER, POSPrinterConst.PTR_BC_TEXT_BELOW);
    }



    private void checkState() throws JposException {
        Debug.logInfo("Checking Printer State...",module);
        if (printer==null) return;
        else if (printer.getCoverOpen() == true)
            throw new JposException(JposConst.JPOS_E_FAILURE,"Printer Cover is Open!");
    }


    public Configuration getFreemarkerConfig() {
        return freemarkerConfig;
    }

    public void setFreemarkerConfig(Configuration freemarkerConfig) {
        this.freemarkerConfig = freemarkerConfig;
    }

    public boolean hasPrinter(){
        return printer!=null;
    }
}
