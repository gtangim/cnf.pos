/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.device.impl;

import jpos.JposException;
import jpos.ScaleConst;

import cnf.pos.util.Debug;
import cnf.pos.device.GenericDevice;
import cnf.pos.screen.PosScreen;
//import cnf.pos.screen.DialogCallback;
//import cnf.pos.screen.*;
import java.math.BigDecimal;


public class Scale extends GenericDevice {

    public static final String module = Scale.class.getName();


    public Scale(String deviceName, int timeout) {
        super(deviceName, timeout);
        this.control = new jpos.Scale();
    }

    protected void initialize() throws JposException {
        Debug.logInfo("Scale [" + control.getPhysicalDeviceName() + "] Claimed : " + control.getClaimed(), module);
        
    }


    public int getUOM()
    {
    	try{
    		return ((jpos.Scale)control).getWeightUnit();    		
    	} 
    	catch(JposException e)
    	{
    		Debug.logWarning("Device error (assuming default weight unit: gram. Reason: "+ e.getMessage(),module);
    		return ScaleConst.SCAL_WU_GRAM;
    	}
    }
    
    
    
   public String getUOMText()
    {
    	int uom = getUOM();
    	if (uom==ScaleConst.SCAL_WU_GRAM) return "WT_g";
    	else if (uom==ScaleConst.SCAL_WU_KILOGRAM) return "WT_kg";
    	else if (uom==ScaleConst.SCAL_WU_OUNCE) return "WT_oz";
    	else if (uom==ScaleConst.SCAL_WU_POUND) return "WT_lb";
    	else 
    	{
    		Debug.logWarning("Scale unit Id " + uom + "is unknown to opentaps!", module);
    		return "unknown";
    	}
    }

   public BigDecimal getWeight(int timeout_ms) throws JposException
   {
	   //PosDialog dlg = null;
	   try
	   {
		   String s = "";
		   //dlg = PosScreen.currentScreen.showDialog("dialog/infopopup", "Place product in Scale...");
		   int[] ret = new int[1];
		   ((jpos.Scale)control).readWeight(ret, timeout_ms);
		   //if (dlg!=null) dlg.close();
		   //dlg=null;
		   if (ret[0]==0) throw new JposException(jpos.JposConst.JPOS_E_FAILURE, "Zero Weight! Cannot Add Product!");
		   double w = ((double)ret[0])/1000.0;
		   PosScreen.currentScreen.getOutput().setHint("Weight: " + Double.toString(w) + " "+getUOMText()+"s");
		   return new BigDecimal(w).setScale(6,BigDecimal.ROUND_HALF_UP);
	   }
	   catch(JposException e)
	   {
		   //if (dlg!=null) dlg.close();
           PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Scale Device"
                                   ,"Unable to retrieve weight from the scale!",e);
		   throw e;
	   }
   }
   
   
}

