/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.device.impl;

import javolution.util.FastMap;
import jpos.CATConst;
import jpos.JposConst;
import jpos.JposException;

import cnf.pos.util.Debug;
import cnf.pos.PosCardSettlementInfo;
import cnf.pos.device.GenericDevice;
import cnf.pos.screen.PosScreen;
//import cnf.pos.screen.DialogCallback;
import cnf.pos.util.Utility;
//import cnf.pos.screen.*;
import java.math.BigDecimal;
import java.util.Map;


public class PaymentTerminal extends GenericDevice {
	public enum CardType {DEBIT, CREDIT, GIFT};
    public static final String module = PaymentTerminal.class.getName();
    protected jpos.CAT cat;
    protected String lastError;
    private Map<String,String> additionalInfo = null;
    private boolean allowCashBack;


    public PaymentTerminal(String deviceName, int timeout) {
        super(deviceName, timeout);
        this.control = new jpos.CAT();
        cat = (jpos.CAT)control;
    }

    protected void initialize() throws JposException {
        Debug.logInfo("CAT [" + control.getPhysicalDeviceName() + "] Claimed : " + control.getClaimed(), module);
        
    }
    
    
    protected boolean doOperation(CardType txType, int operation, int sequenceNumber, BigDecimal totalAmount, BigDecimal otherCharges, String termTxID) throws Exception
    {
    	try
    	{
    		if (txType==CardType.DEBIT) cat.setPaymentMedia(CATConst.CAT_MEDIA_DEBIT);
    		else if (txType==CardType.CREDIT) cat.setPaymentMedia(CATConst.CAT_MEDIA_CREDIT);
    		long amt = totalAmount.movePointRight(2).longValue();
    		long tax = otherCharges.movePointRight(2).longValue();
    		if (operation==CATConst.CAT_TRANSACTION_SALES)
	    	{
                if (allowCashBack)
                {
                    allowCashBack=false;
                    cat.setAdditionalSecurityInformation("CASHBACK:Y");
                }
                else cat.setAdditionalSecurityInformation(null);
	    		cat.authorizeSales(sequenceNumber,amt,tax,-1);
	    		return !Utility.isEmpty(cat.getApprovalCode());
	    	}
	    	else if (operation==CATConst.CAT_TRANSACTION_REFUND)
	    	{
	    		if (cat.getCapAuthorizeRefund())
	    		{
	    			cat.authorizeRefund(sequenceNumber,amt,tax,-1);
                    return !Utility.isEmpty(cat.getApprovalCode());
	    		}
	    		else throw new Exception("Refund not implemented by the driver!");
	    			
	    	}
	    	else if (operation==CATConst.CAT_TRANSACTION_VOID)
	    	{
	    		if (cat.getCapAuthorizeVoid())
	    		{
	    			cat.authorizeVoid(sequenceNumber,amt,tax,-1);
                    return !Utility.isEmpty(cat.getApprovalCode());
	    		}
	    		else throw new Exception("Void payment not implemented by the driver!");
	    	}
	    	else
	    	{
	 		    //PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Payment Terminal"
                //        ,"This type of transaction is not supported!");
                throw new Exception("This type of transaction is not supported!");
	    	}
    	}
    	catch(JposException ex)
    	{
            /*PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Payment Terminal"
                                    ,"Unable to process this payment!\r\n"
                                    +"There was a payment terminal driver error!",ex);*/
            lastError=ex.getMessage();
            throw ex;
    	}
    }

    public boolean sale(CardType txType, int sequenceNumber, BigDecimal totalAmount, BigDecimal otherCharges) throws Exception
    {
    	return doOperation(txType,CATConst.CAT_TRANSACTION_SALES, sequenceNumber, totalAmount,otherCharges,null);
    }    
    public boolean refundSale(CardType txType, int sequenceNumber, BigDecimal totalAmount, BigDecimal otherCharges) throws Exception
    {
    	return doOperation(txType,CATConst.CAT_TRANSACTION_REFUND, sequenceNumber, totalAmount,otherCharges,null);    	
    }
    public boolean voidSale(CardType txType, int sequenceNumber, BigDecimal totalAmount, BigDecimal otherCharges) throws Exception
    {
    	return doOperation(txType,CATConst.CAT_TRANSACTION_VOID, sequenceNumber, totalAmount,otherCharges,null);
    }
    public PosCardSettlementInfo getSettlement(int sequenceNumber, boolean eraseBatch) throws Exception
    {
        if (cat.getCapDailyLog()!=CATConst.CAT_DL_REPORTING_SETTLEMENT) return null;
        int typ = CATConst.CAT_DL_REPORTING;
        if (eraseBatch) typ = CATConst.CAT_DL_SETTLEMENT;
        cat.accessDailyLog(sequenceNumber,typ,-1);
        String dailyLog = cat.getDailyLog();
        return new PosCardSettlementInfo(PosScreen.getActiveScreen().getResource(), dailyLog);
    }

    public boolean supportsSettlement()
    {
        try {
            if (cat.getCapDailyLog()==CATConst.CAT_DL_REPORTING_SETTLEMENT) return true;
            else return false;
        } catch (JposException e) {
            return false;
        }
    }


    private void buildAdditionalInfo()
    {
        try {
            additionalInfo = FastMap.newInstance();
            String[] infoElements = cat.getAdditionalSecurityInformation().split("\\|");
            for(String element:infoElements)
            {
                String[] pair = element.split("\\:");
                if (pair[0].startsWith("ASI")) pair[0] = pair[0].substring(3);
                if (pair.length==1) additionalInfo.put(pair[0],null);
                else additionalInfo.put(pair[0],pair[1]);
            }
        } catch (JposException e) {
            Debug.log(e);
        }
    }
    public String getExpiry()
    {
        try
        {
            if (additionalInfo==null) buildAdditionalInfo();
            if (additionalInfo==null) throw new JposException(JposConst.JPOS_E_ILLEGAL,
                    "There is a problem with payment terminal driver! "
                    +"\r\nCould not obtain additional information!");
            if (additionalInfo.containsKey("EXP")) return additionalInfo.get("EXP");
            else return "";
        }
        catch(JposException ex)
        {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Payment Terminal"
                    , "Error encountered while constructing CAT response! ", ex);
            return "";
        }
    }
    public String getVerification()
    {
        try
        {
            if (additionalInfo==null) buildAdditionalInfo();
            if (additionalInfo==null) throw new JposException(JposConst.JPOS_E_ILLEGAL,
                    "There is a problem with payment terminal driver! "
                            +"\r\nCould not obtain additional information!");
            if (additionalInfo.containsKey("VER")) return additionalInfo.get("VER");
            else return "";
        }
        catch(JposException ex)
        {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Payment Terminal"
                    ,"Error encountered while constructing CAT response! ",ex);
            return "";
        }
    }

    public String getTransactionResponse()
    {
    	StringBuilder res = new StringBuilder();
    	try{

            res.append("T");
            res.append(cat.getTransactionNumber());
            res.append(",");

            res.append("I");
            res.append(cat.getSlipNumber());
            res.append(",");

    		res.append("C");
    		res.append(cat.getCardCompanyID());
    		res.append(",");

    		res.append("AC");
    		res.append(cat.getApprovalCode());
    		res.append(",");

    		res.append("R");
    		res.append(cat.getCenterResultCode());
    		res.append(",");

    		res.append("S");
    		res.append(cat.getSequenceNumber());
    		res.append(",");

    		res.append("ASI");
    		res.append(cat.getAdditionalSecurityInformation());
    		

    		
    	}catch(JposException ex)
    	{
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Payment Terminal"
                                    ,"Error encountered while constructing CAT response!",ex);
    	}
    	
    	return res.toString();
    	
    }
    
    public String getTransactionId()
    {
    	try
    	{
    		return cat.getTransactionNumber();
    	}
    	catch(JposException ex)
    	{
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Payment Terminal"
                                    ,"Error getting TransactionId!",ex);
		    return "";
    	}
    }
    
    public String getInvoiceId()
    {
    	try
    	{
    		return cat.getSlipNumber();
    	}
    	catch(JposException ex)
    	{
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Payment Terminal"
                                    ,"Error getting InvoiceId!",ex);
		   return "";
    	}
    }

    public long getAmount()
    {
        try
        {
            return cat.getSettledAmount();
        }
        catch(JposException ex)
        {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Payment Terminal"
                                    ,"Error getting Amount!",ex);
           return 0;
        }
    }

    public String getCardCompanyId()
    {
        try
        {
            return cat.getCardCompanyID();
        }
        catch(JposException ex)
        {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Payment Terminal"
                                    ,"Error getting CompanyId!",ex);
           return "";
        }
    }
    public String getPartialCardId()
    {
    	try
    	{
    		String s = cat.getAccountNumber();
    		if (s==null) s="??";
    		if (s.length()<4) return s;
    		s = "*" + s.substring(s.length()-4);
    		return s;
    	}
    	catch(JposException ex)
    	{
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Payment Terminal"
                                    ,"Error getting PartialCardId!",ex);
		   return "";
    	}
    }

    public String getAccountNumber()
    {
        try
        {
            String s = cat.getAccountNumber();
            return s;
        }
        catch(JposException ex)
        {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Payment Terminal"
                    ,"Error getting Card Account Number!",ex);
            return "";
        }
    }
    
    
    public String getAuthCode()
    {
    	try
    	{
    		return cat.getApprovalCode();
    	}
    	catch(JposException ex)
    	{
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Payment Terminal"
                                    ,"Error getting Authorization Code!",ex);
		   return "";
    	}
    }

    public String getResponseCode()
    {
        try
        {
            return cat.getCenterResultCode();
        }
        catch(JposException ex)
        {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Payment Terminal"
                    ,"Error getting Authorization Code!",ex);
            return "";
        }
    }

    /*public CardType getPaymentMedia()
    {
    	try
    	{
    		int t=cat.getPaymentMedia();
    		if (t==CATConst.CAT_MEDIA_DEBIT) return CardType.DEBIT;
    		else return CardType.CREDIT;
    	}
    	catch(JposException ex)
    	{
    		return CardType.CREDIT;
    	}
    }*/
    
    public int getTransactionType()
    {
        String t="";
        try
        {
            t = cat.getTransactionType();
            if ("Purchase".equals(t)) return CATConst.CAT_TRANSACTION_SALES;
            else if ("Refund".equals(t)) return CATConst.CAT_TRANSACTION_REFUND;
            else if (t.contains("Void")) return CATConst.CAT_TRANSACTION_VOID;
            else throw new Exception("Unsupported");
        }
        catch(Exception ex)
        {
            PosScreen.getActiveScreen().getTerminalHelper().showError("Error: Payment Terminal"
                    ,"Unsupported transaction " + t + " received from the pinpad!");
            lastError=ex.getMessage();
            return -1;
        }

    }

    public boolean isDeclined()
    {
        try {
            return Utility.isEmpty(cat.getApprovalCode());
        } catch (JposException e) {
            Debug.log(e);
            return true;
        }
    }

    public String getLastError() {
        return lastError;
    }

    public int getSequenceNumber(){
        try {
            return cat.getSequenceNumber();
        } catch (JposException e) {
            Debug.log(e);
            return 0;
        }
    }

    public void setAllowCashBack(boolean allowCashBack) {
        this.allowCashBack = allowCashBack;
    }

    /*


     public int getUOM()
     {
         try{
             return ((jpos.Scale)control).getWeightUnit();
         }
         catch(JposException e)
         {
             Debug.logWarning("Device error (assuming default weight unit: gram. Reason: "+ e.getMessage(),module);
             return ScaleConst.SCAL_WU_GRAM;
         }
     }



    public String getUOMText()
     {
         int uom = getUOM();
         if (uom==ScaleConst.SCAL_WU_GRAM) return "WT_g";
         else if (uom==ScaleConst.SCAL_WU_KILOGRAM) return "WT_kg";
         else if (uom==ScaleConst.SCAL_WU_OUNCE) return "WT_oz";
         else if (uom==ScaleConst.SCAL_WU_POUND) return "WT_lb";
         else
         {
             Debug.logWarning("Scale unit Id " + uom + "is unknown to opentaps!", module);
             return "unknown";
         }
     }

    public BigDecimal getWeight(int timeout_ms) throws JposException
    {
        PosDialog dlg = null;
        try
        {
            String s = "";
            dlg = PosScreen.currentScreen.showDialog("dialog/infopopup", "Place product in Scale...");
            int[] ret = new int[1];
            ((jpos.Scale)control).readWeight(ret, timeout_ms);
            dlg.close();
            double w = ((double)ret[0])/1000.0;
            PosScreen.currentScreen.getOutput().print("Weight: " + Double.toString(w) + " "+getUOMText()+"s");
            return new BigDecimal(w).setScale(6,BigDecimal.ROUND_HALF_UP);
        }
        catch(JposException e)
        {
            if (dlg!=null) dlg.close();
            if (PosScreen.currentScreen!=null)
            {
                PosScreen.currentScreen.showDialog("dialog/error/producterror",e.getMessage());
            }
            throw e;
        }
    }
    */
   
}

