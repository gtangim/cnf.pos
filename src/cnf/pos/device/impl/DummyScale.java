package cnf.pos.device.impl;

import jpos.JposException;
import jpos.ScaleConst;

import cnf.pos.util.Debug;

import java.math.BigDecimal;

/**
 * Created by IntegratingWeb
 * User: Alberto Lobrano
 * Date: Apr 20, 2010
 * Time: 8:46:09 PM
 */
public class DummyScale extends Scale{


  

    public DummyScale(String deviceName, int timeout) {
		super(deviceName, timeout);
	}


	@Override
    protected void initialize() throws JposException {
        Debug.logInfo("Scale [" + control.getPhysicalDeviceName() + "] Claimed : " + control.getClaimed(), module);
    }


    public int getUOM()
    {
        //we return gram as a test UOM
        return ScaleConst.SCAL_WU_GRAM;
    }


   public BigDecimal getWeight(int timeout_ms) throws JposException
   {
       return  new BigDecimal(1234.36688).setScale(6,BigDecimal.ROUND_HALF_UP);
   }
}
