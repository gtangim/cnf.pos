/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.device;

import jpos.JposException;

import cnf.pos.util.Debug;
import cnf.pos.util.GeneralException;
import cnf.pos.cart.resources.elements.PosConfig;
import cnf.pos.device.impl.CashDrawer;
import cnf.pos.device.impl.*;

public class DeviceLoader {

    public static final String module = DeviceLoader.class.getName();

    public static CashDrawer[] drawer = null;
    public static CheckScanner check = null;
    public static Journal journal = null;
    public static Keyboard keyboard = null;
    public static LineDisplay ldisplay = null;
    public static Msr msr = null;
    public static PinPad pinpad = null;
    //public static Receipt receipt = null;
    public static FreemarkerReceipt receipt = null;
    public static Scanner scanner = null;
    public static Scale scale = null;
    public static DummyScale dummyScale = null;
    public static PaymentTerminal paymentTerminal = null;

    public static void load(PosConfig posConfig) throws GeneralException {
        // load the keyboard
        if (posConfig.KeyboardDeviceName != null) {
            keyboard = new Keyboard(posConfig.KeyboardDeviceName, -1);
            try {
                keyboard.open();
            } catch (JposException jpe) {
                Debug.logError(jpe, "JPOS Exception", module);
                throw new GeneralException(jpe.getOrigException());
            }
        }

        // load the scanner
        if (posConfig.ScannerDeviceName != null) {
            scanner = new Scanner(posConfig.ScannerDeviceName, -1);
            try {
                scanner.open();
            } catch (JposException jpe) {
                Debug.logError(jpe, "JPOS Exception", module);
                throw new GeneralException(jpe.getOrigException());
            }
        }

        // load the scale
        if (posConfig.ScaleDeviceName != null) {
            if ("DummyScale".equals(posConfig.ScaleDeviceName)) {
                scale = new DummyScale(posConfig.ScaleDeviceName, -1);
            } else {
                scale = new Scale(posConfig.ScaleDeviceName, -1);
                try {
                    scale.open();
                } catch (JposException jpe) {
                    Debug.logError(jpe, "JPOS Exception", module);
                    throw new GeneralException(jpe.getOrigException());
                }
            }
        }


        // load the check reader
        if (posConfig.CheckScannerDeviceName != null) {
            check = new CheckScanner(posConfig.CheckScannerDeviceName, -1);
            try {
                check.open();
            } catch (JposException jpe) {
                Debug.logError(jpe, "JPOS Exception", module);
                throw new GeneralException(jpe.getOrigException());
            }
        }

        // load the msr
        if (posConfig.MsrDeviceName != null) {
            msr = new Msr(posConfig.MsrDeviceName, -1);
            try {
                msr.open();
            } catch (JposException jpe) {
                Debug.logError(jpe, "JPOS Exception", module);
                throw new GeneralException(jpe.getOrigException());
            }
        }

        // load the receipt printer
        if (posConfig.PrinterDeviceName != null) {
            receipt = new FreemarkerReceipt(posConfig.PrinterDeviceName, -1);
            try {
                receipt.open();
            } catch (JposException jpe) {
                Debug.logError(jpe, "JPOS Exception", module);
                throw new GeneralException(jpe.getOrigException());
            }
        } else {
            receipt = new FreemarkerReceipt(null, -1);
        }


        // load the journal printer
        if (posConfig.JournalDeviceName != null) {
            journal = new Journal(posConfig.JournalDeviceName, -1);
            try {
                journal.open();
            } catch (JposException jpe) {
                Debug.logError(jpe, "JPOS Exception", module);
                throw new GeneralException(jpe.getOrigException());
            }
        }

        // load the line display
        if (posConfig.LineDisplayDeviceName != null) {
            ldisplay = new LineDisplay(posConfig.LineDisplayDeviceName, -1);
            try {
                ldisplay.open();
            } catch (JposException jpe) {
                Debug.logError(jpe, "JPOS Exception", module);
                throw new GeneralException(jpe.getOrigException());
            }
        }

        // In this version we only support one cash drawer. But in future we may want to introduce more than one
        if (posConfig.CashDrawerDeviceName != null) {
            if (drawer == null) {
                drawer = new CashDrawer[10]; // Support for upto 10 Cash Drawer
            }

            // create the instance
            drawer[0] = new CashDrawer(posConfig.CashDrawerDeviceName, -1);
            try {
                drawer[0].open();
            } catch (JposException jpe) {
                Debug.logError(jpe, "JPOS Exception", module);
                throw new GeneralException(jpe.getOrigException());
            }

        }

        // load the Credit/Debit authorizing terminal device
        if (posConfig.CatDeviceName != null) {
            paymentTerminal = new PaymentTerminal(posConfig.CatDeviceName, -1);
            try {
                paymentTerminal.open();
            } catch (JposException jpe) {
                Debug.logError(jpe, "JPOS Exception", module);
                throw new GeneralException(jpe.getOrigException());
            }
        }


    }

    /*public static void enable(boolean enable) {
        if (keyboard != null) {
            keyboard.enable(enable);
        }
        if (scanner != null) {
            scanner.enable(enable);
        }
        if (msr != null) {
            msr.enable(enable);
        }
        if (check != null) {
            check.enable(enable);
        }
        if (ldisplay != null) {
            ldisplay.enable(enable);
        }
        if (pinpad != null) {
            pinpad.enable(enable);
        }
        if (receipt != null) {
            receipt.enable(enable);
        }

        if (paymentTerminal!=null)
            paymentTerminal.enable(enable);

    }*/

    public static void enable(boolean scannerEnabled, boolean pinpadEnabled
            , boolean printerEnabled, boolean otherEnabled)
    {
        if (keyboard != null) {
            keyboard.enable(otherEnabled);
        }
        if (scanner != null) {
            scanner.enable(scannerEnabled);
        }
        if (msr != null) {
            msr.enable(otherEnabled);
        }
        if (check != null) {
            check.enable(otherEnabled);
        }
        if (ldisplay != null) {
            ldisplay.enable(otherEnabled);
        }
        if (pinpad != null) {
            pinpad.enable(pinpadEnabled);
        }
        if (receipt != null) {
            receipt.enable(printerEnabled);
        }

        if (paymentTerminal!=null)
            paymentTerminal.enable(pinpadEnabled);
    }



    public static void stop() throws GeneralException {
        try {
            if (keyboard != null) {
                keyboard.close();
            }
            if (scanner != null) {
                scanner.close();
            }
            if (msr != null) {
                msr.close();
            }
            if (check != null) {
                check.close();
            }
            if (ldisplay != null) {
                ldisplay.close();
            }
            if (pinpad != null) {
                pinpad.close();
            }

            if (drawer != null) {
                for (int i = 0; i < drawer.length; i++) {
                    if (drawer[i] != null) {
                        drawer[i].close();
                    }
                }
            }

            if (receipt != null) {
                receipt.close();
            }
            if (journal != null) {
                journal.close();
            }

            if (paymentTerminal!=null)
                paymentTerminal.close();

        } catch (JposException jpe) {
            Debug.logError(jpe, "JPOS Exception", module);
            throw new GeneralException(jpe.getOrigException());
        }
    }
}
