package cnf.pos.screen;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 12/6/11
 * Time: 4:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExtendedReentrantLock extends ReentrantLock {

    public ExtendedReentrantLock()
    {
        super(true);
    }

    public String getStackTraceOwner()
    {
        Thread owner = this.getOwner();
        if (owner==null) return "N/A";
        StackTraceElement[] steList = owner.getStackTrace();
        StringBuilder sb = new StringBuilder();
        for(StackTraceElement ste:steList)
        {
            sb.append(ste.toString());
            sb.append("\r\n");
        }
        return sb.toString();
    }
    
    public boolean blockerContains(String searchString)
    {
        try {
            Thread owner = this.getOwner();
            if (owner==null) return false;
            StackTraceElement[] steList = owner.getStackTrace();
            for(StackTraceElement ste:steList)
            {
                if (ste.toString().toLowerCase().contains(searchString.toLowerCase())) return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public void shutdownDeadlockThread()
    {
        try {
            Thread owner = this.getOwner();
            owner.interrupt();
        } catch (Exception e) {
        }
    }
}
