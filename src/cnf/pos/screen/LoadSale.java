/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.screen;

import java.awt.*;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.ListSelectionModel;

import net.xoetrope.swing.*;
import net.xoetrope.xui.XPage;
import net.xoetrope.xui.events.XEventHelper;

import cnf.pos.util.Debug;
import cnf.pos.PosTransaction;
import cnf.pos.PosTransactionEntry;


@SuppressWarnings("serial")
public class LoadSale extends XPage {

    /**
     * To load a sale from a shopping list. 2 modes : add to or replace the current sale. Also a button to delete a sale (aka shopping list)
     */

    public static final String module = LoadSale.class.getName();
    protected static PosScreen m_pos = null;
    protected XDialog m_dialog = null;
    protected ArrayList<PosTransactionEntry> m_savedSale = new ArrayList<PosTransactionEntry>();
    protected XList m_salesList = null;
    protected XImageButton m_cancel = null;
    protected XImageButton m_load = null;
    protected XTextArea m_title = null;
    protected XTextArea m_message = null;
    protected DefaultListModel m_listModel = null;
    protected PosTransactionEntry selectedTrans = null;

    public LoadSale(PosScreen page) {
        m_savedSale.addAll(PosTransaction.getSavedTransactions());
        m_pos = page;
    }

    public PosTransactionEntry openDlg() {
        m_dialog = (XDialog) pageMgr.loadPage(m_pos.getScreenLocation() + "/dialog/loadsale");
        m_dialog.setCaption("Load a sale");
        m_dialog.setHideFrame(true);
        m_salesList = (XList) m_dialog.findComponent("salesList");
        //XEventHelper.addMouseHandler(this, m_salesList, "saleDoubleClick");

        m_title = (XTextArea)m_dialog.findComponent("title");
        m_message = (XTextArea)m_dialog.findComponent("message");

        m_cancel = (XImageButton) m_dialog.findComponent("BtnCancel");
        m_load = (XImageButton) m_dialog.findComponent("BtnLoad");
        XEventHelper.addMouseHandler(this, m_cancel, "cancel");
        XEventHelper.addMouseHandler(this, m_load, "loadSale");

        m_listModel = new DefaultListModel();
        for (PosTransactionEntry t : m_savedSale) {
            m_listModel.addElement(t.getName());
        }
        m_salesList.setModel(m_listModel);
        m_salesList.setVisibleRowCount(-1);
        m_salesList.ensureIndexIsVisible(m_salesList.getItemCount());
        m_salesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        m_salesList.setToolTipText("You may use a double click to replace the current sale and delete this one");
        m_salesList.setOpaque(false);
        m_salesList.setBackground(new Color(0x20ff8f00,true));
        m_salesList.setSelectionBackground(new Color(0x800000ff,true));
        if (m_title!=null) m_title.setText("Load a Parked Transaction");
        if (m_message!=null) m_message.setText("Please Select one of the following saved transactions and click Load:");

        m_dialog.pack();
        m_salesList.requestFocusInWindow();
        m_dialog.showDialog(this);
        if (m_dialog.isVisible()) m_dialog.closeDlg();
        return selectedTrans;
    }


    public synchronized void cancel() {
        if (wasMouseClicked()) {
            closeDlg();
        }
    }

    public synchronized void loadSale() {
        if (wasMouseClicked()) {
            selectedTrans = selectedSale();
            if (selectedTrans!=null) closeDlg();
        }
    }


    private PosTransactionEntry selectedSale() {
        try
        {
            if (null != m_salesList.getSelectedValue()) {
                int ind = m_salesList.getSelectedIndex();
                return m_savedSale.get(ind);
            }
            return null;
        }
        catch(Exception ex)
        {
            return null;
        }
    }

    private ClassLoader getClassLoader(PosScreen pos) {
        ClassLoader cl = pos.getClassLoader();
        if (cl == null) {
            try {
                cl = Thread.currentThread().getContextClassLoader();
            } catch (Throwable t) {
            }
            if (cl == null) {
                Debug.log("No context classloader available; using class classloader", module);
                try {
                    cl = this.getClass().getClassLoader();
                } catch (Throwable t) {
                    Debug.logError(t, module);
                }
            }
        }
        return cl;
    }

    private void closeDlg() {
        m_dialog.closeDlg();
    }
}
