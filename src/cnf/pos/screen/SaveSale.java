/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.screen;

import net.xoetrope.swing.*;
import net.xoetrope.xui.XPage;
import net.xoetrope.xui.events.XEventHelper;

import cnf.pos.util.Debug;
import cnf.pos.PosTransaction;

// importing DeviceLoader for pop.drawer
import cnf.pos.PosTransactionEntry;
import cnf.pos.cart.resources.ResourceManager;
import cnf.pos.util.Utility;

@SuppressWarnings("serial")
public class SaveSale extends XPage {

    /**
     * To save a sale. 2 modes : save and keep the current sale or save and clear the current sale.
     */

    public static final String module = SaveSale.class.getName();
    protected static PosScreen m_pos = null;
    protected XDialog m_dialog = null;
    protected XEdit m_saleName = null;
    protected XImageButton m_cancel = null;
    protected XImageButton m_save = null;
    protected XTextArea m_title = null;
    protected XTextArea m_message = null;
    protected XButton m_saveAndClear = null;
    // New button for Save and Print funtion
    //protected XButton m_saveAndPrint = null; //FIXME : button does not exist yet
    protected static PosTransactionEntry m_trans = null;

    public SaveSale(PosScreen pos) {
        PosTransaction trans = PosTransaction.getCurrentTx();
        m_trans = new PosTransactionEntry(trans);
        m_pos = pos;
    }

    public PosTransactionEntry openDlg() {
        m_dialog = (XDialog) pageMgr.loadPage(m_pos.getScreenLocation() + "/dialog/savesale");
        m_saleName = (XEdit) m_dialog.findComponent("saleName");
        //m_dialog.setM_focused(m_saleName);
        m_saleName.setText(m_trans.getName() + " ("+ ResourceManager.Resource.getSecurity().getUser().FirstName+")");
        m_dialog.setCaption("Save a sale");
        m_dialog.setHideFrame(true);
        m_title = (XTextArea)m_dialog.findComponent("title");
        m_message = (XTextArea)m_dialog.findComponent("message");

        m_cancel = (XImageButton) m_dialog.findComponent("BtnCancel");
        m_save = (XImageButton) m_dialog.findComponent("BtnSave");

        if (m_title!=null) m_title.setText("Save/Park the Current Transaction");
        if (m_message!=null)
        {
            m_message.setText("Please select a name for this transaction and click Save:");
            m_message.setFocusable(false);
        }

        XEventHelper.addMouseHandler(this, m_cancel, "cancel");
        XEventHelper.addMouseHandler(this, m_save, "save");
        XEventHelper.addMouseHandler(this, m_saleName, "editSaleName");

        m_dialog.pack();
        m_dialog.showDialog(this);
        if (m_dialog.isVisible()) m_dialog.closeDlg();
        return m_trans;
    }

    public synchronized void cancel()
    {
        if (wasMouseClicked()) {
            m_trans=null;
            this.m_dialog.closeDlg();
        }
    }

    public synchronized void save() {
        if (wasMouseClicked()) {
            String sale = m_saleName.getText();
            if (!Utility.isEmpty(sale))
            {
                m_trans.setName(sale);
                this.m_dialog.closeDlg();
            }
        }
    }



    public synchronized void editSaleName() {
        if (wasMouseClicked()) {
            try {
                m_saleName.setText(m_pos.getTerminalHelper().getKeyboardInput(m_saleName.getText()
                ,"Please enter the transaction name (to save as):",false));
            } catch (Exception e) {
                Debug.logError(e, module);
            }
            m_dialog.repaint();
        }
        return;
    }

    private void saveSale(String sale) {
        final ClassLoader cl = this.getClassLoader(m_pos);
        Thread.currentThread().setContextClassLoader(cl);
        this.m_dialog.closeDlg();
    }

    private ClassLoader getClassLoader(PosScreen pos) {
        ClassLoader cl = pos.getClassLoader();
        if (cl == null) {
            try {
                cl = Thread.currentThread().getContextClassLoader();
            } catch (Throwable t) {
            }
            if (cl == null) {
                Debug.log("No context classloader available; using class classloader", module);
                try {
                    cl = this.getClass().getClassLoader();
                } catch (Throwable t) {
                    Debug.logError(t, module);
                }
            }
        }
        return cl;
    }
}
