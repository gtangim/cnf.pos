/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.screen;

import net.xoetrope.swing.XDialog;
import net.xoetrope.swing.XImageButton;
import net.xoetrope.swing.XList;
import net.xoetrope.swing.XTextArea;
import net.xoetrope.xui.XPage;
import net.xoetrope.xui.events.XEventHelper;
import cnf.pos.util.Debug;
import cnf.pos.cart.resources.elements.BillingAccountElement;

import javax.swing.*;
import java.awt.*;
import java.util.List;


@SuppressWarnings("serial")
public class SelectAccount extends XPage {

    /**
     * To load a sale from a shopping list. 2 modes : add to or replace the current sale. Also a button to delete a sale (aka shopping list)
     */

    public static final String module = SelectAccount.class.getName();
    protected static PosScreen m_pos = null;
    protected XDialog m_dialog = null;
    protected List<BillingAccountElement> m_accounts = null;
    protected XList m_accountList = null;
    protected XImageButton m_cancel = null;
    protected XImageButton m_select = null;
    protected XTextArea m_title = null;
    protected XTextArea m_message = null;
    protected DefaultListModel m_listModel = null;
    protected BillingAccountElement selectedAccount = null;

    public SelectAccount(PosScreen page, List accounts) {
        m_accounts=accounts;
        m_pos = page;
    }

    public BillingAccountElement openDlg() {
        m_dialog = (XDialog) pageMgr.loadPage(m_pos.getScreenLocation() + "/dialog/selectCustAccount");
        m_dialog.setCaption("Select Billing Account");
        m_dialog.setHideFrame(true);
        m_accountList = (XList) m_dialog.findComponent("custAccountList");
        //XEventHelper.addMouseHandler(this, m_accountList, "saleDoubleClick");

        m_title = (XTextArea)m_dialog.findComponent("title");
        m_message = (XTextArea)m_dialog.findComponent("message");

        m_cancel = (XImageButton) m_dialog.findComponent("BtnCancel");
        m_select = (XImageButton) m_dialog.findComponent("BtnLoad");
        XEventHelper.addMouseHandler(this, m_cancel, "cancel");
        XEventHelper.addMouseHandler(this, m_select, "selectAccount");

        m_listModel = new DefaultListModel();
        for (BillingAccountElement ba : m_accounts) {
            m_listModel.addElement("["+ba.BillingAccountId+"] " + ba.Description);
        }
        m_accountList.setModel(m_listModel);
        m_accountList.setVisibleRowCount(-1);
        m_accountList.ensureIndexIsVisible(m_accountList.getItemCount());
        m_accountList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        m_accountList.setToolTipText("Select a billing account and click Select or double click on the account...");
        m_accountList.setOpaque(false);
        m_accountList.setBackground(new Color(0x20ff8f00,true));
        m_accountList.setSelectionBackground(new Color(0x800000ff,true));
        if (m_title!=null) m_title.setText("Select a Customer Billing Account");
        if (m_message!=null) m_message.setText("Please Select one of the following billing account:");

        m_dialog.pack();
        m_accountList.requestFocusInWindow();
        m_dialog.showDialog(this);
        if (m_dialog.isVisible()) m_dialog.closeDlg();
        return selectedAccount;
    }


    public synchronized void cancel() {
        if (wasMouseClicked()) {
            closeDlg();
        }
    }

    public synchronized void selectAccount() {
        if (wasMouseClicked()) {
            selectedAccount = select();
            if (selectedAccount!=null) closeDlg();
        }
    }


    private BillingAccountElement select() {
        try
        {
            if (null != m_accountList.getSelectedValue()) {
                int ind = m_accountList.getSelectedIndex();
                return m_accounts.get(ind);
            }
            return null;
        }
        catch(Exception ex)
        {
            return null;
        }
    }

    private ClassLoader getClassLoader(PosScreen pos) {
        ClassLoader cl = pos.getClassLoader();
        if (cl == null) {
            try {
                cl = Thread.currentThread().getContextClassLoader();
            } catch (Throwable t) {
            }
            if (cl == null) {
                Debug.log("No context classloader available; using class classloader", module);
                try {
                    cl = this.getClass().getClassLoader();
                } catch (Throwable t) {
                    Debug.logError(t, module);
                }
            }
        }
        return cl;
    }

    private void closeDlg() {
        m_dialog.closeDlg();
    }
}
