/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.screen;

import cnf.pos.PosTransaction;
import net.xoetrope.swing.XDialog;
import net.xoetrope.swing.XImageButton;
import net.xoetrope.swing.XList;
import net.xoetrope.xui.XPage;
import net.xoetrope.xui.events.XEventHelper;

import javax.swing.*;
import java.awt.*;
import java.util.*;


@SuppressWarnings("serial")
public class FlipChart extends XPage {

    /**
     * To choose a product in a list of products with the same bar code
     */

    public static final String module = FlipChart.class.getName();
    protected static PosScreen m_pos = null;
    protected XDialog m_dialog = null;
    static protected Hashtable<String, String> m_productsMap = new Hashtable<String, String>();
    static protected ArrayList<String> m_productSorted = new ArrayList<String>();
    protected XList m_productsList = null;
    protected XImageButton m_cancel = null;
    protected XImageButton m_select = null;
    protected DefaultListModel m_listModel = null;
    protected static PosTransaction m_trans = null;
    protected static String m_productIdSelected = null;


    public FlipChart(Map<String, String> saleMap, PosTransaction trans, PosScreen page) {
        m_productsMap.clear();
        m_productsMap.putAll(saleMap);
        m_productSorted.clear();
        for(String v: m_productsMap.values()) m_productSorted.add(v);
        Collections.sort(m_productSorted, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.substring(6).compareToIgnoreCase(o2.substring(6));
            }
        });
        m_trans = trans;
        m_pos = page;
    }

    public String openDlg() {
        XDialog dlg = (XDialog) pageMgr.loadPage(m_pos.getScreenLocation() + "/dialog/FlipChart");
        m_dialog = dlg;
        m_dialog.setCaption("Select a product from the flip chart");
        m_dialog.setHideFrame(true);
        //dlg.setModal(true);
        m_productsList = (XList) dlg.findComponent("productList");
        XEventHelper.addMouseHandler(this, m_productsList, "DoubleClick");

        m_cancel = (XImageButton) dlg.findComponent("BtnCancel");
        m_select = (XImageButton) dlg.findComponent("BtnSelect");

        XEventHelper.addMouseHandler(this, m_cancel, "cancel");
        XEventHelper.addMouseHandler(this, m_select, "selectProduct");
        initButton("All");
        initButton("Abc");
        initButton("Def");
        initButton("Ghi");
        initButton("Jkl");
        initButton("Mno");
        initButton("Pqr");
        initButton("Stu");
        initButton("Vwx");
        initButton("Yz");
        m_listModel = new DefaultListModel();
        setDataModel('a','z');
        m_productsList.setModel(m_listModel);
        //m_productsList.setVisibleRowCount(-1);
        //m_productsList.ensureIndexIsVisible(m_productsList.getItemCount());
        m_productsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        m_productsList.setToolTipText("You may use a double click to replace the current sale and delete this one");
        m_productsList.setOpaque(false);
        m_productsList.setBackground(new Color(0x20ff8f00, true));
        m_productsList.setSelectionBackground(new Color(0x800000ff, true));

        dlg.pack();
        dlg.showDialog(this);
        if (m_dialog.isVisible()) m_dialog.closeDlg();
        return m_productIdSelected;
    }
    private void initButton(String name)
    {
        XImageButton b = (XImageButton) m_dialog.findComponent("Btn"+name);
        XEventHelper.addMouseHandler(this, b, name.toLowerCase());
    }

    public void setDataModel(char s, char e)
    {
        m_listModel.clear();
        for(String v: m_productSorted) {
            char x = v.charAt(6);
            x = Character.toLowerCase(x);
            if (x>=s && x<=e)
                m_listModel.addElement(v);
        }
        m_productsList.setVisibleRowCount(-1);
        m_productsList.ensureIndexIsVisible(m_productsList.getItemCount());
    }

    public synchronized void DoubleClick() {
        if (wasMouseDoubleClicked()) {
            selectProductId();
        }
    }

    public synchronized void cancel() {
        if (wasMouseClicked()) {
            m_productsList.clearSelection();
            m_productIdSelected=null;
            closeDlg();
        }
    }

    public synchronized void selectProduct() {
        if (wasMouseClicked()) {
            selectProductId();
        }
    }

    public synchronized void all(){
        if (wasMouseClicked()) setDataModel('a','z');
    }
    public synchronized void abc(){
        if (wasMouseClicked()) setDataModel('a','c');
    }
    public synchronized void def(){
        if (wasMouseClicked()) setDataModel('d','f');
    }
    public synchronized void ghi(){
        if (wasMouseClicked()) setDataModel('g','i');
    }
    public synchronized void jkl(){
        if (wasMouseClicked()) setDataModel('j','l');
    }
    public synchronized void mno(){
        if (wasMouseClicked()) setDataModel('m','o');
    }
    public synchronized void pqr(){
        if (wasMouseClicked()) setDataModel('p','r');
    }
    public synchronized void stu(){
        if (wasMouseClicked()) setDataModel('s','u');
    }
    public synchronized void vwx(){
        if (wasMouseClicked()) setDataModel('v','x');
    }
    public synchronized void yz(){
        if (wasMouseClicked()) setDataModel('y','z');
    }

    private void selectProductId() {
        if (null != m_productsList.getSelectedValue()) {
            String product = (String) m_productsList.getSelectedValue();
            for (Map.Entry<String, String> entry : m_productsMap.entrySet()) {
                String val = entry.getValue().toString();
                if (val.equals(product)) {
                    m_productIdSelected = entry.getKey().toString();
                }
            }
        }
        closeDlg();
    }

    private void closeDlg() {
        m_dialog.closeDlg();
    }
}
