/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.screen;

import cnf.pos.cart.resources.elements.ReasonElement;
import net.xoetrope.swing.*;
import net.xoetrope.xui.XPage;
import net.xoetrope.xui.events.XEventHelper;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.Locale;


@SuppressWarnings("serial")
public class ReasonSelector extends XPage {

    /**
     * To allow creating or choising a reason for a PAID IN or OUT
     */

    public static final String module = ReasonSelector.class.getName();
    protected XDialog m_dialog = null;
    //protected XComboBox m_reasonCombo = null;
    protected XImageButton m_cancel = null;
    protected XImageButton m_ok = null;
    //protected DefaultComboBoxModel m_comboModel = null;
    protected boolean cancelled = false;
    //protected ResourceManager r = null;
    protected PosScreen m_pos;
    protected List<ReasonElement> reasons;

    protected XList m_reasonList = null;
    protected XTextArea title = null;
    protected XTextArea message = null;
    private String name;
    //private JComboBox mList;

    public ReasonSelector(String name, PosScreen pos, List<ReasonElement> reasons) {
        this.name = name;
        m_pos = pos;
        this.reasons = reasons;
    }

    public ReasonElement openDlg() {
        Locale locale = Locale.getDefault();

        m_dialog = (XDialog) pageMgr.loadPage(m_pos.getScreenLocation() + "/dialog/PriceOverrideReason");

        m_cancel = (XImageButton) m_dialog.findComponent("BtnCancel");
        m_ok = (XImageButton) m_dialog.findComponent("BtnOk");
        title = (XTextArea) m_dialog.findComponent("title");
        message = (XTextArea) m_dialog.findComponent("message");
        XEventHelper.addMouseHandler(this, m_cancel, "cancel");
        XEventHelper.addMouseHandler(this, m_ok, "ok");

        m_dialog.setCaption("Enter " + name + " Reason");
        m_dialog.setHideFrame(true);

        //m_reasonLabel = (XLabel) m_dialog.findComponent("reasonLabel");
        /*m_reasonCombo = (XComboBox) m_dialog.findComponent("priceOverrideReasonCombo");
        m_reasonCombo.setDoubleBuffered(true);
        m_reasonCombo.setToolTipText("Select a reason for the price override!");
        m_reasonCombo.setLightWeightPopupEnabled(false);
        for (Enumeration e:reasons)
            m_reasonCombo.addItem(e.getDescription());
        m_reasonCombo.requestFocusInWindow();*/

        title.setText(title.getText().replace("#",name));
        message.setText(message.getText().replace("#",name));
        m_reasonList = (XList)m_dialog.findComponent("priceOverrideReasonList");
        m_reasonList.setToolTipText("Select a reason for the " + name + "!");
        for (ReasonElement e: reasons)
            m_reasonList.addItem(e.Description);
        m_reasonList.setSelectedIndex(0);
        m_reasonList.requestFocus();
        m_reasonList.setVisibleRowCount(-1);
        m_reasonList.ensureIndexIsVisible(m_reasonList.getItemCount());
        m_reasonList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        m_reasonList.setOpaque(false);
        m_reasonList.setBackground(new Color(0x20ff8f00, true));
        m_reasonList.setSelectionBackground(new Color(0x800000ff, true));


        m_dialog.pack();
        m_dialog.showDialog(this);
        if (m_dialog.isVisible()) m_dialog.closeDlg();
        if (cancelled) {
            return null;
        } else {
            return reasons.get(m_reasonList.getSelectedIndex());
        }
    }

    public synchronized void cancel() {
        if (wasMouseClicked()) {
            cancelled = true;
            m_dialog.closeDlg();
        }
    }

    public synchronized void ok() {
        if (wasMouseClicked()) {
            cancelled=false;
            m_dialog.closeDlg();
        }
    }

}
