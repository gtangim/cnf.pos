/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.screen;


import java.awt.AWTEvent;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.FocusEvent;

import apu.jpos.util.*;
import cnf.jpos.factory.CnfJposServiceFactory;
import cnf.pos.cart.resources.elements.UserElement;
import cnf.pos.event.MenuEvents;
import cnf.pos.util.Utility;
import net.xoetrope.swing.XImage;
import net.xoetrope.swing.XImageButton;
import net.xoetrope.swing.XPanel;
import net.xoetrope.swing.XScrollPane;

import java.util.*;

import net.xoetrope.xui.XPage;
import net.xoetrope.xui.XProject;
import net.xoetrope.xui.XProjectManager;

import org.joda.time.DateTime;
import cnf.pos.PosTransaction;
import cnf.pos.adaptor.KeyboardAdaptor;
import cnf.pos.cart.resources.ResourceManager;
import cnf.pos.cart.resources.SettingsResource;
import cnf.pos.component.InputWithPassword;
import cnf.pos.component.*;
import cnf.pos.component.Output;
import cnf.pos.component.PromoStatusBar;
import cnf.pos.device.DeviceLoader;
import cnf.pos.util.*;
import java.awt.event.FocusListener;
import java.math.BigDecimal;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


import cnf.ui.table.ExtendedModel;
import cnf.ui.table.decoration.TableDecorator;

import cnf.pos.uihelper.*;
import org.joda.time.Duration;

@SuppressWarnings("serial")
public class PosScreen extends XPage implements Runnable, DialogCallback, FocusListener {


    public static final String module = PosScreen.class.getName();
    public static final Frame appFrame = XProjectManager.getCurrentProject().getAppFrame();
    public static final Window appWin = XProjectManager.getCurrentProject().getAppWindow();
    public static final String BUTTON_ACTION_METHOD = "buttonPressed";
    public static long GC_INTERVAL = 60 * 1000;
    public static long MAX_INACTIVITY = 180000;
    //public static int SYNC_NODE_PING_INTERVAL_MS = 5 * 60000;
    public static PosScreen currentScreen;

    protected XProject currentProject = (XProject)XProjectManager.getCurrentProject();
    protected static boolean monitorRunning = false;
    protected static boolean firstInit = false;
    protected static DateTime lastActivity = null;
    protected static DateTime lastGC = new DateTime();
    protected static Thread activityMonitor = null;
    protected static ExtendedModel tm = null;
    protected static TableDecorator decorator = null;
    //protected static JournalHelper jh = null;

    protected ExtendedJournal journal = null;
    protected ClassLoader classLoader = null;
    //protected XuiSession session = null;
    protected Output output = null;
    protected InputWithPassword input = null;
    //protected Operator operator = null;
    protected PosImageButton imagebuttons = null;
    //protected PosButton buttons = null;
    protected String scrLocation = null;
    protected boolean isLocked = false;
    protected boolean isBusy = false;
    protected boolean inDialog = false;
    protected PromoStatusBar promoStatusBar = null;
    protected String currentTab=null;

    protected DiscountUIHelper discountHelper;
    protected OverrideUIHelper overrideHelper;
    protected CustomerUIHelper partyHelper;
    protected PaymentUIHelper paymentHelper;
    protected ProductUIHelper productHelper;
    protected PromoUIHelper promoHelper;
    protected TerminalUIHelper terminalHelper;
    //protected NodeApi node;

    private String lastStat=null;
    private ResourceManager r;
    private SettingsResource settings;


    public synchronized static ExtendedJournal getActiveJournal() {return currentScreen.journal;}
    public synchronized static PosScreen getActiveScreen() {return currentScreen;}
    private Locale defaultLocale = Locale.getDefault();

    protected static ExtendedReentrantLock threadLocker = new ExtendedReentrantLock();

    private static DateTime lastDeadlockDisplay =null;

    public PosScreen() {
        super();
        discountHelper = new DiscountUIHelper();
        overrideHelper = new OverrideUIHelper();
        partyHelper = new CustomerUIHelper();
        paymentHelper = new PaymentUIHelper();
        productHelper = new ProductUIHelper();
        promoHelper = new PromoUIHelper();
        terminalHelper = new TerminalUIHelper();
        this.classLoader = Thread.currentThread().getContextClassLoader();
        this.addFocusListener(this);
    }

    @Override
    public void pageCreated() {
        lockUI();
        try {
            Debug.logInfo(this.getName() + ": Initializing Page....", module);
            super.pageCreated();

            // initial settings
            this.setEnabled(false);
            this.setVisible(false);

            // setup the shared components
            Debug.logInfo(this.getName() + ": Getting Session....", module);
            //this.session = XuiContainer.getSession();
            r = ResourceManager.Resource;
            settings =  r.getSettings();
            //this.node = ((ResourceManager)session.getAttribute("Resource")).getNode();

            Debug.logInfo(this.getName() + ": Initializing IO Containers....", module);
            this.output = new Output(this);
            this.input = new InputWithPassword(this);
            MAX_INACTIVITY = r.getSettings().getAutoLockTerminalMs();
            this.setActivity();


            Debug.logInfo(this.getName() + ": Initializing Journal....", module);
            XScrollPane panel = (XScrollPane)this.findComponent("journal_panel");
            Properties prop = settings.getProperties();
            if (panel!=null)
            {
                panel.setOpaque(true);
                this.journal = new ExtendedJournal(tm,decorator,prop);
                //if (this.jh==null) this.jh = journal.getJournalHelper();
                if (this.tm==null) this.tm=journal.getTable().getTableModel();
                if (this.decorator==null) this.decorator = journal.getTable().getDecorator();
                panel.add(journal.getTable());
            }
            else {this.journal=null;/*this.jh=null;*/this.tm=null;}

            // See if there is a default navigation tab...
            String tab = prop.getProperty("DefaultTab."+this.getName());
            if (tab!=null) setTab(tab);

            if (!firstInit) {
                firstInit = true;

                // pre-load a few screens

                /*ArrayList<String> pages = (ArrayList<String>)session.getAttribute("pages");
                if (pages!=null && pages.size()>0)
                {
                    Debug.logInfo(this.getName() + ": Loading Sister Pages....", module);
                    for(String page:pages)
                    {
                        currentProject.getPageManager().loadPage(page);
                    }
                }*/

                // start the shared monitor thread
                if (activityMonitor == null) {
                    Debug.logInfo(this.getName() + ": Initializing UI Monitor Thread....", module);
                    monitorRunning = true;
                    activityMonitor = new Thread(this);
                    activityMonitor.setDaemon(false);
                    activityMonitor.start();
                }

                // configure the window listener
                Debug.logInfo(this.getName() + ": Attach Keyboard Adaptor....", module);
                KeyboardAdaptor.attachComponents(appWin, false);
                appFrame.addFocusListener(this);
                appWin.addFocusListener(this);

                // close the splash screen
                SplashLoader.close();
            }

            // buttons are different per screen
            this.imagebuttons = new PosImageButton(this);

            // make sure all components have the keyboard set
            Debug.logInfo(this.getName() + ": Keyboard Event Wireup....", module);
            KeyboardAdaptor.attachComponents(this);

        } catch (Exception e) {
            Debug.logError(e,"Unable to Create XUI Page...",module);
        } finally {
            unlockUI();
        }
    }

    @Override
    public void pageActivated() {
        lockUI();
        try {
            super.pageActivated();

            this.journal.getTable().getDecorator().coordToStyle.clear();
            this.journal.getTable().repaint();
            this.setActivity();

            // The input receives keyboard events even when it's page is deactivated, so
            // we'll clear it early to prevent the characters from displaying momentarily
            input.clearInput();

            if (r.getSecurity().getUser() == null) {
                this.setLock(true);
            } else {
                this.setLock(isLocked);
            }

            currentScreen = this;
            this.refresh();
        } catch (Exception e) {
            Debug.logError(e, "Unable to Activate XUI Page...", module);
        } finally {
            unlockUI();
        }
    }

    @Override
    public void pageDeactivated() {
        lockUI();
        try {
            super.pageDeactivated();
        } catch (Exception e) {
            Debug.logError(e, "Unable to Deactivate XUI Page...", module);
        } finally {
            unlockUI();
        }
    }

    public String getCurrentTab() {
        return currentTab;
    }

    String oldImageName=null;
    public void setTrainingDisplay(boolean trainingMode) {
        XImage background = (XImage)this.findComponent("mainBackground");
        if (background!=null)
        {
            String iName = background.getImageName();
            if (oldImageName==null) oldImageName=iName;
            if(trainingMode) iName = oldImageName.replace(".","T.");
            else iName = oldImageName.replace("T.",".");
            background.setImageName(iName);
            journal.setTrainingDisplay(trainingMode);
        }
    }

    public void refresh() {
        lockUI();
        try {
            this.journal.getTable().getDecorator().coordToStyle.clear();
            this.journal.getTable().repaint();
            this.refreshStat();
            PosTransaction trans = PosTransaction.getCurrentTx();
            if (trans!=null)
            {
                trans.refreshOutput(this);
            }

            appFrame.requestFocus();

            if (!isLocked) {
                this.setEnabled(true);
                this.setVisible(true);
                input.clearInput();
            } else {
                output.print("Enter User ID:");
            }
            this.setTab(currentTab);
            this.repaint();


        } catch (Exception e) {
            Debug.logError(e, "Unable to Refresh XUI Page...", module);
        } finally {
            unlockUI();
        }
    }

    public void setMenuLock(boolean lock)
    {
        lockUI();
        try {
            XPanel mainMenuPanel = (XPanel)findComponent("mainMenuPanel");
            if (mainMenuPanel!=null) mainMenuPanel.setVisible(!lock);
            XPanel visibleTab = (XPanel)findComponent("TAB."+currentTab);
            if (visibleTab!=null) visibleTab.setVisible(!lock);
        } catch (Exception e) {
            Debug.logError(e, "Unable set XUI Page Menu lock...", module);
        } finally {
            unlockUI();
        }
    }

    public void setLock(boolean lock) {
        lockUI();
        try {

            setMenuLock(lock);
            this.input.setLock(lock);
            this.output.setLock(lock);
            this.journal.setLock(lock);
            //this.operator.setLock(lock);
            this.isLocked = lock;
            if (lock) {
                this.input.setFunction("LOGIN");
            }
            DeviceLoader.enable(true,!lock,!lock,!lock);
        } catch (IllegalArgumentException e) {
            Debug.logError(e, "Unable set XUI Page Control lock...", module);
        } finally {
            unlockUI();
        }
    }

    public void setBusy(boolean isBusy)
    {
        lockUI();
        try {
            //this.imagebuttons.setLock(lock);
            setMenuLock(isBusy);
            this.input.setLock(isBusy);
            this.output.setLock(isBusy);
            this.journal.setLock(isBusy);
            //this.operator.setLock(lock);
            this.isBusy = isBusy;
            if (isBusy) {
                this.input.clear();
                this.getOutput().print("Please Wait...");
                this.getOutput().setProgress("Please Wait...");
                this.getOutput().setHint("Please Wait...");
            }
            else
            {
                this.input.clear();
                this.output.clear();
            }

            DeviceLoader.enable(true,!isBusy,!isBusy,!isBusy);
        } catch (IllegalArgumentException e) {
            Debug.logError(e, "Unable set XUI Page Control lock...", module);
        } finally {
            unlockUI();
        }
    }


    // generic button XUI event calls into PosButton to lookup the external reference
    public synchronized void buttonPressed() {
        try {
            setActivity();
            imagebuttons.buttonPressed(this, (AWTEvent)this.getCurrentEvent());
            journal.focus();
        } catch (Exception e) {
            Debug.logError(e, "Unable to process Button Press Event!", module);
        }
    }

    // generic page display methods - extends those in XPage
    @Override
    public PosScreen showPage(String pageName) {
        return this.showPage(pageName, true);
    }

    public PosScreen showPage(String pageName, boolean refresh) {
        lockUI();
        try {
            if (pageName.startsWith("/")) {
                pageName = pageName.substring(1);
            }
            String pageAddress = pageName;
            if (!pageName.startsWith(this.getScreenLocation())) pageAddress = this.getScreenLocation() + "/" + pageName;
            XPage newPage = (XPage)currentProject.getPageManager().showPage(pageAddress);
            if (newPage instanceof PosScreen) {
                PosTransaction trans = PosTransaction.getCurrentTx();
                //TODO alberto this code is never executed
                if (PosTransaction.transactionCompleted())
                {
                    currentScreen.getActiveJournal().clear();
                    currentScreen.getActiveJournal().getTable().repaint();
                    currentScreen.getOutput().clear();
                    currentScreen.getOutput().setAmount(BigDecimal.ZERO);
                }
                currentScreen.getOutput().print(null);
                if (refresh) ((PosScreen) newPage).refresh();
                if (pageName.contains("pay") || (currentTab!=null && currentTab.contains("pay"))) currentScreen.getOutput().setHint(trans.getTotalDueMessage());
                return (PosScreen) newPage;
            }
        } catch (Exception e) {
            Debug.logError(e, "Unable to show page!", module);
        } finally {
            unlockUI();
        }
        return null;
    }

    public void showStartupPage()
    {
        lockUI();
        try {
            Properties prop = settings.getProperties();
            String defaultPage = prop.getProperty("StartClass");
            if (Utility.isNotEmpty(defaultPage))
            {
                PosScreen page = showPage(defaultPage);
                String tab = prop.getProperty("DefaultTab."+defaultPage);
                if (tab!=null) page.setTab(tab);
            }
        } catch (Exception e) {
            Debug.logError(e, "Unable to show startup page!", module);
        } finally {
            unlockUI();
        }
    }

    public void setTab(String newTab)
    {
        lockUI();
        try {
            if (Utility.isEmpty(newTab)) return;
            if (Utility.isNotEmpty(currentTab))
            {
                XImageButton button_cur = (XImageButton)findComponent("NAV."+currentTab);
                XPanel panel_cur = (XPanel)findComponent("TAB."+currentTab);
                if (button_cur!=null) button_cur.setEnabled(true);
                if (panel_cur!=null) panel_cur.setVisible(false);
            }
            currentTab = newTab;
            XImageButton button_cur = (XImageButton)findComponent("NAV."+currentTab);
            XPanel panel_cur = (XPanel)findComponent("TAB."+currentTab);
            if (button_cur!=null) button_cur.setEnabled(false);
            if (panel_cur!=null) panel_cur.setVisible(true);
        } catch (Exception e) {
            Debug.logError(e, "Unable to navigate the tabs!", module);
        } finally {
            unlockUI();
        }
    }

    // run method for auto-locking POS on inactivity
    private void postStat(String stat) {
        String saf = ",SAF:0";
        String u = ",U:NA";
        if (CnfJposServiceFactory.IsMonerisInSAFMode) saf = ",SAF:1";
        UserElement user = r.getSecurity().getUser();
        if (user!=null) u = ",U:"+user.UserId;

        if (stat == null) return;
        if (stat.equals("LOGGED_OUT")) output.setProgress("Please log in...");
        else if (stat.equals("LOCKED")) output.setProgress("User " + user.FirstName + " locked this till...");
        else if ("ACTIVE".equals(stat) && !stat.equals(lastStat)) output.setProgress(null);
        stat = stat + saf + u;
        /*if (!stat.equals(lastStat) || lastPing == null || (Utility.elapsedMs(lastPing) > SYNC_NODE_PING_INTERVAL_MS)) {
            Debug.log("POS Status is " + stat);
            lastStat = stat;
            try {
                r.getNode().ping(lastStat);
                lastPing = new DateTime();
            } catch (Exception ex) {
                TerminalUIHelper t = getTerminalHelper();
                if (t != null) t.showError("Communication Error"
                        , "Pos encountered an internal communication problem and will not be able to\r\n"
                        + " finish this transaction. Please contact IT immediately. ");
            }

        }*/
    }

    //private DateTime lastPing=null;
    public void run() {
        long rep=0;
        String lastStat=null;
        while (monitorRunning) {
            rep++;
            if (tryLockUI(2000))
            {
                if (r.getSecurity().getUser()==null) postStat("LOGGED_OUT");
                else if (isLocked) postStat("LOCKED");
                else postStat("ACTIVE");


                lastDeadlockDisplay = new DateTime();
                try {
                    DateTime now = new DateTime();
                    if (!isLocked && (Utility.elapsedMs(lastActivity)) > MAX_INACTIVITY) {
                        Debug.log("POS terminal auto-lock activated", module);
                        if (PosScreen.currentScreen!=null)
                        {
                            PosScreen.currentScreen.showStartupPage();
                            PosScreen.currentScreen.setLock(true);
                        }
                    }
                    if (Utility.elapsedMs(lastGC)>GC_INTERVAL && Utility.elapsedMs(lastActivity)>GC_INTERVAL)
                    {
                        lastGC = now;
                        Debug.log("Performing Garbage Collection!");
                        System.gc();
                    }
                    if (PosScreen.currentScreen!=null)
                    {
                        PosScreen.currentScreen.output.refresh();
                        PosScreen.currentScreen.refreshStat();
                    }
                    //if (rep%30==0) settings.postPingToDb();
                    //if (!r.getSettings().checkTime())
                    //    PosScreen.currentScreen.getTerminalHelper().showError("System Time Error", "System time appears to have changed erratically!");
                } catch (Exception e) {
                    Debug.logError(e, "Unable to perform background refresh!", module);
                } finally {
                    unlockUI();
                }
            }
            //else r.getSettings().checkTime();
            try {
                Thread.sleep(2000);
                //DeviceLoader.scanner.injectData("S4T1X10140");
            } catch (InterruptedException e) {
                Debug.logError(e, module);
            }
        }
    }










    // PosDialog Callback method
    public void receiveDialogCb(PosDialog dialog) {
        Debug.log("Dialog closed; refreshing screen", module);
        this.refresh();
    }

    public void refreshStat()
    {
        try
        {
            PosTransaction trans = PosTransaction.getCurrentTx();
            if (trans!=null) {
            	String stat = trans.getStat();
                String stat2 = this.getOutput().getStat();
            	if (!stat.equals(stat2))
                    this.getOutput().setStat(stat);
            }
        }
        catch(Exception ex)
        {
        }
    }

    public boolean isLocked() {
        return isLocked;
    }

    //public XuiSession getSession() {
    //    return this.session;
    //}

    public ResourceManager getResource()
    {
        return r;
    }

    public InputWithPassword getInput() {
        return this.input;
    }

    public Output getOutput() {
        return this.output;
    }


    public ExtendedJournal getJournal() {
        return this.journal;
    }

    public PosImageButton getButtons() {
        return this.imagebuttons;
    }

    public PromoStatusBar getPromoStatusBar() {
        return this.promoStatusBar;
    }

    public void setActivity()
    {
        lastActivity = new DateTime();
    }

    public ClassLoader getClassLoader() {
        return classLoader;
    }

    public void focusGained(FocusEvent event) {
        /*if (Debug.verboseOn()) {
            String from = event != null && event.getOppositeComponent() != null ? event.getOppositeComponent().getName() : "??";
            Debug.log(event.getSource() + " focus gained from " + from, module);
        }*/
    }

    public void focusLost(FocusEvent event) {
        /*if (Debug.verboseOn()) {
            String to = event != null && event.getOppositeComponent() != null ? event.getOppositeComponent().getName() : "??";
            Debug.log(event.getSource() + " focus lost to " + to, module);
        }*/
    }

    public String getScreenLocation() {
        if (this.scrLocation == null) {
            synchronized(this) {
                if (this.scrLocation == null) {
                    Properties prop = settings.getProperties();
                    String startClass = prop.getProperty("StartClass");
                    this.scrLocation = startClass.substring(0, startClass.lastIndexOf("/"));
                }
            }
        }
        return this.scrLocation;
    }

    public void setWaitCursor() {
        setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
    }
    public void setNormalCursor() {
        setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
    }


    public static void lockUI()
    {
        // ****** Enable the following only when you want to debug thread issues *************
        //Debug.logInfo("******************  ENTERING LOCK: "+Thread.currentThread().getName()+ " **************",module);
        //StackTraceElement[] trace = Thread.currentThread().getStackTrace();
        //for(StackTraceElement ste:trace)
        //    Debug.logInfo(">>>>>>>>>>>>>>>>>>           "+ ste.toString(),module);

        threadLocker.lock();
    }

    public static void unlockUI()
    {
        // ****** Enable the following only when you want to debug thread issues *************
        //if (((ReentrantLock)threadLocker).getHoldCount()<2)
        //    Debug.logInfo("******************  EXITING LOCK: "+Thread.currentThread().getName()+ " **************",module);
        threadLocker.unlock();
    }

    public static boolean tryLockUI(int timeout)
    {
        try {
            if (threadLocker.tryLock((long)timeout, TimeUnit.MILLISECONDS)) return true;
            else
            {
                boolean elapsed = true;

                if (lastDeadlockDisplay !=null)
                {
                    elapsed=false;
                    DateTime now = new DateTime();
                    Duration d = new Duration(lastDeadlockDisplay,now);
                    if (d.getMillis()>300000)
                    {
                        elapsed=true;
                    }

                }
                if (elapsed)
                {
                    Debug.logInfo("******** UNABLE TO REFRESH (POSSIBLE DEADLOCK) ************\r\n"
                        + threadLocker.getStackTraceOwner(),module);
                    if (threadLocker.blockerContains("awt.WaitDispatchSupport"))
                        threadLocker.shutdownDeadlockThread();
                    lastDeadlockDisplay =new DateTime();
                }
                return false;
            }
        } catch (InterruptedException e) {
            return false;
        }
    }


    public ProductUIHelper getProductHelper() {
        return productHelper;
    }

    public TerminalUIHelper getTerminalHelper() {
        return terminalHelper;
    }

    public DiscountUIHelper getDiscountHelper() {
        return discountHelper;
    }

    public OverrideUIHelper getOverrideHelper() {
        return overrideHelper;
    }

    public CustomerUIHelper getCustomerHelper() {
        return partyHelper;
    }

    public PaymentUIHelper getPaymentHelper() {
        return paymentHelper;
    }

    public PromoUIHelper getPromoHelper() {
        return promoHelper;
    }

    public SettingsResource getSettings() {
        return settings;
    }
}
