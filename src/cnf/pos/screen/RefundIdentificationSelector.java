/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.screen;

import net.xoetrope.swing.XDialog;
import net.xoetrope.swing.XImageButton;
import net.xoetrope.swing.XList;
import net.xoetrope.xui.XPage;
import net.xoetrope.xui.events.XEventHelper;

import javax.swing.*;
import java.awt.*;
import java.util.Locale;


@SuppressWarnings("serial")
public class RefundIdentificationSelector extends XPage {

    public static final int ID_CANCELLED = -1;
    public static final int ID_RECEIPT = 0;
    public static final int ID_LICENSE = 1;
    public static final int ID_CUSTOMER = 2;

    public static final String module = RefundIdentificationSelector.class.getName();
    protected XDialog m_dialog = null;
    //protected XComboBox m_reasonCombo = null;
    protected XImageButton m_cancel = null;
    protected XImageButton m_ok = null;
    //protected DefaultComboBoxModel m_comboModel = null;
    protected boolean cancelled = false;
    protected PosScreen m_pos;

    protected XList m_identificationList = null;

    private JComboBox mList;

    public RefundIdentificationSelector(PosScreen pos) {
        m_pos = pos;
    }

    public int openDlg() {
        Locale locale = Locale.getDefault();

        m_dialog = (XDialog) pageMgr.loadPage(m_pos.getScreenLocation() + "/dialog/RefundSelector");

        m_cancel = (XImageButton) m_dialog.findComponent("BtnCancel");
        m_ok = (XImageButton) m_dialog.findComponent("BtnOk");
        XEventHelper.addMouseHandler(this, m_cancel, "cancel");
        XEventHelper.addMouseHandler(this, m_ok, "ok");

        m_dialog.setCaption("Enter a Refund Identification Type");
        m_dialog.setHideFrame(true);

        //m_reasonLabel = (XLabel) m_dialog.findComponent("reasonLabel");
        /*m_reasonCombo = (XComboBox) m_dialog.findComponent("priceOverrideReasonCombo");
        m_reasonCombo.setDoubleBuffered(true);
        m_reasonCombo.setToolTipText("Select a reason for the price override!");
        m_reasonCombo.setLightWeightPopupEnabled(false);
        for (Enumeration e:reasons)
            m_reasonCombo.addItem(e.getDescription());
        m_reasonCombo.requestFocusInWindow();*/

        m_identificationList = (XList)m_dialog.findComponent("refundIdentificationList");
        m_identificationList.setToolTipText("Select an identification type provided by the customer!");
        m_identificationList.addItem("Sales Receipt Number (OrderId)");
        m_identificationList.addItem("Drivers License Number");
        m_identificationList.addItem("Customer Identification Number");
        m_identificationList.setSelectedIndex(0);
        m_identificationList.requestFocus();
        m_identificationList.setVisibleRowCount(-1);
        m_identificationList.ensureIndexIsVisible(m_identificationList.getItemCount());
        m_identificationList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        m_identificationList.setOpaque(false);
        m_identificationList.setBackground(new Color(0x20ff8f00, true));
        m_identificationList.setSelectionBackground(new Color(0x800000ff, true));


        m_dialog.pack();
        m_dialog.showDialog(this);
        if (m_dialog.isVisible())
        {
            if (m_dialog.isShowing()) cancelled=true;
            m_dialog.closeDlg();
        }
        if (cancelled) {
            return -1;
        } else {
            return m_identificationList.getSelectedIndex();
        }
    }

    public synchronized void cancel() {
        if (wasMouseClicked()) {
            cancelled = true;
            m_dialog.closeDlg();
        }
    }

    public synchronized void ok() {
        if (wasMouseClicked()) {
            cancelled=false;
            m_dialog.closeDlg();
        }
    }

}
