package cnf.pos.screen;

import net.xoetrope.swing.XApplet;

import javax.swing.*;

/**
 * Created by russela on 4/21/2015.
 */
public class XuiScreen extends XApplet {
    protected String startupProperties = "";

    public XuiScreen(String[] args, JFrame frame) {
        super(args, frame);
        if (args.length > 0) {
            startupProperties = args[0];
        }

        frame.setVisible(true);
        frame.getContentPane().add(this);
        frame.validate();
    }
}
