/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.screen;


import javolution.util.FastMap;
import net.xoetrope.swing.*;
import net.xoetrope.xui.XPage;
import net.xoetrope.xui.events.XEventHelper;
import cnf.pos.util.Debug;
import cnf.pos.PosCardSettlementInfo;
import cnf.pos.cart.resources.ResourceManager;
import cnf.pos.util.Utility;

import java.awt.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Locale;
import java.util.Map;


@SuppressWarnings("serial")
public class Settlement extends XPage {

    /**
     * To allow creating or choising a reason for a PAID IN or OUT
     */

    public static final String module = Settlement.class.getName();
    public static BigDecimal maxLimit = new BigDecimal("100000.00");

    protected static PosScreen m_pos = null;
    protected XDialog m_dialog = null;


    protected XEdit m_cash = null;
    protected XEdit m_giftCard = null;
    protected XEdit m_giftCert = null;
    protected XEdit m_moneyOrder = null;
    protected XEdit m_cheque = null;
    protected XEdit m_vendorCoupon = null;
    protected XEdit m_billingAccount = null;
    protected XEdit m_other = null;
    protected XEdit m_debit = null;
    protected XEdit m_visa = null;
    protected XEdit m_master = null;
    protected XEdit m_amex = null;
    protected XEdit m_jcb = null;
    protected XEdit m_discover = null;
    protected XEdit m_otherCC = null;
    protected XTextArea m_message = null;

    protected XImageButton m_cancel = null;
    protected XImageButton m_ok = null;



    protected ResourceManager resource;
    protected PosCardSettlementInfo cardInfo;
    protected boolean cancelled = false;


    public Settlement(ResourceManager resource, PosScreen page, PosCardSettlementInfo cardInfo) {
        //m_trans = trans;
        this.resource=resource;
        this.cardInfo = cardInfo;
        m_pos = page;
    }

    public Map<String, BigDecimal> openDlg() {
        m_dialog = (XDialog) pageMgr.loadPage(m_pos.getScreenLocation() + "/dialog/Settlement");
        m_dialog.setHideFrame(true);

        m_cash = (XEdit) m_dialog.findComponent("cash");
        m_giftCard = (XEdit) m_dialog.findComponent("giftCard");
        m_giftCert = (XEdit) m_dialog.findComponent("giftCert");
        m_moneyOrder = (XEdit) m_dialog.findComponent("moneyOrder");
        m_cheque = (XEdit) m_dialog.findComponent("cheque");
        m_vendorCoupon = (XEdit) m_dialog.findComponent("vendorCoupon");
        m_billingAccount = (XEdit) m_dialog.findComponent("billingAccount");
        m_other = (XEdit) m_dialog.findComponent("other");
        m_debit = (XEdit) m_dialog.findComponent("debit");
        m_visa = (XEdit) m_dialog.findComponent("visa");
        m_master = (XEdit) m_dialog.findComponent("master");
        m_amex = (XEdit) m_dialog.findComponent("amex");
        m_jcb = (XEdit) m_dialog.findComponent("jcb");
        m_discover = (XEdit) m_dialog.findComponent("discover");
        m_otherCC = (XEdit) m_dialog.findComponent("otherCC");
        m_message = (XTextArea) m_dialog.findComponent("message");
        m_message.setLineWrap(true);
        m_message.setWrapStyleWord(true);

        m_cancel = (XImageButton) m_dialog.findComponent("cancelBtn");
        m_ok = (XImageButton) m_dialog.findComponent("okBtn");
        Locale locale = Locale.getDefault();

        XEventHelper.addMouseHandler(this, m_cancel, "cancel");
        XEventHelper.addMouseHandler(this, m_ok, "verify");
        XEventHelper.addMouseHandler(this, m_cash, "editCash");
        XEventHelper.addMouseHandler(this, m_giftCard, "editGiftCard");
        XEventHelper.addMouseHandler(this, m_giftCert, "editGiftCert");
        XEventHelper.addMouseHandler(this, m_moneyOrder, "editMoneyOrder");
        XEventHelper.addMouseHandler(this, m_cheque, "editCheque");
        XEventHelper.addMouseHandler(this, m_vendorCoupon, "editVendorCoupon");
        XEventHelper.addMouseHandler(this, m_billingAccount, "editBillingAccount");
        XEventHelper.addMouseHandler(this, m_other, "editOther");
        XEventHelper.addMouseHandler(this, m_debit, "editDebit");
        XEventHelper.addMouseHandler(this, m_visa, "editVisa");
        XEventHelper.addMouseHandler(this, m_master, "editMaster");
        XEventHelper.addMouseHandler(this, m_amex, "editAmex");
        XEventHelper.addMouseHandler(this, m_jcb, "editJcb");
        XEventHelper.addMouseHandler(this, m_discover, "editDiscover");
        XEventHelper.addMouseHandler(this, m_otherCC, "editOtherCC");

        m_cash.setText("0.00");
        m_giftCert.setText("0.00");
        m_giftCard.setText("0.00");
        m_moneyOrder.setText("0.00");
        m_cheque.setText("0.00");
        m_vendorCoupon.setText("0.00");
        m_billingAccount.setText("0.00");
        m_other.setText("0.00");
        m_debit.setText("0.00");
        m_visa.setText("0.00");
        m_master.setText("0.00");
        m_amex.setText("0.00");
        m_jcb.setText("0.00");
        m_discover.setText("0.00");
        m_otherCC.setText("0.00");

        if (cardInfo!=null)
        {
            m_message.setText(cardInfo.getResult()+" - " + cardInfo.getResultText());
            m_debit.setText(cardInfo.getPaymentTotal("DEBIT_CARD").setScale(2, RoundingMode.HALF_EVEN).toString());
            m_visa.setText(cardInfo.getPaymentTotal("VISA_CARD").setScale(2, RoundingMode.HALF_EVEN).toString());
            m_master.setText(cardInfo.getPaymentTotal("MASTER_CARD").setScale(2, RoundingMode.HALF_EVEN).toString());
            m_amex.setText(cardInfo.getPaymentTotal("AMEX_CARD").setScale(2, RoundingMode.HALF_EVEN).toString());
            m_jcb.setText(cardInfo.getPaymentTotal("JCB_CARD").setScale(2, RoundingMode.HALF_EVEN).toString());
            m_discover.setText(cardInfo.getPaymentTotal("DISCOVER_CARD").setScale(2, RoundingMode.HALF_EVEN).toString());
        }

        if (!m_message.getText().toLowerCase().contains("error"))
            m_message.setForeground(new Color(20,80,20));

        m_dialog.pack();
        m_dialog.showDialog(this);
        if (m_dialog.isVisible()) m_dialog.closeDlg();
        if (cancelled) {
            return null;
        } else {
            Map<String,BigDecimal> ret = FastMap.newInstance();
            ret.put("cash",getAmount(m_cash));
            ret.put("giftCard",getAmount(m_giftCard));
            ret.put("giftCert",getAmount(m_giftCert));
            ret.put("moneyOrder",getAmount(m_moneyOrder));
            ret.put("cheque",getAmount(m_cheque));
            ret.put("vendorCoupon",getAmount(m_vendorCoupon));
            ret.put("billingAccount",getAmount(m_billingAccount));
            ret.put("other",getAmount(m_other));
            ret.put("debit",getAmount(m_debit));
            ret.put("visa",getAmount(m_visa));
            ret.put("master",getAmount(m_master));
            ret.put("amex",getAmount(m_amex));
            ret.put("jcb",getAmount(m_jcb));
            ret.put("discover",getAmount(m_discover));
            ret.put("otherCC",getAmount(m_otherCC));

            return ret;
            }
    }

    private BigDecimal getAmount(XEdit e)
    {
        try
        {
            String amt = e.getText();
            if (amt==null || amt.trim().equals(""))  return null;
            return new BigDecimal(amt);
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public synchronized void cancel() {
        if (wasMouseClicked()) {
            cancelled = true;
            m_dialog.closeDlg();
        }
    }


    private void check(String item,XEdit itemEdit) throws Exception
    {
        BigDecimal amt = getAmount(itemEdit);
        if (amt==null) throw new Exception("Invalid amount entered for "+item);
        else if (amt.abs().compareTo(maxLimit)>0)
            throw new Exception("Out of range amount entered for "+item);
    }


    public synchronized void verify() {
        if (wasMouseClicked()) {
            try
            {
                check("Cash",m_cash);
                check("Gift Card.",m_giftCard);
                check("Gift Cert.",m_giftCert);
                check("Money Order",m_moneyOrder);
                check("Cheque",m_cheque);
                check("Vendor Coupon",m_vendorCoupon);
                check("Billing Account",m_billingAccount);
                check("Other",m_other);
                check("Debit Card",m_debit);
                check("Visa Card",m_visa);
                check("Master Card",m_master);
                check("Amex Card",m_amex);
                check("JCB Card",m_jcb);
                check("Discover Card",m_discover);
                check("Other Cr Card",m_otherCC);
                m_dialog.closeDlg();
            }
            catch (Exception ex)
            {
                Debug.logError(ex,module);
                m_pos.getTerminalHelper().showError("Error: Settlement","Unable to verify settlement amounts!",ex);
            }
        }
    }


    private String formatAmount(String amt)
    {
        try
        {
            if (Utility.isEmpty(amt)) return null;
            BigDecimal amount = new BigDecimal(amt);
            if (amount.abs().compareTo(maxLimit)>0) return null;
            return amount.setScale(2,BigDecimal.ROUND_HALF_DOWN).toString();
        }
        catch (Exception ex)
        {
            return null;
        }

    }


    private void inp(String itemName, XEdit itemEdit)
    {
        if (wasMouseClicked())
        {
            String amt = formatAmount(m_pos.getTerminalHelper().getNumpadInput2("",
                    "Please declare dollar amount for " + itemName+":",false));

            if (amt==null)
            {
                m_pos.getTerminalHelper().showError("Error: Settlement Declare"
                        ,"Invalid value entered for declared "+itemName);
                return;
            }
            else if (("Cash".equals(itemName) || "Gift Certificate".equals(itemName)
                    || "Gift Card".equals(itemName) || "Money Order".equals(itemName) || "Cheque".equals(itemName)
                    || "Vendor Coupon".equals(itemName)) && amt.startsWith("-"))
            {
                m_pos.getTerminalHelper().showError("Error: Settlement Declare"
                        ,"Invalid value entered for declared "+itemName);
                return;
            }

            itemEdit.setText(amt);
            m_dialog.repaint();
        }

    }

    public synchronized void editCash() {
        inp("Cash",m_cash);
    }

    public synchronized void editGiftCard() {
        inp("Gift Card",m_giftCard);
    }

    public synchronized void editGiftCert() {
        inp("Gift Certificate",m_giftCert);
    }

    public synchronized void editMoneyOrder() {
        inp("Money Order",m_moneyOrder);
    }

    public synchronized void editCheque() {
        inp("Cheque",m_cheque);
    }

    public synchronized void editVendorCoupon() {
        inp("Vendor Coupon",m_vendorCoupon);
    }

    public synchronized void editBillingAccount() {
        inp("Billing Account",m_billingAccount);
    }

    public synchronized void editOther() {
        inp("Other Tender",m_other);
    }

    public synchronized void editDebit() {
        inp("Debit Card",m_debit);
    }

    public synchronized void editVisa() {
        inp("Visa Card",m_visa);
    }

    public synchronized void editMaster() {
        inp("Master Card",m_master);
    }

    public synchronized void editAmex() {
        inp("American Express Card",m_amex);
    }

    public synchronized void editJcb() {
        inp("JCB Card",m_jcb);
    }

    public synchronized void editDiscover() {
        inp("Discover Card",m_discover);
    }

    public synchronized void editOtherCC() {
        inp("Other Credit Card",m_otherCC);
    }




}
