/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.screen;


import cnf.pos.cart.resources.elements.ReasonElement;
import javolution.util.FastList;
import net.xoetrope.swing.*;
import net.xoetrope.xui.XPage;
import net.xoetrope.xui.events.XEventHelper;
import cnf.pos.util.*;
import cnf.pos.cart.resources.ResourceManager;

import javax.swing.*;
import java.awt.*;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@SuppressWarnings("serial")
public class PaidInOut extends XPage {

    /**
     * To allow creating or choising a reason for a PAID IN or OUT
     */

    public static final String module = PaidInOut.class.getName();
    protected static PosScreen m_pos = null;
    protected XDialog m_dialog = null;
    protected XLabel m_amoutLabel = null;
    protected XEdit m_amountEdit = null;
    protected XLabel m_reasonLabel = null;
    //protected XComboBox m_reasonCombo = null;
    protected XList m_reasonList = null;
    protected XLabel m_reasonCommentLabel = null;
    protected XEdit m_reasonCommentEdit = null;
    protected XTextArea m_title = null;
    protected XTextArea m_message = null;
    protected XImageButton m_cancel = null;
    protected XImageButton m_ok = null;
    protected DefaultComboBoxModel m_comboModel = null;
    //protected static PosTransaction m_trans = null;
    protected ResourceManager resource;
    protected String m_type = null;
    protected boolean cancelled = false;

    //TODO : make getter and setter for members (ie m_*) if needed (extern calls). For that in Eclipse use Source/Generate Getters and setters

    public PaidInOut(ResourceManager resource, PosScreen page, String type) {
        //m_trans = trans;
        this.resource=resource;
        m_pos = page;
        m_type = type;
    }

    public Map<String, String> openDlg() {
        m_dialog = (XDialog) pageMgr.loadPage(m_pos.getScreenLocation() + "/dialog/PaidInOut");
        m_dialog.setHideFrame(true);
        m_amoutLabel = (XLabel) m_dialog.findComponent("amoutLabel");
        m_amountEdit = (XEdit) m_dialog.findComponent("amountEdit");

        m_reasonLabel = (XLabel) m_dialog.findComponent("reasonLabel");

        m_reasonCommentLabel = (XLabel) m_dialog.findComponent("reasonCommentLabel");
        m_reasonCommentEdit = (XEdit) m_dialog.findComponent("reasonCommentEdit");

        m_title = (XTextArea)m_dialog.findComponent("title");
        m_message = (XTextArea)m_dialog.findComponent("message");

        m_cancel = (XImageButton) m_dialog.findComponent("BtnCancel");
        m_ok = (XImageButton) m_dialog.findComponent("BtnOk");
        Locale locale = Locale.getDefault();

        XEventHelper.addMouseHandler(this, m_cancel, "cancel");
        XEventHelper.addMouseHandler(this, m_ok, "verify");
        XEventHelper.addMouseHandler(this, m_amountEdit, "editAmount");
        XEventHelper.addMouseHandler(this, m_reasonCommentEdit, "editComment");

        List<ReasonElement> posPaidReasons = FastList.newInstance();
        if (m_type.equals("IN")) {
            m_dialog.setCaption("Enter amount to paid in and create or choose a reason");
            if (m_title!=null) m_title.setText("PAY IN: Pay cash into the till");
            posPaidReasons=resource.getSettings().getPaidInReasons();

        } else { // OUT
            m_dialog.setCaption("Enter amount to withdraw and create or choose a reason");
            if (m_title!=null) m_title.setText("PAY OUT: Pay cash from the till");
            posPaidReasons=resource.getSettings().getPaidOutReasons();
        }
        if (m_message!=null) m_message.setText("Please fill out the following form to pay cash "
                + m_type.toLowerCase()+":");

        /*m_reasonCombo = (XComboBox) m_dialog.findComponent("reasonCombo");
        m_comboModel = new DefaultComboBoxModel();
        for (Enumeration reason : posPaidReasons) {
            m_comboModel.addElement(reason.getDescription());
        }
        m_reasonCombo.setModel(m_comboModel);
        m_reasonCombo.setToolTipText(UtilProperties.getMessage(PosTransaction.stringResource, "PosCreateOrChooseReasonInOut", locale));
        */

        m_reasonList = (XList)m_dialog.findComponent("payReasonList");
        m_reasonList.setToolTipText("You have to create or choose a reason");
        for (ReasonElement e:posPaidReasons)
            m_reasonList.addItem(e.Description);
        m_reasonList.setSelectedIndex(0);
        m_reasonList.requestFocus();
        m_reasonList.setVisibleRowCount(-1);
        m_reasonList.ensureIndexIsVisible(m_reasonList.getItemCount());
        m_reasonList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        m_reasonList.setOpaque(false);
        m_reasonList.setBackground(new Color(0x20ff8f00, true));
        m_reasonList.setSelectionBackground(new Color(0x800000ff, true));




        m_dialog.pack();
        //m_trans.popDrawer();
        m_dialog.showDialog(this);
        if (m_dialog.isVisible()) m_dialog.closeDlg();
        if (cancelled) {
            return new HashMap<String, String>();
        } else {
            return Utility.toMap("amount", m_amountEdit.getText(),
                    "reason", (String) (posPaidReasons.get(m_reasonList.getSelectedIndex())).ReasonId,
                    "reasonText", (String) (posPaidReasons.get(m_reasonList.getSelectedIndex())).Description,
                    "reasonComment", (String) m_reasonCommentEdit.getText());
            }
    }

    public synchronized void cancel() {
        if (wasMouseClicked()) {
            cancelled = true;
            m_dialog.closeDlg();
        }
    }

    private boolean isValidAmount()
    {
        try
        {
            BigDecimal amt = new BigDecimal(m_amountEdit.getText());
            if (amt.compareTo(BigDecimal.ZERO)>0) return true;
            else return false;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public synchronized void verify() {
        if (wasMouseClicked()) {
            try
            {
                if (!isValidAmount())
                    throw new Exception("User entered an invalid amount: " + m_amountEdit.getText());
                String reason = (String) m_reasonList.getSelectedObject();
                if (Utility.isEmpty(reason)) throw new Exception("User must select a reason!");
                m_dialog.closeDlg();
            }
            catch (Exception ex)
            {
                Debug.logError(ex,module);
                m_pos.getTerminalHelper().showError("Error: Pay IN/OUT","Unable to validate pay in/out info!",ex);
            }
        }
    }

    public synchronized void editAmount() {
        if (wasMouseClicked())
        {
            m_amountEdit.setText(m_pos.getTerminalHelper().getNumpadInput(m_amountEdit.getText(),
                    "Please enter amount to pay "+m_type+":",false
            ));
            m_dialog.repaint();
        }
    }
    public synchronized void editComment() {
        if (wasMouseClicked())
        {
            m_reasonCommentEdit.setText(m_pos.getTerminalHelper().getKeyboardInput(m_reasonCommentEdit.getText(),
                    "Please enter a comment for the amount to be paid " + m_type + ":", false
            ));
            m_dialog.repaint();
        }
    }
}
