/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.screen;

import net.xoetrope.swing.*;
import net.xoetrope.xui.XPage;
import net.xoetrope.xui.events.XEventHelper;

import cnf.pos.util.Debug;
import cnf.pos.PosTransaction;

// importing DeviceLoader for pop.drawer
import cnf.pos.util.Utility;

@SuppressWarnings("serial")
public class CommonDialog extends XPage {

    public static final String module = SaveSale.class.getName();
    protected static PosScreen m_pos = null;
    protected XDialog m_dialog = null;
    protected XImageButton m_cancelButton = null;
    protected XImageButton m_okButton = null;
    protected XImageButton m_retryButton = null;
    protected XImageButton m_ignoreButton = null;
    protected XImageButton m_prnButton = null;
    protected XImageButton m_emlButton = null;
    protected XImageButton m_naButton = null;
    protected XTextArea m_title = null;
    protected XTextArea m_message = null;
    protected XTextArea m_info = null;
    protected XTextArea m_confirm = null;
    protected String pageName = null;
    protected String res;

    public CommonDialog(PosScreen pos, String pageName) {
        PosTransaction trans = PosTransaction.getCurrentTx();
        m_pos = pos;
        if (pageName.startsWith("/")) {
            pageName = pageName.substring(1);
        }
        this.pageName=pageName;
    }

    public String openDlg(String title, String message, String info, String confirm) {
        m_dialog = (XDialog) pageMgr.loadPage(m_pos.getScreenLocation() + "/" + pageName);
        m_dialog.setHideFrame(true);

        m_cancelButton = (XImageButton) m_dialog.findComponent("cancelBtn");
        m_okButton = (XImageButton) m_dialog.findComponent("okBtn");
        m_retryButton = (XImageButton) m_dialog.findComponent("retryBtn");
        m_ignoreButton = (XImageButton) m_dialog.findComponent("ignoreBtn");
        m_prnButton = (XImageButton) m_dialog.findComponent("prnBtn");
        m_emlButton = (XImageButton) m_dialog.findComponent("emlBtn");
        m_naButton = (XImageButton) m_dialog.findComponent("naBtn");

        m_title = (XTextArea)m_dialog.findComponent("title");
        m_message = (XTextArea)m_dialog.findComponent("message");
        m_info = (XTextArea)m_dialog.findComponent("info");
        m_confirm = (XTextArea)m_dialog.findComponent("confirm");

        if (m_message!=null) { m_message.setLineWrap(true); m_message.setWrapStyleWord(true); }
        if (m_info!=null) { m_info.setLineWrap(true); m_info.setWrapStyleWord(true); }
        if (m_confirm!=null) { m_confirm.setLineWrap(true); m_confirm.setWrapStyleWord(true); }

        if (m_cancelButton!=null) XEventHelper.addMouseHandler(this, m_cancelButton, "cancel");
        if (m_okButton!=null) XEventHelper.addMouseHandler(this, m_okButton, "ok");
        if (m_retryButton!=null) XEventHelper.addMouseHandler(this, m_retryButton, "retry");
        if (m_ignoreButton!=null) XEventHelper.addMouseHandler(this, m_ignoreButton, "ignore");
        if (m_prnButton!=null) XEventHelper.addMouseHandler(this, m_prnButton, "prn");
        if (m_emlButton!=null) XEventHelper.addMouseHandler(this, m_emlButton, "eml");
        if (m_naButton!=null) XEventHelper.addMouseHandler(this, m_naButton, "na");
        if (title==null) title="";
        if (message==null) message="";
        if (info==null) info="";
        if (confirm==null) confirm="";

        setTitle(title);
        setMessage(message);
        setInfo(info);
        setConfirm(confirm);


        res="";
        m_dialog.pack();
        /*Debug.logInfo("***** Showing Dialog ********", module);
        Debug.logInfo("Name: " + this.pageName, module);
        Debug.logInfo("title: " + this.m_title.getText(), module);
        Debug.logInfo("OK: " + (this.m_okButton == null ? "NULL" : m_okButton.getText()), module);
        Debug.logInfo("CANCEL: " + (this.m_cancelButton == null ? "NULL" : m_cancelButton.getText()), module);
        Debug.logInfo("RETRY: " + (this.m_retryButton == null ? "NULL" : m_retryButton.getText()), module);
        Debug.logInfo("IGNORE: " + (this.m_ignoreButton==null?"NULL":m_ignoreButton.getText()),module);    */

        this.m_dialog.setModal(true);
        this.m_dialog.setVisible(true);

        try {
            this.m_dialog.showDialog(this);
            if (m_dialog.isVisible()) m_dialog.closeDlg();
        } catch (Exception e) {
            Debug.logError(e,module);
        }
        
        if (this.m_dialog.isShowing()) 
        {
            if (Utility.isEmpty(res)) res="cancel";
            ensureClose("cancel");
        }

        return res;
    }

    public String getResult()
    {
        return res;
    }

    public void logState()
    {
        if (this.m_dialog!=null)
        {
            Debug.logInfo("Showing="+Boolean.toString(m_dialog.isShowing()),module);
            Debug.logInfo("Visible="+Boolean.toString(m_dialog.isVisible()),module);
            Debug.logInfo("Enabled="+Boolean.toString(m_dialog.isEnabled()),module);
        }
    }

    public void ensureClose(String closeAction)
    {
        //logState();
        while (this.m_dialog.isVisible() || this.m_dialog.isShowing() || this.m_dialog.hasFocus())
        {
            //Debug.logInfo("***** Ending Dialog: " + res + " ********",module);
            this.m_dialog.closeDlg();
            this.m_dialog.setVisible(false);
            this.m_dialog.setModal(false);
            //logState();
        }
    }

    public void prn()
    {
        if (wasMouseClicked()) {
            res="prn";
            ensureClose(res);
        }
    }
    public void eml()
    {
        if (wasMouseClicked()) {
            res="eml";
            ensureClose(res);
        }
    }
    public void na()
    {
        if (wasMouseClicked()) {
            res="na";
            ensureClose(res);
        }
    }

    public void cancel()
    {
        if (wasMouseClicked()) {
            res="cancel";
            ensureClose(res);
        }
    }
    public void ok()
    {
        if (wasMouseClicked()) {
            res="ok";
            ensureClose(res);
        }
    }
    public void retry()
    {
        if (wasMouseClicked()) {
            res="retry";
            ensureClose(res);
        }
    }
    public void ignore()
    {
        if (wasMouseClicked()) {
            res="ignore";
            ensureClose(res);
        }
    }

    public void setTitle(String title)
    {
        if (m_title!=null)
            m_title.setText(title);
    }
    public void setMessage(String message)
    {
        if (m_message!=null)
            m_message.setText(message);
    }
    public void setInfo(String info)
    {
        if (m_info!=null)
            m_info.setText(info);
    }
    public void setConfirm(String confirm)
    {
        if (m_confirm!=null)
            m_confirm.setText(confirm);
    }


    private ClassLoader getClassLoader(PosScreen pos) {
        ClassLoader cl = pos.getClassLoader();
        if (cl == null) {
            try {
                cl = Thread.currentThread().getContextClassLoader();
            } catch (Throwable t) {
            }
            if (cl == null) {
                Debug.log("No context classloader available; using class classloader", module);
                try {
                    cl = this.getClass().getClassLoader();
                } catch (Throwable t) {
                    Debug.logError(t, module);
                }
            }
        }
        return cl;
    }
}
