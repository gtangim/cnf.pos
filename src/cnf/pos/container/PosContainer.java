/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.container;



import cnf.pos.util.Debug;
import cnf.pos.cart.resources.*;

public class PosContainer {
    public static String module = PosContainer.class.getName();

    public void configure() throws Exception {
        /*
        //XuiSession session = XuiContainer.getSession();
        GenericValue productStore = null;
        GenericValue facility = null;
        // get the facility Id
        //String facilityId = ContainerConfig.getPropertyValue(cc, "facility-Id", null);
        String facilityId = this.getProperty("facility-id",null);
        if (UtilValidate.isEmpty(facilityId)) {
            throw new ContainerException("No facility-Id value set in pos-container!");
        } else {
            try {
                facility = session.getDelegator().findByPrimaryKey("Facility", UtilMisc.toMap("facilityId", facilityId));
            } catch (GenericEntityException e) {
                throw new ContainerException("Invalid facilityId : " + facilityId);
            }
        }

        // verify the facility exists
        if (facility == null) {
            throw new ContainerException("Invalid facility; facility ID not found [" + facilityId + "]");
        }
        session.setAttribute("facilityId", facilityId);
        session.setAttribute("facility", facility);

        // get the product store Id
        String productStoreId = facility.getString("productStoreId");
        if (UtilValidate.isEmpty(productStoreId)) {
            throw new ContainerException("No productStoreId set on facility [" + facilityId + "]!");
        } else {
            productStore = this.getProductStore(productStoreId, session.getDelegator());
            if (productStore == null) {
                throw new ContainerException("Invalid productStoreId : " + productStoreId);
            }
        }
        session.setAttribute("productStoreId", productStoreId);
        session.setAttribute("productStore", productStore);

        // get the store locale
        //String localeStr = ContainerConfig.getPropertyValue(cc, "locale", null);
        String localeStr = this.getProperty("locale", null);
        if (UtilValidate.isEmpty(localeStr)) {
            localeStr = productStore.getString("defaultLocaleString");
        }
        if (UtilValidate.isEmpty(localeStr)) {
            throw new ContainerException("Invalid Locale for POS!");
        }
        Locale locale = UtilMisc.parseLocale(localeStr);
        session.setAttribute("locale", locale);

        // get the store currency
        String currencyStr = this.getProperty("currency", null);
        if (UtilValidate.isEmpty(currencyStr)) {
            currencyStr = productStore.getString("defaultCurrencyUomId");
        }
        if (UtilValidate.isEmpty(currencyStr)) {
            throw new ContainerException("Invalid Currency for POS!");
        }
        session.setAttribute("currency", currencyStr);
        session.setAttribute("properties",this.properties);
        String prefix = getProperty("IdPrefix","");
        session.setAttribute("idPrefix",prefix);

        ArrayList<String> customerPatterns = new ArrayList<String>();
        for (int i=1;i<10;i++)
        {
            String customerPattern = getProperty("CustomerPattern."+Integer.toString(i),null);
            if(UtilValidate.isNotEmpty(customerPattern))
            {
                customerPatterns.add(customerPattern);
            }
            else break;
        }
        session.setAttribute("customerPatterns",customerPatterns);

        ArrayList<String> pcPatterns = new ArrayList<String>();
        for (int i=1;i<10;i++)
        {
            String pcPattern = getProperty("PromoCodePattern."+Integer.toString(i),null);
            if(UtilValidate.isNotEmpty(pcPattern))
            {
                pcPatterns.add(pcPattern);
            }
            else break;
        }
        session.setAttribute("pcPatterns",pcPatterns);


        ArrayList<String> plPatterns = new ArrayList<String>();
        for (int i=1;i<10;i++)
        {
            String plPattern = getProperty("PriceLabelPattern."+Integer.toString(i),null);
            if(UtilValidate.isNotEmpty(plPattern))
            {
                plPatterns.add(plPattern);
            }
            else break;
        }
        session.setAttribute("plPatterns",plPatterns);

        ArrayList<String> wlPatterns = new ArrayList<String>();
        for (int i=1;i<10;i++)
        {
            String wlPattern = getProperty("WeightLabelPattern."+Integer.toString(i),null);
            if(UtilValidate.isNotEmpty(wlPattern))
            {
                plPatterns.add(wlPattern);
            }
            else break;
        }
        session.setAttribute("wlPatterns",wlPatterns);



        ArrayList<String> inputFilters = new ArrayList<String>();
        for (int i=1;i<20;i++)
        {
            String inputFilter = getProperty("InputFilter."+Integer.toString(i),null);
            if(UtilValidate.isNotEmpty(inputFilter))
            {
                inputFilters.add(inputFilter);
            }
            else break;
        }
        session.setAttribute("inputFilters",inputFilters);

        String rMode = getProperty("RoundingMode","HALF_EVEN");
        if ("CEILING".equals(rMode)) session.setAttribute("RoundingMode", RoundingMode.CEILING);
        else if ("DOWN".equals(rMode)) session.setAttribute("RoundingMode", RoundingMode.DOWN);
        else if ("FLOOR".equals(rMode)) session.setAttribute("RoundingMode", RoundingMode.FLOOR);
        else if ("HALF_DOWN".equals(rMode)) session.setAttribute("RoundingMode", RoundingMode.HALF_DOWN);
        else if ("HALF_EVEN".equals(rMode)) session.setAttribute("RoundingMode", RoundingMode.HALF_EVEN);
        else if ("HALF_UP".equals(rMode)) session.setAttribute("RoundingMode", RoundingMode.HALF_UP);
        else if ("UNNECESSARY".equals(rMode)) session.setAttribute("RoundingMode", RoundingMode.UNNECESSARY);
        else if ("UP".equals(rMode)) session.setAttribute("RoundingMode", RoundingMode.UP);



        ArrayList<String> pages = new ArrayList<String>();
        for (int i=1;i<30;i++)
        {
            String preloadPage = getProperty("PreloadPage."+Integer.toString(i),null);
            if(UtilValidate.isNotEmpty(preloadPage))
            {
                pages.add(preloadPage);
            }
            else break;
        }
        session.setAttribute("pages",pages);

        session.setAttribute("defaultUom", getProperty("DefaultUom", "OTH_ea"));
        session.setAttribute("EmployeePricingCostPlusPercent", getProperty("EmployeePricingCostPlusPercent", "20.00"));
        session.setAttribute("WeightLabelUomId", getProperty("WeightLabelUomId", "WT_g"));
        session.setAttribute("TaxLiteral", getProperty("TaxLiteral", "T"));
        session.setAttribute("ShowZeroTax", getProperty("ShowZeroTax", "Y"));
        session.setAttribute("AlwaysPopDrawer", getProperty("AlwaysPopDrawer", "Y"));
        session.setAttribute("OnScreenKeyboard", getProperty("OnScreenKeyboard", "Y"));
        session.setAttribute("AllowPosCashback", getProperty("AllowPosCashback", "Y"));
        session.setAttribute("PrintReceiptBeforeCommit", getProperty("PrintReceiptBeforeCommit", "Y"));
        session.setAttribute("CalculatePriceLabelQuantity", getProperty("CalculatePriceLabelQuantity", "N"));
        session.setAttribute("PromptForReceipt", getProperty("PromptForReceipt", "N"));
        session.setAttribute("Map",getMaps());
        session.setAttribute("ReceiptTemplateLocation",getProperty("ReceiptTemplateLocation","hot-deploy/pos/config/templates"));
        session.setAttribute("CurrencyFormat",getProperty("CurrencyFormat",null));
        session.setAttribute("ShortCurrencyFormat",getProperty("ShortCurrencyFormat",null));
        session.setAttribute("StartTab",getProperty("StartTab",null));
        session.setAttribute("CurrencyLimits",getProperty("CurrencyLimits",""));
        session.setAttribute("CurrencyLimitWarning",getProperty("CurrencyLimitWarning",""));
        session.setAttribute("ReceiptValidation",getProperty("ReceiptValidation",""));
        session.setAttribute("AutoLockTerminalMs",getProperty("AutoLockTerminalMs","1800000"));
        session.setAttribute("symNodeFileLocation",getProperty("SymNodeFileLocation"
                ,".\\symmetric-ds-2.4.2\\conf\\iwSym.properties")); */
        ResourceManager resource = new ResourceManager();
        try {
            resource.loadAll();
        } catch (Exception e) {
            Debug.log(e);
            Debug.logError("Unable to start the POS, Data Loading failed!", module);
            System.exit(0);
        }
        //resource.getSettings().postLogoutToDb();
        //session.setAttribute("Resource",resource);
    }

    /*public static GenericValue getProductStore(String productStoreId, Delegator delegator) {
           if (productStoreId == null || delegator == null) {
               return null;
           }
           GenericValue productStore = null;
           try {
               productStore = delegator.findByPrimaryKeyCache("ProductStore", UtilMisc.toMap("productStoreId", productStoreId));
           } catch (GenericEntityException e) {
               Debug.logError(e, "Problem getting ProductStore entity", module);
           }
           return productStore;
       }
     */
}
