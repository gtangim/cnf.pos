package cnf.pos;

import cnf.pos.cart.resources.ResourceManager;
import cnf.pos.screen.XuiScreen;
import cnf.pos.util.Debug;
import cnf.pos.util.SplashLoader;
import cnf.pos.util.Utility;

import javax.swing.*;
import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

public class Main {

    //public static String module = Main.class.getName();
    public static final String splashFile = "resources/images/cnfpos.png";

    public static void main(String[] args) {

        Debug.log("===============================================================");
        Debug.log("             " + Utility.fancyTitle("CNF POS [V" + ResourceManager.Version+"]"));
        Debug.log("===============================================================");
        Debug.log("Initializing application resources...");
        try {
            SplashLoader splash = new SplashLoader();
            splash.load(splashFile, ResourceManager.Version);
            addPath("./resources/");


            ResourceManager r = new ResourceManager();
            r.loadAll();
            String laf = r.getSettings().getProperties().getProperty("look-and-feel", null);
            if (Utility.isNotEmpty(laf)) UIManager.setLookAndFeel(laf);
            JFrame jframe = new JFrame();
            jframe.setUndecorated(true);

            XuiScreen app = new XuiScreen(new String[] {"pos.properties","net.xoetrope.swing"}, jframe);
            splash.unload();
        } catch (Exception e) {
            Debug.logError(e, "Fatal Error");
            System.exit(-1);
        }
    }

    public static void addPath(String s) throws Exception {
        File f = new File(s);
        URL u = f.toURI().toURL();
        URLClassLoader urlClassLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
        Class urlClass = URLClassLoader.class;
        Method method = urlClass.getDeclaredMethod("addURL", new Class[]{URL.class});
        method.setAccessible(true);
        method.invoke(urlClassLoader, new Object[]{u});
    }

}
