package cnf.pos.component;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.math.BigDecimal;
import java.util.*;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import cnf.ui.table.*;
import cnf.ui.table.decoration.*;
import cnf.ui.table.element.*;
import cnf.ui.table.indexer.*;
import cnf.ui.table.indexer.StyleKey.AddressType;
import cnf.pos.util.*;
import cnf.pos.cart.PosShoppingCart;
import cnf.pos.cart.PosShoppingCartItem;
import cnf.pos.cart.PosShoppingCartPayment;
import cnf.pos.cart.resources.datastructures.PromoAdjustment;
import cnf.pos.cart.resources.elements.FeatureElement;
import cnf.pos.cart.resources.elements.TaxElement;
import cnf.pos.screen.PosScreen;


public class ExtendedJournal {
	public enum JournalEntryType {ITEM, ADJUSTMENT, TOTAL, PAYMENT, DUE, REFUND_ITEM, REFUND_ADJUSTMENT, REFUND_TOTAL, REFUND_PAYMENT, NULL}
	//protected final String TOT_ST = "tot_1";
	//protected final String TOT_TX = "tot_2";
	//protected final String TOT_T = "tot_3";
	protected final String DUE = "due";
	
	protected final String ROW_ALTER = "border-bottom:none";
	protected final String LAST_SUBROW = "border-bottom:1 solid (100,150,80);";
	
	protected ExtendedTable myTable;
	protected ExtendedModel tm;
	
	//protected JournalHelper jh=null;
    protected Properties prop;
    protected HashMap<StyleKey,SimpleStyle> masterStyles;
    //protected FastList<String> features=null;

	public void clear()
	{
		myTable.clear();
		/*jh.itemCount=0;
		jh.adjustmentCount=0;
		jh.paymentCount=0;
		jh.refundCount=0;
		jh.nextRowId=0;
		jh.refundItemCount.clear();
		jh.refundPaymentCount.clear();*/
		initializeStyles();		
	}
	
	public void init(ExtendedModel tableModel, TableDecorator decorator, Properties prop)
	{
		/*if (jh==null) jh=new JournalHelper();
		this.jh = jh;*/
		this.prop=prop;
		myTable = new ExtendedTable(new JournalEntity(),decorator,tableModel);
		tm = myTable.getTableModel();
        String dr = prop.getProperty("ImageLocation");
		ImageIcon imgBg = new ImageIcon(dr+prop.getProperty("JournalBackgroundImage"));
	    ImageIcon imgId = new ImageIcon(dr+prop.getProperty("IdColumnHeaderImage"));
	    ImageIcon imgT = new ImageIcon(dr+prop.getProperty("TaxColumnHeaderImage"));
	    ImageIcon imgItem = new ImageIcon(dr+prop.getProperty("ItemColumnHeaderImage"));
	    ImageIcon imgQty = new ImageIcon(dr+prop.getProperty("QuantityColumnHeaderImage"));
	    ImageIcon imgRate = new ImageIcon(dr+prop.getProperty("RateColumnHeaderImage"));
	    ImageIcon imgValue = new ImageIcon(dr+prop.getProperty("ValueColumnHeaderImage"));
	    ImageIcon imgHDBg = new ImageIcon(dr+prop.getProperty("HeaderBackgroundImage"));
	    ImageIcon imgHDLeft = new ImageIcon(dr+prop.getProperty("HeaderLeftImage"));
	    ImageIcon imgHDRight = new ImageIcon(dr+prop.getProperty("HeaderRightImage"));
        myTable.setBackground(imgBg);
        myTable.setColumnHeader("id", new Integer(prop.getProperty("Header.column.id","100")), imgId, true, imgHDBg, imgHDLeft, imgHDRight );
        myTable.setColumnHeader("T", new Integer(prop.getProperty("Header.column.t","20")), imgT, true, imgHDBg, imgHDLeft, imgHDRight );
        myTable.setColumnHeader("item", new Integer(prop.getProperty("Header.column.item","255")), imgItem, true, imgHDBg, imgHDLeft, imgHDRight );
        myTable.setColumnHeader("qty", new Integer(prop.getProperty("Header.column.qty","80")), imgQty, true, imgHDBg, imgHDLeft, imgHDRight );
        myTable.setColumnHeader("rate", new Integer(prop.getProperty("Header.column.rate","90")), imgRate, true, imgHDBg, imgHDLeft, imgHDRight );
        myTable.setColumnHeader("value", new Integer(prop.getProperty("Header.column.value","90")), imgValue, true, imgHDBg, imgHDLeft, imgHDRight );
        myTable.setRowHeight(30);
        myTable.getTable().setCellSelectionEnabled(true);
        myTable.getTable().setIntercellSpacing(new Dimension(0,0));
        myTable.getTable().setShowGrid(false);
        myTable.getTable().setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        myTable.setGridColor(Color.LIGHT_GRAY);
        myTable.setWheelScrollingEnabled(true);
        myTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        myTable.getTable().setRowSelectionAllowed(true);
        myTable.getTable().setColumnSelectionAllowed(true);
        myTable.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        myTable.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        initializeStyles();

	}

    public void setTrainingDisplay(boolean trainingMode)
    {
        String dr = prop.getProperty("ImageLocation");
        String iName = dr+prop.getProperty("JournalBackgroundImage");
        if (trainingMode) iName=iName.replace(".","T.");
        ImageIcon imgBg = new ImageIcon(iName);
        myTable.setBackground(imgBg);
    }

    /*
    public void setupFeatureList(ResourceManager res)
    {
        if (features==null)
        {
            Map<String,String> fMap = res.getSettings().getMap();
            List<ShoppingCartLayers> layers = res.getSettings().getCartLayers();
            features = FastList.newInstance();
            for(ShoppingCartLayers scl : layers)
                if ("CL_FEATURE".equals(scl.getPrimaryTypeId()) && fMap.containsKey(scl.getSecondaryTypeId()))
                    features.add(fMap.get(scl.getSecondaryTypeId()));
        }
    }
	*/

	public ExtendedJournal(JPanel parentContainer, ExtendedModel tableModel, TableDecorator decorator, Properties prop)
	{
		parentContainer.add(myTable, "Center");	    
		init(tableModel,decorator, prop);
	}
	
	public ExtendedJournal(ExtendedModel tableModel, TableDecorator decorator, Properties prop)
	{
		init(tableModel,decorator,prop);
	}
	
	/*public int getPaymentCount() {return jh.paymentCount;}
	public int getRefundCount() {return jh.refundCount;}
	public int getItemCount() {return jh.itemCount;}*/
	public ExtendedTable getTable(){return myTable;}



    public void reloadJournalFromCart(PosScreen pos, PosShoppingCart cart, boolean scrollToBottom)
    {
        //setupFeatureList(cart.getResource());
        int ind = 0;
        List<PosShoppingCartItem> items = cart.getItems();
        ArrayList<RowElement> jItems = new ArrayList<RowElement>();
        HashMap<String,String> styleClasses = new HashMap<String, String>();
        HashMap<StyleKey,SimpleStyle> customStyles = (HashMap)masterStyles.clone();

        for (PosShoppingCartItem item:items)
        {
            String indS = Integer.toString(ind);
            String rowId = "itm_"+indS;
            RowElement r = new RowElement(new Index(rowId,null),new JournalEntity(
                item.getSku(),item.getDescription(),item.getTaxable(),item.getQuantity(),
                    item.getRate(),item.getSubtotal()
            ));
            // 01. First Show Override Message
            String override = item.getPriceOverrideText();
            if (override!=null) {
                r.addSubRow(indS+"ovr_0",new JournalEntity("",override,"",null,"",null));
            }
            else {
                if (item.getSelectedPrice().getPriceSaving(cart.isUseGrossPrice()).compareTo(BigDecimal.ZERO)!=0) {
                   r.addSubRow(indS+"inf_0",new JournalEntity("","*SALE* you saved "
                           +pos.getTerminalHelper().formatCurrency(
                           item.getSelectedPrice().getPriceSaving(cart.isUseGrossPrice())
                           .multiply(item.getQuantity())),"",null,"",null));
                }
            }

            // 02. Show Features
            int fIndex = 0;
            for(String featureType:cart.getFeatureTypeNames())
            {
                List<FeatureElement> fList = item.getFeatures(featureType);
                for(FeatureElement fae:fList)
                {
                    BigDecimal v = item.getFeatureDisplayValue(fae);
                    if (v!=null)
                        r.addSubRow(indS+"fea_"+Integer.toString(fIndex++),new JournalEntity(
                            "",item.getFeatureDisplayName(fae),"",null,"",v
                        ));
                }
            }

            // 03. Show Discount
            String discount = item.getItemDiscountText();
            if (discount!=null)
                r.addSubRow(indS+"dis_0",new JournalEntity("",discount,"",null,"",item.getItemDiscountAmount()));
            jItems.add(r);
            styleClasses.put(rowId, "itm");

            // 04. Show Promotions
            int pIndex = 0;
            for(PromoAdjustment pad : cart.getPromoAdjustments(ind))
            {
                r.addSubRow(indS+"pro_"+Integer.toString(pIndex++),new JournalEntity(
                    pad.getDisplayPromoCode(),cart.getPromoName(pad.getPromoId()),"",null,"",pad.getAmount()
                ));
            }



            // Now setting complex row style...
            if (r.Count()>1)
            {
                setCustomStyle(customStyles,"itm", AddressType.ROW, rowId, ROW_ALTER);
                setCustomStyle(customStyles,"itm", AddressType.SUB_ROW, r.getIndex(r.Count()-1).getSubRowId(), LAST_SUBROW);
            }
            ind++;
        }

        if (items.size()>0)
        {
            // Now First do Order level Promos...
            int pIndex=0;
            for(PromoAdjustment pad : cart.getPromoAdjustments(-1))
            {
                String rowId = "pro_"+Integer.toString(pIndex++);
                RowElement r = new RowElement(new Index(rowId,null)
                        ,new JournalEntity(pad.getDisplayPromoCode(),cart.getPromoName(pad.getPromoId()),"",null,"",pad.getAmount()));
                jItems.add(r);
                styleClasses.put(rowId,"tot");
            }

            // Now First do Sale Discount....
            String discount = cart.getDiscountText();
            if (discount!=null)
            {
                String rowId = "dis_0";
                RowElement r = new RowElement(new Index(rowId,null)
                        ,new JournalEntity("",discount,"",null,"",cart.getDiscountAmount()));
                jItems.add(r);
                styleClasses.put(rowId,"tot");
            }

            // Now Show Subtotal, taxes and Total...
            ind = 0;
            String rowId = "tot_"+Integer.toString(ind++);
            RowElement r = new RowElement(new Index(rowId,null)
                    ,new JournalEntity("","","",null,"Sub Total",cart.getSubTotal()));
            jItems.add(r);
            styleClasses.put(rowId,"tot");
            List<TaxElement> tList = cart.getResource().getTaxes().getStoreTaxes();
            for(TaxElement t:tList)
            {
                BigDecimal taxValue = cart.getTax(t.TaxId);
                if (cart.getResource().getSettings().isShowZeroTax()
                        || !taxValue.equals(cart.ZERO))
                {
                    rowId = "tot_"+Integer.toString(ind++);
                    r = new RowElement(new Index(rowId,null)
                            ,new JournalEntity("","","",null,t.Description,taxValue));
                    jItems.add(r);
                    styleClasses.put(rowId,"tot");
                }
            }
            rowId = "tot_"+Integer.toString(ind++);
            r = new RowElement(new Index(rowId,null)
                    ,new JournalEntity("","","",null,"Grand Total",cart.getGrandTotal()));
            jItems.add(r);
            styleClasses.put(rowId,"tot");

            // Show all payments...
            for(PosShoppingCartPayment payment:cart.getPayments())
            {
                rowId = "pmt_"+payment.getIndex();
                r = new RowElement(new Index(rowId,null)
                        ,new JournalEntity(payment.getPaymentMethodTypeShortText()
                                ,payment.getDisplayText()
                                ,"",null,payment.getStatusText(),payment.getAmount()));
                jItems.add(r);
                styleClasses.put(rowId,"pmt");
            }



            // Show due...
            String savingText = "";
            BigDecimal savings = cart.getTotalSavings();
            if(savings.compareTo(cart.ZERO)!=0)
                savingText = "** You saved: "
                        + pos.getTerminalHelper().formatCurrency(savings.negate())+" **";
            rowId = "tot_"+Integer.toString(ind++);
            r = new RowElement(new Index(rowId,null)
                    ,new JournalEntity("",savingText,"",null,"Due",cart.getDue()));
            jItems.add(r);
            styleClasses.put(rowId,"tot");


        }

        myTable.setStyle(customStyles);
        tm.loadDataBatch(jItems,styleClasses);
        if (scrollToBottom) myTable.scrollToBottom();
        myTable.repaint();

    }




	
	/*
	public void addItem(int Index,  String sku, String taxable, String item, BigDecimal qty, String rate, BigDecimal value)
	{
		//String Id = "itm_" + Integer.toString(Index);
		//Index ind = new Index(Id,null);
		//int pos = tm.getIndex(ind);
		if (Index>jh.itemCount) Index = jh.itemCount;
		
		String Id = "itm_"+jh.getNextRowId();
		if (Index<jh.itemCount)
		{
			// update the item by deleting existing and adding new...
			tm.removeRow(Index);
			tm.insertRow(Index, Id, "itm",
					new JournalEntity(sku,item,taxable,qty,rate,value));
		}
		else
		{
			// add a new item...			
			tm.insertRow(jh.itemCount++, Id, "itm",
					new JournalEntity(sku,item,taxable,qty,rate,value));
			myTable.getDecorator().coordToStyle.clear();
		}
		
		tm.selectRow(Index);
		
	}	
	public void addItem(String sku, String taxable, String item, BigDecimal qty, String rate, BigDecimal value)
	{
		addItem(jh.itemCount,sku,taxable,item, qty, rate, value);
	}
	public void removeItem(int itemIndex)
	{
		//String Id = "itm_" + Integer.toString(itemIndex);
		//Index ind = new Index(Id,null);
		//RowElement r = tm.getRow(ind);
		if (itemIndex<0 || itemIndex>=jh.itemCount) return;
		RowElement r = tm.getRow(itemIndex);
		if (r!=null)
		{
			tm.removeRow(r.getIndex());
			myTable.getDecorator().coordToStyle.clear();
			jh.itemCount--;
		}
	}
	
	public void addSubItem(int Index, String srowtype, int srowid,String sku, String taxable, String item, BigDecimal qty, String rate, BigDecimal value)
	{		
		//String Id = "itm_" + Integer.toString(Index);
		if (Index<0 || Index>=jh.itemCount) return;
		RowElement r = tm.getRow(Index);
		//RowElement r = tm.getRow(Id);
		if (r!=null)
		{
			String Id = r.getIndex().getRowId();
			String SubId = Id+"_" + srowtype +"_" + Integer.toString(srowid);
			EntityContainer ec = r.getSubRowEntityContainer(new Index(Id,SubId));
			if (ec!=null)
				tm.removeSubRow(Id, SubId);
			tm.addSubRow(Id, SubId, 
					new JournalEntity(sku,item,taxable,qty,rate,value));
			myTable.getDecorator().coordToStyle.clear();
			this.initializeRowStyle(new Index(Id,null));
		}
	}
	
	public void removeSubItem(int Index, String srowid)
	{
		RowElement r = tm.getRow(Index);
		String Id = r.getIndex().getRowId();
		//String Id = "itm_" + Integer.toString(Index);
		String SubId = Id+"_" + srowid;
		//RowElement r = tm.getRow(Id);
		if (r!=null)
		{
			EntityContainer ec = r.getSubRowEntityContainer(new Index(Id,SubId));
			if (ec!=null)
				tm.removeSubRow(Id, SubId);	
			myTable.getDecorator().coordToStyle.clear();
			this.initializeRowStyle(new Index(Id,null));
		}		
	}

	
	public void addAdjustment(int Index,  String sku, String taxable, String item, BigDecimal qty, String rate, BigDecimal value)
	{
		String Id = "adj_" + Integer.toString(Index);
		Index ind = new Index(Id,null);
		int pos = tm.getIndex(ind);
		if (pos!=-1)
		{
			// update the item by deleting existing and adding new...
			int row_pos = Index + jh.itemCount;
			tm.removeRow(row_pos);
			tm.insertRow(row_pos, Id, "tot",
					new JournalEntity(sku,item,taxable,qty,rate,value));
			myTable.getDecorator().coordToStyle.clear();
		}
		else
		{
			// add a new item...
			tm.insertRow(jh.itemCount + jh.adjustmentCount++, Id, "tot",
					new JournalEntity(sku,item,taxable,qty,rate,value));
			myTable.getDecorator().coordToStyle.clear();
		}
		
	}	
	public void addAdjustment(String sku, String taxable, String item, BigDecimal qty, String rate, BigDecimal value)
	{
		addAdjustment(jh.adjustmentCount,sku,taxable,item, qty, rate, value);
	}
	public void removeAdjustment(int itemIndex)
	{
		String Id = "adj_" + Integer.toString(itemIndex);
		Index ind = new Index(Id,null);
		RowElement r = tm.getRow(ind);
		if (r!=null)
		{
			tm.removeRow(ind);
			myTable.getDecorator().coordToStyle.clear();
			jh.adjustmentCount--;
		}
	}
	public void removeAdjustments()
	{
		for (int i=0;i<jh.adjustmentCount;i++)
		{
			String Id = "adj_" + Integer.toString(i);
			Index ind = new Index(Id,null);
			RowElement r = tm.getRow(ind);
			if (r!=null)
			{
				tm.removeRow(ind);
			}			
		}
		myTable.getDecorator().coordToStyle.clear();
		jh.adjustmentCount=0;
	}
	
	
	
	
	
	
	public void setTotal(BigDecimal subtotal, BigDecimal tax, BigDecimal total, String refundText)
	{
		clearTotal();
		int pos = jh.itemCount+jh.adjustmentCount;
		if (refundText==null) refundText="";
		tm.insertRow(pos, TOT_T, "tot", new JournalEntity("",refundText,"",null,"Total",total));
		tm.insertRow(pos, TOT_TX, "tot", new JournalEntity("","","",null,"Tax",tax));
		tm.insertRow(pos, TOT_ST, "tot", new JournalEntity("","","",null,"Item Total",subtotal));
		myTable.repaint();
	}
	
	public void clearTotal()
	{
		tm.removeRow(TOT_ST);
		tm.removeRow(TOT_TX);
		tm.removeRow(TOT_T);
	}

	public void setDue(int txmode, BigDecimal dueTotal)
	{
		String due_msg;
		if (txmode!=1 && txmode!=2) return;
		if (txmode==1)
		{
			due_msg = "Total Due";
			if (dueTotal.compareTo(BigDecimal.ZERO)<0)
			{
				due_msg = "Change Due";
				dueTotal = dueTotal.negate();
			}
		}
		else
		{
			due_msg = "Refund Due";
			if (dueTotal.compareTo(BigDecimal.ZERO)>0)
			{
				due_msg = "Overdrawn";
			}
			else dueTotal = dueTotal.negate();

		}
		clearDue();
		int pos = jh.itemCount+jh.adjustmentCount+jh.paymentCount;
		if (tm.getIndex(new Index(TOT_T,null))!=-1) pos+=3;
		tm.insertRow(pos, DUE, "tot", new JournalEntity("","","",null,due_msg,dueTotal));
		myTable.repaint();
	}
	
	public void clearDue()
	{
		tm.removeRow(DUE);
	}
	
	public boolean isDueVisible()
	{
		if (tm.getIndex(new Index(DUE,null))!=-1) return true;		
		else return false;
	}
	
	
	public void addPayment(int Index, String ref, String Desc, BigDecimal amount)
	{
		int pos = jh.itemCount+jh.adjustmentCount;
		if (tm.getIndex(new Index(TOT_T,null))!=-1) pos+=3;
		pos+=Index;
		String Id = "pmt_"+Integer.toString(Index);
		Index ind = new Index(Id,null);
		RowElement pr = tm.getRow(pos);
		if (tm.getIndex(ind)!=-1)
		{
			Id=Id+"_"+Integer.toString((int)(Math.random()*1000000));
		}
		tm.insertRow(pos, Id, "pmt", 
				new JournalEntity(ref,Desc,"",null,"",amount));
		jh.paymentCount++;
	}

	public void addPayment(String ref, String Desc, BigDecimal amount)
	{
		addPayment(jh.paymentCount,ref,Desc,amount);
	}
	
	public void removePayment(int Index)
	{
		int pos = jh.itemCount+jh.adjustmentCount;
		if (tm.getIndex(new Index(TOT_T,null))!=-1) pos+=3;
		pos+=Index;
		RowElement pr = tm.getRow(pos);
		if (pr!=null)
		{
			tm.removeRow(pos);
			jh.paymentCount--;
		}
	}
	
	
	public void newRefundEntry()
	{
		this.jh.refundCount++;
		this.jh.refundItemCount.add(0);
		this.jh.refundPaymentCount.add(0);		
	}
	
	public void addRefundItem()
	{
		
	}
	
	public void addRefundPayment(int Index, String ref, String Desc, BigDecimal amount)
	{
		
		int pos = jh.itemCount+jh.adjustmentCount+jh.paymentCount;
		if (tm.getIndex(new Index(TOT_T,null))!=-1) pos+=3;
		if (tm.getIndex(new Index(DUE,null))!=-1) pos+=1;
		pos+=Index;
		String Id = "rfp_"+Integer.toString(Index);
		Index ind = new Index(Id,null);
		RowElement pr = tm.getRow(pos);
		if (tm.getIndex(ind)!=-1)
		{
			Id=Id+"_"+Integer.toString((int)(Math.random()*1000000));
		}
		tm.insertRow(pos, Id, "rfp", 
				new JournalEntity(ref,Desc,"",null,"",amount));
		if (jh.refundCount==0) 
		{
			jh.refundItemCount.add(0);
			jh.refundPaymentCount.add(1);
		}
		int c = jh.refundPaymentCount.get(0);
		jh.refundPaymentCount.set(0, c+1);
	}
	
	*/

    public void selectLastItem(String itemPrefix)
    {
        int lRow = -1;
        for (int i=0;i<tm.getPrimaryRowCount();i++)
        {
            RowElement re = tm.getRow(i);
            if (re.getIndex().getRowId().contains(itemPrefix))
                lRow=i;
        }
        if (lRow!=-1)
        {
            int rn = 0;
            for (int i=0;i<lRow;i++)
                rn+=tm.getRow(i).Count();
            tm.selectRow(rn);
        }
        else tm.unselect();
    }

	public Index getSelectionIndex(){return tm.getSelectedRow();}
	public Index getSelectedSubIndex(){return tm.getSelectedSubRow();}
	public int getSelectedIndexInt()
	{
        Index i = tm.getSelectedRow();
        int ind=-1;
        if (i!=null)
        {
            String[] idc = i.getRowId().split("_");
            try
            {
                ind = Integer.parseInt(idc[idc.length-1]);
            }
            catch(Exception ex){}
        }
        return ind;
	}

	public JournalEntryType getSelectedIndexType()
	{
		Index i = tm.getSelectedRow();
		if (i!=null)		
		{
			String id = i.getRowId();
			if (id==null || id.trim().isEmpty()) return JournalEntryType.NULL;
			id = id.trim();
			if (id.contains("ref_"))
			{
				if (id.contains("itm_")) return JournalEntryType.REFUND_ITEM;						
				else if (id.contains("adj_")) return JournalEntryType.REFUND_ADJUSTMENT;						
				else if (id.contains("tot_")) return JournalEntryType.REFUND_TOTAL;						
				else if (id.contains("pmt_")) return JournalEntryType.REFUND_PAYMENT;
			}
			else
			{
				if (id.contains("itm_")) return JournalEntryType.ITEM;						
				else if (id.contains("adj_")) return JournalEntryType.ADJUSTMENT;						
				else if (id.contains("tot_")) return JournalEntryType.TOTAL;						
				else if (id.contains("pmt_")) return JournalEntryType.PAYMENT;				
			}
		}
		return JournalEntryType.NULL;
	}


    /*
	public int getRefundGroupIndex()
	{
		Index idx = tm.getSelectedRow();
		int ind = -1;
		if (idx!=null)		
		{
			String Id = idx.getRowId();
			if (Id.contains("ref")){
				try
				{
					String[] comp = Id.substring(Id.indexOf("ref")).split("_");
					if (comp.length>2) return Integer.parseInt(comp[1]);
				}
				catch(Exception ex)
				{					
				}
			}			
		}
		return ind;		
	}
	*/
	
	
	public int getSelectedSubIndexInt()
	{
		Index i = tm.getSelectedSubRow();
		int ind = -1;
		if (i!=null)		
		{
			String[] idc = i.getSubRowId().split("_");
			try
			{
				ind = Integer.parseInt(idc[idc.length-1]);
			}
			catch(Exception ex)
			{
				ind=-1;
			}			
		}
		return ind;
	}

	public String getSelectedSubIndexType()
	{
		Index i = tm.getSelectedSubRow();
		if (i!=null)		
		{
			String[] idc = i.getSubRowId().split("_");
			if (idc.length>1) return idc[idc.length-2];
		}
		return "";
	}
	







    private void setCustomStyle(HashMap styleMap, String group, AddressType addressType, String target, String sName)
    {
        if (!Utility.isEmpty(sName))
        {
            try
            {
                StyleKey ind = new StyleKey(group,addressType,target,-1);
                SimpleStyle st = new SimpleStyle(sName);
                styleMap.put(ind, st);
            }
            catch(Exception ex)
            {
            }

        }
    }



	private void setStyle(HashMap styleMap, String group, AddressType addressType, String target, String propName)
    {
        String val = prop.getProperty(propName);
        if (!Utility.isEmpty(val))
        {
            //myTable.setStyle(group,addressType,target,Val);
            try
            {
                StyleKey ind = new StyleKey(group,addressType,target,-1);
                SimpleStyle st = new SimpleStyle(val);
                styleMap.put(ind, st);
            }
            catch(Exception ex)
            {
            }

        }
    }

    private void setStyleSet(String group, String prefix)
    {
        setStyle(masterStyles,group,AddressType.ROW,"",prefix+"RowStyle");
        setStyle(masterStyles,group,AddressType.ROW_ALT,"",prefix+"AltRowStyle");
        setStyle(masterStyles,group,AddressType.SUB_ROW,"",prefix+"SubRowStyle");
        setStyle(masterStyles,group,AddressType.SUB_ROW_ALT,"",prefix+"AltSubRowStyle");
        setStyle(masterStyles,group,AddressType.ROW_SEL,"",prefix+"RowSelStyle");
        setStyle(masterStyles,group,AddressType.SUB_ROW_SEL,"",prefix+"SubRowSelStyle");
        setStyle(masterStyles,group,AddressType.CELL_SEL,"",prefix+"CellSelStyle");
        setStyle(masterStyles,group,AddressType.COL,"id",prefix+"IdColStyle");
        setStyle(masterStyles,group,AddressType.COL,"T",prefix+"TaxColStyle");
        setStyle(masterStyles,group,AddressType.COL,"item",prefix+"ItemColStyle");
        setStyle(masterStyles,group,AddressType.COL,"qty",prefix+"QuantityColStyle");
        setStyle(masterStyles,group,AddressType.COL,"rate",prefix+"RateColStyle");
        setStyle(masterStyles,group,AddressType.COL,"value",prefix+"ValueColStyle");
    }

    public void initializeStyles()
	{
        masterStyles = new HashMap<StyleKey, SimpleStyle>();

        setStyleSet("itm","Itm");
        setStyleSet("tot","Tot");
        setStyleSet("pmt","Pmt");
        setStyleSet("rfi","Rfi");
        setStyleSet("rfp","Rfp");

        myTable.setStyle(masterStyles);
	}
	
	
	public void initializeRowStyle(Index ind)
	{
		RowElement r = tm.getRow(ind);
		String styleClass = tm.getRowStyleClass(ind.getRowId());
		if (r!=null)
		{
			if (r.Count()==1)
			{
				myTable.removeStyle(styleClass, AddressType.ROW, ind.getRowId());
			}
			else
			{
				myTable.setStyle(styleClass, AddressType.ROW, ind.getRowId(), ROW_ALTER);
				for (int i=1;i<r.Count()-1;i++)
					myTable.removeStyle(styleClass, AddressType.SUB_ROW, r.getIndex(i).getSubRowId());
				myTable.setStyle(styleClass, AddressType.SUB_ROW, r.getIndex(r.Count()-1).getSubRowId(), LAST_SUBROW);
            }
		}
	}
	

	/*public JournalHelper getJournalHelper(){return jh;}*/


    public void focus()
    {
        myTable.requestFocus();
    }
    public void setLock(boolean lock)
    {
           myTable.setVisible(!lock);
           myTable.setEnabled(!lock);
    }



}
