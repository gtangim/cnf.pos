package cnf.pos.component;

public class JournalEntry {
	protected String Id;
	protected String Item;
	protected String Qty;
	protected String Rate;
	protected String Value;
	protected String eType;
	protected int Index;
	
	public JournalEntry()
	{
		Id = "";
		Item = "";
		Qty = "";
		Rate ="";
		Value="";
		eType="";
		Index = -1;
	}
	
	public JournalEntry(String Id, String Item, String Qty, String Rate, String Value, String eType, int Index)
	{
		this.Id=Id;
		this.Item=Item;
		this.Qty=Qty;
		this.Rate=Rate;
		this.Value=Value;
		this.eType = eType;
		this.Index = Index;		
		if (this.Id==null) this.Id="";
		if (this.Item==null) this.Item="";
		if (this.Qty==null) this.Qty="";
		if (this.Rate==null) this.Rate="";
		if (this.Value==null) this.Value="";
		if (this.eType==null) this.eType="";
	}
	
	public String getId()
	{
		return this.Id;
	}
	public void setId(String Id)
	{
		if (Id==null) this.Id="";
		else this.Id=Id;
	}
	
	public String getItem()
	{
		return this.Item;
	}
	public void setItem(String Item)
	{
		if (Item==null) this.Item="";
		else this.Item=Item;
	}
	
	public String getQty()
	{
		return this.Qty;
	}
	public void setQty(String Qty)
	{
		if (Qty==null) this.Qty="";
		else this.Qty=Qty;
	}
	
	public String getRate()
	{
		return this.Rate;
	}
	public void setRate(String Rate)
	{
		if (Rate==null) this.Rate="";
		else this.Rate=Rate;
	}
	
	public String getValue()
	{
		return this.Value;
	}
	public void setValue(String Value)
	{
		if (Value==null) this.Value="";
		else this.Value=Value;
	}
	
	public String geteType()
	{
		return this.eType;
	}
	public void seteType(String eType)
	{
		if (eType==null) this.eType="";
		else this.eType=eType;
	}
	
	public int getIndex()
	{
		return this.Index;
	}
	public void setIndex(int Index)
	{
		this.Index=Index;
	}
	
	
}
