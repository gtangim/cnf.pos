/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.pos.component;

import cnf.pos.cart.resources.ResourceManager;
import net.xoetrope.swing.XEdit;
import cnf.pos.screen.PosScreen;
import cnf.pos.PosTransaction;
import cnf.pos.util.Utility;
//import org.ofbiz.guiapp.xui.XuiSession;

import java.awt.Color;
import java.math.BigDecimal;
import java.util.Locale;

public class Output {

    public static final String module = Output.class.getName();

    public static final int TX_MODE_NEW = 0;
    public static final int TX_MODE_SALE = 1;
    public static final int TX_MODE_REFUND = 2;
    public static final int TX_MODE_ROA = 3;

    public static final int TILL_STATUS_READY = 0;
    public static final int TILL_STATUS_LOGIN = 1;
    public static final int TILL_STATUS_LOCKED = 2;
    public static final int TILL_STATUS_CLOSED = 3;
    public static final int TILL_STATUS_COMPLETE = 4;
    public static final int TILL_STATUS_MUST_CLOSE = 5;

    public static long OVERLAY_DELAY = 4000;
    
    // login labels

    private Locale defaultLocale = Locale.getDefault();
    //protected XuiSession session = null;
    protected XEdit output = null;
    protected XEdit pos_mode = null;
    protected XEdit pos_hint = null;
    protected XEdit pos_total = null;
    protected XEdit pos_stat = null;
    protected XEdit pos_progress = null;
    
    protected boolean showChange = false;
    protected boolean isNegativeQty = false;
    protected int tx_mode = 0;
    protected int till_status = TILL_STATUS_LOGIN;
    protected String till_status_overlay = null;
    protected long till_status_overlay_time = 0;
    protected double till_amount = 0;
    
    public void clear() {
        tx_mode = 0;
        if (PosScreen.getActiveScreen()!=null && PosScreen.getActiveScreen().getResource().getSecurity().getUser()!=null)
            till_status = TILL_STATUS_READY;
        else
            till_status = TILL_STATUS_LOGIN;
        till_status_overlay = null;
        till_status_overlay_time = 0;
        till_amount = 0;
        this.print(null);
        this.setStat("");
        this.setProgress(null);
        refresh();
    }

    public Output(PosScreen page) {
        this.output = (XEdit) page.findComponent("pos_output");
        this.pos_mode = (XEdit) page.findComponent("pos_mode");
        this.pos_total = (XEdit) page.findComponent("pos_total");
        this.pos_hint = (XEdit) page.findComponent("pos_hint");
        this.pos_stat = (XEdit) page.findComponent("pos_stat");
        this.pos_progress = (XEdit) page.findComponent("pos_progress");
        //this.session = page.getSession();
        this.output.setFocusable(false);
        this.pos_mode.setFocusable(false);
        this.pos_total.setFocusable(false);
        this.pos_hint.setFocusable(false);
        this.pos_stat.setFocusable(false);
        this.pos_progress.setFocusable(false);
		this.pos_progress.setForeground(new Color(255,255,80));        
		this.pos_progress.setOpaque(false);        
        this.clear();
    }

    public void setLock(boolean lock) {
        if (lock) {
        	setTillStatus(this.TILL_STATUS_LOCKED);
            this.print("Enter User ID:");
        } else {
        	this.print(null);
            PosTransaction trans=PosTransaction.getCurrentTx();
            setTillStatus(this.TILL_STATUS_READY);
        }
    }

    public void print(String message) {
        if (message==null)
        	this.output.setText("Enter Product Code:");
        else 
        	this.output.setText(message);
    }
    
    public void setTxMode(int mode, boolean isNegativeQuantity)
    {
    	tx_mode = mode;
        this.isNegativeQty=isNegativeQuantity;
    	refresh();
    }
    public void setTillStatus(int status)
    {
    	till_status = status;
    	refresh();
    }
    public void setHint(String hint)
    {
    	if (hint!=null)
    	{
    		till_status_overlay = hint;
    		till_status_overlay_time = System.currentTimeMillis();
    		refresh();
    	}
    	
    }

    public String getStat()
    {
        String stat = pos_stat.getText();
        if (stat==null) stat="";
        return stat;
    }

    public void setStat(String stat)
    {
    	pos_stat.setText(stat);
    }
    
    public void setProgress(String prg)
    {
    	if (prg==null || prg.isEmpty())
    	{
    		pos_progress.setText("");
    		//pos_progress.setOpaque(false);
    		//pos_progress.setBackground(new Color(0,0,0));
    	}
    	else
    	{
    		pos_progress.setText(prg);
    		//pos_progress.setOpaque(true);
    		//pos_progress.setBackground(new Color(145,155,30));    		
    	}    	
    }
    

    public void setAmount(BigDecimal amt)
    {
    	setAmount(amt,false);
    }
    public  void setAmount(double amt)
    {
    	setAmount(amt,false);
    }
    public void setAmount(BigDecimal amt, boolean showChange)
    {
    	setAmount(amt.doubleValue(), showChange);
    }
    public void setAmount(double amt, boolean showChange)
    {
    	this.showChange = showChange;
    	till_amount = amt;
    	refresh();
    }
    

    
    
    public void refresh()
    {
        String pmText;
        String phText;
        String ptText;

        PosScreen pos  = PosScreen.getActiveScreen();

        if (till_status==TILL_STATUS_LOCKED || till_status==TILL_STATUS_CLOSED)
    		pmText="Inactive";
    	else if (till_status==TILL_STATUS_MUST_CLOSE)
            pmText="Must Close";
        else if (tx_mode==TX_MODE_NEW)
    		pmText="New Tx";
    	else if (tx_mode==TX_MODE_SALE && showChange)
    	{
    		if (till_amount>=0)
    			pmText="Amount Due";
    		else
    			pmText="Change Due";
    	}
    	else if (tx_mode==TX_MODE_REFUND && showChange)
    	{
    		if (till_amount>=0)
    			pmText="Refund Overpaid";
    		else
    			pmText="Refund Due";
    	}
    	else if (tx_mode==TX_MODE_SALE)
    		pmText="Sale";
    	else if (tx_mode==TX_MODE_REFUND)
        {
    		pmText="Refund (" + (isNegativeQty?"-":"+") +")";
        }
    	else if (tx_mode==TX_MODE_ROA)
            pmText="Rcv. Acc.";
        else
    		pmText="Error";

        if (pos!=null && pos.getTerminalHelper().isTrainingMode())
            pmText=pmText+" *Train*";
        String pmText2 = pos_mode.getText();
        if (!pmText.equals(pmText2)) pos_mode.setText(pmText);
    	
    	if (till_status_overlay!=null)
    	{
    		phText = till_status_overlay;
    		long t = System.currentTimeMillis();
    		if (t-till_status_overlay_time>OVERLAY_DELAY)
    		{
    			till_status_overlay=null;
    			till_status_overlay_time = 0;
    		}
    	}
    	else
    	{
    		if (till_status==TILL_STATUS_READY)
    			phText = "Ready!";
    		else if (till_status==TILL_STATUS_LOGIN)
    			phText = "Please Login...";
    		else if (till_status==TILL_STATUS_LOCKED)
    			phText = "Please Wait...";
    		else if (till_status==TILL_STATUS_CLOSED)
    			phText = "Register Is Closed!";
    		else if (till_status==TILL_STATUS_COMPLETE)
    			phText = "Sale Complete!";
            else if (till_status==TILL_STATUS_MUST_CLOSE)
                phText = "Register Must Be Closed...";
    		else 
    			phText = "Error!";
    	}

        String phText2 = pos_hint.getText();
        if (!phText.equals(phText2)) pos_hint.setText(phText);
    	
    	//DecimalFormat df = new DecimalFormat("$#,###,##0.00;$(#,###,##0.00)");
    	//ptText = df.format(till_amount);
        if (pos!=null)
        {
            ptText = pos.getTerminalHelper().formatCurrency(till_amount);
        }
        else ptText = Utility.formatPrice(till_amount);
        String ptText2 = pos_total.getText();
        if (!ptText.equals(ptText2))
            pos_total.setText(ptText);
    	
    	
    	
    }
    
    
}
