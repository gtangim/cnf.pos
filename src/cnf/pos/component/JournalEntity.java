package cnf.pos.component;

import java.math.BigDecimal;

import cnf.ui.table.element.EMC;


public class JournalEntity {
	
    @EMC(RendererClass="cnf.ui.table.renderer.TextRenderer")
	protected String id;
    @EMC(RendererClass="cnf.ui.table.renderer.TextRenderer")
	protected String T;
    @EMC(RendererClass="cnf.ui.table.renderer.TextRenderer")
	protected String item;
    @EMC(RendererClass="cnf.ui.table.renderer.NumberRenderer", Format="#######.######")
	protected BigDecimal qty;
    @EMC(RendererClass="cnf.ui.table.renderer.TextRenderer")
	protected String rate;
    @EMC(RendererClass="cnf.ui.table.renderer.NumberRenderer", Format="$ #,###,##0.00;$ (#,###,##0.00)")
	protected BigDecimal value;
    
    public JournalEntity()
    {
    	id="";
    	item="";
    	qty=value=BigDecimal.ZERO;
    	rate="";
    	T="";
    }
    public JournalEntity(String id, String item, String T, BigDecimal qty, String rate, BigDecimal value)
    {
    	this.id=id;
    	this.item=item;
    	this.T=T;
    	this.qty=qty;
    	this.rate=rate;
    	this.value=value;
    	
    }
    
    public String getId(){return id;}
    public String getItem(){return item;}
    public BigDecimal getQty(){return qty;}
    public String getRate(){return rate;}
    public BigDecimal getValue(){return value;}
    public String getT(){return T;}
    
    public void setId(String id) {this.id=id;}
    public void setItem(String item) {this.item=item;}
    public void setQty(BigDecimal qty) {this.qty=qty;}
    public void setRate(String rate) {this.rate=rate;}
    public void setValue(BigDecimal value) {this.value=value;}
    public void setT(String T) {this.T=T;}
}
