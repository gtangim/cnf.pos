/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
package cnf.pos.component;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Container;
import java.util.HashMap;
import java.util.Map;

import net.xoetrope.swing.XImageButton;
import net.xoetrope.xui.events.XEventHelper;
import net.xoetrope.xui.helper.SwingWorker;

import cnf.pos.util.Debug;
import cnf.pos.config.ButtonEventConfig;
import cnf.pos.screen.PosScreen;
import cnf.pos.util.Utility;

public class PosImageButton {

    public static final String module = PosImageButton.class.getName();

    protected Map<String, PosImageButtonWrapper> loadedXImageButtons = new HashMap<String, PosImageButtonWrapper>();
    protected PosScreen pos = null;

    public PosImageButton(PosScreen pos) {
        this.pos = pos;
        this.loadButtons(pos.getComponents());

        try {
            ButtonEventConfig.loadButtonConfig();
        } catch (Exception e) {
            Debug.logError(e, module);
        }
    }

    private void loadButtons(Component[] component) {
        for (int i = 0; i < component.length; i++) {
            if (component[i] instanceof XImageButton) {
                XImageButton button = (XImageButton) component[i];
                String buttonName = button.getName();
                String styleName = buttonName == null ? null : (String) pos.getAttribute("style", buttonName);
                PosImageButtonWrapper wrapper = new PosImageButtonWrapper(button, styleName);
                if (Utility.isEmpty(buttonName)) {
                    wrapper.setEnabled(false);
                } else {
                    XEventHelper.addActionHandler(pos, button, PosScreen.BUTTON_ACTION_METHOD);
                    loadedXImageButtons.put(button.getName(), wrapper);
                }
            }
            if (component[i] instanceof Container) {
                Component[] subComponents = ((Container) component[i]).getComponents();
                loadButtons(subComponents);
            }
        }
    }

    public boolean isLockable(String name) {
        if (!loadedXImageButtons.containsKey(name)) {
            return false;
        }

        return ButtonEventConfig.isLockable(name);
    }

    public void setLock(boolean lock) {
        for (String buttonName : loadedXImageButtons.keySet()) {
            if (this.isLockable(buttonName) && lock) {
                this.setLock(buttonName, lock);
            } else {
                this.setLock(buttonName, false);
            }
        }
    }

    public void setLock(String buttonName, boolean lock) {
        //try {
        //    Debug.log("Setting Lock for >> " + buttonName);
        //    Thread.sleep(200);
        //} catch (InterruptedException e) {
        //    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        //}
        PosImageButtonWrapper button = (PosImageButtonWrapper) loadedXImageButtons.get(buttonName);
        button.setEnabled(!lock);
    }

    public void buttonPressed(final PosScreen pos, final AWTEvent event) {
        if (pos == null) {
            Debug.logWarning("Received a null PosScreen object in buttonPressed event", module);
            return;
        }
        if (event == null) {
            Debug.logWarning("Received a null AWTEvent object in buttonPressed event", module);
            return;
        }
        final String buttonName = ButtonEventConfig.getButtonName(event);
        final ClassLoader cl = this.getClassLoader(pos);

        if (buttonName != null) {
            final SwingWorker worker = new SwingWorker() {
                @Override
                public Object construct() {
                    PosScreen.lockUI();
                    try {
                        if (cl != null) {
                            Thread.currentThread().setContextClassLoader(cl);
                        }
                        ButtonEventConfig.invokeButtonEvent(buttonName, pos, event);
                    } catch (ButtonEventConfig.ButtonEventNotFound e) {
                        Debug.logWarning(e, "Button not found - " + buttonName, module);
                    } catch (ButtonEventConfig.ButtonEventException e) {
                        Debug.logError(e, "Button invocation exception - " + buttonName, module);
                    }
                    catch (Exception ex)
                    {
                        Debug.logError(ex, buttonName+": Exception while handling button event!", module);
                    }
                    finally {
                        PosScreen.unlockUI();
                    }
                    return null;
                }
            };
            worker.start();
        } else {
            Debug.logWarning("No button name found for buttonPressed event", module);
        }
    }

    private ClassLoader getClassLoader(PosScreen pos) {
        ClassLoader cl = pos.getClassLoader();
        if (cl == null) {
            try {
                cl = Thread.currentThread().getContextClassLoader();
            } catch (Throwable t) {
            }
            if (cl == null) {
                Debug.log("No context classloader available; using class classloader", module);
                try {
                    cl = this.getClass().getClassLoader();
                } catch (Throwable t) {
                    Debug.logError(t, module);
                }
            }
        }

        return cl;
    }
}
