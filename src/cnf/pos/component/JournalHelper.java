package cnf.pos.component;

import java.util.ArrayList;

import cnf.ui.table.ExtendedModel;
import cnf.ui.table.ExtendedTable;

public class JournalHelper {
	public ExtendedTable myTable;
	public ExtendedModel tm;
	public int itemCount;
	public int adjustmentCount;
	public int paymentCount;
	public int refundCount;
	public int nextRowId;
	public ArrayList<Integer> refundItemCount=new ArrayList<Integer>();
	public ArrayList<Integer> refundPaymentCount=new ArrayList<Integer>();
	
	public String getNextRowId() {return Integer.toString(nextRowId++);}

}
