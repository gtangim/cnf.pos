package cnf.ui.table;
import java.lang.reflect.*;
import java.util.*;

import javax.swing.table.*;


public class SimpleModel extends AbstractTableModel{

  private ArrayList Columns = new ArrayList();
  private ArrayList Rows = new ArrayList();

  public SimpleModel(Object entity) {
     Field[] fld = entity.getClass().getDeclaredFields();
     for (int i = 0; i < fld.length; i++) {
      Columns.add(fld[i].getName());
    }
  }

  public void finalize() throws Throwable {
	  super.finalize();
  }

  // Self written
  public Object getRow(int row) {
    return Rows.get(row);
  }

  // Self written
  public void AddRow(Object newVal) {
    Rows.add(newVal);
    fireTableDataChanged();
  }

  // Self written
  public void UpdateRow(int row, Object newVal) {
    if (row >= 0 && row < Columns.size())
    {
      Rows.set(row, newVal);
      fireTableDataChanged();
    }
  }

//self written
  public void DeleteRow(int row) {
	  try{    Rows.remove(row);
    fireTableDataChanged();
  }catch(IndexOutOfBoundsException exp){System.err.println("Error: No Rows to Delete !");}
  }

  // Self written
  public ArrayList getColumns() {
    return Columns;
  }

  // Unnecessary...
  public void setColumns(ArrayList newVal) {
    Columns = newVal;
  }
  
  /**
   *
   * @param obj
   * @param row
   * @param col
   */
  public void setValueAt(Object objval, int row, int column) {
    Object object = new Object();
    Method method = null;

    //System.out.println(objval);

    if (row < Rows.size() && row >= 0) {
      object = Rows.get(row);
    }

    try {
        if (column < Columns.size() && column >= 0) {
          method = object.getClass().getDeclaredMethod("set" +
              Columns.get(column).toString(), new Class[] {});
          method.invoke(object, new Object[] {objval});
        }
      }
      catch (Exception exp) {
    }
    fireTableCellUpdated(row, column);
  }

  /**
   *
   * @param row
   * @param column
   */
  public boolean isCellEditable(int row, int column) {
	  return false;
  }

  public boolean containsValue(Object value)
  {
	  if(Rows.contains(value))
		  return true;
	  else
		  return false;
  }



  
  
  
  
  
  
  
  
  

  public int getRowCount() {
	    return Rows.size();
	  }

	  public int getColumnCount() {
	    return Columns.size();
	  }

	  /**
	   *
	   * @param row
	   * @param column
	   */
	  public Object getValueAt(int row, int column) {
	    Object object = new Object();
	    Object obj = new Object();
	    Method method = null;

	    if (row < Rows.size() && row >= 0) {
	      object = Rows.get(row);
	    }

	    try {
	      if (column < Columns.size() && column >= 0) {
	          try
	          {
	              method = object.getClass().getDeclaredMethod("get" +
	                      Columns.get(column).toString(), new Class[] {});
	              obj = method.invoke(object, new Object[]{});
	          }
	          catch(Exception exp){
	            method = object.getClass().getDeclaredMethod("is" +
	                    Columns.get(column).toString(), new Class[] {});
	            obj = method.invoke(object, new Object[]{});
	          }
	        return obj;
	      }
	      return obj;
	    }
	    catch (Exception exp) {
	      return null;
	    }
	  }












}
