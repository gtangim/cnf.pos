package cnf.ui.table;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import cnf.ui.table.decoration.CustomBorder;
import cnf.ui.table.decoration.SimpleStyle;
import cnf.ui.table.decoration.TableDecorator;
import cnf.ui.table.element.EMC;
import cnf.ui.table.element.RowElement;
import cnf.ui.table.indexer.Index;
import cnf.ui.table.indexer.RowColKey;
import cnf.ui.table.indexer.StyleKey;
import cnf.ui.table.indexer.StyleKey.AddressType;
import cnf.ui.table.renderer.ColumnHeaderDynaRenderer;
import cnf.ui.table.renderer.IExtendedRenderer;

import java.lang.reflect.*;
import java.lang.annotation.Annotation;



public class ExtendedTable extends JScrollPane implements ListSelectionListener{
    protected JTable table;
    protected ExtendedModel tableModel;

    protected TableDecorator decorator=null;
    //protected ImageIcon image = null;
    //protected HashMap<StyleKey,SimpleStyle> keyToStyle = new HashMap<StyleKey,SimpleStyle>(); 
    //protected HashMap<RowColKey,SimpleStyle> coordToStyle = new HashMap<RowColKey,SimpleStyle>();

    public void clear()
    {
    	table.clearSelection();
    	tableModel.clear();
    	if (decorator==null) 
    		{
    			decorator=new TableDecorator();
    			decorator.keyToStyle.clear();
    			decorator.coordToStyle.clear();
    		}
    }
 
    public ExtendedTable(Object entity)
    {
    	decorator = new TableDecorator();
        tableModel = new ExtendedModel(entity);
    	init(entity);
    }
    
    public ExtendedTable(Object entity, TableDecorator decorator, ExtendedModel tableModel)
    {
    	if (decorator==null) decorator = new TableDecorator();
    	if (tableModel==null) tableModel = new ExtendedModel(entity);
    	this.decorator = decorator;
    	this.tableModel = tableModel;
    	init(entity);
    	
    }
    
    protected void init(Object entity) { //(SimpleModel dataModel) {

        // Define the data model for the JTable.
        // Create the JTable and assign it the predefined data model.
    	
    	table = new JTable() {
            public Component prepareRenderer(TableCellRenderer renderer,
                                             int row, int column) {
                
        		Index ind = tableModel.getIndex(row);
        		if (ind!=null)
        		{
                    RowColKey coord = RowColKey.GenerateKeyCached(row,column);
	            	SimpleStyle s;
	            	if (decorator.coordToStyle.containsKey(coord)) s=decorator.coordToStyle.get(coord);
	            	else
	            	{
	            		// Do the cascade....
	            		//Index ind = tableModel.getIndex(row);
	            		//s = new SimpleStyle("background-color:(200,255,230,160);color:(0,0,80);border-top:1 solid (150,220,190); border-bottom:1 solid (100,150,80);border-right:1 dotted (100,100,100)");
	            		//if (row%2==1) 
	            		//	s.merge(new SimpleStyle("background-color:+(35,35,35,0);"));
	            		//if (!ind.getSubRowId().isEmpty()) 
	            		//	s.merge(new SimpleStyle("background-color:-(100,100,100,0);"));
	           			s = new SimpleStyle();
	           			String styleClass = tableModel.getRowStyleClass(ind.getRowId());
	           			Index m_ind = new Index(ind.getRowId(),null);
	           			int rpos = tableModel.getIndex(m_ind);
	           			int srpos=-1;
	           			srpos = tableModel.getRow(m_ind).getIndex(ind);
	           			if (styleClass!=null && !styleClass.isEmpty()) 
	           				s = cascadeStyle(s,styleClass, ind.getRowId(), rpos%2==1 , ind.getSubRowId(), srpos%2==1, column);
	            		            		
	           			decorator.coordToStyle.put(coord, s);
	            	}
					Object c = super.prepareRenderer(renderer, row, column);
					if (c instanceof IExtendedRenderer)
						((IExtendedRenderer)c).setStyle(s);
	                return (Component)c;
        		}
                //if (c instanceof JComponent) {
                //    ((JComponent) c).setOpaque(true);
                //}
                return (Component)super.prepareRenderer(renderer, row, column);
            }
        };
	        
	        
	        
        table.getColumnModel().getSelectionModel().addListSelectionListener(this);
        table.getSelectionModel().addListSelectionListener(this);
        table.setOpaque(false);

        // Assign a custom renderer to column A, so that an icon can
        // be displayed in column A's header.

        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        table.setRowSelectionAllowed(true);

        table.setModel(tableModel);

        ArrayList lst = tableModel.getColumns();
        for (int i = 0; i < lst.size(); i++) {
        	table.getColumnModel().getColumn(i).setIdentifier(lst.get(i));
            table.getColumnModel().getColumn(i).setHeaderValue(lst.get(i));
        }
        
        setViewportView(table);
        getViewport().setOpaque(false);
        setOpaque(false);
        setTableDimensions(new Dimension(250, 180));
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);        
        table.setRowSelectionAllowed(true);

        
        //Annotation annote;

        //Set DefaulTableCellRenderers
        Field[] fld = entity.getClass().getDeclaredFields();
        for (int i = 0; i < fld.length; i++)
        {
           Annotation[] classAnnotations = fld[i].getAnnotations();
          for(Annotation ano:classAnnotations)
          {

              if(ano instanceof EMC){
                  EMC emc = (EMC) ano;
                  //System.out.println("EMC > Renderer "+emc.RendererClass());
                try{
                  Class renderer =Class.forName(emc.RendererClass());
                  Object r = renderer.newInstance();
                  if (r instanceof IExtendedRenderer)
                  {
                	  ((IExtendedRenderer)r).setDefault(emc.Default());
                	  ((IExtendedRenderer)r).setFormat(emc.Format());
                  }
                  getColumn(fld[i].getName()).setCellRenderer((TableCellRenderer)r);
                } catch (IllegalAccessException ex) { ex.printStackTrace();
                } catch (InstantiationException ex) { ex.printStackTrace();
                } catch (ClassNotFoundException ex) { ex.printStackTrace();
                }

                }          		
           }
        }
    }

    /**
     * Sets the Table Resize Property for Column Adjustment.
     *
     * @param RESIZE int
     */
    public void setTableResizeMode(int RESIZE) {
        table.setAutoResizeMode(RESIZE);
    }

    public void setTableModel(ExtendedModel model) {
        tableModel = model;
        table.setModel(tableModel);

        ArrayList lst = model.getColumns();
        for (int i = 0; i < lst.size(); i++) {
            table.getColumnModel().getColumn(i).setIdentifier(lst.get(i));
            table.getColumnModel().getColumn(i).setHeaderValue(lst.get(i));
        }
    }

    public void setTable(JTable table) {
        this.table = table;
    }

    public ExtendedModel getTableModel() {
        return tableModel;
    }

    public JTable getTable() {
        return table;
    }
    public void setTableDimensions(Dimension dimensions) {
        table.setPreferredScrollableViewportSize(dimensions);
    }
    public void setGridColor(Color gridColor) {
        table.setGridColor(gridColor);
    }
    public void setName(String tblName) {
        table.setName(tblName);
    }
    public void setRowHeight(int nHeight) {
        table.setRowHeight(nHeight);
    }
    public void setBackground(ImageIcon imageicon) {
        this.decorator.image = imageicon;
    }
    public void setSelectionMode(int SelectionMode) {
        table.setSelectionMode(SelectionMode);
    }
    public ListSelectionModel getSelectionModel() {
        return table.getSelectionModel();
    }
    public TableColumn getColumn(String ColumnName) {
        return table.getColumn(ColumnName);
    }
    public void addMouseListener(MouseListener ml) {
        table.addMouseListener(ml);
    }
    public void setColumnRenderer(Object objCol, DefaultTableCellRenderer renderer) {
        table.getColumn(objCol).setCellRenderer(renderer);
    }
    public void setColumnHeader(String objCol, int nWidth, ImageIcon imgHeader) {
        TableColumn col = table.getColumn(objCol);
        col.setHeaderRenderer(new ColumnHeaderDynaRenderer(imgHeader));
        table.getColumn(objCol).setPreferredWidth(nWidth);
        table.getColumnModel().getColumn(col.getModelIndex()).setHeaderValue("");
    }
    public void setColumnHeader(String objCol, int nWidth, ImageIcon imgHeader,
                                boolean isCenter, ImageIcon imgbg,
                                ImageIcon imgleft, ImageIcon imgright) {
        TableColumn col = table.getColumn(objCol);
        col.setHeaderRenderer(new ColumnHeaderDynaRenderer(imgHeader, isCenter,
                imgbg, imgleft, imgright));
        table.getColumn(objCol).setPreferredWidth(nWidth);
        table.getColumnModel().getColumn(col.getModelIndex()).setHeaderValue("");
    }
    public void setScrolls(int vsbPolicy, int hsbPolicy) {
        this.setVerticalScrollBarPolicy(vsbPolicy);
        this.setHorizontalScrollBarPolicy(hsbPolicy);
    }
    public void setStyle(String styleClass, AddressType styleTarget, String targetId, String style)
    {
    	try
    	{
    		StyleKey ind = new StyleKey(styleClass,styleTarget,targetId, -1);
    		SimpleStyle st = new SimpleStyle(style);
    		decorator.keyToStyle.put(ind, st);
    		decorator.coordToStyle.clear();
    	}
    	catch(Exception ex)
    	{    		
    	}
    }
    public void setStyle(HashMap<StyleKey,SimpleStyle> styles)
    {
    	//decorator.keyToStyle = styles;
		decorator.keyToStyle = (HashMap<StyleKey,SimpleStyle>)styles.clone();
    	decorator.coordToStyle.clear();
    }
    public SimpleStyle getStyle(String styleClass, AddressType styleTarget, String targetId)
    {
    	try
    	{
    		StyleKey ind = new StyleKey(styleClass,styleTarget,targetId, -1);
    		if (decorator.keyToStyle.containsKey(ind)) return decorator.keyToStyle.get(ind);
    		return null;
    	}
    	catch(Exception ex)
    	{
    		return null;
    	}
    }
    public void removeStyle(String styleClass, AddressType styleTarget, String targetId)
    {
		StyleKey ind = new StyleKey(styleClass,styleTarget,targetId, -1);
		if (decorator.keyToStyle.containsKey(ind)) decorator.keyToStyle.remove(ind);
    }
    
    public void setStyle(String styleClass, AddressType styleTarget, String targetId, String style, int cellIndex)
    {
    	try
    	{
    		StyleKey ind = new StyleKey(styleClass,styleTarget,targetId, cellIndex);
    		SimpleStyle st = new SimpleStyle(style);
    		decorator.keyToStyle.put(ind, st);
    		decorator.coordToStyle.clear();
    	}
    	catch(Exception ex)
    	{    		
    	}
    }
    public SimpleStyle getStyle(String styleClass, AddressType styleTarget, String targetId, int cellIndex)
    {
    	try
    	{
    		StyleKey ind = new StyleKey(styleClass,styleTarget,targetId, -1);
    		if (decorator.keyToStyle.containsKey(ind)) return decorator.keyToStyle.get(ind);
    		return null;
    	}
    	catch(Exception ex)
    	{
    		return null;
    	}
    }
    

    protected SimpleStyle cascadeStyle(SimpleStyle master, String styleClass, String rowId, boolean isRowAlt, String subRowId, boolean isSubRowAlt, int colIndex)
    {
    	StyleKey sk;
    	boolean isColSel = false;
    	boolean isRowSel = false;
    	boolean isSubRowSel = false;
    	boolean isCellSel = false;
    	String colId = tableModel.getColumnName(colIndex);
    	
    	int sc = tableModel.getSelectedColumn();
    	if (sc>=0 && sc==colIndex) isColSel = true;
    	Index sri = tableModel.getSelectedSubRow();
    	if (sri!=null)
    	{
    		if (sri.getRowId().equals(rowId)) isRowSel = true;
    		if (!subRowId.isEmpty() && sri.getSubRowId().equals(subRowId)) isSubRowSel = true;
    		if (isRowSel && isSubRowSel && sc==colIndex) isCellSel=true;
    	}
    	else
    	{
    		sri = tableModel.getSelectedRow();
    		if (sri!=null)
    		{
        		if (sri.getRowId().equals(rowId)) isRowSel = true;
        		isSubRowSel=false;
        		if (isRowSel && subRowId.isEmpty() && sc==colIndex) isCellSel=true;    			
    		}
    	}
    	
    	sk=new StyleKey(styleClass,AddressType.COL,"",-1);
		if (decorator.keyToStyle.containsKey(sk)) master.merge(decorator.keyToStyle.get(sk));
    	if (colIndex%2==1)
    	{
    		sk=new StyleKey(styleClass,AddressType.COL_ALT,"",-1);
        	if (decorator.keyToStyle.containsKey(sk)) master.merge(decorator.keyToStyle.get(sk));
    	}
    	sk=new StyleKey(styleClass,AddressType.COL,colId,-1);
    	if (decorator.keyToStyle.containsKey(sk)) master.merge(decorator.keyToStyle.get(sk));
    	if (colIndex%2==1)
    	{
    		sk=new StyleKey(styleClass,AddressType.COL_ALT,colId,-1);
    		if (decorator.keyToStyle.containsKey(sk)) master.merge(decorator.keyToStyle.get(sk));
    	}
    	if (isColSel)
    	{
    		sk=new StyleKey(styleClass,AddressType.COL_SEL,"",-1);
    		if (decorator.keyToStyle.containsKey(sk)) master.merge(decorator.keyToStyle.get(sk));
    	}
    	
    	
    	sk=new StyleKey(styleClass,AddressType.ROW,"",-1);
    	if (decorator.keyToStyle.containsKey(sk)) master.merge(decorator.keyToStyle.get(sk));
    	if (isRowAlt)
    	{
    		sk=new StyleKey(styleClass,AddressType.ROW_ALT,"",-1);
    		if (decorator.keyToStyle.containsKey(sk)) master.merge(decorator.keyToStyle.get(sk));
    	}
    	sk=new StyleKey(styleClass,AddressType.ROW,rowId,-1);
    	if (decorator.keyToStyle.containsKey(sk)) master.merge(decorator.keyToStyle.get(sk));
    	if (isRowAlt)
    	{
    		sk=new StyleKey(styleClass,AddressType.ROW_ALT,rowId,-1);
    		if (decorator.keyToStyle.containsKey(sk)) master.merge(decorator.keyToStyle.get(sk));
    	}
    	if (isRowSel)
    	{
    		sk=new StyleKey(styleClass,AddressType.ROW_SEL,"",-1);
    		if (decorator.keyToStyle.containsKey(sk)) master.merge(decorator.keyToStyle.get(sk));
    	}
    	
    	
    	if (subRowId!=null && !subRowId.isEmpty())
    	{
        	sk=new StyleKey(styleClass,AddressType.SUB_ROW,"",-1);
        	if (decorator.keyToStyle.containsKey(sk)) master.merge(decorator.keyToStyle.get(sk));
        	if (isSubRowAlt)
        	{
        		sk=new StyleKey(styleClass,AddressType.SUB_ROW_ALT,"",-1);
        		if (decorator.keyToStyle.containsKey(sk)) master.merge(decorator.keyToStyle.get(sk));
        	}
        	sk=new StyleKey(styleClass,AddressType.SUB_ROW,subRowId,-1);
        	if (decorator.keyToStyle.containsKey(sk)) master.merge(decorator.keyToStyle.get(sk));
        	if (isSubRowAlt)
        	{
        		sk=new StyleKey(styleClass,AddressType.SUB_ROW_ALT,subRowId,-1);
        		if (decorator.keyToStyle.containsKey(sk)) master.merge(decorator.keyToStyle.get(sk));    		
        	}
        	if (isSubRowSel)
        	{
        		sk=new StyleKey(styleClass,AddressType.SUB_ROW_SEL,"",-1);
        		if (decorator.keyToStyle.containsKey(sk)) master.merge(decorator.keyToStyle.get(sk));
        	}
    	}
    	
    	if (colIndex>=0)
    	{
        	sk=new StyleKey(styleClass,AddressType.CELL,subRowId,-1);
        	if (decorator.keyToStyle.containsKey(sk)) master.merge(decorator.keyToStyle.get(sk));    		
        	if (isCellSel)
        	{
        		sk=new StyleKey(styleClass,AddressType.CELL_SEL,"",-1);
        		if (decorator.keyToStyle.containsKey(sk)) master.merge(decorator.keyToStyle.get(sk));
        	}
    	}
    	
    	return master;
    }
    
    
    public void paint(Graphics g) {
        if (decorator.image != null) {
            Rectangle d = getViewport().getViewRect();
            for (int x = 0; x < d.width; x += decorator.image.getIconWidth()) {
                for (int y = 0; y < d.height; y += decorator.image.getIconHeight()) {
                    g.drawImage(decorator.image.getImage(), x, y, null, null);
                }
            }
        }
        // Now paint the inner components (Table)
        super.paint(g);
    }

    public void scrollToVisible(int rowIndex, int vColIndex) {
        if (!(table.getParent() instanceof JViewport)) {
            return;
        }
        JViewport viewport = (JViewport)table.getParent();
        Rectangle rect = table.getCellRect(rowIndex, vColIndex, true);
        Point pt = viewport.getViewPosition();
        rect.setLocation(rect.x-pt.x, rect.y+40-pt.y);
        viewport.scrollRectToVisible(rect);
    }
    
    public void scrollToBottom()
    {
        Rectangle rect = table.getCellRect(tableModel.getRowCount()-1, 0, true);        
        rect.y+=table.getRowHeight();
        this.getViewport().setViewPosition(rect.getLocation());
    }
    
    // Selection handler
	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getValueIsAdjusting()==false)
		{
		if (e.getSource() == table.getSelectionModel()
                && table.getRowSelectionAllowed()) {
            // Row selection changed
			decorator.coordToStyle.clear();
            // Row selection changed
            int sel = table.getSelectedRow();
            tableModel.selectRow(sel);
            table.repaint();
            //System.out.println("Row Selected");
          } else if (e.getSource() == table.getColumnModel().getSelectionModel()
                 && table.getColumnSelectionAllowed() ){
        	  decorator.coordToStyle.clear();
              int sel = table.getSelectedColumn();
              tableModel.selectCol(sel);
              table.repaint();
              //System.out.println("Col Selected");
          }
		}
	}
	
	public TableDecorator getDecorator() {return decorator;}

}
