package cnf.ui.table.element;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface EMC
{
    String RendererClass() default "NONE";
    String Format() default "";
    String Style() default "";
    String Default() default "";
}
