package cnf.ui.table.element;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;

public final class EntityContainer {
	protected Object dataEntity;
	protected HashMap<String,Object> idToEntity;
	protected HashMap<String,Integer> idToIndex;
	protected Object[] values;
	
	public EntityContainer(Object entity)
	{	
		if (entity!=null)
		{
			this.dataEntity = entity;
	        Field[] fld = dataEntity.getClass().getDeclaredFields();
	        values = new Object[fld.length];
	        idToEntity = new HashMap<String,Object>();
	        idToIndex = new HashMap<String,Integer>();
	        int n = fld.length;
	        for (int i =0;i<n;i++)
	        {
	        	String f = fld[i].getName();
	        	values[i] = getFieldValue(f);
	        	idToEntity.put(f, values[i]);
	        	idToIndex.put(f, i);
	        }			
		}
	}
	
	public Object getEntity(){return dataEntity;}
	public Object getCell(String Id) {if (idToEntity.containsKey(Id)) return idToEntity.get(Id); return null;}
	public Object getCell(int ind) {if (ind<0 || ind>=values.length) return null; return values[ind];}
	public boolean setCell(String Id, Object value)
	{
		if (!idToEntity.containsKey(Id)) return false;
		idToEntity.put(Id, value);
		int ind = idToIndex.get(Id);
		values[ind]=value;
		try{
			Id = Character.toUpperCase(Id.charAt(0)) + Id.substring(1);
			Method method = dataEntity.getClass().getDeclaredMethod("set" +
                Id, new Class[] {});
            method.invoke(dataEntity, new Object[] {value});
		}
		catch(Exception ex){return false;}
		return true;
	}
	protected Object getFieldValue(String fid)
	{
		fid = Character.toUpperCase(fid.charAt(0)) + fid.substring(1);
		Object obj=null;
        try
        {
            Method method = dataEntity.getClass().getDeclaredMethod("get" +
                    fid, new Class[] {});
            obj = method.invoke(dataEntity, new Object[]{});
        }
        catch(Exception exp){
          try
          {
              Method method = dataEntity.getClass().getDeclaredMethod("get" +
                      fid, new Class[] {});
              obj = method.invoke(dataEntity, new Object[]{});
          } catch(Exception ex) {}
        }
		return obj;
	}
}
