package cnf.ui.table.element;

import java.util.ArrayList;
import java.util.HashMap;

import cnf.ui.table.indexer.Index;

public class RowElement {
	Index index;
	EntityContainer master;
	ArrayList<Index> srowIndexes;
	ArrayList<EntityContainer> srows;
	HashMap<Index,EntityContainer> indexToSrow;
	
	public RowElement(Index rowIndex, Object entity)
	{
		master = new EntityContainer(entity);
		index = rowIndex;
		srowIndexes = new ArrayList<Index>();
		srows=new ArrayList<EntityContainer>();
		indexToSrow=new HashMap<Index,EntityContainer>();
	}
	public int Count() {return srows.size()+1;}
	public Index getIndex(){return index;}
	public Index getIndex(int combinedIndex){if (combinedIndex<0 || combinedIndex>srows.size()) return null; if (combinedIndex==0) return index; return srowIndexes.get(combinedIndex-1);}
	public int getIndex(Index ind) {if (ind.equals(index)) return 0; return srowIndexes.indexOf(ind);}
	public EntityContainer getRowEntityConainter() {return master;}
	public Object getRow() {return master.getEntity();}
	public Object getCell(String Id) {return master.getCell(Id);}
	public Object getCell(int ind) {return master.getCell(ind);}
	public boolean setCell(String Id, Object value) {return master.setCell(Id, value);}
	public EntityContainer getRowEntityContainerByIndex(int combinedIndex)
	{
		if (combinedIndex<0 || combinedIndex>srows.size()) return null;
		if (combinedIndex==0) return master;
		return srows.get(combinedIndex-1);
	}
	public Object getRowByIndex(int combinedIndex)
	{
		if (combinedIndex<0 || combinedIndex>srows.size()) return null;
		if (combinedIndex==0) return master.getEntity();
		return srows.get(combinedIndex-1).getEntity();
	}
	public EntityContainer getSubRowEntityContainer(Index srIndex) 
	{
		if (indexToSrow.containsKey(srIndex)) return indexToSrow.get(srIndex);
		return null;
	}
	public EntityContainer getSubRowEntityContainer(int srIndex) 
	{
		if (srIndex<0 || srIndex>=srows.size()) return null;
		return srows.get(srIndex);
	}
	public Object getSubRow(Index srIndex)
	{
		if (indexToSrow.containsKey(srIndex)) return indexToSrow.get(srIndex).getEntity();
		return null;		
	}
	public Object getSubRow(int srIndex) 
	{
		if (srIndex<0 || srIndex>=srows.size()) return null;
		return srows.get(srIndex).getEntity();
	}
	public Object getSubRowCell(Index srIndex, String colId)
	{
		if (indexToSrow.containsKey(srIndex)) return indexToSrow.get(srIndex).getCell(colId);
		return null;		
	}
	public Object getSubRowCell(Index srIndex, int colInd)
	{
		if (indexToSrow.containsKey(srIndex)) return indexToSrow.get(srIndex).getCell(colInd);
		return null;		
	}
	public Object getSubRowCell(int srIndex, String colId) 
	{
		if (srIndex<0 || srIndex>=srows.size()) return null;
		return srows.get(srIndex).getCell(colId);
	}
	public Object getSubRowCell(int srIndex, int colInd) 
	{
		if (srIndex<0 || srIndex>=srows.size()) return null;
		return srows.get(srIndex).getCell(colInd);
	}
	public boolean setSubRowCell(Index srIndex, String colId, Object value)
	{
		if (!indexToSrow.containsKey(srIndex)) return false;
		return indexToSrow.get(srIndex).setCell(colId, value);
	}
	public boolean setSubRowCell(int srIndex, String colId, Object value)
	{
		if (srIndex<0 || srIndex>=srows.size()) return false;
		return srows.get(srIndex).setCell(colId, value);
	}
	
	
	public Index indexOf(int ind) 
	{
		if (ind<0 || ind>=srows.size()) return null;
		return srowIndexes.get(ind);		
	}
	
	public boolean addSubRow(String subRowId, Object entity)
	{
		Index ind = new Index(index.getRowId(), subRowId);
		return addSubRow(ind,entity);
	}
	public boolean addSubRow(Index ind, Object entity)
	{
		if (indexToSrow.containsKey(ind)) return false;
		EntityContainer ec = new EntityContainer(entity);
		srowIndexes.add(ind);
		srows.add(ec);
		indexToSrow.put(ind, ec);
		return true;		
	}
	
	
	public boolean removeSubRow(String subRowId)
	{
		Index ind = new Index(index.getRowId(), subRowId);
		return removeSubRow(ind);
	}
	public boolean removeSubRow(Index ind)
	{
		if (!indexToSrow.containsKey(ind)) return false;
		int i = srowIndexes.indexOf(ind);
		if (i==-1) return false;
		srowIndexes.remove(i);
		srows.remove(i);
		indexToSrow.remove(ind);
		return true;
	}
	public boolean removeSubRow(int subRowInd)
	{
		if (subRowInd<0 || subRowInd>=srows.size()) return false;
		if (indexToSrow.containsKey(srowIndexes.get(subRowInd)))
			indexToSrow.remove(srowIndexes.get(subRowInd));
		srowIndexes.remove(subRowInd);
		srows.remove(subRowInd);
		return true;
	}
	
	public boolean insertSubRow(int pos, String subRowId, Object entity)
	{
		Index ind = new Index(index.getRowId(),subRowId);
		return insertSubRow(pos,ind,entity);
	}
	public boolean insertSubRow(int pos, Index ind, Object entity)
	{
		if (pos<0) pos=0;
		else if (pos>srows.size()) pos=srows.size();
		if (indexToSrow.containsKey(ind)) return false;
		EntityContainer ec = new EntityContainer(entity);
		srowIndexes.add(pos, ind);
		srows.add(pos, ec);
		indexToSrow.put(ind, ec);
		return true;				
	}
	public boolean insertSubRowBefore(String existingSrowId, String newSubRowId, Object entity)
	{
		Index ind1 = new Index(index.getRowId(),existingSrowId);
		Index ind2 = new Index(index.getRowId(),newSubRowId);
		return insertSubRowBefore(ind1,ind2,entity);
	}
	public boolean insertSubRowBefore(Index existingSrowIndex, String newSubRowId, Object entity)
	{
		Index ind = new Index(index.getRowId(),newSubRowId);
		return insertSubRowBefore(existingSrowIndex,ind,entity);
	}
	public boolean insertSubRowBefore(String existingSrowId, Index newSubRowIndex, Object entity)
	{
		Index ind = new Index(index.getRowId(),existingSrowId);
		return insertSubRowBefore(ind,newSubRowIndex,entity);
	}
	public boolean insertSubRowBefore(Index existingSrowIndex, Index newSubRowIndex, Object entity)
	{
		if (indexToSrow.containsKey(newSubRowIndex)) return false;
		if (!indexToSrow.containsKey(existingSrowIndex)) return false;
		EntityContainer ec = new EntityContainer(entity);
		int pos = srowIndexes.indexOf(existingSrowIndex);
		if (pos==-1) return false;
		srowIndexes.add(pos, newSubRowIndex);
		srows.add(pos, ec);
		indexToSrow.put(newSubRowIndex, ec);
		return true;				
	}

	
	public boolean insertSubRowAfter(String existingSrowId, String newSubRowId, Object entity)
	{
		Index ind1 = new Index(index.getRowId(),existingSrowId);
		Index ind2 = new Index(index.getRowId(),newSubRowId);
		return insertSubRowAfter(ind1,ind2,entity);
	}	
	public boolean insertSubRowAfter(Index existingSrowIndex, String newSubRowId, Object entity)
	{
		Index ind = new Index(index.getRowId(),newSubRowId);
		return insertSubRowAfter(existingSrowIndex,ind,entity);
	}
	public boolean insertSubRowAfter(String existingSrowId, Index newSubRowIndex, Object entity)
	{
		Index ind = new Index(index.getRowId(),existingSrowId);
		return insertSubRowAfter(ind,newSubRowIndex,entity);
	}
	public boolean insertSubRowAfter(Index existingSrowIndex, Index newSubRowIndex, Object entity)
	{
		if (indexToSrow.containsKey(newSubRowIndex)) return false;
		if (!indexToSrow.containsKey(existingSrowIndex)) return false;
		EntityContainer ec = new EntityContainer(entity);
		int pos = srowIndexes.indexOf(existingSrowIndex);
		if (pos==-1) return false;
		pos++;
		srowIndexes.add(pos, newSubRowIndex);
		srows.add(pos, ec);
		indexToSrow.put(newSubRowIndex, ec);
		return true;				
	}
}
