package cnf.ui.table;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import cnf.ui.table.decoration.CustomBorder;
import cnf.ui.table.decoration.SimpleStyle;
import cnf.ui.table.element.EMC;
import cnf.ui.table.indexer.RowColKey;
import cnf.ui.table.renderer.ColumnHeaderDynaRenderer;
import cnf.ui.table.renderer.IExtendedRenderer;

import java.lang.reflect.*;
import java.lang.annotation.Annotation;


/**
 *
 * The SimpleTable is part of the EMC Framework.  Entity Modeled Components are created
 * by generating components based on the entity provided.  The SimpleTable generates columns
 * based on the entity object passed in as the parameter to the constructor.
 */

public class SimpleTable extends JScrollPane {
    private JTable table;
    private SimpleModel tableModel;
    private ImageIcon image = null;
    private int currentRow = 0;
    
    protected HashMap<RowColKey,SimpleStyle> coordToStyle = new HashMap<RowColKey,SimpleStyle>();

    /**
     * The SimpleTable constructor takes a single parameter of an entity object.
     * An Entity Object is a java class which has attributes with getters and setters.
     * The table renders the attributes in the class as columns.
     *
     * @param entity Object
     */
    public SimpleTable(Object entity) { //(SimpleModel dataModel) {

        // Define the data model for the JTable.
        // Create the JTable and assign it the predefined data model.
        tableModel = new SimpleModel(entity);

        table = new JTable() {
            public Component prepareRenderer(TableCellRenderer renderer,
                                             int row, int column) {
                
            	
            	RowColKey coord = new RowColKey(row,column);
            	SimpleStyle s;
            	if (coordToStyle.containsKey(coord)) s=coordToStyle.get(coord);
            	else
            	{
            		// Do the cascade....
            		s = new SimpleStyle("background-color:(200,255,230,80);color:(0,0,80);border-top:1 solid (150,220,190); border-bottom:1 solid (100,150,80);border-right:1 dotted (100,100,100)");
            		if (row%2==1) 
            			s.merge(new SimpleStyle("background-color:+(35,35,35,0);"));
            		coordToStyle.put(coord, s);
            	}
				Object c = super.prepareRenderer(renderer, row, column);
				if (c instanceof IExtendedRenderer)
					((IExtendedRenderer)c).setStyle(s);
				
                //if (c instanceof JComponent) {
                //    ((JComponent) c).setOpaque(true);
                //}
                return (Component)c;
            }
        };

        table.setOpaque(false);

        // Assign a custom renderer to column A, so that an icon can
        // be displayed in column A's header.

        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        table.setRowSelectionAllowed(true);

        table.setModel(tableModel);

        ArrayList lst = tableModel.getColumns();
        for (int i = 0; i < lst.size(); i++) {
        	table.getColumnModel().getColumn(i).setIdentifier(lst.get(i));
            table.getColumnModel().getColumn(i).setHeaderValue(lst.get(i));
        }
        
        setViewportView(table);
        getViewport().setOpaque(false);
        setOpaque(false);
        setTableDimensions(new Dimension(250, 180));
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);        
        table.setRowSelectionAllowed(true);

        
        //Annotation annote;

        //Set DefaulTableCellRenderers
        Field[] fld = entity.getClass().getDeclaredFields();
        for (int i = 0; i < fld.length; i++)
        {
           Annotation[] classAnnotations = fld[i].getAnnotations();
          for(Annotation ano:classAnnotations)
          {

              if(ano instanceof EMC){
                  EMC emc = (EMC) ano;
                  //System.out.println("EMC > Renderer "+emc.RendererClass());
                try{
                  Class renderer =Class.forName(emc.RendererClass());
                  Object r = renderer.newInstance();
                  if (r instanceof IExtendedRenderer)
                  {
                	  ((IExtendedRenderer)r).setDefault(emc.Default());
                  }
                	  
                  getColumn(fld[i].getName()).setCellRenderer((TableCellRenderer)renderer.newInstance());
                } catch (IllegalAccessException ex) { ex.printStackTrace();
                } catch (InstantiationException ex) { ex.printStackTrace();
                } catch (ClassNotFoundException ex) { ex.printStackTrace();
                }

                }
           }
        }

    }

    /**
     * Sets the Table Resize Property for Column Adjustment.
     *
     * @param RESIZE int
     */
    public void setTableResizeMode(int RESIZE) {
        table.setAutoResizeMode(RESIZE);
    }

    /**
     * Sets the TableModel object which in turn is used to manipulate the data contents in the table entity objects.
     * @param model SimpleModel
     */
    public void setTableModel(SimpleModel model) {
        tableModel = model;
        table.setModel(tableModel);

        ArrayList lst = model.getColumns();
        for (int i = 0; i < lst.size(); i++) {
            table.getColumnModel().getColumn(i).setIdentifier(lst.get(i));
            table.getColumnModel().getColumn(i).setHeaderValue(lst.get(i));
        }
    }

    /**
     * Setting the default JTable.
     * @param table JTable
     */
    public void setTable(JTable table) {
        this.table = table;
    }

    /**
     * Setting current row selection of the table.
     * @param currentRow int
     */
    public void setCurrentRow(int currentRow) {
        this.currentRow = currentRow;
    }

    /**
     * Used to obtain the current SimpleModel (TableModel) object.
     * @return SimpleModel
     */
    public SimpleModel getTableModel() {
        return tableModel;
    }

    /**
     * Returns the JTable Object to be displayed.
     * @return JTable
     */
    public JTable getTable() {
        return table;
    }

    /**
     * Returns the current row object.
     * @return int
     */
    public int getCurrentRow() {
        return currentRow;
    }

    /**
     * Used to set the default table size.
     * @param dimensions Dimension
     */
    public void setTableDimensions(Dimension dimensions) {
        table.setPreferredScrollableViewportSize(dimensions);
    }

    /**
     * Sets the default gird color of the table.
     * @param gridColor Color
     */
    public void setGridColor(Color gridColor) {
        table.setGridColor(gridColor);
    }

    /**
     * Sets the table Name property.
     * @param tblName String
     */
    public void setName(String tblName) {
        table.setName(tblName);
    }

    /**
     * Sets the row heignt of the table.
     * @param nHeight int
     */
    public void setRowHeight(int nHeight) {
        table.setRowHeight(nHeight);
    }

    /**
     * Sets the background image of the table object
     * @param imageicon ImageIcon
     */
    public void setBackground(ImageIcon imageicon) {
        setBackgroundImage(imageicon);
    }

    /**
     * Sets the current Table Selection Mode.
     * @param SelectionMode int
     */
    public void setSelectionMode(int SelectionMode) {
        table.setSelectionMode(SelectionMode);
    }

    /**
     * Obtains the currents selection model.
     * @return ListSelectionModel
     */
    public ListSelectionModel getSelectionModel() {
        return table.getSelectionModel();
    }

    /**
     * Returns a TableColumn based on the ColomnName
     *
     * @param ColumnName String
     * @return TableColumn
     */
    public TableColumn getColumn(String ColumnName) {
        return table.getColumn(ColumnName);
    }

    /**
     * Adds a Mouse Listener to a Table object.
     * @param ml MouseListener
     */
    public void addMouseListener(MouseListener ml) {
        table.addMouseListener(ml);
    }

    /**
     * Returns the currently selected row in the table.
     * @return int
     */
    public int getSelectedRow() {
        return table.getSelectedRow();
    }

    /**
     * Set a Custom Renderer for a Column using DefaultTableCellRenderer.
     * A CheckBoxRenderer is provided as sample.
     *
     * @param objCol Object
     * @param renderer DefaultTableCellRenderer
     */
    public void setColumnRenderer(Object objCol, DefaultTableCellRenderer renderer) {
        table.getColumn(objCol).setCellRenderer(renderer);
    }


    /**
     * A Column Heading is provided as Image with the, the width of the column,
     * the column Name as string.  The column header text is replaced with an image.
     *
     * @param objCol String
     * @param nWidth int
     * @param imgHeader ImageIcon
     */
    public void setColumnHeader(String objCol, int nWidth, ImageIcon imgHeader) {
        TableColumn col = table.getColumn(objCol);
        col.setHeaderRenderer(new ColumnHeaderDynaRenderer(imgHeader));
        table.getColumn(objCol).setPreferredWidth(nWidth);
        table.getColumnModel().getColumn(col.getModelIndex()).setHeaderValue("");
    }

    /**
     * An Advanced column header object is created.  The column name is mentioned, along with the width,
     * and ImageIcon of the Header, with a baground image for the header, a left corner image, and a right corner image.
     * <br>
     *[LAYER 1] [ left corner image]     [image heading]      [right corner image]<br>
     *[LAYER 2] [  b   a   c   k   g   r   o   u   n   d    -     i   m   a   g   e  ]
     * <br>
     * @param objCol String
     * @param nWidth int
     * @param imgHeader ImageIcon
     * @param isCenter boolean
     * @param imgbg ImageIcon
     * @param imgleft ImageIcon
     * @param imgright ImageIcon
     */
    public void setColumnHeader(String objCol, int nWidth, ImageIcon imgHeader,
                                boolean isCenter, ImageIcon imgbg,
                                ImageIcon imgleft, ImageIcon imgright) {
        TableColumn col = table.getColumn(objCol);
        col.setHeaderRenderer(new ColumnHeaderDynaRenderer(imgHeader, isCenter,
                imgbg, imgleft, imgright));
        table.getColumn(objCol).setPreferredWidth(nWidth);
        table.getColumnModel().getColumn(col.getModelIndex()).setHeaderValue("");
    }


    /**
     * The baground image supplied will be painted as tiles.
     * @param g Graphics
     */
    public void paint(Graphics g) {
        if (image != null) {
            // Draw the background image
            Rectangle d = getViewport().getViewRect();
            for (int x = 0; x < d.width; x += image.getIconWidth()) {
                for (int y = 0; y < d.height; y += image.getIconHeight()) {
                    g.drawImage(image.getImage(), x, y, null, null);
                }
            }
            // Do not use cached image for scrolling
            // getViewport().setBackingStoreEnabled(false);
        }
        super.paint(g);
    }

    /**
     * Sets the background image of the table.
     * @param image ImageIcon
     */
    public void setBackgroundImage(ImageIcon image) {
        this.image = image;
    }

    //DataModel Proxy Pattern
    /**
     * Gets a row from the table model.
     * @param row int
     * @return Object
     */
    public Object getRow(int row) {
        return tableModel.getRow(row);
    }

    /**
     * Retuns a value as object for given row and column.
     * @param row int
     * @param column int
     * @return Object
     */
    public Object getValueAt(int row, int column) {
        return tableModel.getValueAt(row, column);
    }

    /**
     * Updates a row with an entity object provided as parameter
     * @param row int
     * @param newVal Object
     */
    public void UpdateRow(int row, Object newVal) {
        tableModel.UpdateRow(row, newVal);
    }

    /**
     * Gets the table row count from the model object.
     * @return int
     */
    public int getRowCount() {
        return tableModel.getRowCount();
    }

    /**
     * Adds an entity object to the table by updating table model.
     * @param newVal Object
     */
    public void AddRow(Object newVal) {
        tableModel.AddRow(newVal);
    }

    /**
     * Removes an entity object from the table by removing entity from model.
     * @param row int
     */
    public void DeleteRow(int row) {
        tableModel.DeleteRow(row);
    }

    /**
     * Checks if an entity is present in a table.
     * @param value Object
     * @return boolean
     */
    public boolean containsValue(Object value) {
        return tableModel.containsValue(value);
    }

    /**
     * Sets the Horizontal and Vertical ScrollBar Policies.
     * @param vsbPolicy int
     * @param hsbPolicy int
     */
    public void setScrolls(int vsbPolicy, int hsbPolicy) {
        this.setVerticalScrollBarPolicy(vsbPolicy);
        this.setHorizontalScrollBarPolicy(hsbPolicy);
    }
}
