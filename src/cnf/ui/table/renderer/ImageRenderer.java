package cnf.ui.table.renderer;

/**
 * <p>Title: Simple Table</p>
 *
 * <p>Description: An Entity Modeled Swing Table Component</p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: Christalign Innovative Solutions Pvt. Ltd.</p>
 *
 * @author Ruben Gerad Mathew
 * @version 1.0
 */


import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.ImageIcon;

import cnf.ui.table.decoration.SimpleStyle;

import java.awt.Image;
import java.net.URL;

public class ImageRenderer extends JLabel implements TableCellRenderer,IExtendedRenderer {
    protected ImageIcon imageIcon = null;
    protected ExtendedRendererHelper rh = new ExtendedRendererHelper();
    protected String defaultvalue=null;
    
    public ImageRenderer() {
        this.setOpaque(true);
      }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
    	
            if( value!=null )
            {
                if(value instanceof String){
                    imageIcon = new ImageIcon(value.toString());
                    setIcon(imageIcon);
                }
                else if(value instanceof Image )
                 imageIcon = new ImageIcon((Image)value);
                else if( value instanceof URL )
                imageIcon = new ImageIcon((URL)value);
                else
                {
                	if (defaultvalue!=null)
                		imageIcon = new ImageIcon(defaultvalue);
                	else setText("");
                }
                if(imageIcon!=null)
                    setIcon(imageIcon);
            }
            else
            {
                setText("");
            }
            return this;
    }

	@Override
	public String getFormat() {
		return rh.getFormat();
	}

	@Override
	public SimpleStyle getStyle() {
		return rh.getStyle();
	}

	@Override
	public void setFormat(String format) {
		rh.setFormat(format);
	}

	@Override
	public void setStyle(SimpleStyle style) {
		rh.setStyle(style);
		decorate();
	}
	
	private void decorate()
	{
		rh.stylizeComponent(this);
		this.setHorizontalAlignment(rh.getStyle().getHorizontalAlignment());
		this.setVerticalAlignment(rh.getStyle().getVerticalAlignment());		
	}
	@Override
	public void setDefault(String dvalue)
	{
		defaultvalue=dvalue;
	}

}

