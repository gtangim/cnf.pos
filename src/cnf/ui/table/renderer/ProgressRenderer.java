package cnf.ui.table.renderer;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import cnf.ui.table.decoration.SimpleStyle;

public class ProgressRenderer extends JPanel implements TableCellRenderer, IExtendedRenderer
{
        private JProgressBar inner;
        protected ExtendedRendererHelper rh = new ExtendedRendererHelper();
        
        public ProgressRenderer() {
            inner = new JProgressBar(0,1000);
            inner.setStringPainted(true);
            inner.setValue(0);
            inner.setString("0%");
            this.add(inner);
        }
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
        {
        	long v=0;
        	if (value instanceof Integer)
        	{
        		v = (long)((Integer)value);
        		if (v<0) v=0;
        		else if (v>1000) v=1000;        		
        	}
        	else if (value instanceof Long)
        	{
        		v = (Long)value;
        		if (v<0) v=0;
        		else if (v>1000) v=1000;        		
        	}
        	else if (value instanceof Float)
        	{
        		v = (int)Math.round(((Float)value)*1000.0);
        		if (v<0) v=0;
        		else if (v>1000) v=1000;        		
        	}
        	else if (value instanceof Double)
        	{
        		v = (int)Math.round(((Double)value)*1000.0);
        		if (v<0) v=0;
        		else if (v>1000) v=1000;        		
        	}
        	inner.setValue((int)v);
        	inner.setString(Long.toString(v/10)+"."+Long.toString(v%10)+"%");
        	Dimension d = new Dimension(table.getColumnModel().getColumn(column).getWidth()-10,table.getRowHeight()-10);
        	inner.setMinimumSize(d);
        	inner.setPreferredSize(d);
        	inner.setMaximumSize(d);
        	
        	return this;
        }
		@Override
		public String getFormat() {
			return rh.getFormat();
		}
		@Override
		public SimpleStyle getStyle() {
			return rh.getStyle();
		}
		@Override
		public void setFormat(String format) {
			rh.setFormat(format);
		}
		@Override
		public void setStyle(SimpleStyle style) {
			rh.setStyle(style);
			decorate();
		}
		private void decorate()
		{
			rh.stylizeComponent(this);
		}
		
		@Override
		public void setDefault(String dvalue)
		{
		}
		
}
