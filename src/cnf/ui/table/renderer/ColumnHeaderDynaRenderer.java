package cnf.ui.table.renderer;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;

import  javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.table.TableCellRenderer;

public class ColumnHeaderDynaRenderer extends JPanel implements TableCellRenderer {
	 // private JPanel pnlHeading = new JPanel();
	  private Image imgTitle = null;
	  private Image imgHeader = null;
	  private Image imgRight = null;
	  private Image imgLeft = null;
	  private JLabel lblLeft = null;
	  private boolean isCenter = false;
	  private boolean isPaintable=false;
	  private String strMessage;

	  public ColumnHeaderDynaRenderer(ImageIcon imgHeader)
		{
		    setLayout(new BorderLayout());
		    lblLeft = new JLabel(imgHeader);
		    lblLeft.setHorizontalAlignment(JLabel.LEFT);
		    add(lblLeft, BorderLayout.CENTER);
		}

	public ColumnHeaderDynaRenderer(ImageIcon Heading, boolean center, ImageIcon TitleBg, ImageIcon ImageLeft, ImageIcon ImageRight)
	{
            isCenter = center;
	    imgTitle  =TitleBg.getImage();
	    imgHeader =Heading.getImage();
	    imgLeft   =ImageLeft.getImage();
	    imgRight  =ImageRight.getImage();
	    lblLeft   = new JLabel(new ImageIcon(imgLeft));
	    lblLeft.setBounds(0,0,imgLeft.getWidth(this), imgLeft.getHeight(this));

	    setLayout(new BorderLayout());
	    lblLeft.setHorizontalAlignment(JLabel.LEFT);
	    add(lblLeft, BorderLayout.NORTH);
	    isPaintable=true;
	}

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			// Extract the original header renderer for this column.

			TableCellRenderer tcr = table.getTableHeader().getDefaultRenderer();

			// Extract the component used to render the column header.

			Component c = tcr.getTableCellRendererComponent(table, value,
					isSelected, hasFocus, row, column);

			// Establish the font, foreground color, and border for the
			// JLabel so that the rendered header will look the same as the
			// other rendered headers.

			setFont(c.getFont());
			setForeground(c.getForeground());
			setBorder(BorderFactory.createEmptyBorder());
			//setBorder(((JComponent) c).getBorder());


			// Establish the column name.
			if(imgHeader==null)
			lblLeft.setText((String) value);

			// Return the cached JLabel a the renderer for this column
			// header.

			return this;
	}

	protected void paintComponent(Graphics g)
	{
		if(isPaintable){
	  int width = getWidth();
	  int height = getHeight();

	  int imageW = imgTitle.getWidth(this);
	  int imageH = imgTitle.getHeight(this);

	  // Tile the image to fill our area.
	  for (int x = 0; x < width; x += imageW) {
	//      for (int y = 0; y < height; y += imageH) {
	          g.drawImage(imgTitle, x, 0, this);
	 //    }
	  }

	  if(imgLeft !=null && imgRight !=null)
	  {
	    g.drawImage(imgLeft, 0,0, this);
	    g.drawImage(imgRight, (width-imgRight.getWidth(this)), 0, this);
	  }

	  if(imgHeader!=null)
		  if(isCenter)
			  g.drawImage(imgHeader, (width-imgHeader.getWidth(this))/2, 0, this);
		  else
			  g.drawImage(imgHeader, imgLeft.getWidth(this), 0, this);
	  else
	    g.drawString(strMessage, imgLeft.getWidth(this), 18);
		}
	}

}

//import javax.swing.*;
//import javax.swing.table.TableCellRenderer;
//
//import java.awt.*;
///**
// * <p>Title: Generic Analyst Work Bench</p>
// *
// * <p>Description: Source Analysis Tool</p>
// *
// * <p>Copyright: Copyright (c) 2008</p>
// *
// * <p>Company: ADA Software Group</p>
// *
// * @author Ruben Gerad Mathew
// * @version 1.0
// */
//public class TitleBar extends JPanel
//{
//
////  public TitleBar(String TitleBg)
////  {
////    imgTitle =new ImageIcon(com.adasoftware.awb.TitleBar.class.getResource(TitleBg)).getImage();
////  }
////
////  public TitleBar(String TitleBg, String Heading)
////  {
////    imgTitle =new ImageIcon(com.adasoftware.awb.TitleBar.class.getResource(TitleBg)).getImage();
////    imgHeader =new ImageIcon(com.adasoftware.awb.TitleBar.class.getResource(Heading)).getImage();
////  }
//
///** Title Bar  */
//
//  public TitleBar(String TitleBg, String Heading, String ImageRight, String ImageLeft)
//  {
//
//  }
//
//  /** Status Bar  */
//  public TitleBar(String TitleBg, String ImageRight, String ImageLeft)
//  {
//    imgTitle =new ImageIcon(com.adasoftware.awb.TitleBar.class.getResource(TitleBg)).getImage();
//    imgLeft =new ImageIcon(com.adasoftware.awb.TitleBar.class.getResource(ImageLeft)).getImage();
//    imgRight =new ImageIcon(com.adasoftware.awb.TitleBar.class.getResource(ImageRight)).getImage();
//    strMessage = new String("...");
//    lblLeft = new JLabel(new ImageIcon(imgLeft));
//    lblLeft.setBounds(0,0,imgLeft.getWidth(this), imgLeft.getHeight(this));
//    this.setLayout(new BorderLayout());
//    lblLeft.setHorizontalAlignment(JLabel.LEFT);
//    this.add(lblLeft, BorderLayout.NORTH);
//  }
//
//  public void setMessage(String message)
//  {
//    strMessage = message;
//  }
//
//  public boolean isOpaque() {
//    return true;
//}
//
//protected void paintComponent(Graphics g) {
//    int width = getWidth();
//    int height = getHeight();
//
//    int imageW = imgTitle.getWidth(this);
//    int imageH = imgTitle.getHeight(this);
//
//    // Tile the image to fill our area.
//    for (int x = 0; x < width; x += imageW) {
////        for (int y = 0; y < height; y += imageH) {
//            g.drawImage(imgTitle, x, 0, this);
//   //    }
//    }
//
//    if(imgLeft !=null && imgRight !=null)
//    {
//      g.drawImage(imgLeft, 0,0, this);
//      g.drawImage(imgRight, (width-imgRight.getWidth(this)), 0, this);
//    }
//
//    if(imgHeader!=null)
//      g.drawImage(imgHeader, imgLeft.getWidth(this), 0, this);
//    else
//      g.drawString(strMessage, imgLeft.getWidth(this), 18);
//}
//
//
//}
