package cnf.ui.table.renderer;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.swing.JComponent;


import cnf.ui.table.decoration.CustomBorder;
import cnf.ui.table.decoration.SimpleStyle;

public class ExtendedRendererHelper {

	public static String currencyFormat = "$#.00";
    protected SimpleStyle style = null;
	protected String format = null;
	DecimalFormat formatter = null;

	public ExtendedRendererHelper()
	{
		setFormat(currencyFormat);
	}
	
	public void setStyle(SimpleStyle style)
	{
		this.style=style;
	}
	public void setFormat(String format)
	{
		this.format = format;
		try
		{
			formatter = new DecimalFormat(format);
		}
		catch(Exception ex)
		{
			formatter = new DecimalFormat();			
		}
	}
	public SimpleStyle getStyle()
	{
		if (style==null) style=new SimpleStyle();
		return style;
	}
	public String getFormat()
	{
		return format;
	}
	
	
	public CustomBorder createBorder()
	{
		if (style==null) style=new SimpleStyle();
        CustomBorder cb = new CustomBorder(style.getBorderStroke(SimpleStyle.LEFT), style.getBorderColor(SimpleStyle.LEFT), 
        		style.getBorderStroke(SimpleStyle.RIGHT), style.getBorderColor(SimpleStyle.RIGHT), 
        		style.getBorderStroke(SimpleStyle.TOP), style.getBorderColor(SimpleStyle.TOP), 
        		style.getBorderStroke(SimpleStyle.BOTTOM), style.getBorderColor(SimpleStyle.BOTTOM)); 
        return cb;
	}
	
	public void stylizeComponent(JComponent c)
	{
		c.setBorder(createBorder());
		if (style.getFont()!=null) c.setFont(style.getFont());
		if (style.getBackgroundColor()!=null) c.setBackground(style.getBackgroundColor());
		if (style.getForegroundColor()!=null) c.setForeground(style.getForegroundColor());		
	}
	
	
	public String formatNumber(Object value)
	{
		String res = "#ERR";
		try
		{
			if (value instanceof Float)
			{
				double v = ((Float)value).doubleValue();
				return formatter.format(v);
			}
			else if (value instanceof Double)
			{
				double v = ((Double)value).doubleValue();
				return formatter.format(v);				
			}
			else if (value instanceof BigDecimal)
			{
				double v = ((BigDecimal)value).doubleValue();
				return formatter.format(v);				
			}
			else if (value instanceof Integer)
			{
				double v = ((Integer)value).doubleValue();
				return formatter.format(v);				
			}
			else if (value instanceof Long)
			{
				double v = ((Long)value).doubleValue();
				return formatter.format(v);	
			}
		}
		catch(Exception ex)
		{
			res="#ERR#";
		}
		
		return res;
	}
	
	
}
