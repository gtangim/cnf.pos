package cnf.ui.table.renderer;

import cnf.ui.table.decoration.SimpleStyle;

public interface IExtendedRenderer {
	public void setFormat(String format);
	public void setStyle(SimpleStyle style);
	public SimpleStyle getStyle();
	public String getFormat();
	public void setDefault(String dvalue);
}
