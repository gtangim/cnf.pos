package cnf.ui.table.renderer;
import java.awt.Component;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import cnf.ui.table.decoration.SimpleStyle;

public class CheckBoxRenderer extends JPanel implements TableCellRenderer, IExtendedRenderer
{
		protected String defaultvalue=null;
        protected JCheckBox inner;
        protected ExtendedRendererHelper rh = new ExtendedRendererHelper();
        public CheckBoxRenderer() {
            inner = new JCheckBox();
            inner.setOpaque(false);
            this.add(inner);
        }
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
        {
			decorate();
        	
        	if (value!=null)
        	{
        		String v = value.toString().trim().toLowerCase();
        		if (v.equals("1") || v.equals("t") || v.equals("true") || v.equals("y") || v.equals("yes"))
        			inner.setSelected(true);
        		else inner.setSelected(false);
        	}
        	else 
        	{
        		if (defaultvalue!=null)
        			inner.setSelected(Boolean.parseBoolean(defaultvalue));
        		else inner.setSelected(false);
        	}
        	return this;
        }
		@Override
		public String getFormat() {
			return rh.getFormat();
		}
		@Override
		public SimpleStyle getStyle() {
			return rh.getStyle();
		}
		@Override
		public void setFormat(String format) {
			rh.setFormat(format);
		}
		@Override
		public void setStyle(SimpleStyle style) {
			rh.setStyle(style);
			decorate();
		}
		private void decorate()
		{
			rh.stylizeComponent(this);
			inner.setHorizontalAlignment(rh.getStyle().getHorizontalAlignment());
			inner.setVerticalAlignment(rh.getStyle().getVerticalAlignment());		
		}

		@Override
		public void setDefault(String dvalue)
		{
			defaultvalue=dvalue;
		}
		
}
