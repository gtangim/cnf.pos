package cnf.ui.table;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.table.AbstractTableModel;

import cnf.ui.table.element.*;
import cnf.ui.table.indexer.*;

public class ExtendedModel extends AbstractTableModel{

	protected ArrayList<String> columns = new ArrayList<String>();
	protected ArrayList<RowElement> rows = new ArrayList<RowElement>();
	protected HashMap<Index,RowElement> indexToRow = new HashMap<Index,RowElement>();
	protected HashMap<Index,Integer> indexToPos = new HashMap<Index,Integer>();
	protected HashMap<Integer,Index> rownumberToIndex = new HashMap<Integer,Index>();
	protected HashMap<Index,Integer> indexToRownumber = new HashMap<Index,Integer>();
    protected HashMap<String,String> rowStyleClasses = new HashMap<String,String>();

	protected int search_i=0;
	protected int search_row=0;
	protected int search_subrow=-1;
	
	protected int n=0;
	
	protected Index sel_row = null;
	protected Index sel_subrow = null;
	protected int sel_rownumber = -1;
	protected int sel_rowindex = -1;
	protected int sel_col=-1;
	
	public void clear()
	{
		synchronized(this)
		{
			n=0;
			rows.clear();
			indexToRow.clear();
			indexToPos.clear();
			rownumberToIndex.clear();
			indexToRownumber.clear();
			rowStyleClasses.clear();
			unselect();
		}
	}
	
	public boolean select(int r, int c)
	{
		synchronized(this)
		{
			selectCol(c);
			return selectRow(r);
		}
	}
	public boolean selectRow(int r)
	{
		if (r<0 || r>=n) return false;
		if (!rownumberToIndex.containsKey(r)) return false;
		sel_rownumber=r;
		sel_subrow = rownumberToIndex.get(r);
		sel_row = new Index(sel_subrow.getRowId(),null);
		sel_rowindex = this.getIndex(sel_row);
		if (sel_subrow.equals(sel_row)) sel_subrow=null;
		return true;		
	}
	public void selectCol(int c)
	{
		sel_col=c;
	}
	public void unselect()
	{
		synchronized(this)
		{
			sel_row=null;
			sel_subrow=null;
			sel_col=-1;
			sel_rownumber=-1;
			sel_rowindex=-1;
		}
	}
	public Index getSelectedRow(){return sel_row;}
	public Index getSelectedSubRow(){return sel_subrow;}
	public int getSelectedRownumber(){return sel_rownumber;}
	public int getSelectedRowIndex(){return sel_rowindex;}
	public int getSelectedColumn(){return sel_col;}
	
	
	
	public ExtendedModel(Object entityInstance)
	{
	     Field[] fld = entityInstance.getClass().getDeclaredFields();
	     for (int i = 0; i < fld.length; i++) {
	      columns.add(fld[i].getName());
	      
	     }
	}
	
	public ArrayList<String> getColumns() {return columns;}
	
	
	public Index getIndex(int combinedIndex)
	{
		synchronized(this)
		{
			if (rownumberToIndex.containsKey(combinedIndex))
				return rownumberToIndex.get(combinedIndex);
			else return null;
		}
	}	
	public int getIndex(Index i)
	{
		synchronized(this)
		{
			if (indexToPos.containsKey(i)) return indexToPos.get(i);
			else return -1;
		}
	}
	public RowElement getRow(String rowId)
	{
		synchronized(this)
		{
			Index i = new Index(rowId,null);
			return getRow(i);
		}
	}
	public String getRowStyleClass(String rowId)
	{
		synchronized(this)
		{
			if (rowId!=null && rowStyleClasses.containsKey(rowId))
				return rowStyleClasses.get(rowId);
			else return "";
		}
	}
	public RowElement getRow(int rowInd)
	{
		synchronized(this)
		{
			if (rowInd<0 || rowInd>=rows.size()) return null;
			return rows.get(rowInd);
		}
	}
	public RowElement getRow(Index ind)
	{
		synchronized(this)
		{
			if (indexToRow.containsKey(ind)) return indexToRow.get(ind);
			else return null;
		}
	}
	public EntityContainer getSubRow(String rowId, String subRowId)
	{
		synchronized(this)
		{
			Index i = new Index(rowId,subRowId);
			return getSubRow(i);		
		}
	}
	public EntityContainer getSubRow(Index ind)
	{
		synchronized(this)
		{
			Index i1 = new Index(ind.getRowId(),null);
			if (!indexToRow.containsKey(i1)) return null;
			return indexToRow.get(i1).getSubRowEntityContainer(ind);
		}
	}
	
	
	public boolean addRow(String rowId, String styleClass, Object entity)
	{
		synchronized(this)
		{
			Index i = new Index(rowId,null);
			if (indexToRow.containsKey(i)) return false;
			RowElement r = new RowElement(i,entity);
			indexToRow.put(i, r);
			rows.add(r);
			int pos = rows.size()-1;
			indexToPos.put(i, pos);
			buildRowLookupCache(n,pos);
			this.fireTableRowsInserted(n,n);
			n++;
			rowStyleClasses.put(rowId, styleClass);
			return true;
		}
	}
	
	
	
	public boolean removeRow(String rowId)
	{
		synchronized(this)
		{
			Index i = new Index(rowId,null);
			return removeRow(i);
		}
	}
	public boolean removeRow(int rowInd)
	{
		synchronized(this)
		{
			if (rowInd<0 || rowInd>=rows.size()) return false;
			RowElement r=rows.get(rowInd);
			rows.remove(rowInd);
			Index i = r.getIndex();
			indexToRow.remove(i);
			indexToPos.remove(i);
			rowInd=rowInd-1;
			if (rowInd<=0) buildRowLookupCache(0,0);
			else buildRowLookupCache(indexToRownumber.get(rows.get(rowInd).getIndex()),rowInd);
			n-=r.Count();
			return true;		
		}
	}
	public boolean removeRow(Index ind)
	{
		synchronized(this)
		{
			if (!indexToRow.containsKey(ind)) return false;
			RowElement r = indexToRow.get(ind);
			int pos = indexToPos.get(ind);
			rows.remove(pos);
			indexToRow.remove(ind);
			indexToPos.remove(ind);
			pos--;
			n-=r.Count();
			if (pos<=0) buildRowLookupCache(0,0);
			else buildRowLookupCache(indexToRownumber.get(rows.get(pos).getIndex()),pos);
			return true;
		}
	}
	
	
	public boolean insertRow(int existingRowInd, String newRowId, String styleClass, Object entity)
	{		
		synchronized(this)
		{
			Index i = new Index(newRowId,null);
			if (indexToRow.containsKey(i)) return false;
			RowElement r = new RowElement(i,entity);
			indexToRow.put(i, r);
			rows.add(existingRowInd,r);
			indexToPos.put(i, existingRowInd);
			rowStyleClasses.put(newRowId, styleClass);
			for (int j=existingRowInd+1;j<rows.size();j++)
				indexToPos.put(rows.get(j).getIndex(), j);
			
			int pos = existingRowInd-1;
			if (pos<=0) buildRowLookupCache(0,0);
			else buildRowLookupCache(indexToRownumber.get(rows.get(pos).getIndex()),pos);
			int n2 = indexToRownumber.get(i);
			this.fireTableRowsInserted(n2,n2);
			n++;
			return true;
		}
	}
	
	public boolean insertRowBefore(String existingRowId, String newRowId, String styleClass, Object entity)
	{
		synchronized(this)
		{
			Index i = new Index(existingRowId,null);
			if (!indexToPos.containsKey(i)) return false;
			return insertRow(indexToPos.get(i),newRowId,styleClass,entity);
		}	
	}
	public boolean insertRowAfter(String existingRowId, String newRowId, String styleClass, Object entity)
	{
		synchronized(this)
		{
			Index i = new Index(existingRowId,null);
			if (!indexToPos.containsKey(i)) return false;
			return insertRow(indexToPos.get(i)+1,newRowId,styleClass,entity);		
		}	
	}
	
	public boolean addSubRow(String rowId, String subrowId, Object entity)
	{
		synchronized(this)
		{
			Index i1=new Index(rowId,null);
			Index i2=new Index(rowId,subrowId);
			if (!indexToRow.containsKey(i1)) return false;
			if (indexToRow.containsKey(i2)) return false;
			if (!indexToPos.containsKey(i1)) return false;
			if (!indexToRownumber.containsKey(i1)) return false;
			int pos = indexToPos.get(i1);
			RowElement r = indexToRow.get(i1);
			boolean s=r.addSubRow(i2, entity);
			if (s) 
			{
				n++;
				int n1 = indexToRownumber.get(i1);
				buildRowLookupCache(n1,pos);
				n1+=r.Count()-1;
				this.fireTableRowsInserted(n1, n1);
			}
			return s;
		}
	}
	public boolean removeSubRow(String rowId, String subrowId)
	{
		synchronized(this)
		{
			Index i1=new Index(rowId,null);
			Index i2=new Index(rowId,subrowId);
			if (!indexToRow.containsKey(i1)) return false;
			if (indexToRow.containsKey(i2)) return false;
			if (!indexToPos.containsKey(i1)) return false;
			if (!indexToRownumber.containsKey(i1)) return false;
			int pos = indexToPos.get(i1);
			RowElement r = indexToRow.get(i1);
			boolean s=r.removeSubRow(subrowId);
			if (s) 
			{
				n--;
				int n1 = indexToRownumber.get(i1);
				buildRowLookupCache(n1,pos);
				n1+=r.Count()-1;
				this.fireTableRowsInserted(n1, n1);
			}
			return s;
		}
	}
	
	
	
	protected void buildRowLookupCache(int start, int rowIndex)
	{
		synchronized(this)
		{
			for (int i = rowIndex; i<rows.size();i++)
			{
				RowElement r = rows.get(i);
				indexToPos.put(r.getIndex(), i);
				int n2 = r.Count();
				for (int j=0;j<n2;j++) 
				{
					rownumberToIndex.put(start,r.getIndex(j));
					indexToRownumber.put(r.getIndex(j), start);
					start++;
				}
			}
			while(rownumberToIndex.containsKey(start))
			{
				rownumberToIndex.remove(start);
				start++;
			}
		}		
	}
	
	
	public void loadDataBatch(ArrayList<RowElement> rows, HashMap<String,String> rowStyleClasses)
	{
		synchronized(this)
		{
			this.rows=rows;
			this.rowStyleClasses = rowStyleClasses;
			// Build Main indexes
			int ind = 0;
			int rowNum=0;
			indexToRow.clear();
			indexToPos.clear();		
			rownumberToIndex.clear();
			indexToRownumber.clear();
			for(RowElement r : rows)
			{
				indexToRow.put(r.getIndex(),r);
				indexToPos.put(r.getIndex(), ind++);
				int n2 = r.Count();
				for (int j=0;j<n2;j++) 
				{
					rownumberToIndex.put(rowNum,r.getIndex(j));
					indexToRownumber.put(r.getIndex(j), rowNum);
					rowNum++;
				}				
			}
			n=rowNum;
			this.fireTableRowsInserted(0, n-1);
		}
	}
	
	
	
	//@Override
	public int getColumnCount() {
		return columns.size();
	}
	@Override
	public String getColumnName(int ind){if(ind<0 || ind>=columns.size()) return null; return columns.get(ind);}
	//@Override
	public int getRowCount() {
		synchronized(this)
		{
			return n;
		}
	}

    public int getPrimaryRowCount()
    {
        return rows.size();
    }

	//@Override
	public Object getValueAt(int r, int c) {
		synchronized(this)
		{
			if (rownumberToIndex.containsKey(r)) 
			{
				Index i1 = rownumberToIndex.get(r);
				Index i2= new Index(i1.getRowId(),null);
				if (!indexToRow.containsKey(i2)) return null;
				RowElement mainrow = indexToRow.get(i2);
				if (i1.getSubRowId().isEmpty()) return mainrow.getCell(c);
				else return mainrow.getSubRowCell(i1, c);
			}		
			return null;
		}		
	}
	@Override
	public void setValueAt(Object value, int r, int c) {
		synchronized(this)
		{
			if (rownumberToIndex.containsKey(r)) 
			{
				boolean s=false;
				Index i1 = rownumberToIndex.get(r);
				Index i2= new Index(i1.getRowId(),null);
				if (!indexToRow.containsKey(i2)) return;
				RowElement mainrow = indexToRow.get(i2);
				if (i1.getSubRowId().isEmpty()) s=mainrow.setCell(columns.get(c), value);
				else s=mainrow.setSubRowCell(i1, columns.get(c), value);
			    if (s) fireTableCellUpdated(r, c);
			}		
		}
	}

}
