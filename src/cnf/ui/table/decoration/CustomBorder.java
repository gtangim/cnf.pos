package cnf.ui.table.decoration;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;

import javax.swing.border.AbstractBorder;

public class CustomBorder extends AbstractBorder {
	
	protected BasicStroke sl;
	protected BasicStroke sr;
	protected BasicStroke st;
	protected BasicStroke sb;
	protected Color cl;
	protected Color cr;
	protected Color ct;
	protected Color cb;
	
	protected static final Color dc = new Color(0,0,0);
	protected static final BasicStroke ds = new BasicStroke();
	
	
  public CustomBorder(BasicStroke sLeft, Color cLeft,BasicStroke sRight, Color cRight,BasicStroke sTop, Color cTop,BasicStroke sBottom, Color cBottom) {
	  sl=sLeft;
	  sr=sRight;
	  st=sTop;
	  sb=sBottom;
	  cl=cLeft;
	  cr=cRight;
	  ct=cTop;
	  cb=cBottom;
	  if (cl==null) cl=dc;
	  if (cr==null) cr=dc;
	  if (ct==null) ct=dc;
	  if (cb==null) cb=dc;	  
  }


  public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {

	Graphics2D g2d = (Graphics2D)g;
	if (sl!=null)
    {
    	g2d.setColor(cl);
    	g2d.setStroke(sl);
    	int off = x+(int)(sl.getLineWidth()/2);
    	g2d.drawLine(off, y, off, y+height);
    }
    if (sr!=null)
    {
    	g2d.setColor(cr);
    	g2d.setStroke(sr);
    	int lw=(int)sr.getLineWidth();
    	int off = x+width-lw/2-lw%2;
    	g2d.drawLine(off, y, off, y+height);
    }
    if (st!=null)
    {
    	g2d.setColor(ct);
    	g2d.setStroke(st);
    	int off = y+(int)(st.getLineWidth()/2);
    	g2d.drawLine(x, off, x+width, off);
    }
    if (sb!=null)
    {
    	g2d.setColor(cb);
    	g2d.setStroke(sb);
    	int lw=(int)sb.getLineWidth();
    	int off = y+height-lw/2-lw%2;
    	g2d.drawLine(x, off, x+width, off);
    }
    
    
    
  }

  public Insets getBorderInsets(Component c) {
    return getBorderInsets(c,new Insets(0, 0, 0, 0));
  }

  public Insets getBorderInsets(Component c, Insets insets) {
    insets.left = 0;
    insets.top = 0;
    insets.right = 0;
    insets.bottom = 0;
    if (sl!=null) insets.left = (int)sl.getLineWidth();
    if (sr!=null) insets.right = (int)sr.getLineWidth();
    if (st!=null) insets.top = (int)st.getLineWidth();
    if (sb!=null) insets.bottom = (int)sb.getLineWidth();
    return insets;
  }


  /**
   * Returns whether or not the border is opaque.
   */
  public boolean isBorderOpaque() {
    return false;
  }


}