package cnf.ui.table.decoration;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.SwingConstants;

public class SimpleStyle {
	
	protected static HashMap<String,Font> fontLookup=new HashMap<String,Font>();
	protected Font font;
	protected Color foreColor;
	protected Color backColor;
	protected Color leftBorderColor;
	protected Color rightBorderColor;
	protected Color topBorderColor;
	protected Color bottomBorderColor;
	protected BasicStroke leftBorderStroke;
	protected BasicStroke rightBorderStroke;
	protected BasicStroke topBorderStroke;
	protected BasicStroke bottomBorderStroke;
	protected int halign;
	protected int valign;
	
	private int additive = 0;
	protected int fca=0;
	protected int bca=0;
	protected int lbca=0;
	protected int rbca=0;
	protected int tbca=0;
	protected int bbca=0;
	
	protected boolean fs=false;
	protected boolean fcs=false;
	protected boolean bcs=false;
	protected boolean lbcs=false;
	protected boolean rbcs=false;
	protected boolean tbcs=false;
	protected boolean bbcs=false;
	protected boolean lbss=false;
	protected boolean rbss=false;
	protected boolean tbss=false;
	protected boolean bbss=false;
	protected boolean has=false;
	protected boolean vas=false;
	
	
	protected final float[] solid = new float[0];
	protected final float[] dotted = new float[]{1.0f,1.0f};
	protected final float[] dashed = new float[]{10.0f,10.0f};
	public static final int ALL = 0;
	public static  final int LEFT = 1;
	public static  final int RIGHT = 2;
	public static  final int TOP = 3;
	public static  final int BOTTOM = 4;
	public static  final int CENTER = 4;
	
	
	public Font getFont(){return font;}
	public BasicStroke getBorderStroke(int borderType)
	{
		if (borderType==LEFT) return this.leftBorderStroke;
		else if (borderType==RIGHT) return this.rightBorderStroke;
		else if (borderType==TOP) return this.topBorderStroke;
		else if (borderType==BOTTOM) return this.bottomBorderStroke;
		else return null;		
	}
	public Color getBorderColor(int borderType)
	{
		if (borderType==LEFT) return this.leftBorderColor;
		else if (borderType==RIGHT) return this.rightBorderColor;
		else if (borderType==TOP) return this.topBorderColor;
		else if (borderType==BOTTOM) return this.bottomBorderColor;
		else return null;				
	}
	public Color getBackgroundColor()
	{
		return backColor;
	}
	public Color getForegroundColor()
	{
		return foreColor;
	}
	public int getHorizontalAlignment()
	{
		return halign;
	}
	public int getVerticalAlignment()
	{
		return valign;
	}
	
	public void setFont(Font f)
	{
		font=f;
		fs=true;
	}
	public void setAlignment(int h_align, int v_align)
	{
		if (h_align==LEFT) {halign=SwingConstants.LEFT;has=true;}
		else if (h_align==RIGHT) {halign=SwingConstants.RIGHT;has=true;}
		else if (h_align==CENTER) {halign=SwingConstants.CENTER;has=true;}
		if (v_align==TOP) {valign=SwingConstants.TOP;vas=true;}
		else if (v_align==CENTER) {valign=SwingConstants.CENTER;vas=true;}
		else if (v_align==BOTTOM) {valign=SwingConstants.BOTTOM;vas=true;}		
	}
	public void setForeColor(Color foreground)
	{
		foreColor=foreground;
		fcs=true;
	}
	public void setBackColor(Color background)
	{
		backColor=background;
		bcs=true;
	}
	public void setBorder(int borderType, Color c, int size, String lineStyle)
	{
		BasicStroke s=null;
		if (lineStyle==null) lineStyle="solid";
		float[] ls = getLineStyle(lineStyle);
		if (ls==null || ls.length==0) 
			s = new BasicStroke((float)size, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER);
		else 
			s = new BasicStroke((float)size,BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER, 10.0f, ls, 0.0f);

		if (borderType==ALL)
		{
			if (c!=null)
			{
				this.leftBorderColor = c;
				this.rightBorderColor = c;
				this.topBorderColor = c;
				this.bottomBorderColor = c;
				lbcs=rbcs=tbcs=bbcs=true;
			}
			if (s!=null)
			{
				this.leftBorderStroke = s;
				this.rightBorderStroke = s;
				this.topBorderStroke = s;
				this.bottomBorderStroke = s;
				lbss=rbss=tbss=bbss=true;
			}			
		}
		else if (borderType==LEFT)
		{
			if (c!=null) {this.leftBorderColor=c;lbcs=true;}
			if (s!=null) {this.leftBorderStroke=s;lbss=true;}
		}
		else if (borderType==RIGHT)
		{
			if (c!=null) {this.rightBorderColor=c;rbcs=true;}
			if (s!=null) {this.rightBorderStroke=s;rbss=true;}
		}
		else if (borderType==TOP)
		{
			if (c!=null) {this.topBorderColor=c;tbcs=true;}
			if (s!=null) {this.topBorderStroke=s;tbss=true;}
		}
		else if (borderType==BOTTOM)
		{
			if (c!=null) {this.bottomBorderColor=c;bbcs=true;}
			if (s!=null) {this.bottomBorderStroke=s;bbss=true;}
		}
		
	}
	
	
	public void reset()
	{
		font = null;
		foreColor = null;
		backColor = null;
		leftBorderColor=null;
		rightBorderColor=null;
		topBorderColor=null;
		bottomBorderColor=null;
		leftBorderStroke=null;
		rightBorderStroke=null;
		topBorderStroke=null;
		bottomBorderStroke=null;
		halign=SwingConstants.LEFT;
		valign=SwingConstants.CENTER;
		fs=false;
		fcs=false;
		bcs=false;
		lbcs=false;
		rbcs=false;
		tbcs=false;
		bbcs=false;
		lbss=false;
		rbss=false;
		tbss=false;
		bbss=false;
		has=false;
		vas=false;
				
	}
	
	public SimpleStyle()
	{
		reset();		
	}
	public SimpleStyle(String style)
	{
		reset();
		parse(style);
	}
	
	public void parse(String style)
	{
		try
		{
			String[] sentries = style.split(";");
			for (String se: sentries)
			{
				String[] se_v = se.split(":");
				if (se_v.length>1 && se_v[0]!=null && se_v[1]!=null)
				{
					se_v[0]=se_v[0].toLowerCase().trim();
					se_v[1]=se_v[1].trim();
					if (se_v[0].equals("font"))
					{
						font = getFont(se_v[1]);
						fs=true;
					}
					else if (se_v[0].equals("border"))
					{
						Object[] b = getBorder(se_v[1]);
						if (b.length==0)
						{
							this.leftBorderColor = null;
							this.rightBorderColor = null;
							this.topBorderColor = null;
							this.bottomBorderColor = null;
							this.leftBorderStroke = null;
							this.rightBorderStroke = null;
							this.topBorderStroke = null;
							this.bottomBorderStroke = null;
							lbcs=rbcs=tbcs=bbcs=true;
							lbca=rbca=tbca=bbca=0;
							lbss=rbss=tbss=bbss=true;
						}
						else
						{
							if (b[0]!=null)
							{
								this.leftBorderColor = (Color)b[0];
								this.rightBorderColor = (Color)b[0];
								this.topBorderColor = (Color)b[0];
								this.bottomBorderColor = (Color)b[0];
								lbcs=rbcs=tbcs=bbcs=true;
								lbca=rbca=tbca=bbca=additive;
							}
							if (b[1]!=null)
							{
								this.leftBorderStroke = (BasicStroke)b[1];
								this.rightBorderStroke = (BasicStroke)b[1];
								this.topBorderStroke = (BasicStroke)b[1];
								this.bottomBorderStroke = (BasicStroke)b[1];
								lbss=rbss=tbss=bbss=true;
							}
						}
					}
					else if (se_v[0].equals("border-left"))
					{
						Object[] b = getBorder(se_v[1]);
						if (b.length==0)
						{
							this.leftBorderColor = null;
							this.leftBorderStroke = null;
							lbca=0;
							lbcs=lbss=true;
						}
						else{
							if (b[0]!=null)
								{this.leftBorderColor = (Color)b[0];lbcs=true;lbca=additive;}
							if (b[1]!=null)
								{this.leftBorderStroke = (BasicStroke)b[1];lbss=true;}
						}
					}
					else if (se_v[0].equals("border-right"))
					{
						Object[] b = getBorder(se_v[1]);
						if (b.length==0)
						{
							this.rightBorderColor = null;
							this.rightBorderStroke = null;
							rbca=0;
							rbcs=rbss=true;
						}
						else{
							if (b[0]!=null)
								{this.rightBorderColor = (Color)b[0];rbcs=true;rbca=additive;}
							if (b[1]!=null)
								{this.rightBorderStroke = (BasicStroke)b[1];rbss=true;}
						}						
					}
					else if (se_v[0].equals("border-top"))
					{
						Object[] b = getBorder(se_v[1]);
						if (b.length==0)
						{
							this.topBorderColor = null;
							this.topBorderStroke = null;
							tbca=0;
							tbcs=tbss=true;
						}
						else{
							if (b[0]!=null)
								{this.topBorderColor = (Color)b[0];tbcs=true;tbca=additive;}
							if (b[1]!=null)
								{this.topBorderStroke = (BasicStroke)b[1];tbss=true;}
						}						
					}
					else if (se_v[0].equals("border-bottom"))
					{
						Object[] b = getBorder(se_v[1]);
						if (b.length==0)
						{
							this.bottomBorderColor = null;
							this.bottomBorderStroke = null;
							bbca=0;
							bbcs=bbss=true;
						}
						else{
							if (b[0]!=null)
								{this.bottomBorderColor = (Color)b[0];bbcs=true;bbca=additive;}
							if (b[1]!=null)
								{this.bottomBorderStroke = (BasicStroke)b[1];bbss=true;}												
						}
					}
					else if (se_v[0].equals("color"))
					{
						this.foreColor = getColor(se_v[1]);
						fcs=true;
						fca=additive;
					}
					else if (se_v[0].equals("background-color"))
					{
						this.backColor = getColor(se_v[1]);
						bcs=true;
						bca=additive;
					}
					else if (se_v[0].equals("align"))
					{
						int a = getAlign(se_v[1]);
						if (a>=0) halign = a;
						has=true;
					}
					else if (se_v[0].equals("vertical-align"))
					{
						int a = getAlign(se_v[1]);
						if (a>=0) valign = a;
						vas=true;
					}
				}
			}
		}
		catch(Exception ex)
		{
			String m = ex.getMessage();
		}
	}
	
	protected String[] getParams(String v)
	{
		try
		{
			String[] r = v.split("\\s+");
			ArrayList<String> plist=new ArrayList<String>();
			int pos = 0;
			while (pos<r.length)
			{
				if (r[pos].startsWith("(") || r[pos].startsWith("+(") || r[pos].startsWith("-("))
				{
					StringBuilder t=new StringBuilder();
					while(pos<r.length)
					{
						t.append(r[pos]);
						pos++;
						if (r[pos-1].endsWith(")")) break;
					}
					plist.add(t.toString());
				}
				else if (r[pos].startsWith("\""))
				{
					StringBuilder t=new StringBuilder();
					while(pos<r.length)
					{
						if (t.length()>0) t.append(' ');
						t.append(r[pos]);
						pos++;
						if (r[pos-1].endsWith("\"")) break;
					}
					plist.add(t.toString());
				}
				else
					plist.add(r[pos++]);
			}
			return plist.toArray(new String[plist.size()]);
		}
		catch(Exception ex)
		{
			return new String[0];
		}
	}
	
	protected int getAlign(String v)
	{
		v=v.trim().toLowerCase();
		if (v.equals("left")) return SwingConstants.LEFT;
		else if (v.equals("right")) return SwingConstants.RIGHT;
		else if (v.equals("center")) return SwingConstants.CENTER;
		else if (v.equals("top")) return SwingConstants.TOP;
		else if (v.equals("bottom")) return SwingConstants.BOTTOM;
		else if (v.equals("middle")) return SwingConstants.CENTER;
		else return -1;
	}
	
	
	protected Font getFont(String v)
	{
		if (fontLookup.containsKey(v)) return fontLookup.get(v);
		//System.out.println(v);
		String p[] = getParams(v);
		String fname = Font.MONOSPACED;
		int fsize = 10;
		int fstyle = Font.PLAIN;
		for (String pe:p)
		{
			int fst = getFontStyle(pe);
			if (fst!=-1) fstyle = fst;
			else
			{
				int ti = getInt(pe);
				if (ti>=0) fsize = ti;
				else 
				{
					pe = pe.replace("\"", "");
					fname = pe;
				}
			}
		}
		try
		{
			Font font= new Font(fname,fstyle,fsize);
			fontLookup.put(v, font);
			return font;
		}
		catch(Exception ex)
		{
			return null;
		}
	}
	
	protected int getFontStyle(String v)
	{
		v=v.trim().toLowerCase();
		if (v.equals("regular")) return Font.PLAIN;
		else if (v.equals("bold")) return Font.BOLD;
		else if (v.equals("italic")) return Font.ITALIC;
		else return -1;
	}
	
	
	protected Object[] getBorder(String v)
	{
		int w = 0;
		Color c = null;
		float[] ls = null;
		String[] p = getParams(v);
		if (p.length>0 && p[0].equals("none")) return new Object[] {};
		for (String pe:p)
		{
			Color t = getColor(pe);
			if (t!=null) c=t;
			else
			{
				float[] ts = getLineStyle(pe);
				if (ts!=null) ls=ts;
				else
				{
					int ti = getInt(pe);
					if (ti>=0) w=ti;
				}
			}
		}
		Object[] ret = new Object[2];
		
		ret[0] = c;
		ret[1]=null;
		if (w>0)
		{
			if (ls==null || ls.length==0) 
				ret[1] = new BasicStroke((float)w, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER);
			else 
				ret[1] = new BasicStroke((float)w,BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER, 10.0f, ls, 0.0f);
			
		}
		return ret;
	}
	
	protected float[] getLineStyle(String v)
	{
		if (v==null) return null;
		v=v.toLowerCase().trim();
		if (v.equals("solid")) return solid;
		else if (v.equals("dotted")) return dotted;
		else if (v.equals("dashed")) return dashed;
		else return null;
	}
		
	protected Color getColor(String v)
	{
		try
		{
			v=v.trim();
			if (!(v.startsWith("(") || v.startsWith("+(") || v.startsWith("-(")) && !v.endsWith(")")) return null;
			additive=0;
			int startpos = 1;
			if (v.charAt(0)=='+')
			{
				additive=1;
				startpos=2;
			}
			else if (v.charAt(0)=='-')
			{
				additive=-1;
				startpos=2;				
			}
			v=v.substring(startpos,v.length()-1);
			String[] nums = v.split(",");
			int[] col = new int[]{0,0,0,255};
			int n = nums.length;
			if (n>4) n=4;
			for (int i=0;i<n;i++) 
			{
				col[i]=getInt(nums[i]);
				if (col[i]<0) col[i]=0;
				else if (col[i]>255) col[i]=255;
			}
			return new Color(col[0],col[1],col[2],col[3]);
		}
		catch(Exception ex)
		{
			return null;
		}
	}
	
	protected int getInt(String v)
	{
		try
		{
			int i = Integer.parseInt(v);
			return i;
		}
		catch(Exception ex)
		{
			return -1;
		}
	}
	
	public static int v=0;
	public void merge(SimpleStyle s)
	{
		//System.out.println(Integer.toString(v++));
		if (s.fs) {this.font=s.font;fs=true;}
		if (s.fcs) {this.foreColor=colorMerge(this.foreColor,this.fca,s.foreColor,s.fca);fcs=true;fca=0;}
		if (s.bcs) {this.backColor=colorMerge(this.backColor,this.bca,s.backColor,s.bca);bcs=true;bca=0;}
		if (s.lbcs) {this.leftBorderColor=colorMerge(this.leftBorderColor,this.lbca,s.leftBorderColor,s.lbca);lbcs=true;lbca=0;}
		if (s.rbcs) {this.rightBorderColor=colorMerge(this.rightBorderColor,this.rbca,s.rightBorderColor,s.rbca);rbcs=true;rbca=0;}
		if (s.tbcs) {this.topBorderColor=colorMerge(this.topBorderColor,this.tbca,s.topBorderColor,s.tbca);tbcs=true;tbca=0;}
		if (s.bbcs) {this.bottomBorderColor=colorMerge(this.bottomBorderColor,this.bbca,s.bottomBorderColor,s.bbca);bbcs=true;bbca=0;}
		if (s.lbss) {this.leftBorderStroke=s.leftBorderStroke;lbss=true;}
		if (s.rbss) {this.rightBorderStroke=s.rightBorderStroke;rbss=true;}
		if (s.tbss) {this.topBorderStroke=s.topBorderStroke;tbss=true;}
		if (s.bbss) {this.bottomBorderStroke=s.bottomBorderStroke;bbss=true;}
		if (s.has) {this.halign=s.halign;has=true;}
		if (s.vas) {this.valign=s.valign;vas=true;}
	}
	
	
	public Color colorMerge(Color a, int add_a, Color b, int add_b)
	{
		if (b==null && add_a!=0) return a;
		else if (a!=null && b!=null && (add_a!=0 || add_b!=0))
		{
			int ra = a.getRed();
			int ga = a.getGreen();
			int ba = a.getBlue();
			int aa = a.getAlpha();
			if (add_a<0) {ra=-ra;ga=-ga;ba=-ba;aa=-aa;}
			int rb = b.getRed();
			int gb = b.getGreen();
			int bb = b.getBlue();
			int ab = b.getAlpha();
			if (add_b<0) {rb=-rb;gb=-gb;bb=-bb;ab=-ab;}
			int R=ra+rb;
			int G=ga+gb;
			int B=ba+bb;
			int A=aa+ab;
			if (R<0)R=0;else if (R>255) R=255;
			if (G<0)G=0;else if (G>255) G=255;
			if (B<0)B=0;else if (B>255) B=255;
			if (A<0)A=0;else if (A>255) A=255;
			return new Color(R,G,B,A);			
		}				
		return b;		
	}
	
}
