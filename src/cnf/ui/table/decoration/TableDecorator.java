package cnf.ui.table.decoration;

import java.util.HashMap;

import javax.swing.ImageIcon;

import cnf.ui.table.indexer.RowColKey;
import cnf.ui.table.indexer.StyleKey;

public class TableDecorator {
    public ImageIcon image = null;
    public HashMap<StyleKey,SimpleStyle> keyToStyle = new HashMap<StyleKey,SimpleStyle>(); 
    public HashMap<RowColKey,SimpleStyle> coordToStyle = new HashMap<RowColKey,SimpleStyle>();
    
}
