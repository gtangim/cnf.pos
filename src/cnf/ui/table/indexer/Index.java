package cnf.ui.table.indexer;

public final class Index {
	private final String rowId;
	private final String subRowId;
	private final int hcode;
	public Index(String rowId, String subRowId)
	{
		if (rowId==null) this.rowId="";
		else this.rowId=rowId;
		if (subRowId==null) this.subRowId="";
		else this.subRowId=subRowId;	
		this.hcode = this.rowId.hashCode() * 433459867 + this.subRowId.hashCode() * 974941657;
	}
	
	public String getRowId(){return rowId;}
	public String getSubRowId() {return subRowId;}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Index)) return false;
		Index s = (Index)obj;
		if (!subRowId.isEmpty() && !s.subRowId.isEmpty() && subRowId.equals(s.subRowId)) return true;
		if (subRowId.isEmpty() && s.subRowId.isEmpty() && rowId.equals(s.rowId)) return true;
		return false;
	}
	
	@Override
	public int hashCode() {
		return hcode;
	}
}
