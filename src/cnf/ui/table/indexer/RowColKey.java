package cnf.ui.table.indexer;

public final class RowColKey {
	private final int row;
	private final int col;
	private final int hcode;
    static private RowColKey[][] keyCache = new RowColKey[10000][10];

    static public RowColKey GenerateKeyCached(int row,int col)
    {
        if (row>=0 && col>=0 && row<10000 && col<10)
        {
            if (keyCache[row][col]==null) keyCache[row][col]=new RowColKey(row,col);
            return keyCache[row][col];
        }
        else return new RowColKey(row,col);
    }

    public RowColKey(int row, int col)
	{
		this.row=row;
		this.col=col;
		this.hcode = row * 433459867 + col * 974941657;
	}
	
	int getRow(){return row;}
	int getCol() {return col;}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof RowColKey)) return false;
		RowColKey k = (RowColKey)obj;
		if (row!=k.row) return false;
		if (col!=k.col) return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		return hcode;
	}
}
