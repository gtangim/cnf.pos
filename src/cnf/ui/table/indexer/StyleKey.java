package cnf.ui.table.indexer;

public final class StyleKey {
	public enum AddressType {COL, COL_ALT, COL_SEL, ROW, ROW_ALT, ROW_SEL, SUB_ROW, SUB_ROW_ALT, SUB_ROW_SEL, CELL, CELL_ALT, CELL_SEL}; 
	public final AddressType addressType;
	public final String id;
	public final String styleClass;	
	public final int ind;
	public final int hcode;

	public StyleKey(String styleClass, AddressType aType, String id, int index)
	{
		if (styleClass==null) styleClass="";
		if (id==null) id="";
		this.styleClass = styleClass;
		this.addressType = aType;
		this.id=id;
		this.ind = index;
		this.hcode = styleClass.hashCode()+aType.hashCode()+id.hashCode() + ind*134342111;
	}
	public AddressType getAddressType() {return addressType;}
	public String getId() {return id;}
	public String getCategory() {return styleClass;}
	public int getIndex() {return ind;}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof StyleKey)) return false;
		StyleKey s = (StyleKey)obj;
		if (addressType!=s.addressType) return false;
		if (!styleClass.equals(s.styleClass)) return false;
		if (!id.equals(s.id)) return false;
		if (ind!=s.ind) return false;
		return true;
	}

	@Override
	public int hashCode() {
		return hcode;
	}

	@Override
	public String toString() {
		return "["+styleClass+","+addressType.toString()+","+id+","+Integer.toString(ind)+"]";
	}

}
