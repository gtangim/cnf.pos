package cnf.loader;

import org.joda.time.DateTime;

import java.io.*;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;

/**
 * Created by russela on 4/8/2015.
 */
public class BinaryBuffer {
    public static final byte FALSE = 0;
    public static final byte TRUE = 1;
    public static final byte NULL = 2;
    public static final byte BYTE = 3;
    public static final byte CHAR = 4;
    public static final byte SHORT = 5;
    public static final byte INT = 6;
    public static final byte LONG = 7;
    public static final byte FLOAT = 8;
    public static final byte DOUBLE = 9;
    public static final byte DECIMAL = 10;     //A
    public static final byte DATETIME = 11;    //B
    public static final byte STRING = 12;      //C
    public static final byte BYTEARRAY = 13;   //D
    public static final byte LIST = 14;        //E
    public static final byte ARRAY = 15;       //F
    public static final byte STRUCT = 16;      //10

    private static final long TICKS_AT_EPOCH = 621355968000000000L;
    private static final long TICKS_PER_MILLISECOND = 10000;

    ByteBuffer buf;

    public BinaryBuffer(String fname) throws Exception{
        File f = new File(fname);
        if (!f.exists()) throw new Exception("Data File " + fname + " was not found!");
        int len = (int)f.length();
        InputStream gzip = new GZIPInputStream( new BufferedInputStream( new FileInputStream( fname ) ), 512 * 1024 );
        ByteBuffer buffer = ByteBuffer.allocate(30 * len);

        byte[] temp = new byte[512*1024];
        int n=0;
        while ((n=gzip.read(temp))>0)
            buffer.put(temp,0,n);
        buffer.flip();
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        buf = buffer;
        gzip.close();
    }

    public BinaryBuffer(ByteBuffer dataBuffer)
    {
        buf = dataBuffer;
    }

    public boolean ReadBoolean() throws Exception {
        byte rd = buf.get();
        if (rd == NULL) return false;
        else if (rd == TRUE) return true;
        else if (rd == FALSE) return false;
        else dataError();
        return false;
    }

    public Boolean ReadBooleanNullable() throws Exception {
        byte rd = buf.get();
        if (rd == NULL) return null;
        else if (rd == TRUE) return true;
        else if (rd == FALSE) return false;
        else dataError();
        return false;
    }

    public byte ReadByte() throws Exception {
        byte rd = buf.get();
        if (rd == NULL) return 0;
        else if (rd != BYTE) dataError();
        return buf.get();
    }

    public Byte ReadByteNullable() throws Exception {
        byte rd = buf.get();
        if (rd == NULL) return null;
        else if (rd != BYTE) dataError();
        return buf.get();
    }

    public char ReadChar() throws Exception {
        byte rd = buf.get();
        if (rd == NULL) return 0;
        else if (rd != CHAR) dataError();
        return (char)buf.get();
    }

    public Character ReadCharNullable() throws Exception {
        byte rd = buf.get();
        if (rd == NULL) return null;
        else if (rd != CHAR) dataError();
        return (char)buf.get();
    }

    public short ReadShort() throws Exception {
        byte rd = buf.get();
        if (rd == NULL) return 0;
        else if (rd != SHORT) dataError();
        return buf.getShort();
    }

    public Short ReadShortNullable() throws Exception {
        byte rd = buf.get();
        if (rd == NULL) return null;
        else if (rd != SHORT) dataError();
        return buf.getShort();
    }

    public int ReadInt() throws Exception {
        byte rd = buf.get();
        if (rd == NULL) return 0;
        else if (rd != INT) dataError();
        return buf.getInt();
    }

    public Integer ReadIntNullable() throws Exception {
        byte rd = buf.get();
        if (rd == NULL) return null;
        else if (rd != INT) dataError();
        return buf.getInt();
    }

    public long ReadLong() throws Exception {
        byte rd = buf.get();
        if (rd == NULL) return 0;
        else if (rd != LONG) dataError();
        return buf.getLong();
    }

    public Long ReadLongNullable() throws Exception {
        byte rd = buf.get();
        if (rd == NULL) return null;
        else if (rd != LONG) dataError();
        return buf.getLong();
    }

    public float ReadFloat() throws Exception {
        byte rd = buf.get();
        if (rd == NULL) return 0;
        else if (rd != FLOAT) dataError();
        return buf.getFloat();
    }

    public Float ReadFloatNullable() throws Exception {
        byte rd = buf.get();
        if (rd == NULL) return null;
        else if (rd != FLOAT) dataError();
        return buf.getFloat();
    }

    public double ReadDouble() throws Exception {
        byte rd = buf.get();
        if (rd == NULL) return 0;
        else if (rd != DOUBLE) dataError();
        return buf.getDouble();
    }

    public Double ReadDoubleNullable() throws Exception {
        byte rd = buf.get();
        if (rd == NULL) return null;
        else if (rd != DOUBLE) dataError();
        return buf.getDouble();
    }

    public BigDecimal ReadDecimal() throws Exception {
        byte rd = buf.get();
        if (rd == NULL) return BigDecimal.ZERO;
        else if (rd != DECIMAL) dataError();
        long v = buf.getLong();
        int ignore = buf.getInt();
        int flag = buf.getInt();
        if ((flag&0x80000000)!=0) v=-v;
        flag = (flag & 0x00FF0000)>>16;
        BigDecimal ret = new BigDecimal(v);
        if (flag>0) ret = ret.movePointLeft(flag);
        return ret;
    }
    public BigDecimal ReadDecimalNullable() throws Exception {
        byte rd = buf.get();
        if (rd == NULL) return null;
        else if (rd != DECIMAL) dataError();
        long v = buf.getLong();
        int ignore = buf.getInt();
        int flag = buf.getInt();
        if ((flag&0x80000000)!=0) v=-v;
        flag = (flag & 0x00FF0000)>>16;
        BigDecimal ret = new BigDecimal(v);
        if (flag>0) ret = ret.movePointLeft(flag);
        return ret;
    }

    public DateTime ReadDateTime() throws Exception {
        byte rd = buf.get();
        if (rd == NULL) return null;
        else if (rd != DATETIME) dataError();
        long val = buf.getLong();
        val = (val - TICKS_AT_EPOCH) / TICKS_PER_MILLISECOND;
        DateTime ret = new DateTime(val);
        return ret;
    }


    public String ReadString() throws Exception {
        byte rd = buf.get();
        if (rd == NULL) return null;
        else if (rd != STRING) dataError();
        int len = (int)ReadCompressedNumber();
        byte[] sbuffer = new byte[len];
        buf.get(sbuffer);
        String ret = new String(sbuffer, StandardCharsets.UTF_8);
        return ret;
    }
    private String ReadStringRaw() throws Exception {
        int len = (int)ReadCompressedNumber();
        byte[] sbuffer = new byte[len];
        buf.get(sbuffer);
        String ret = new String(sbuffer, StandardCharsets.UTF_8);
        return ret;
    }


    public long ReadCompressedNumber()
    {
        int shift = 0;
        long ret = 0;
        long b;
        do
        {
            b = buf.get();
            ret |= (b & 0x7f) << shift;
            shift += 7;
        }
        while ((b & 0x80) != 0);
        return ret;
    }


    public boolean Match(byte[] data)
    {
        int n = data.length;
        for (int i = 0; i < n; i++) if (buf.get() != data[i]) return false;
        return true;
    }


    private void dataError() throws Exception
    {
        throw new Exception("Unexpected data type at FastBuffer read location!");
    }







}
