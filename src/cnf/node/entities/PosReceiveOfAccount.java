package cnf.node.entities;

import org.joda.time.DateTime;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 4/14/14
 * Time: 10:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class PosReceiveOfAccount {
    public long RoaId;
    public PosIdentity Pos;
    public CustomerIdentity Account;
    public DateTime EntryDateTime;
    public PosPaymentDetails Payment;


    public void validate() throws Exception
    {
        if (RoaId<=0) fail();
        if (Pos==null || Account==null || Payment==null) fail();
        Pos.validate();
        Account.validate();
        Payment.validate();
        if (!Payment.IsAccepted) fail();
        if (EntryDateTime==null) fail();
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Roa Data!");
    }


}
