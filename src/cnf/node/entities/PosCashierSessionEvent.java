package cnf.node.entities;

import org.joda.time.DateTime;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 4/14/14
 * Time: 10:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class PosCashierSessionEvent {
    public long CashierEventId;
    public String EventType;
    public DateTime EventDate;
    public PosIdentity Pos;
    public CashierIdentity Cashier;


    public void validate() throws Exception
    {
        if (CashierEventId<=0) fail();
        if (EventType==null) fail();
        if (!EventType.equals("LOGIN") && !EventType.equals("LOGOUT")) fail();
        if (EventDate==null) fail();
        if (Pos==null || Cashier==null) fail();
        Pos.validate();
        Cashier.validate();
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Cashier Session Data!");
    }

}
