package cnf.node.entities;

import cnf.pos.util.Utility;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 4/14/14
 * Time: 10:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class CashierIdentity {
    public long CashierId;
    public String CashierType;
    public String CashierPartyId;
    public String CashierName;

    public void validate() throws Exception
    {
        if (CashierId<=0) fail();
        if (CashierType==null) fail();
        if (!CashierType.equals("MANAGER") && !CashierType.equals("CASHIER")) fail();
        if (Utility.isEmpty(CashierName)) fail();
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Cashier Data!");
    }
}
