package cnf.node.entities;
import org.joda.time.DateTime;
import cnf.pos.util.Utility;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 4/14/14
 * Time: 10:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class PosCashTransfer {
    public long CashTransferId;
    public PosIdentity Pos;
    public DateTime EntryDate;
    public String TransferType;
    public String Reason;
    public String Description;
    public BigDecimal Value;

    public void validate() throws Exception
    {
        if (CashTransferId<=0) fail();
        if (Pos==null) fail();
        Pos.validate();
        if (EntryDate==null) fail();
        if (TransferType==null) fail();
        if (!TransferType.equals("IN") && !TransferType.equals("OUT")) fail();
        if (Utility.isEmpty(Reason)) fail();
        if (Value==null || Value.compareTo(BigDecimal.ZERO)==0) fail();
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Cash transfer Data!");
    }

}
