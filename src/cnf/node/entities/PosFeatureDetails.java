package cnf.node.entities;

import cnf.pos.util.Utility;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 4/14/14
 * Time: 10:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class PosFeatureDetails {
    public int SeqId;
    public String FeatureId;
    public String FeatureType;
    public String FeatureName;
    public BigDecimal FeatureValue;
    public String FeatureMode;


    public void validate() throws Exception
    {
        if (SeqId<0) fail();
        if (Utility.isEmpty(FeatureId)) fail();
        if (Utility.isEmpty(FeatureType)) fail();
        if (FeatureValue==null) fail();
        if (FeatureMode==null) fail();
        if (!FeatureMode.equals("PRE") && ! FeatureMode.equals("PST") && !FeatureMode.equals("STD")) fail();
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Feature Data!");
    }

}
