package cnf.node.entities;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 4/14/14
 * Time: 10:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class PosSettlement {
    public long SettlementId;
    public PosIdentity Pos;
    public String SettlementType;
    public DateTime SettlementDateTime;
    public List<PosSettlementDeclare> DeclaredValues = new ArrayList<PosSettlementDeclare>();
    public BigDecimal CalculatedValue;
    public BigDecimal DeclaredValue;
    public BigDecimal ErrorValue;

    public PosSettlement()
    {
        if (DeclaredValues==null) DeclaredValues = new ArrayList<PosSettlementDeclare>();
        if (CalculatedValue==null) CalculatedValue = BigDecimal.ZERO;
        if (DeclaredValue==null) DeclaredValue = BigDecimal.ZERO;
        if (ErrorValue==null) ErrorValue = BigDecimal.ZERO;
    }

    public void add(PosSettlementDeclare declare)
    {
        if (DeclaredValues==null) DeclaredValues = new ArrayList<PosSettlementDeclare>();
        if (CalculatedValue==null) CalculatedValue = BigDecimal.ZERO;
        if (DeclaredValue==null) DeclaredValue = BigDecimal.ZERO;
        if (ErrorValue==null) ErrorValue = BigDecimal.ZERO;
        DeclaredValues.add(declare);
        DeclaredValue = DeclaredValue.add(declare.DeclaredValue);
        CalculatedValue = CalculatedValue.add(declare.CalculatedValue);
        ErrorValue = ErrorValue.add(declare.ErrorValue);
    }

    public void addCalculated(String type, BigDecimal value)
    {
        int dseq = DeclaredValues.size();
        PosSettlementDeclare dec = null;
        for(PosSettlementDeclare d:DeclaredValues) if (d.DeclareType.equals(type)) {dec=d;break;}
        if (dec==null){
            dec = new PosSettlementDeclare(dseq,type,value,BigDecimal.ZERO);
            DeclaredValues.add(dec);
        }
        else dec.CalculatedValue = dec.CalculatedValue.add(value);
    }

    public BigDecimal getCalculatedValue(String type)
    {
        int dseq = DeclaredValues.size();
        PosSettlementDeclare dec = null;
        for(PosSettlementDeclare d:DeclaredValues) if (d.DeclareType.equals(type)) {dec=d;break;}
        if (dec==null){
            dec = new PosSettlementDeclare(dseq,type,BigDecimal.ZERO,BigDecimal.ZERO);
            DeclaredValues.add(dec);
        }
        return dec.CalculatedValue;
    }
    public PosSettlementDeclare get(String type)
    {
        int dseq = DeclaredValues.size();
        PosSettlementDeclare dec = null;
        for(PosSettlementDeclare d:DeclaredValues) if (d.DeclareType.equals(type)) {dec=d;break;}
        if (dec==null){
            dec = new PosSettlementDeclare(dseq,type,BigDecimal.ZERO,BigDecimal.ZERO);
            DeclaredValues.add(dec);
        }
        return dec;
    }


    public void validate() throws Exception
    {
        if (DeclaredValues==null) DeclaredValues = new ArrayList<PosSettlementDeclare>();
        if (CalculatedValue==null) CalculatedValue = BigDecimal.ZERO;
        if (DeclaredValue==null) DeclaredValue = BigDecimal.ZERO;
        if (ErrorValue==null) ErrorValue = BigDecimal.ZERO;
        if (SettlementId<=0) fail();
        if (Pos==null) fail();
        Pos.validate();
        if (SettlementType==null) fail();
        if (!SettlementType.equals("OPEN") && !SettlementType.equals("CLOSE")) fail();
        if (SettlementDateTime==null) fail();
        if (DeclaredValues.size()==0) fail();
        for(PosSettlementDeclare d : DeclaredValues) d.validate();
        if (CalculatedValue==null || DeclaredValue==null || ErrorValue==null) fail();
        if (!DeclaredValue.subtract(CalculatedValue).equals(ErrorValue)) fail();
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Settlement Data!");
    }


}
