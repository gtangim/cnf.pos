package cnf.node.entities;

import cnf.pos.util.Utility;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 4/14/14
 * Time: 10:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class PosPaymentDetails {
    public int SeqId;
    public String TransactionType;
    public String PaymentMethod;
    public long BillingAccountId;
    public BigDecimal LocalCurrencyValue;
    public BigDecimal OriginalCurrencyValue;
    public String OriginalCurrency;
    public String ReferenceNum;
    public String CardCompanyId;
    public String CardExpiry;
    public String CardTransactionNumber;
    public int CardInvoiceNum;
    public int CardSeqNum;
    public String CardApprovalCode;
    public String CardResponseCode;
    public String PinPadResponse;
    public String PinPadReversalResponse;
    public String TransactionStatus;
    public Boolean IsManual;
    public boolean IsAccepted;

    public void validate() throws Exception
    {
        if (SeqId<0) fail();
        if (!TransactionType.equals("PAYMENT") && !TransactionType.equals("REFUND")) fail();
        if (!TransactionStatus.equals("APPROVED") && !TransactionStatus.equals("DECLINED")
                && !TransactionStatus.equals("CANCELLED") && !TransactionStatus.equals("VOIDED")) fail();
        if (Utility.isEmpty(PaymentMethod)) fail();
        else if (PaymentMethod.equals("EXT_BILLACT") && BillingAccountId<=0) fail();
        if (LocalCurrencyValue==null) fail();
        if (OriginalCurrencyValue==null) fail();
        if (Utility.isEmpty(OriginalCurrency)) fail();

    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Payment Data!");
    }

}
