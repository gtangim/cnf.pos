package cnf.node.entities;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 4/14/14
 * Time: 10:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class PosAct {
    public String S;
    public String T;
    public String V;


    public PosAct(){}

    public PosAct(String source, String type)
    {
        S=source;
        T=type;
        V=null;
    }
    public PosAct(String source, String type, String value0)
    {
        S=source;
        T=type;
        V=filter(value0);
    }
    public PosAct(String source, String type, String value0, String value1)
    {
        S=source;
        T=type;
        V=filter(value0)+","+filter(value1);
    }
    public PosAct(String source, String type, String value0, String value1,String value2)
    {
        S=source;
        T=type;
        V=filter(value0)+","+filter(value1)+","+filter(value2);
    }
    public PosAct(String source, String type, String value0, String value1,String value2, String value3)
    {
        S=source;
        T=type;
        V=filter(value0)+","+filter(value1)+","+filter(value2)+","+filter(value3);
    }

    private String filter(String val)
    {
        if(val==null) return "#NULL";
        else return val;
    }

    @Override
    public String toString()
    {
        if (V!=null) return "ACTION ["+S+"] :: " + T +" = " + V;
        else return "ACTION ["+S+"] :: " + T;
    }
}
