package cnf.node.entities;

import cnf.pos.util.Utility;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 4/14/14
 * Time: 10:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class PosPromoDetails {
    public int SeqId;
    public int PromoId;
    public String ReportingCode;
    public String PromoType;
    public String PromoClass;
    public String PromoName;
    public String PromoCode;
    public BigDecimal DiscountedValue;


    public void validate() throws Exception
    {
        if (SeqId<0) fail();
        if (PromoId<=0) fail();
        if (PromoType==null || PromoClass==null) fail();
        if (!PromoType.equals("I") && !PromoType.equals("S")) fail();
        if (!PromoClass.equals("C") && !PromoClass.equals("D") && !PromoClass.equals("R")) fail();
        if (PromoClass.equals("C") && Utility.isEmpty(PromoCode)) fail();
        if (DiscountedValue==null) fail();
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Promo Data!");
    }

}
