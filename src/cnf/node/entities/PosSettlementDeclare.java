package cnf.node.entities;


import cnf.pos.util.Utility;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 4/14/14
 * Time: 10:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class PosSettlementDeclare {
    public int SeqId;
    public String DeclareType;
    public BigDecimal CalculatedValue = BigDecimal.ZERO;
    public BigDecimal DeclaredValue = BigDecimal.ZERO;
    public BigDecimal ErrorValue = BigDecimal.ZERO;

    public PosSettlementDeclare(){}
    public PosSettlementDeclare(int seqId, String declareType, BigDecimal calculatedValue, BigDecimal declaredValue)
    {
        SeqId=seqId;
        DeclareType = declareType;
        DeclaredValue = declaredValue;
        CalculatedValue = calculatedValue;
        ErrorValue = declaredValue.subtract(calculatedValue);
    }


    public void validate() throws Exception
    {
        if (SeqId<0) fail();
        if (Utility.isEmpty(DeclareType)) fail();
        if (CalculatedValue==null || DeclaredValue==null || ErrorValue==null) fail();
        if (!DeclaredValue.subtract(CalculatedValue).equals(ErrorValue)) fail();
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Declare Data!");
    }


}
