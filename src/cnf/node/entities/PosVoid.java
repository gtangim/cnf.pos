package cnf.node.entities;

import org.joda.time.DateTime;
import cnf.pos.util.Utility;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 4/17/14
 * Time: 4:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class PosVoid {

    public long OrderNumber;
    public String OrderId;
    public DateTime EntryDateTime;
    public PosIdentity Pos;
    public CashierIdentity Cashier;
    public List<PosAct> History;


    public void validate() throws Exception
    {
        if (OrderNumber<0) fail();
        if (Utility.isEmpty(OrderId)) fail();
        if (EntryDateTime==null) fail();
        if (Pos==null) fail();
        Pos.validate();
        if (Cashier==null) fail();
        Cashier.validate();
        if (History==null) fail();
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Sales Void Data!");
    }


}
