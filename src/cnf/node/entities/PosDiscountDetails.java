package cnf.node.entities;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 4/14/14
 * Time: 10:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class PosDiscountDetails {
    public BigDecimal DiscountPercent;
    public BigDecimal DiscountValue;
    public String ReasonId;
    public String ReportingCode;

    public void validate() throws Exception
    {
        if (DiscountPercent!=null &&
                (DiscountPercent.compareTo(BigDecimal.ZERO)==-1
                        || DiscountPercent.compareTo(new BigDecimal("100"))==1)) fail();
        if (DiscountValue==null) fail();
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Discount Data!");
    }


}
