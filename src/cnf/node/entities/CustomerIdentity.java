package cnf.node.entities;

import cnf.pos.util.Utility;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 4/14/14
 * Time: 10:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class CustomerIdentity {
    public long CustomerId;
    public String CustomerType;
    public String CustomerPartyId;
    public String CustomerName;

    public void validate() throws Exception
    {
        if (CustomerId<=0) fail();
        if (Utility.isEmpty(CustomerPartyId)) fail();
        if (Utility.isEmpty(CustomerName)) fail();
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Customer Data!");
    }


}
