package cnf.node.entities;

import cnf.pos.util.Utility;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 4/14/14
 * Time: 10:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class PosSalesItem {
    public int SeqId;
    public int ProductId;
    public String Sku;
    public String InputProductCode;
    public String ProductName;
    public int DepartmentId;
    public int SubDepartmentId;
    public int CategoryId;
    public String Unit;

    public Boolean IsMisc;
    public Boolean IsTaxable;
    public Boolean IsWeightProduct;

    public BigDecimal UnitCost;
    public BigDecimal UnitListPrice;
    public BigDecimal UnitPromoPrice;
    public BigDecimal UnitSpecialPromoPrice;
    public List<BigDecimal> UnitPackagePriceQuantityList;
    public List<BigDecimal> UnitPackagePriceList;
    public List<BigDecimal> UnitPromoPackagePriceQuantityList;
    public List<BigDecimal> UnitPromoPackagePriceList;

    public BigDecimal UnitSelectedPrice;
    public BigDecimal UnitFinalPrice;
    public int PriceRuleId;
    public String SelectedPriceType;
    public String PriceClampType;
    public String OverrideReason;
    public String OverrideReportingCode;
    public BigDecimal OverrideDifference;

    public BigDecimal Quantity;
    public int EffectiveQuantity;

    public BigDecimal DisplaySubTotal;


    public List<PosFeatureDetails> Features;
    public PosDiscountDetails ItemDiscount;
    public List<PosPromoDetails> ItemPromos;
    public List<PosPromoDetails> DistributedPromos;
    public PosDiscountDetails DistributedSaleDiscount;

    public BigDecimal ValueCost;
    public BigDecimal ValueListed;
    public BigDecimal ValueSelected;
    public BigDecimal ValueFinal;
    public BigDecimal ManualDiscountItemLevel;
    public BigDecimal PromoItemLevel;
    public BigDecimal FeaturePreTax;
    public BigDecimal FeaturePostTax;
    public BigDecimal FeatureStd;
    public BigDecimal FeatureTotal;


    public void validate() throws Exception
    {
        if (SeqId<0) fail();
        if (ProductId<=0) fail();
        if (Utility.isEmpty(Sku)) fail();
        if (Utility.isEmpty(InputProductCode)) fail();
        if (DepartmentId<=0) DepartmentId=1;
        if (SubDepartmentId<=0) SubDepartmentId=11;
        if (CategoryId<=0) CategoryId=111;
        if (Utility.isEmpty(Unit)) Unit="ea";

        if (UnitCost==null) fail();
        if (UnitListPrice==null) fail();
        if (UnitSelectedPrice==null) fail();
        if (UnitFinalPrice==null) fail();
        if (Utility.isEmpty(SelectedPriceType)) fail();
        if (OverrideReason!=null && OverrideDifference==null) fail();
        if (Quantity==null) fail();
        if (DisplaySubTotal==null) fail();

        for(PosFeatureDetails f:Features) f.validate();
        for(PosPromoDetails p:ItemPromos) p.validate();
        for(PosPromoDetails p:DistributedPromos) p.validate();
        if (ItemDiscount!=null) ItemDiscount.validate();

        if (ValueCost==null) fail();
        if (ValueListed==null) fail();
        if (ValueSelected==null) fail();
        if (ValueFinal==null) fail();
        if (ManualDiscountItemLevel==null) fail();
        if (PromoItemLevel==null) fail();
        if (FeaturePreTax==null) fail();
        if (FeaturePostTax==null) fail();
        if (FeatureStd==null) fail();
        if (FeatureTotal==null) fail();

        // TODO: In future recalc the total values and make sure the assigned values are correct

    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Settlement Data!");
    }


}
