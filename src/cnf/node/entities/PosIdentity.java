package cnf.node.entities;

import cnf.pos.util.Utility;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 4/14/14
 * Time: 10:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class PosIdentity {
    public int StoreNumber;
    public int TillNulber;
    public String Alias;
    public long CashierId;


    public void validate() throws Exception
    {
        if (StoreNumber<1 || StoreNumber>100) fail();
        if (TillNulber<1 || TillNulber>100) fail();
        if (Utility.isEmpty(Alias)) fail();
        if (CashierId<0) fail();
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Pos Data!");
    }

}
