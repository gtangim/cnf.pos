package cnf.node.entities;

import org.joda.time.DateTime;
import cnf.pos.util.Utility;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 4/14/14
 * Time: 10:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class PosSales {
    public long OrderNumber;
    public long ParentOrderNumber;
    public String ParentOrderRef;
    public String OrderId;
    public String TransactionMode;
    public String PaymentMode;
    public String ReceiptMode;
    public DateTime EntryDateTime;

    public PosIdentity Pos;
    public CashierIdentity Cashier;
    public CustomerIdentity Customer;

    public List<PosSalesItem> Items;
    public PosDiscountDetails SaleDiscount;
    public List<PosPromoDetails> SalePromos;
    public List<PosPaymentDetails> Payments;


    public BigDecimal SubTotal;
    public BigDecimal Tax;
    public BigDecimal GrandTotal;
    public BigDecimal TenderChange;
    public BigDecimal TenderRounding;
    public BigDecimal GCRounding;

    public BigDecimal TotalQuantity;
    public int TotalEffectiveQuantity;
    public BigDecimal TotalCost;
    public BigDecimal TotalPaid;
    public BigDecimal TotalValueListed;
    public BigDecimal TotalValueSelected;
    public BigDecimal TotalValueFinal;
    public BigDecimal TotalManualItemDiscount;
    public BigDecimal TotalManualSaleDiscount;
    public BigDecimal TotalManualDiscount;
    public BigDecimal TotalItemPromo;
    public BigDecimal TotalSalePromo;
    public BigDecimal TotalPromo;
    public BigDecimal TotalFeatureStd;
    public BigDecimal TotalFeaturePreTax;
    public BigDecimal TotalFeaturePostTax;
    public BigDecimal TotalFeature;

    public int ItemVoidCount;

    public List<PosAct> History;


    public void validate() throws Exception
    {
        if (OrderNumber<0) fail();
        if (Utility.isEmpty(OrderId)) fail();
        if (!OrderId.matches("^S\\d{1,2}T\\d{1,2}X\\d{5,8}$")) fail();
        if (TransactionMode==null) fail();
        valueIn(TransactionMode,"E","R","S");
        valueIn(PaymentMode,"D","E","I","T");
        valueIn(ReceiptMode, "PRN", "EML", "P_E","NA");
        if (EntryDateTime==null) fail();
        if (Pos==null) fail();
        Pos.validate();
        if (Cashier==null) fail();
        Cashier.validate();
        if (Customer!=null) Customer.validate();
        if (Items==null || SalePromos==null || Payments==null || Items.size()==0 || Payments.size()==0) fail();
        for(PosSalesItem item : Items) item.validate();
        for(PosPromoDetails promo : SalePromos) promo.validate();
        if (SaleDiscount!=null) SaleDiscount.validate();
        if (SubTotal==null) fail();
        if (Tax==null) fail();
        if (GrandTotal==null) fail();
        if (TenderChange==null) fail();
        if (TotalQuantity==null) fail();
        if (TotalCost==null) fail();
        if (TotalPaid==null) fail();
        if (TotalValueListed==null) fail();
        if (TotalValueSelected==null) fail();
        if (TotalValueFinal==null) fail();
        if (TotalManualItemDiscount==null) fail();
        if (TotalManualSaleDiscount==null) fail();
        if (TotalManualDiscount==null) fail();
        if (TotalItemPromo==null) fail();
        if (TotalSalePromo==null) fail();
        if (TotalPromo==null) fail();
        if (TotalFeatureStd==null) fail();
        if (TotalFeaturePreTax==null) fail();
        if (TotalFeaturePostTax==null) fail();
        if (TotalFeature==null) fail();
        if (History==null || History.size()==0) fail();
        if (SubTotal.add(Tax).compareTo(GrandTotal)!=0) fail();

        // TODO: Sum all items again and make sure they match with global totals
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Sales Data!");
    }

    private void valueIn(String value,String val0) throws Exception
    {
        if (value==null) fail();
        if (value.equals(val0)) return;
        fail();
    }
    private void valueIn(String value,String val0, String val1) throws Exception
    {
        if (value==null) fail();
        if (value.equals(val0)) return;
        if (value.equals(val1)) return;
        fail();
    }
    private void valueIn(String value,String val0, String val1, String val2) throws Exception
    {
        if (value==null) fail();
        if (value.equals(val0)) return;
        if (value.equals(val1)) return;
        if (value.equals(val2)) return;
        fail();
    }
    private void valueIn(String value,String val0, String val1, String val2, String val3) throws Exception
    {
        if (value==null) fail();
        if (value.equals(val0)) return;
        if (value.equals(val1)) return;
        if (value.equals(val2)) return;
        if (value.equals(val3)) return;
        fail();
    }


}
