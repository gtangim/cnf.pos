package cnf.node;

import bongo.node.NodeApi;
import bongo.util.*;
import cnf.node.entities.*;
import org.joda.time.DateTime;
import cnf.pos.util.Debug;
import cnf.pos.cart.PosShoppingCart;
import cnf.pos.cart.PosShoppingCartItem;
import cnf.pos.cart.PosShoppingCartPayment;
import cnf.pos.cart.resources.ResourceManager;
import cnf.pos.cart.resources.datastructures.PriceResult;
import cnf.pos.cart.resources.datastructures.PromoAdjustment;
import cnf.pos.cart.resources.elements.*;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.*;
import org.joda.time.format.ISODateTimeFormat;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 4/16/14
 * Time: 3:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class NodeClient extends Loggable {
    public static final String module = NodeClient.class.getName();

    //private NodeApi node;
    private NodeApi customerScreen;
    ResourceManager r;
    private Gson serializer = new GsonBuilder()
            .serializeNulls().setPrettyPrinting().registerTypeAdapter(DateTime.class, new JsonSerializer<DateTime>(){
                @Override
                public JsonElement serialize(DateTime json, Type typeOfSrc, JsonSerializationContext context) {
                    return new JsonPrimitive(ISODateTimeFormat.dateTime().print(json));
                }
            })
            .create();
    public static String DataFolder = "./resources/data/";
    //DateTime operationDay = PosState.startOfDay();

    public NodeClient(ResourceManager resource, NodeApi customerScreen)
    {
        //this.node = node;
        this.customerScreen = customerScreen;
        this.r = resource;
    }



     public void clearCustomerScreen()
     {
         try
         {
             //Debug.log("Node...........2ND SCREEN CLEAR");
             if (customerScreen!=null && customerScreen.isConnected())
                 customerScreen.sendCommand("clear","");
         }
         catch (Exception ex)
         {
             error(ex);
         }
     }



     public void postSalesToSecondScreen(PosShoppingCart cart)
     {
         //Debug.log("Node...........2ND SCREEN");
         try {
             if (customerScreen==null || !customerScreen.isConnected()) return;
             long seq = r.getSettings().getNextDocumentId();
             PosSales sale = new PosSales();
             sale.OrderNumber = seq;
             sale.ParentOrderNumber = -1;
             sale.ParentOrderRef = cart.getParentOrderId();
             //sale.OrderId = cart.getOrderId();
             sale.OrderId = Long.toString(seq); // Retired the old order ID
             sale.EntryDateTime = new DateTime(cart.getOrderDate());

             if (cart.isRoaMode()) sale.TransactionMode = "X";
             else if (cart.getParentOrderId()!=null || cart.getCustomerExternalId()!=null) sale.TransactionMode="R";
             else sale.TransactionMode = "S";

             if (cart.isEmpty()) sale.PaymentMode="N";
             else if (cart.isReadOnly()) sale.PaymentMode="C";
             else if (cart.isPaymentProcessing()) sale.PaymentMode="P";
             else sale.PaymentMode="A";


             if (cart.getSaleDiscount()!=null)
             {
                 PosDiscountDetails s = new PosDiscountDetails();
                 //s.SeqId=0;
                 s.DiscountPercent = cart.getSaleDiscount().getPercent();
                 s.DiscountValue = cart.getDiscountAmount();
                 s.ReportingCode = cart.getSaleDiscount().getReportingCode();
                 //s.ReportingCode = "31";
                 sale.TotalManualSaleDiscount = s.DiscountValue;
                 sale.SaleDiscount = s;
             }
             else sale.TotalManualSaleDiscount = BigDecimal.ZERO;

             sale.TotalPaid=BigDecimal.ZERO;
             sale.Payments = new ArrayList<PosPaymentDetails>();
             List<PosShoppingCartPayment> payments = cart.getPayments();
             for (int i=0;i<payments.size();i++)
             {
                 PosPaymentDetails pay = getPaymentCS(i,payments.get(i));
                 if (pay.IsAccepted)
                 {
                     sale.TotalPaid = sale.TotalPaid.add(pay.LocalCurrencyValue);
                     if (pay.PaymentMethod.equals("EXT_BILLACT") && sale.Customer!=null)
                     {
                         pay.BillingAccountId = sale.Customer.CustomerId;
                     }
                 }
                 sale.Payments.add(pay);
             }

             sale.SubTotal = cart.getSubTotal();
             sale.Tax = BigDecimal.ZERO;
             List<TaxElement> tList = cart.getResource().getTaxes().getStoreTaxes();
             for(TaxElement t:tList)
             {
                 BigDecimal taxValue = cart.getTax(t.TaxId);
                 sale.Tax = sale.Tax.add(taxValue);
             }
             sale.GrandTotal = cart.getGrandTotal();
             sale.TenderChange = cart.getDue().negate();


             sale.Pos = getPosIdentity();
             sale.Cashier = getCashier();
             sale.Customer = getCustomer(cart);


             sale.Items = new ArrayList<PosSalesItem>();

             for (int i=0;i<cart.getItems().size();i++)
             {
                 PosSalesItem item = getItemCS(i,cart);
                 sale.Items.add(item);
             }

             sale.SalePromos = new ArrayList<PosPromoDetails>();
             List<PromoAdjustment> promos = cart.getPromoAdjustments(-1);
             int pseq = 0;
             for(PromoAdjustment promo:promos)
             {
                 PosPromoDetails p = new PosPromoDetails();
                 p.SeqId= pseq++;
                 p.PromoId = Integer.parseInt(promo.getPromoId());
                 p.PromoCode = "02";
                 p.PromoType = "S";
                 p.PromoName = r.getPromos().getPromo(promo.getPromoId()).Name;
                 p.DiscountedValue = promo.getAmount();
                 p.PromoClass = r.getPromos().getPromo(promo.getPromoId()).PromoType.toString().substring(0, 1);
                 p.PromoCode = promo.getPromoCode();
                 sale.SalePromos.add(p);
                 //sale.TotalSalePromo = sale.TotalSalePromo.add(p.DiscountedValue);
             }

             if (customerScreen.isConnected())
                customerScreen.sendCommand("submit", sale);
         }
         catch (Exception ex)
         {
             error(ex);
         }

     }











    public boolean postSalesToDataSyncQueue(PosShoppingCart cart, List<PosAct> history)
    {
        //Debug.log("Node...........SALES");
        try {
            if (r.getSecurity().isTrainingMode()) return true;
            if (cart.isRoaMode()) throw new Exception("Invalid shopping cart state!");
            long seq = r.getSettings().getNextDocumentId();
            PosSales sale = new PosSales();
            sale.OrderNumber = seq;
            sale.ParentOrderNumber = -1;
            sale.ParentOrderRef = cart.getParentOrderId();
            //sale.OrderId = cart.getOrderId();
            sale.OrderId = Long.toString(seq);
            sale.EntryDateTime = new DateTime(cart.getOrderDate());
            sale.ReceiptMode = cart.getReceiptOption();

            // These two needs to be calculated....
            //sale.PaymentMode = cart.getActivePayments()[0].
            //sale.TransactionMode = cart.isRe



            sale.TotalPaid=BigDecimal.ZERO;
            sale.PaymentMode="T";
            sale.Payments = new ArrayList<PosPaymentDetails>();
            List<PosShoppingCartPayment> payments = cart.getPayments();
            for (int i=0;i<payments.size();i++)
            {
                PosPaymentDetails pay = getPayment(i,payments.get(i));
                if (pay.IsAccepted)
                {
                    sale.TotalPaid = sale.TotalPaid.add(pay.LocalCurrencyValue);
                    if (pay.PaymentMethod.equals("EXT_BILLACT") && sale.Customer!=null)
                    {
                        sale.PaymentMode="E";
                        pay.BillingAccountId = sale.Customer.CustomerId;
                        if (sale.Customer.CustomerType.startsWith("INT")) sale.PaymentMode="I";
                    }
                }
                sale.Payments.add(pay);
            }
            if (cart.isDonation()) sale.PaymentMode="D";

            sale.SubTotal = cart.getSubTotal();
            sale.Tax = BigDecimal.ZERO;
            List<TaxElement> tList = cart.getResource().getTaxes().getStoreTaxes();
            for(TaxElement t:tList)
            {
                BigDecimal taxValue = cart.getTax(t.TaxId);
                //if (!taxValue.equals(cart.ZERO))
                //    taxes.add(UtilMisc.toMap("text", tarp.getDescription(), "value", taxValue));
                sale.Tax = sale.Tax.add(taxValue);
            }
            sale.GrandTotal = cart.getGrandTotal();
            sale.TenderChange = cart.getDue().negate();
            sale.TenderRounding = cart.getCashRounding();
            sale.GCRounding = cart.getGcRounding();

            sale.Pos = getPosIdentity();
            sale.Cashier = getCashier();
            sale.Customer = getCustomer(cart);


            sale.TotalQuantity = BigDecimal.ZERO;
            sale.TotalEffectiveQuantity = 0;
            sale.TotalCost = BigDecimal.ZERO;
            //sale.TotalPaid = BigDecimal.ZERO;
            sale.TotalValueListed = BigDecimal.ZERO;
            sale.TotalValueSelected = BigDecimal.ZERO;
            sale.TotalValueFinal = BigDecimal.ZERO;
            sale.TotalManualItemDiscount = BigDecimal.ZERO;
            //sale.TotalManualSaleDiscount = BigDecimal.ZERO;
            //sale.TotalManualDiscount = BigDecimal.ZERO;
            sale.TotalItemPromo = BigDecimal.ZERO;
            sale.TotalSalePromo = BigDecimal.ZERO;
            //sale.TotalPromo = BigDecimal.ZERO;
            sale.TotalFeatureStd = BigDecimal.ZERO;
            sale.TotalFeaturePreTax = BigDecimal.ZERO;
            sale.TotalFeaturePostTax = BigDecimal.ZERO;
            sale.TotalFeature = BigDecimal.ZERO;
            sale.Items = new ArrayList<PosSalesItem>();

            for (int i=0;i<cart.getItems().size();i++)
            {
                PosSalesItem item = getItem(i,cart);

                sale.TotalQuantity = sale.TotalQuantity.add(item.Quantity);
                sale.TotalEffectiveQuantity += item.EffectiveQuantity;
                sale.TotalCost = sale.TotalCost.add(item.UnitCost.multiply(item.Quantity));
                sale.TotalValueListed = sale.TotalValueListed.add(item.ValueListed);
                sale.TotalValueSelected = sale.TotalValueSelected.add(item.ValueSelected);
                sale.TotalValueFinal = sale.TotalValueFinal.add(item.ValueFinal);
                if (item.ItemDiscount!=null)
                    sale.TotalManualItemDiscount = sale.TotalManualItemDiscount.add(item.ItemDiscount.DiscountValue);
                sale.TotalItemPromo = sale.TotalItemPromo.add(item.PromoItemLevel);
                sale.TotalFeatureStd = sale.TotalFeatureStd.add(item.FeatureStd);
                sale.TotalFeaturePreTax = sale.TotalFeaturePreTax.add(item.FeaturePreTax);
                sale.TotalFeaturePostTax = sale.TotalFeaturePostTax.add(item.FeaturePostTax);
                sale.TotalFeature = sale.TotalFeature.add(item.FeatureTotal);
                if (item.Quantity.compareTo(BigDecimal.ZERO)==-1 && sale.TransactionMode==null)
                    sale.TransactionMode="R";
                else if (item.Quantity.compareTo(BigDecimal.ZERO)==1 && sale.TransactionMode==null)
                    sale.TransactionMode="S";
                else if (item.Quantity.compareTo(BigDecimal.ZERO)==-1 && sale.TransactionMode.equals("S"))
                    sale.TransactionMode="E";
                else if (item.Quantity.compareTo(BigDecimal.ZERO)==1 && sale.TransactionMode.equals("R"))
                    sale.TransactionMode="E";
                sale.Items.add(item);
            }
            sale.TotalCost = sale.TotalCost.setScale(r.getSettings().getScale(),r.getSettings().getRoundingMode());


            if (cart.getSaleDiscount()!=null)
            {
                PosDiscountDetails s = new PosDiscountDetails();
                //s.SeqId=0;
                s.DiscountPercent = cart.getSaleDiscount().getPercent();
                s.DiscountValue = cart.getDiscountAmount();
                s.ReasonId = cart.getSaleDiscount().getReasonId();
                s.ReportingCode = cart.getSaleDiscount().getReportingCode();
                //s.ReportingCode = "31";
                sale.TotalManualSaleDiscount = s.DiscountValue;
                sale.SaleDiscount = s;
                BigDecimal remaining = s.DiscountValue;
                double totalSourceValue = 0;
                PosSalesItem maxItem = null;
                for(PosSalesItem item: sale.Items) {
                    totalSourceValue += item.DisplaySubTotal.doubleValue();
                    if (maxItem==null || maxItem.DisplaySubTotal.abs().compareTo(item.DisplaySubTotal.abs())==-1)
                        maxItem = item;
                }
                for (PosSalesItem item: sale.Items) {
                    double amtD = s.DiscountValue.doubleValue() * item.DisplaySubTotal.doubleValue() / totalSourceValue;
                    BigDecimal amt = new BigDecimal(amtD).setScale(r.getSettings().getScale(), RoundingMode.FLOOR);
                    if (amt.compareTo(BigDecimal.ZERO)!=0){
                        PosDiscountDetails sd = new PosDiscountDetails();
                        sd.DiscountPercent = s.DiscountPercent;
                        sd.DiscountValue = amt;
                        sd.ReportingCode = s.ReportingCode;
                        sd.ReasonId = s.ReasonId;
                        item.DistributedSaleDiscount = sd;
                        remaining = remaining.subtract(amt);
                    }
                }
                if (remaining.compareTo(BigDecimal.ZERO)!=0){
                    if (maxItem.DistributedSaleDiscount==null){
                        PosDiscountDetails sd = new PosDiscountDetails();
                        sd.DiscountPercent = s.DiscountPercent;
                        sd.DiscountValue = BigDecimal.ZERO;
                        sd.ReportingCode = s.ReportingCode;
                        sd.ReasonId = s.ReasonId;
                        maxItem.DistributedSaleDiscount = sd;
                    }
                    maxItem.DistributedSaleDiscount.DiscountValue = maxItem.DistributedSaleDiscount.DiscountValue.add(remaining);
                }
            }
            else sale.TotalManualSaleDiscount = BigDecimal.ZERO;



            sale.TotalSalePromo = BigDecimal.ZERO;
            sale.SalePromos = new ArrayList<PosPromoDetails>();
            List<PromoAdjustment> promos = cart.getPromoAdjustments(-1);
            int pseq = 0;
            for(PromoAdjustment promo:promos)
            {
                PosPromoDetails p = new PosPromoDetails();
                p.SeqId= pseq++;
                p.PromoId = Integer.parseInt(promo.getPromoId());
                p.PromoName = r.getPromos().getPromo(promo.getPromoId()).Name;
                p.ReportingCode = r.getPromos().getPromo(promo.getPromoId()).ReportingCode;
                p.PromoType = "S";
                p.DiscountedValue = promo.getAmount();
                p.PromoClass = r.getPromos().getPromo(promo.getPromoId()).PromoType.toString().substring(0,1);
                p.PromoCode = promo.getPromoCode();
                sale.SalePromos.add(p);
                sale.TotalSalePromo = sale.TotalSalePromo.add(p.DiscountedValue);
            }

            sale.TotalManualDiscount = sale.TotalManualItemDiscount.add(sale.TotalManualSaleDiscount);
            sale.TotalPromo = sale.TotalItemPromo.add(sale.TotalSalePromo);

            sale.ItemVoidCount = cart.getItemVoidCount();

            sale.History = history;



            //String res = (String)node.sendCommand("submit", "trans", seq, sale);
            saveDocument(seq, sale);


            return true;
        }
        catch (Exception ex)
        {
            error(ex);
            return false;
        }
    }


    public boolean postCashierLoginToDataSyncQueue()
    {
        //Debug.log("Node...........LOGIN");
        try {
            if (r.getSecurity().isTrainingMode()) return true;
            long seq = r.getSettings().getNextDocumentId();
            PosCashierSessionEvent event = new PosCashierSessionEvent();
            event.Cashier = getCashier();
            event.Pos = getPosIdentity();
            event.CashierEventId = seq;
            event.EventDate =new DateTime();
            event.EventType = "LOGIN";
            if (r.getSettings().isUserEventsPostEnabled()) saveDocument(seq, event);
            return true;
        }
        catch (Exception ex)
        {
            error(ex);
            return false;
        }
    }

    public boolean postCashierLogoutToDataSyncQueue()
    {
        //Debug.log("Node...........LOGOUT");
        try {
            if (r.getSecurity().isTrainingMode()) return true;
            long seq = r.getSettings().getNextDocumentId();
            PosCashierSessionEvent event = new PosCashierSessionEvent();
            event.Cashier = getCashier();
            event.Pos = getPosIdentity();
            event.CashierEventId = seq;
            event.EventDate =new DateTime();
            event.EventType = "LOGOUT";
            if (r.getSettings().isUserEventsPostEnabled()) saveDocument(seq, event);
            return true;
        }
        catch (Exception ex)
        {
            error(ex);
            return false;
        }
    }

    public boolean postSalesVoidToDataSyncQueue(PosShoppingCart cart, List<PosAct> history)
    {
        //Debug.log("Node...........VOID SALES");
        try {
            if (r.getSecurity().isTrainingMode()) return true;
            long seq = r.getSettings().getNextDocumentId();
            PosVoid v = new PosVoid();
            v.OrderNumber = seq;
            //v.OrderId = cart.getOrderId();
            v.OrderId = Long.toString(seq);
            v.Cashier = getCashier();
            v.Pos = getPosIdentity();
            v.EntryDateTime = new DateTime();
            v.History = history;

            saveDocument(seq, v);
            return true;
        }
        catch (Exception ex)
        {
            error(ex);
            return false;
        }
    }

    public PosCashTransfer createCashTransfer(String transferType
            , String reasonId, String comment, BigDecimal amount)
    {
        //Debug.log("Node...........CASHTX");
        try {
            long seq = r.getSettings().getNextDocumentId();
            PosCashTransfer tx = new PosCashTransfer();
            tx.CashTransferId = seq;
            tx.Pos = getPosIdentity();
            tx.EntryDate = new DateTime();
            tx.TransferType = transferType;
            tx.Reason = reasonId;
            tx.Description = comment;
            tx.Value = amount;
            if (transferType.equals("OUT")) tx.Value = tx.Value.negate();
            return tx;
        }
        catch (Exception ex)
        {
            error(ex);
            return null;
        }
    }
    public boolean postCashTransferToDataSyncQueue(PosCashTransfer tx)
    {
        //Debug.log("Node...........CASHTX");
        try {
            long seq = r.getSettings().getNextDocumentId();
            tx.CashTransferId = seq;
            saveDocument(seq, tx);
            return true;
        }
        catch (Exception ex)
        {
            error(ex);
            return false;
        }
    }

    public PosReceiveOfAccount createRoa(PosShoppingCart roaCart)
    {
        //Debug.log("Node...........ROA");
        try {
            if (!roaCart.isRoaMode()) throw new Exception("Invalid shopping cart state!");
            if (roaCart.getActivePayments().size()>1) throw new Exception("Invalid shopping cart state!");
            PosShoppingCartPayment payment = roaCart.getActivePayments().get(0);

            long seq = r.getSettings().getNextDocumentId();
            PosReceiveOfAccount roa = new PosReceiveOfAccount();
            roa.RoaId = seq;
            roa.Pos = getPosIdentity();
            roa.Account = getCustomer(roaCart);
            if (roa.Account==null) throw new Exception("Must have a customer account in roa mode!");
            roa.Payment = getPayment(0,payment);
            roa.Payment.BillingAccountId = roa.Account.CustomerId;
            roa.EntryDateTime = new DateTime(roaCart.getOrderDate());
            return roa;
        }
        catch (Exception ex)
        {
            error(ex);
            return null;
        }
    }
    public boolean postRoaToDataSyncQueue(PosReceiveOfAccount roa)
    {
        //Debug.log("Node...........ROA");
        try {
            if (r.getSecurity().isTrainingMode()) return true;
            long seq = r.getSettings().getNextDocumentId();
            roa.RoaId = seq;
            saveDocument(seq, roa);
            return true;
        }
        catch (Exception ex)
        {
            error(ex);
            return false;
        }
    }









    public long allocateTransactionSequence() throws Exception
    {
        //Debug.log("Node...........SEQ");
        long seq = r.getSettings().getNextDocumentId();
        return seq;
    }








    private CashierIdentity getCashier() throws Exception
    {
        UserElement user = r.getSecurity().getUser();
        if (user==null) user = r.getSecurity().getSystemUser();
        if (user==null) throw new Exception("No user is logged in and the default system user is not found!");
        CashierIdentity c = new CashierIdentity();
        c.CashierId = Long.parseLong(user.UserNumber);
        c.CashierName = user.Description;
        c.CashierPartyId = user.UserId;
        if (user.hasRole("MANAGER"))
            c.CashierType = "MANAGER";
        else c.CashierType = "CASHIER";
        return c;
    }

    private PosIdentity getPosIdentity() throws Exception
    {
        UserElement user = r.getSecurity().getUser();
        if (user==null) user = r.getSecurity().getSystemUser();
        if (user==null) throw new Exception("No user is logged in and the default system user is not found!");
        PosIdentity ret = new PosIdentity();
        ret.Alias = r.getSettings().getPosTerminalPrefix();
        ret.CashierId = Long.parseLong(user.UserNumber);
        ret.StoreNumber = Integer.parseInt(r.getConfig().StoreId.substring(1));
        ret.TillNulber = r.getConfig().TerminalNumber;
        return ret;
    }

    private void saveDocument(long documentId, Object document) throws Exception {
        Class objClass = document.getClass();
        String documentType = objClass.getSimpleName();
        String alias = r.getSettings().getPosTerminalPrefix();
        String oDay = r.getSettings().getCurrentOperationDay().toString("yyyy_MM_dd");
        String path = Utility.appendDir(DataFolder, alias+ "/"+oDay+"/");
        if (!Utility.createDirectory(path))
            throw new Exception("Unable to create path: " + path);
        String dt = new DateTime().toString("yyyyMMddHHmmss");
        String fname = alias + "_" + documentType + "_"+dt + "_" + Long.toString((documentId))+".json";
        String json = serializer.toJson(document, objClass);
        if (!Utility.stringToFile(path+fname, json))
            throw  new Exception("Unable to save document to file: " + fname);
        r.getSettings().savePosState(documentId);
    }

    private CustomerIdentity getCustomer(PosShoppingCart cart)
    {
        if (cart.getCustomer()==null) return null;
        CustomerElement c = cart.getCustomer();
        CustomerIdentity ret = new CustomerIdentity();
        ret.CustomerId = Long.parseLong(c.CustomerId);
        ret.CustomerName = c.Description;
        ret.CustomerPartyId = c.PartyId;
        ret.CustomerType = c.ClassificationId;
        return ret;
    }
    private PosPaymentDetails getPayment(int seqId, PosShoppingCartPayment payment)
    {
        PosPaymentDetails ret = new PosPaymentDetails();
        ret.SeqId=seqId;
        ret.PaymentMethod = payment.getPayTypeExtended();
        //ret.BillingAccountId = null;
        ret.LocalCurrencyValue = payment.getAmount();
        ret.OriginalCurrencyValue = payment.getInputAmount();
        ret.OriginalCurrency = payment.getInputAmountUomId();
        ret.ReferenceNum = payment.getReferenceNumber();
        ret.CardCompanyId = payment.getCardCompany();
        ret.CardExpiry = payment.getExpiry();
        ret.CardInvoiceNum = 0;
        if (!StringUtil.empty(payment.getInvoiceId()))
        {
            String[] id = payment.getInvoiceId().split("\\-");
            if (id.length>0) ret.CardInvoiceNum = Integer.parseInt(id[id.length-1]);
            if (id.length>=2) ret.CardTransactionNumber = id[0];
        }
        ret.PinPadResponse = payment.getAdditionalDescription();
        ret.PinPadReversalResponse = payment.getReversalDescription();
        ret.CardSeqNum = 0;
        if (payment.isCATPayment() && payment.getAdditionalDescription()!=null)
            ret.CardSeqNum = Integer.parseInt(StringUtil.between(payment.getAdditionalDescription(), ",S", ","));

        ret.CardApprovalCode = payment.getAuthCode();
        ret.CardResponseCode = "OK";
        if (payment.isCATPayment() && payment.getAdditionalDescription()!=null)
            ret.CardResponseCode = StringUtil.between(payment.getAdditionalDescription(),",R",",");
        ret.IsManual = !payment.isCATPayment();
        ret.IsAccepted =  payment.isActive();
        if (payment.getTransactionType()==PosShoppingCartPayment.PAYMENT) ret.TransactionType="PAYMENT";
        else if (payment.getTransactionType()==PosShoppingCartPayment.REFUND) ret.TransactionType="REFUND";
        if (payment.getTransactionStatus()==PosShoppingCartPayment.STAT_APPROVED) ret.TransactionStatus="APPROVED";
        else if (payment.getTransactionStatus()==PosShoppingCartPayment.STAT_DECLINED) ret.TransactionStatus="DECLINED";
        else if (payment.getTransactionStatus()==PosShoppingCartPayment.STAT_CANCELLED) ret.TransactionStatus="CANCELLED";
        else if (payment.getTransactionStatus()==PosShoppingCartPayment.STAT_VOIDED) ret.TransactionStatus="VOIDED";

        return ret;
    }
    private PosPaymentDetails getPaymentCS(int seqId, PosShoppingCartPayment payment)
    {
        PosPaymentDetails ret = new PosPaymentDetails();
        ret.SeqId=seqId;
        ret.PaymentMethod = payment.getPayTypeExtended();
        //ret.BillingAccountId = null;
        ret.LocalCurrencyValue = payment.getAmount();
        ret.OriginalCurrencyValue = payment.getInputAmount();
        ret.OriginalCurrency = payment.getInputAmountUomId();
        ret.ReferenceNum = payment.getReferenceNumber();
        ret.CardCompanyId = payment.getCardCompany();
        ret.CardInvoiceNum = 0;
        if (!StringUtil.empty(payment.getInvoiceId()))
        {
            String[] id = payment.getInvoiceId().split("\\-");
            if (id.length>0) ret.CardInvoiceNum = Integer.parseInt(id[id.length-1]);
            if (id.length>=2) ret.CardTransactionNumber = id[0];
        }

        ret.CardApprovalCode = payment.getAuthCode();
        ret.CardResponseCode = "OK";
        if (payment.isCATPayment() && payment.getAdditionalDescription()!=null)
            ret.CardResponseCode = StringUtil.between(payment.getAdditionalDescription(),",R",",");
        ret.IsManual = !payment.isCATPayment();
        ret.IsAccepted =  payment.isActive();
        if (payment.getTransactionType()==PosShoppingCartPayment.PAYMENT) ret.TransactionType="PAYMENT";
        else if (payment.getTransactionType()==PosShoppingCartPayment.REFUND) ret.TransactionType="REFUND";
        if (payment.getTransactionStatus()==PosShoppingCartPayment.STAT_APPROVED) ret.TransactionStatus="APPROVED";
        else if (payment.getTransactionStatus()==PosShoppingCartPayment.STAT_DECLINED) ret.TransactionStatus="DECLINED";
        else if (payment.getTransactionStatus()==PosShoppingCartPayment.STAT_CANCELLED) ret.TransactionStatus="CANCELLED";
        else if (payment.getTransactionStatus()==PosShoppingCartPayment.STAT_VOIDED) ret.TransactionStatus="VOIDED";

        return ret;
    }
    private PosSalesItem getItem(int seqId, PosShoppingCart cart)
    {
        PosShoppingCartItem item = cart.getItems().get(seqId);
        PosSalesItem ret = new PosSalesItem();
        ret.Features = new ArrayList<PosFeatureDetails>();
        ret.ItemPromos = new ArrayList<PosPromoDetails>();
        ret.DistributedPromos = new ArrayList<PosPromoDetails>();

        ret.SeqId = seqId;
        ret.ProductId = Integer.parseInt(item.getProductId());
        ret.Sku = item.getSku();
        ret.InputProductCode = item.getInputProductCode();
        String d = item.getProduct().DepartmentId;
        String sd = item.getProduct().SubDepartmentId;
        String sdc = item.getProduct().CategoryId;
        if (d==null) d="1";
        if (sd==null) sd="11";
        if (sdc==null) sdc="111";
        ret.DepartmentId = Integer.parseInt(d);
        ret.SubDepartmentId = Integer.parseInt(sd);
        ret.CategoryId = Integer.parseInt(sdc);
        ret.ProductName = item.getProduct().Name;
        ret.Unit = item.getProductUom().Abbrev;
        ret.IsMisc = false; // not yet fully implemented...
        ret.IsTaxable = item.isTaxable();
        ret.IsWeightProduct = item.isScalable();
        ret.UnitCost = item.getCost();
        PriceElement pr = item.getProduct().Price;
        boolean pmode = item.isUseGrossPrice();
        ret.UnitListPrice = pr.ListPrice.getPrice(pmode);
        if (pr.PromoPrice!=null) ret.UnitPromoPrice = pr.PromoPrice.getPrice(pmode);
        if (pr.SpecialPromoPrice!=null) ret.UnitSpecialPromoPrice = pr.SpecialPromoPrice.getPrice(pmode);
        ret.UnitPackagePriceQuantityList = new ArrayList<BigDecimal>();
        ret.UnitPackagePriceList = new ArrayList<BigDecimal>();
        if (pr.PackagePriceList!=null)
            for (int i=0;i<pr.PackagePriceList.size();i++){
                ret.UnitPackagePriceQuantityList.add(pr.PackagePriceQuantityList.get(i));
                ret.UnitPackagePriceList.add(pr.PackagePriceList.get(i).getPrice(pmode));
            }
        ret.UnitPromoPackagePriceQuantityList = new ArrayList<BigDecimal>();
        ret.UnitPromoPackagePriceList = new ArrayList<BigDecimal>();
        if (pr.PromoPackagePriceList!=null)
            for (int i=0;i<pr.PromoPackagePriceList.size();i++){
                ret.UnitPromoPackagePriceQuantityList.add(pr.PromoPackagePriceQuantityList.get(i));
                ret.UnitPromoPackagePriceList.add(pr.PromoPackagePriceList.get(i).getPrice(pmode));
            }



        PriceResult pres = item.getSelectedPrice();
        ret.PriceRuleId=0;
        if (pres.getPriceRuleId()!=null) ret.PriceRuleId = Integer.parseInt(pres.getPriceRuleId());
        ret.UnitSelectedPrice = pres.getPrice().getPrice(item.isUseGrossPrice());
        ret.SelectedPriceType = pres.getPriceType();
        ret.PriceClampType = pres.getClampType();
        ret.UnitFinalPrice = item.getBasePrice();
        if (item.getOverrideReason()!=null)
        {
            ret.OverrideReason = item.getOverrideReason();
            ret.OverrideReportingCode = item.getOverrideReportingCode();
            ret.OverrideDifference = item.getSubtotal().subtract(item.getSubtotalBeforeOverride());
        }

        ret.Quantity = item.getQuantity();
        if (ret.IsWeightProduct) ret.EffectiveQuantity = 1;
        else ret.EffectiveQuantity = ret.Quantity.intValue();

        ret.DisplaySubTotal = item.getSubtotal();


        ret.ValueCost = ret.UnitCost.multiply(ret.Quantity);
        ret.ValueListed = ret.UnitListPrice.multiply(ret.Quantity);
        ret.ValueSelected = ret.UnitSelectedPrice.multiply(ret.Quantity);
        ret.ValueFinal = ret.UnitFinalPrice.multiply(ret.Quantity);

        if (item.getItemDiscount()!=null)
        {
            PosDiscountDetails idisc = new PosDiscountDetails();
            //idisc.SeqId = 0;
            idisc.DiscountPercent = item.getItemDiscount().getPercent();
            idisc.DiscountValue = item.getItemDiscountAmount();
            idisc.ReasonId = item.getItemDiscount().getReasonId();
            idisc.ReportingCode = item.getItemDiscount().getReportingCode();
            //idisc.ReportingCode = "41";
            ret.ItemDiscount = idisc;
            ret.ManualDiscountItemLevel = idisc.DiscountValue;
        }
        else ret.ManualDiscountItemLevel = BigDecimal.ZERO;

        ret.FeatureStd = BigDecimal.ZERO;
        ret.FeaturePreTax = BigDecimal.ZERO;
        ret.FeaturePostTax = BigDecimal.ZERO;
        ret.FeatureTotal = BigDecimal.ZERO;

        int fseq = 0;
        for(String featureType:cart.getFeatureTypeNames())
        {
            List<FeatureElement> fList = item.getFeatures(featureType);
            for(FeatureElement fae:fList)
            {
                PosFeatureDetails f = new PosFeatureDetails();
                f.SeqId = fseq++;
                f.FeatureId = fae.FeatureId;
                f.FeatureName = item.getFeatureDisplayName(fae);
                f.FeatureType = fae.FeatureApplTypeId;
                f.FeatureValue = item.getFeatureDisplayValue(fae);
                if (fae.FeatureApplTypeId.equals("STANDARD_FEATURE"))
                {
                    f.FeatureMode = "STD";
                    ret.FeatureStd = ret.FeatureStd.add(f.FeatureValue);
                    ret.FeatureTotal = ret.FeatureTotal.add(f.FeatureValue);
                }
                else if (cart.isFeaturePreTax(f.FeatureId))
                {
                    f.FeatureMode = "PRE";
                    ret.FeaturePreTax = ret.FeaturePreTax.add(f.FeatureValue);
                    ret.FeatureTotal = ret.FeatureTotal.add(f.FeatureValue);
                }
                else
                {
                    f.FeatureMode = "PST";
                    ret.FeaturePostTax = ret.FeaturePostTax.add(f.FeatureValue);
                    ret.FeatureTotal = ret.FeatureTotal.add(f.FeatureValue);
                }
                ret.Features.add(f);
            }
        }

        ret.PromoItemLevel =BigDecimal.ZERO;
        List<PromoAdjustment> promos = cart.getPromoAdjustments(seqId);
        int pseq = 0;
        for(PromoAdjustment promo:promos)
        {
            PosPromoDetails p = new PosPromoDetails();
            p.SeqId= pseq++;
            p.PromoId = Integer.parseInt(promo.getPromoId());
            p.PromoName = r.getPromos().getPromo(promo.getPromoId()).Name;
            p.ReportingCode = r.getPromos().getPromo(promo.getPromoId()).ReportingCode;
            p.PromoType = "I";
            p.DiscountedValue = promo.getAmount();
            p.PromoClass = r.getPromos().getPromo(promo.getPromoId()).PromoType.toString().substring(0,1);
            p.PromoCode = promo.getPromoCode();
            ret.ItemPromos.add(p);
            ret.PromoItemLevel = ret.PromoItemLevel.add(p.DiscountedValue);
        }

        List<PromoAdjustment> dPromos = cart.getItemDistributedPromos(seqId);
        pseq = 0;
        for(PromoAdjustment promo:dPromos)
            if (promo.getAmount().compareTo(BigDecimal.ZERO)!=0)
            {
                PosPromoDetails p = new PosPromoDetails();
                p.SeqId= pseq++;
                p.PromoId = Integer.parseInt(promo.getPromoId());
                p.PromoName = r.getPromos().getPromo(promo.getPromoId()).Name;
                p.ReportingCode = r.getPromos().getPromo(promo.getPromoId()).ReportingCode;
                if (promo.isItemPromo()) p.PromoType = "I";
                else p.PromoType="S";
                p.DiscountedValue = promo.getAmount();
                p.PromoClass = r.getPromos().getPromo(promo.getPromoId()).PromoType.toString().substring(0,1);
                p.PromoCode = promo.getPromoCode();
                ret.DistributedPromos.add(p);
            }

        // Calculate Values.....

        ret.ValueCost = ret.ValueCost.setScale(r.getSettings().getScale(),r.getSettings().getRoundingMode());
        ret.ValueListed = ret.ValueListed.setScale(r.getSettings().getScale(),r.getSettings().getRoundingMode());
        ret.ValueSelected = ret.ValueSelected.setScale(r.getSettings().getScale(),r.getSettings().getRoundingMode());
        ret.ValueFinal = ret.ValueFinal.setScale(r.getSettings().getScale(),r.getSettings().getRoundingMode());
        ret.ManualDiscountItemLevel = ret.ManualDiscountItemLevel.setScale(r.getSettings().getScale(),r.getSettings().getRoundingMode());
        ret.PromoItemLevel = ret.PromoItemLevel.setScale(r.getSettings().getScale(),r.getSettings().getRoundingMode());
        ret.FeaturePreTax = ret.FeaturePreTax.setScale(r.getSettings().getScale(),r.getSettings().getRoundingMode());
        ret.FeaturePostTax = ret.FeaturePostTax.setScale(r.getSettings().getScale(),r.getSettings().getRoundingMode());
        ret.FeatureStd = ret.FeatureStd.setScale(r.getSettings().getScale(),r.getSettings().getRoundingMode());
        ret.FeatureTotal = ret.FeatureTotal.setScale(r.getSettings().getScale(),r.getSettings().getRoundingMode());

        return ret;
    }
    private PosSalesItem getItemCS(int seqId, PosShoppingCart cart)
    {
        PosShoppingCartItem item = cart.getItems().get(seqId);
        PosSalesItem ret = new PosSalesItem();
        ret.Features = new ArrayList<PosFeatureDetails>();
        ret.ItemPromos = new ArrayList<PosPromoDetails>();
        ret.DistributedPromos = new ArrayList<PosPromoDetails>();

        ret.SeqId = seqId;
        ret.ProductId = Integer.parseInt(item.getProductId());
        ret.ProductName = item.getProduct().Name;
        ret.Sku = item.getSku();
        ret.InputProductCode = item.getInputProductCode();
        String d = item.getProduct().DepartmentId;
        String sd = item.getProduct().SubDepartmentId;
        String sdc = item.getProduct().CategoryId;
        if (d==null) d="1";
        if (sd==null) sd="11";
        if (sdc==null) sdc="111";
        ret.DepartmentId = Integer.parseInt(d);
        ret.SubDepartmentId = Integer.parseInt(sd);
        ret.CategoryId = Integer.parseInt(sdc);
        ret.Unit = item.getProductUom().Abbrev;
        ret.IsMisc = false; // not yet fully implemented...
        ret.IsTaxable = item.isTaxable();
        ret.IsWeightProduct = item.isScalable();
        PriceElement pr = item.getProduct().Price;
        boolean pmode = item.isUseGrossPrice();
        ret.UnitListPrice = pr.ListPrice.getPrice(pmode);
        if (pr.PromoPrice!=null) ret.UnitPromoPrice = pr.PromoPrice.getPrice(pmode);
        if (pr.SpecialPromoPrice!=null) ret.UnitSpecialPromoPrice = pr.SpecialPromoPrice.getPrice(pmode);
        ret.UnitPackagePriceQuantityList = new ArrayList<BigDecimal>();
        ret.UnitPackagePriceList = new ArrayList<BigDecimal>();
        if (pr.PackagePriceList!=null)
            for (int i=0;i<pr.PackagePriceList.size();i++){
                ret.UnitPackagePriceQuantityList.add(pr.PackagePriceQuantityList.get(i));
                ret.UnitPackagePriceList.add(pr.PackagePriceList.get(i).getPrice(pmode));
            }
        ret.UnitPromoPackagePriceQuantityList = new ArrayList<BigDecimal>();
        ret.UnitPromoPackagePriceList = new ArrayList<BigDecimal>();
        if (pr.PromoPackagePriceList!=null)
            for (int i=0;i<pr.PromoPackagePriceList.size();i++){
                ret.UnitPromoPackagePriceQuantityList.add(pr.PromoPackagePriceQuantityList.get(i));
                ret.UnitPromoPackagePriceList.add(pr.PromoPackagePriceList.get(i).getPrice(pmode));
            }
        PriceResult pres = item.getSelectedPrice();
        ret.PriceRuleId=0;
        if (pres.getPriceRuleId()!=null) ret.PriceRuleId = Integer.parseInt(pres.getPriceRuleId());
        ret.UnitSelectedPrice = pres.getPrice().getPrice(item.isUseGrossPrice());
        ret.SelectedPriceType = pres.getPriceType();
        ret.PriceClampType = pres.getClampType();
        ret.UnitFinalPrice = item.getBasePrice();
        if (item.getOverrideReason()!=null)
        {
            ret.OverrideReason = item.getOverrideReason();
            ret.OverrideReportingCode = item.getOverrideReportingCode();
            ret.OverrideDifference = item.getSubtotal().subtract(item.getSubtotalBeforeOverride());
        }

        ret.Quantity = item.getQuantity();
        if (ret.IsWeightProduct) ret.EffectiveQuantity = 1;
        else ret.EffectiveQuantity = ret.Quantity.intValue();

        ret.DisplaySubTotal = item.getSubtotal();

        if (item.getItemDiscount()!=null)
        {
            PosDiscountDetails idisc = new PosDiscountDetails();
            //idisc.SeqId = 0;
            idisc.DiscountPercent = item.getItemDiscount().getPercent();
            idisc.DiscountValue = item.getItemDiscountAmount();
            ret.ItemDiscount = idisc;
            ret.ManualDiscountItemLevel = idisc.DiscountValue;
        }
        else ret.ManualDiscountItemLevel = BigDecimal.ZERO;

        int fseq = 0;
        for(String featureType:cart.getFeatureTypeNames())
        {
            List<FeatureElement> fList = item.getFeatures(featureType);
            for(FeatureElement fae:fList)
            {
                PosFeatureDetails f = new PosFeatureDetails();
                f.SeqId = fseq++;
                f.FeatureId = fae.FeatureId;
                f.FeatureType = fae.FeatureApplTypeId;
                f.FeatureName = fae.Description;
                f.FeatureValue = item.getFeatureDisplayValue(fae);
                if (fae.FeatureApplTypeId.equals("STANDARD_FEATURE"))
                {
                    f.FeatureMode = "STD";
                }
                else if (cart.isFeaturePreTax(f.FeatureId))
                {
                    f.FeatureMode = "PRE";
                }
                else
                {
                    f.FeatureMode = "PST";
                }
                ret.Features.add(f);
            }
        }

        ret.PromoItemLevel =BigDecimal.ZERO;
        List<PromoAdjustment> promos = cart.getPromoAdjustments(seqId);
        int pseq = 0;
        for(PromoAdjustment promo:promos)
        {
            PosPromoDetails p = new PosPromoDetails();
            p.SeqId= pseq++;
            p.PromoId = Integer.parseInt(promo.getPromoId());
            p.ReportingCode = r.getPromos().getPromo(promo.getPromoId()).ReportingCode;
            p.PromoName = r.getPromos().getPromo(promo.getPromoId()).Name;
            p.PromoType = "I";
            p.DiscountedValue = promo.getAmount();
            p.PromoClass = r.getPromos().getPromo(promo.getPromoId()).PromoType.toString().substring(0,1);
            p.PromoCode = promo.getPromoCode();
            ret.ItemPromos.add(p);
        }

        return ret;
    }



    private void error(Exception ex)
    {
        Debug.logError(ex, "Unable to queue data to the server! ", module);
        log(ex);
        logError("Unable to queue data to the server! ");
    }

    private void error2(Exception ex)
    {
        Debug.logError(ex, "Unable to send data to second Screen! ", module);
    }

}
